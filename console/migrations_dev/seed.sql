SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

INSERT INTO `instansi` (`id`, `nama_instansi`, `alamat`, `id_pengguna_jasa`) VALUES
('1', 'Kementerian PUPR', NULL, '1'),
('2', 'Pemkot DKI Jakarta', NULL, '3')
;

INSERT INTO `satminkal` (`id`, `nama_satminkal`, `id_instansi`) VALUES
('1', 'Direktorat OP SDA', '1'),
('2', 'Direktorat Jenderal Cipta Karya', '1'),
('3', 'Satminkal di Pemkot DKI Jakarta', '2')
;

INSERT INTO `satker` (`id`, `nama_satker`, `id_satminkal`) VALUES
('1', 'Biro Pemasaran', '1'),
('2', 'Biro Lingkungan', '1'),
('3', 'Biro Keuangan', '2')
;

INSERT INTO `mitra` (`id`, `nama_perusahaan`, `alamat_perusahaan`) VALUES
(1,	'Technopro',	NULL),
(2,	'Hutama Karya',	NULL),
(3,	'Nindya Karya',	'a'),
(4,	'PP',	NULL)
;

INSERT INTO `rab_item` (`name`, `detail_structure`) VALUES
(
	'Biaya Tenaga Ahli', 
	'{"no": {"name": "No.", "type": "increment", "order": 1}, 
		"uraian": {"name": "Uraian", "type": "string", "order": 2, "numbering": true}, 
		"satuan": {"name": "Satuan", "type": "string", "order": 3}, 
		"volume": {"name": "Volume", "type": "numeric", "order": 4}, 
		"harga": {"name": "Harga Satuan (Rp.)", "type": "numeric", "order": 5}, 
		"jumlah": {"name": "Jumlah Harga (Rp.)", "type": "pattern", "order": 6, "pattern": "$volume*$harga"}, 
		"_config": {"total": "jumlah"}
	}'
),
(
	'Biaya Asisten Tenaga Ahli', 
	'{"no": {"name": "No.", "type": "increment", "order": 1}, 
		"uraian": {"name": "Uraian", "type": "string", "order": 2, "numbering": true}, 
		"satuan": {"name": "Satuan", "type": "string", "order": 3}, 
		"volume": {"name": "Volume", "type": "numeric", "order": 4}, 
		"harga": {"name": "Harga Satuan (Rp.)", "type": "numeric", "order": 5}, 
		"jumlah": {"name": "Jumlah Harga (Rp.)", "type": "pattern", "order": 6, "pattern": "$volume*$harga"}, 
		"_config": {"total": "jumlah"}
	}'
)
;

INSERT INTO `tenaga_ahli` (`id`, `nama`, `tanggal_lahir`, `jenis`, `kewarganegaraan`) VALUES
(1,	'Ir., Christophores Kristijatno, CES',	'1949-01-07',	'Tetap',	'WNI'),
(2,	'Bambang Adi Riyanto, S.T., M.Eng.',	'1955-02-09',	'Tidak Tetap',	'WNA');

INSERT INTO `tenaga_ahli_keahlian` (`id`, `id_tenaga_ahli`, `keahlian`, `level`, `sertifikat`) VALUES
(1,	1,	'Ahli Teknik Sumber Daya Air',	'Muda',	'SKA No: 1.2.211.2.088.09.1126731 / 01 Maret 2014'),
(2,	2,	'Ahli Teknik Sumber Daya Air',	'Muda',	'SKA No: 1.2.211.2.088.10.1052300 / 19 Sep 2014'),
(3,	2,	'Ahli Teknik Bendungan Besar',	'Madya',	'SKA No: 1.2.211.2.088.09.1126731 / 08 Agustus 2014');

INSERT INTO `tenaga_ahli_pendidikan` (`id`, `id_tenaga_ahli`, `jenjang`, `jurusan`, `id_pendidikan_rumpun`, `tahun_lulus`, `nomor_ijazah`) VALUES
(1,	1,	'S1',	'Sistem Komputer',	2,	'1990',	'UGM, 12021/BS-WR/81/TS-ST/45-1213'),
(2,	2,	'S2',	'Teknik Sipil',	1,	'1990',	'Univ. Atmajaya, Yogyakarta, 1884'),
(3,	2,	'S1',	'Master of Engineering',	1,	'1995',	'Asian of Technology');