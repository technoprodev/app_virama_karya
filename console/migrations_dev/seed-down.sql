SET foreign_key_checks = 0;
DELETE FROM `satker`;
DELETE FROM `satminkal`;
DELETE FROM `instansi`;
DELETE FROM `mitra`;
DELETE FROM `tenaga_ahli`;
DELETE FROM `tenaga_ahli_keahlian`;
DELETE FROM `tenaga_ahli_pendidikan`;

ALTER TABLE `satker` AUTO_INCREMENT=1;
ALTER TABLE `satminkal` AUTO_INCREMENT=1;
ALTER TABLE `instansi` AUTO_INCREMENT=1;
ALTER TABLE `mitra` AUTO_INCREMENT=1;
ALTER TABLE `tenaga_ahli` AUTO_INCREMENT=1;
ALTER TABLE `tenaga_ahli_keahlian` AUTO_INCREMENT=1;
ALTER TABLE `tenaga_ahli_pendidikan` AUTO_INCREMENT=1;