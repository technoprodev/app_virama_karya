SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

UPDATE `configuration` SET `id` = '1', `name` = 'app.name', `type` = 'var', `value` = 'Simas' WHERE `id` = '1';
UPDATE `configuration` SET `id` = '2', `name` = 'app.description', `type` = 'var', `value` = 'Sistem Informasi Pemasaran' WHERE `id` = '2';
UPDATE `configuration` SET `id` = '3', `name` = 'app.keywords', `type` = 'var', `value` = 'technopro, virama karya, simas' WHERE `id` = '3';
UPDATE `configuration` SET `id` = '4', `name` = 'app.owner', `type` = 'var', `value` = 'Virama Karya' WHERE `id` = '4';
UPDATE `configuration` SET `id` = '5', `name` = 'app.author', `type` = 'var', `value` = 'Virama Karya' WHERE `id` = '5';
UPDATE `configuration` SET `id` = '6', `name` = 'enable.permission-checking', `type` = 'bool', `value` = 'false' WHERE `id` = '6';
UPDATE `configuration` SET `id` = '7', `name` = 'enable.permission-init', `type` = 'bool', `value` = 'false' WHERE `id` = '7';

DROP TABLE IF EXISTS `bidang_jasa`;
CREATE TABLE `bidang_jasa` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama_bidang_jasa` varchar(64) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `bidang_jasa` (`id`, `nama_bidang_jasa`) VALUES
(1, 'Perencanaan Arsitektur'),
(2, 'Perencanaan Rekayasa'),
(3, 'Perencanaan Penataan Ruang'),
(4, 'Pengawasan Arsitektur'),
(5, 'Pengawasan Rekayasa'),
(6, 'Pengawasan Penataan Ruang'),
(7, 'Konsultasi Spesialis'),
(8, 'Konsultasi Lainnya');

DROP TABLE IF EXISTS `bidang_unit_kerja`;
CREATE TABLE `bidang_unit_kerja` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama_bidang_unit_kerja` varchar(64) NOT NULL,
  `id_unit_kerja` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_unit_kerja` (`id_unit_kerja`),
  CONSTRAINT `bidang_unit_kerja_ibfk_1` FOREIGN KEY (`id_unit_kerja`) REFERENCES `unit_kerja` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `bidang_unit_kerja` (`id`, `nama_bidang_unit_kerja`, `id_unit_kerja`) VALUES
(1, 'Bidang Permukiman & Tata Ruang', 1),
(2, 'Bidang Transportasi',  1),
(3, 'Bidang SDA', 1),
(4, 'Bidang Gedung',  1),
(5, 'Bidang Permukiman & Tata Ruang', 2),
(6, 'Bidang Transportasi',  2),
(7, 'Bidang SDA', 2),
(8, 'Bidang Gedung',  2),
(9, 'Bidang Permukiman & Tata Ruang', 3),
(10,  'Bidang Transportasi',  3),
(11,  'Bidang SDA', 3),
(12,  'Bidang Gedung',  3),
(13,  'Bidang Permukiman & Tata Ruang', 4),
(14,  'Bidang Transportasi',  4),
(15,  'Bidang SDA', 4),
(16,  'Bidang Gedung',  4),
(17,  'Bidang Permukiman & Tata Ruang', 5),
(18,  'Bidang Transportasi',  5),
(19,  'Bidang SDA', 5),
(20,  'Bidang Gedung',  5),
(21,  'Bidang Permukiman & Tata Ruang', 6),
(22,  'Bidang Transportasi',  6),
(23,  'Bidang SDA', 6),
(24,  'Bidang Gedung',  6),
(25,  'Bidang Permukiman & Tata Ruang', 7),
(26,  'Bidang Transportasi',  7),
(27,  'Bidang SDA', 7),
(28,  'Bidang Gedung',  7),
(29,  'Bidang Permukiman & Tata Ruang', 8),
(30,  'Bidang Transportasi',  8),
(31,  'Bidang SDA', 8),
(32,  'Bidang Gedung',  8),
(33,  'Bidang Permukiman & Tata Ruang', 9),
(34,  'Bidang Transportasi',  9),
(35,  'Bidang SDA', 9),
(36,  'Bidang Gedung',  9);

DROP TABLE IF EXISTS `form`;
CREATE TABLE `form` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `app_nomor_akta` varchar(4) NOT NULL COMMENT 'Akte Pendirian Perusahaan',
  `app_tanggal` date NOT NULL COMMENT 'Akte Pendirian Perusahaan',
  `app_nama_notaris` varchar(128) NOT NULL COMMENT 'Akte Pendirian Perusahaan',
  `app_nomor_pengesahan` varchar(64) NOT NULL COMMENT 'Akte Pendirian Perusahaan',
  `app_tanggal_pengesahan` date NOT NULL COMMENT 'Akte Pendirian Perusahaan',
  `apt_nomor_akta` varchar(4) NOT NULL COMMENT 'Akte Perubahan Terakhir',
  `apt_tanggal` date NOT NULL COMMENT 'Akte Perubahan Terakhir',
  `apt_nama_notaris` varchar(128) NOT NULL COMMENT 'Akte Perubahan Terakhir',
  `apt_nomor_pengesahan` varchar(64) NOT NULL COMMENT 'Akte Perubahan Terakhir',
  `apt_tanggal_pengesahan` date NOT NULL COMMENT 'Akte Perubahan Terakhir',
  `iujk_berlaku_mulai` date NOT NULL COMMENT 'IUJK',
  `iujk_berlaku_sampai` date NOT NULL COMMENT 'IUJK',
  `iujk_pemberi_izin` varchar(128) NOT NULL COMMENT 'IUJK',
  `iujk_perencanaan` varchar(64) NOT NULL COMMENT 'IUJK',
  `iujk_pengawasan` varchar(64) NOT NULL COMMENT 'IUJK',
  `iujk_konsultansi` varchar(64) NOT NULL COMMENT 'IUJK',
  `sbuk_berlaku_mulai` date NOT NULL COMMENT 'SBUK',
  `sbuk_berlaku_sampai` date NOT NULL COMMENT 'SBUK',
  `sbuk_pemberi_izin` varchar(128) NOT NULL COMMENT 'SBUK',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `form` (`id`, `app_nomor_akta`, `app_tanggal`, `app_nama_notaris`, `app_nomor_pengesahan`, `app_tanggal_pengesahan`, `apt_nomor_akta`, `apt_tanggal`, `apt_nama_notaris`, `apt_nomor_pengesahan`, `apt_tanggal_pengesahan`, `iujk_berlaku_mulai`, `iujk_berlaku_sampai`, `iujk_pemberi_izin`, `iujk_perencanaan`, `iujk_pengawasan`, `iujk_konsultansi`, `sbuk_berlaku_mulai`, `sbuk_berlaku_sampai`, `sbuk_pemberi_izin`) VALUES
(1, '60', '1972-03-15', 'Djojo Mulijadi SH.', '7.A.5/58/24.-',  '1973-02-22', '01', '2016-07-19', 'Estrelyta Taher, SH.', 'AHU-AH.01.03-0065523', '2016-07-20', '2016-12-01', '2018-07-09', 'Pemerintah Provinsi DKI Jakarta',  '1-900-598-3171-1-00698', '1-900-598-3171-3-00698', '1-900-598-3171-4-00698', '2015-07-10', '2018-07-09', 'INKINDO (Ikatan Nasional Konsultan Indonesia)');

DROP TABLE IF EXISTS `form_bukti_pajak`;
CREATE TABLE `form_bukti_pajak` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_form` int(11) NOT NULL,
  `bulan` tinyint(2) NOT NULL,
  `tahun` year(4) NOT NULL,
  `nomor_pasal21_26` varchar(128) NOT NULL,
  `tanggal_pasal21_26` date NOT NULL,
  `nomor_pasal23_26` varchar(128) NOT NULL,
  `tanggal_pasal23_26` date NOT NULL,
  `nomor_pasal4ayat2` varchar(128) NOT NULL,
  `tanggal_pasal4ayat2` date NOT NULL,
  `nomor_pasal25` varchar(128) NOT NULL,
  `tanggal_pasal25` date NOT NULL,
  `nomor_ppnbm` varchar(128) NOT NULL,
  `tanggal_ppnbm` date NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_form` (`id_form`),
  CONSTRAINT `form_bukti_pajak_ibfk_1` FOREIGN KEY (`id_form`) REFERENCES `form` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `form_bukti_pajak` (`id`, `id_form`, `bulan`, `tahun`, `nomor_pasal21_26`, `tanggal_pasal21_26`, `nomor_pasal23_26`, `tanggal_pasal23_26`, `nomor_pasal4ayat2`, `tanggal_pasal4ayat2`, `nomor_pasal25`, `tanggal_pasal25`, `nomor_ppnbm`, `tanggal_ppnbm`) VALUES
(1, 1,  2,  '2017', 'S-010007515/PPH2114/WPJ.19/KP.0403/2017',  '2017-03-20', 'S-010007518/PPH23/WPJ.19/KP.0403/2017',  '2017-03-20', 'S-010007520/PPH42/WPJ.19/KP.0403/2017',  '2017-03-20', 'S-010007513/PPH25/WPJ.19/KP.0403/2017',  '2017-03-20', 'S-010007571/PPN1111/WPJ.19/KP.0403/2017',  '2017-03-31');

DROP TABLE IF EXISTS `form_direksi`;
CREATE TABLE `form_direksi` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_form` int(11) NOT NULL,
  `nama` varchar(128) NOT NULL,
  `nomor_ktp` varchar(16) NOT NULL,
  `jabatan` enum('Direktur Utama','Direktur') NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_form` (`id_form`),
  CONSTRAINT `form_direksi_ibfk_1` FOREIGN KEY (`id_form`) REFERENCES `form` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `form_direksi` (`id`, `id_form`, `nama`, `nomor_ktp`, `jabatan`) VALUES
(1, 1,  'Jusarwanto, S.E., Ak', '3275090708640006', 'Direktur Utama'),
(2, 1,  'Ir., Merlany Legitasari, M.T.,', '3276024603730016', 'Direktur');

DROP TABLE IF EXISTS `form_ijin_lainnya`;
CREATE TABLE `form_ijin_lainnya` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_form` int(11) NOT NULL,
  `sertifikat` varchar(128) NOT NULL,
  `nomor_sertifikat` varchar(128) NOT NULL,
  `berlaku_mulai` date NOT NULL,
  `berlaku_sampai` date NOT NULL,
  `pemberi_izin` varchar(64) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_form` (`id_form`),
  CONSTRAINT `form_ijin_lainnya_ibfk_1` FOREIGN KEY (`id_form`) REFERENCES `form` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `form_ijin_lainnya` (`id`, `id_form`, `sertifikat`, `nomor_sertifikat`, `berlaku_mulai`, `berlaku_sampai`, `pemberi_izin`) VALUES
(1, 1,  'Tanda Daftar Perusahaan',  'No. 09.03.1.70.26887', '2016-08-10', '2020-11-21', 'Pemerintah Provinsi DKI Jakarta');

DROP TABLE IF EXISTS `form_komisaris`;
CREATE TABLE `form_komisaris` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_form` int(11) NOT NULL,
  `nama` varchar(128) NOT NULL,
  `nomor_ktp` varchar(16) NOT NULL,
  `jabatan` enum('Komisaris Utama','Komisaris') NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_form` (`id_form`),
  CONSTRAINT `form_komisaris_ibfk_1` FOREIGN KEY (`id_form`) REFERENCES `form` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `form_komisaris` (`id`, `id_form`, `nama`, `nomor_ktp`, `jabatan`) VALUES
(1, 1,  'Ir., Hari Suprayogi',  '3175070711590004', 'Komisaris Utama'),
(2, 1,  'Laris Siringo, S.E.',  '3175060505600022', 'Komisaris'),
(3, 1,  'Ir., Arief Witjaksono, M.Sc.', '3175070910580008', 'Komisaris');

DROP TABLE IF EXISTS `form_pajak`;
CREATE TABLE `form_pajak` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_form` int(11) NOT NULL,
  `dokumen` varchar(128) NOT NULL,
  `nomor_dokumen` varchar(128) NOT NULL,
  `tanggal` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id_form` (`id_form`),
  CONSTRAINT `form_pajak_ibfk_1` FOREIGN KEY (`id_form`) REFERENCES `form` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `form_pajak` (`id`, `id_form`, `dokumen`, `nomor_dokumen`, `tanggal`) VALUES
(1, 1,  'Nomor Pokok Wajib Pajak (NPWP)', '0 1.000.485.1-093.000',  NULL);

DROP TABLE IF EXISTS `form_pemilik_saham`;
CREATE TABLE `form_pemilik_saham` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_form` int(11) NOT NULL,
  `nama` varchar(128) NOT NULL,
  `nomor_ktp` varchar(16) DEFAULT NULL,
  `alamat` text,
  `persentase` float DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id_form` (`id_form`),
  CONSTRAINT `form_pemilik_saham_ibfk_1` FOREIGN KEY (`id_form`) REFERENCES `form` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `form_pemilik_saham` (`id`, `id_form`, `nama`, `nomor_ktp`, `alamat`, `persentase`) VALUES
(1, 1,  'Republik Indonesia', '', '', 100);

DROP TABLE IF EXISTS `form_sbuk`;
CREATE TABLE `form_sbuk` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_form` int(11) NOT NULL,
  `sertifikat` varchar(128) NOT NULL,
  `nomor_sertifikat` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_form` (`id_form`),
  CONSTRAINT `form_sbuk_ibfk_1` FOREIGN KEY (`id_form`) REFERENCES `form` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `form_sbuk` (`id`, `id_form`, `sertifikat`, `nomor_sertifikat`) VALUES
(1, 1,  'Perencanaan Arsitektur', 'No. 1.3171.01.008.1.09.900598 | No. 0409515 AR101, AR102, AR103, AR104, AR105'),
(2, 1,  'Pengawasan Arsitektur',  'No. 2.3171.13.008.1.09.040951 | No. 0409516 AR201'),
(3, 1,  'Jasa Konsultasi Lainnya',  'No. 4.3171.04.008.1.09.900598 | No. 0409517 KL401, KL402, KL403, KL404, KL405, KL406, KL407, KL408'),
(4, 1,  'Perencanaan Penataan Ruang', 'No. 1.3171.03.008.1.09.900598 | No. 0409518 PR101, PR102, PR103, PR104'),
(5, 1,  'Pengawasan Penataan Ruang',  'No. 2.3171.15.008.1.09.900598 | No. 0409519 PR201'),
(6, 1,  'Perencanaan Rekayasa', 'No. 1.3171.02.008.1.09.900598 | No. 0409520 RE101, RE1O2, RE103, RE104, RE105, RE106, RE107, RE108'),
(7, 1,  'Pengawasan Rekayasa',  'No. 2.3171.14.008.1.09.900598 | No. 0409521 RE201, RE202, RE203, RE204'),
(8, 1,  'Jasa Konsultasi Spesialis',  'No. 3.3171.05.008.1.09.900598 | No. 0409522 SP301, SP302, SP303, SP304, SP305, SP306, SP307, SP308');

DROP TABLE IF EXISTS `form_sbunk`;
CREATE TABLE `form_sbunk` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_form` int(11) NOT NULL,
  `sertifikat` varchar(128) NOT NULL,
  `nomor_sertifikat` varchar(128) NOT NULL,
  `berlaku_mulai` date DEFAULT NULL,
  `berlaku_sampai` date DEFAULT NULL,
  `pemberi_izin` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id_form` (`id_form`),
  CONSTRAINT `form_sbunk_ibfk_1` FOREIGN KEY (`id_form`) REFERENCES `form` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `form_sbunk` (`id`, `id_form`, `sertifikat`, `nomor_sertifikat`, `berlaku_mulai`, `berlaku_sampai`, `pemberi_izin`) VALUES
(1, 1,  'Layanan Jasa Studi, Penelitian dan Bantuan Teknik',  'No Seri L13.000239', NULL, NULL, ''),
(2, 1,  'Layanan Jasa Konsultansi Manajemen', 'No Seri L13.000239', '2016-04-07', '2018-04-07', 'Inkindo & Kadin Indonesia'),
(3, 1,  'Pengembangan Pertanian dan Pedesaan',  'No Seri B16.000752', NULL, NULL, ''),
(4, 1,  'Transportasi', 'No Seri B16.000753', NULL, NULL, ''),
(5, 1,  'Telematka',  'No Seri B16.000754', NULL, NULL, ''),
(6, 1,  'Pendidikan', 'No Seri B16.000755', '2016-08-22', '2018-08-22', 'Inkindo & Kadin Indonesia');

DROP TABLE IF EXISTS `instansi`;
CREATE TABLE `instansi` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama_instansi` varchar(64) NOT NULL,
  `alamat` text,
  `id_pengguna_jasa` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_pengguna_jasa` (`id_pengguna_jasa`),
  CONSTRAINT `instansi_ibfk_1` FOREIGN KEY (`id_pengguna_jasa`) REFERENCES `pengguna_jasa` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



INSERT INTO `menu` (`code`, `id`, `parent`, `order`, `enable`, `title`, `icon`, `url`, `url_controller`, `url_action`, `param_key_1`, `param_value_1`, `param_key_2`, `param_value_2`, `param_key_3`, `param_value_3`) VALUES
('sidebar', 1,  NULL, 0,  1,  'Dashboard',  NULL, NULL, 'site', 'index',  NULL, NULL, NULL, NULL, NULL, NULL),
('sidebar', 2,  NULL, 0,  1,  'Prospek',  NULL, NULL, 'site', 'login',  NULL, NULL, NULL, NULL, NULL, NULL),
('sidebar', 3,  NULL, 0,  1,  'Proses Tender',  NULL, NULL, 'paket',  'create', NULL, NULL, NULL, NULL, NULL, NULL),
('sidebar', 4,  NULL, 0,  1,  'Pengalaman Perusahaan',  NULL, NULL, 'paket',  'create-pengalaman-perusahaan', NULL, NULL, NULL, NULL, NULL, NULL),
('sidebar', 5,  NULL, 0,  1,  'Tender Diikuti', NULL, NULL, 'tender', 'diikuti',  NULL, NULL, NULL, NULL, NULL, NULL),
('sidebar', 6,  NULL, 0,  1,  'Tender Dimenangkan', NULL, NULL, 'site', 'login',  NULL, NULL, NULL, NULL, NULL, NULL),
('sidebar', 7,  NULL, 0,  1,  'Tender Kalah', NULL, NULL, 'site', 'login',  NULL, NULL, NULL, NULL, NULL, NULL),
('sidebar', 8,  NULL, 0,  1,  'Data Master',  NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('sidebar', 9,  8,  0,  1,  'Form Isian (A-G)', NULL, NULL, 'form', 'index',  NULL, NULL, NULL, NULL, NULL, NULL),
('sidebar', 10, 8,  0,  1,  'Edit Form Isian (A-G)',  NULL, NULL, 'form', 'update', NULL, NULL, NULL, NULL, NULL, NULL),
('sidebar', 11, 8,  0,  1,  'Tenaga Ahli',  NULL, NULL, 'tenaga-ahli',  'index',  NULL, NULL, NULL, NULL, NULL, NULL),
('sidebar', 12, 8,  0,  1,  'Tambah Tenaga Ahli', NULL, NULL, 'tenaga-ahli',  'create', NULL, NULL, NULL, NULL, NULL, NULL);

DROP TABLE IF EXISTS `mitra`;
CREATE TABLE `mitra` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama_perusahaan` varchar(64) NOT NULL,
  `alamat_perusahaan` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `paket`;
CREATE TABLE `paket` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `status` set('Info Paket','Prakualifikasi','Tidak Shortlist','Proposal','Kontrak','Kalah','Mundur') NOT NULL,
  `auto_forward` tinyint(1) NOT NULL DEFAULT '1',
  `nama_paket` varchar(256) NOT NULL COMMENT 'info paket',
  `lokasi` varchar(256) NOT NULL COMMENT 'info paket',
  `id_unit_kerja` int(11) NOT NULL COMMENT 'info paket',
  `id_bidang_unit_kerja` int(11) NOT NULL COMMENT 'info paket',
  `id_pengguna_jasa` int(11) NOT NULL COMMENT 'info paket',
  `id_instansi` int(11) NOT NULL COMMENT 'info paket',
  `id_satminkal` int(11) NOT NULL COMMENT 'info paket',
  `id_satker` int(11) NOT NULL COMMENT 'info paket',
  `id_bidang_jasa` int(11) NOT NULL COMMENT 'info paket',
  `id_sub_bidang_jasa` int(11) NOT NULL COMMENT 'info paket',
  `tahun_anggaran` year(4) NOT NULL COMMENT 'info paket',
  `sumber_anggaran` varchar(256) NOT NULL COMMENT 'info paket',
  `pagu` int(32) NOT NULL COMMENT 'info paket',
  `hps` int(32) NOT NULL COMMENT 'info paket',
  `bobot_teknis` float NOT NULL COMMENT 'info paket',
  `bobot_biaya` float NOT NULL COMMENT 'info paket',
  `is_kso` enum('Ya','Tidak') NOT NULL COMMENT 'info paket',
  `peluang` enum('25','50','75','90','95') NOT NULL COMMENT 'info paket',
  `tindak_lanjut_25` text COMMENT 'info paket',
  `tindak_lanjut_50` text,
  `tindak_lanjut_75` text,
  `tindak_lanjut_90` text,
  `tindak_lanjut_95` text,
  `nilai_kontrak` int(32) DEFAULT NULL COMMENT 'kontrak',
  `nomor_kontrak` varchar(256) DEFAULT NULL COMMENT 'kontrak',
  `kontrak_mulai` date DEFAULT NULL COMMENT 'kontrak',
  `kontrak_selesai` date DEFAULT NULL COMMENT 'kontrak',
  PRIMARY KEY (`id`),
  KEY `id_unit_kerja` (`id_unit_kerja`),
  KEY `id_bidang_unit_kerja` (`id_bidang_unit_kerja`),
  KEY `id_pengguna_jasa` (`id_pengguna_jasa`),
  KEY `id_instansi` (`id_instansi`),
  KEY `id_satminkal` (`id_satminkal`),
  KEY `id_satker` (`id_satker`),
  KEY `id_bidang_jasa` (`id_bidang_jasa`),
  KEY `id_sub_bidang_jasa` (`id_sub_bidang_jasa`),
  CONSTRAINT `paket_ibfk_1` FOREIGN KEY (`id_unit_kerja`) REFERENCES `unit_kerja` (`id`),
  CONSTRAINT `paket_ibfk_2` FOREIGN KEY (`id_bidang_unit_kerja`) REFERENCES `bidang_unit_kerja` (`id`),
  CONSTRAINT `paket_ibfk_3` FOREIGN KEY (`id_pengguna_jasa`) REFERENCES `pengguna_jasa` (`id`),
  CONSTRAINT `paket_ibfk_4` FOREIGN KEY (`id_instansi`) REFERENCES `instansi` (`id`),
  CONSTRAINT `paket_ibfk_5` FOREIGN KEY (`id_satminkal`) REFERENCES `satminkal` (`id`),
  CONSTRAINT `paket_ibfk_6` FOREIGN KEY (`id_satker`) REFERENCES `satker` (`id`),
  CONSTRAINT `paket_ibfk_7` FOREIGN KEY (`id_bidang_jasa`) REFERENCES `bidang_jasa` (`id`),
  CONSTRAINT `paket_ibfk_8` FOREIGN KEY (`id_sub_bidang_jasa`) REFERENCES `sub_bidang_jasa` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `paket_form`;
CREATE TABLE `paket_form` (
  `id_paket` int(11) NOT NULL,
  `app_nomor_akta` varchar(4) NOT NULL COMMENT 'Akte Pendirian Perusahaan',
  `app_tanggal` date NOT NULL COMMENT 'Akte Pendirian Perusahaan',
  `app_nama_notaris` varchar(128) NOT NULL COMMENT 'Akte Pendirian Perusahaan',
  `app_nomor_pengesahan` varchar(64) NOT NULL COMMENT 'Akte Pendirian Perusahaan',
  `app_tanggal_pengesahan` date NOT NULL COMMENT 'Akte Pendirian Perusahaan',
  `apt_nomor_akta` varchar(4) NOT NULL COMMENT 'Akte Perubahan Terakhir',
  `apt_tanggal` date NOT NULL COMMENT 'Akte Perubahan Terakhir',
  `apt_nama_notaris` varchar(128) NOT NULL COMMENT 'Akte Perubahan Terakhir',
  `apt_nomor_pengesahan` varchar(64) NOT NULL COMMENT 'Akte Perubahan Terakhir',
  `apt_tanggal_pengesahan` date NOT NULL COMMENT 'Akte Perubahan Terakhir',
  `iujk_berlaku_mulai` date NOT NULL COMMENT 'IUJK',
  `iujk_berlaku_sampai` date NOT NULL COMMENT 'IUJK',
  `iujk_pemberi_izin` varchar(128) NOT NULL COMMENT 'IUJK',
  `iujk_perencanaan` varchar(64) NOT NULL COMMENT 'IUJK',
  `iujk_pengawasan` varchar(64) NOT NULL COMMENT 'IUJK',
  `iujk_konsultansi` varchar(64) NOT NULL COMMENT 'IUJK',
  `sbuk_berlaku_mulai` date NOT NULL COMMENT 'SBUK',
  `sbuk_berlaku_sampai` date NOT NULL COMMENT 'SBUK',
  `sbuk_pemberi_izin` varchar(128) NOT NULL COMMENT 'SBUK',
  PRIMARY KEY (`id_paket`),
  CONSTRAINT `paket_form_ibfk_1` FOREIGN KEY (`id_paket`) REFERENCES `paket` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `paket_form_bukti_pajak`;
CREATE TABLE `paket_form_bukti_pajak` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_paket_form` int(11) NOT NULL,
  `bulan` tinyint(2) NOT NULL,
  `tahun` year(4) NOT NULL,
  `nomor_pasal21_26` varchar(128) NOT NULL,
  `tanggal_pasal21_26` date NOT NULL,
  `nomor_pasal23_26` varchar(128) NOT NULL,
  `tanggal_pasal23_26` date NOT NULL,
  `nomor_pasal4ayat2` varchar(128) NOT NULL,
  `tanggal_pasal4ayat2` date NOT NULL,
  `nomor_pasal25` varchar(128) NOT NULL,
  `tanggal_pasal25` date NOT NULL,
  `nomor_ppnbm` varchar(128) NOT NULL,
  `tanggal_ppnbm` date NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_paket_form` (`id_paket_form`),
  CONSTRAINT `paket_form_bukti_pajak_ibfk_1` FOREIGN KEY (`id_paket_form`) REFERENCES `paket_form` (`id_paket`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `paket_form_direksi`;
CREATE TABLE `paket_form_direksi` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_paket_form` int(11) NOT NULL,
  `nama` varchar(128) NOT NULL,
  `nomor_ktp` varchar(16) NOT NULL,
  `jabatan` enum('Direktur Utama','Direktur') NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_paket_form` (`id_paket_form`),
  CONSTRAINT `paket_form_direksi_ibfk_1` FOREIGN KEY (`id_paket_form`) REFERENCES `paket_form` (`id_paket`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `paket_form_ijin_lainnya`;
CREATE TABLE `paket_form_ijin_lainnya` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_paket_form` int(11) NOT NULL,
  `sertifikat` varchar(128) NOT NULL,
  `nomor_sertifikat` varchar(128) NOT NULL,
  `berlaku_mulai` date NOT NULL,
  `berlaku_sampai` date NOT NULL,
  `pemberi_izin` varchar(64) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_paket_form` (`id_paket_form`),
  CONSTRAINT `paket_form_ijin_lainnya_ibfk_1` FOREIGN KEY (`id_paket_form`) REFERENCES `paket_form` (`id_paket`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `paket_form_komisaris`;
CREATE TABLE `paket_form_komisaris` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_paket_form` int(11) NOT NULL,
  `nama` varchar(128) NOT NULL,
  `nomor_ktp` varchar(16) NOT NULL,
  `jabatan` enum('Komisaris Utama','Komisaris') NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_paket_form` (`id_paket_form`),
  CONSTRAINT `paket_form_komisaris_ibfk_1` FOREIGN KEY (`id_paket_form`) REFERENCES `paket_form` (`id_paket`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `paket_form_pajak`;
CREATE TABLE `paket_form_pajak` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_paket_form` int(11) NOT NULL,
  `dokumen` varchar(128) NOT NULL,
  `nomor_dokumen` varchar(128) NOT NULL,
  `tanggal` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id_paket_form` (`id_paket_form`),
  CONSTRAINT `paket_form_pajak_ibfk_1` FOREIGN KEY (`id_paket_form`) REFERENCES `paket_form` (`id_paket`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `paket_form_pemilik_saham`;
CREATE TABLE `paket_form_pemilik_saham` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_paket_form` int(11) NOT NULL,
  `nama` varchar(128) NOT NULL,
  `nomor_ktp` varchar(16) DEFAULT NULL,
  `alamat` text,
  `persentase` float DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id_paket_form` (`id_paket_form`),
  CONSTRAINT `paket_form_pemilik_saham_ibfk_1` FOREIGN KEY (`id_paket_form`) REFERENCES `paket_form` (`id_paket`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `paket_form_sbuk`;
CREATE TABLE `paket_form_sbuk` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_paket_form` int(11) NOT NULL,
  `sertifikat` varchar(128) NOT NULL,
  `nomor_sertifikat` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_paket_form` (`id_paket_form`),
  CONSTRAINT `paket_form_sbuk_ibfk_1` FOREIGN KEY (`id_paket_form`) REFERENCES `paket_form` (`id_paket`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `paket_form_sbunk`;
CREATE TABLE `paket_form_sbunk` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_paket_form` int(11) NOT NULL,
  `sertifikat` varchar(128) NOT NULL,
  `nomor_sertifikat` varchar(128) NOT NULL,
  `berlaku_mulai` date DEFAULT NULL,
  `berlaku_sampai` date DEFAULT NULL,
  `pemberi_izin` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id_paket_form` (`id_paket_form`),
  CONSTRAINT `paket_form_sbunk_ibfk_1` FOREIGN KEY (`id_paket_form`) REFERENCES `paket_form` (`id_paket`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `paket_mitra`;
CREATE TABLE `paket_mitra` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_paket` int(11) NOT NULL,
  `id_mitra` int(11) NOT NULL,
  `porsi` float DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id_paket` (`id_paket`),
  KEY `id_mitra` (`id_mitra`),
  CONSTRAINT `paket_mitra_ibfk_1` FOREIGN KEY (`id_paket`) REFERENCES `paket` (`id`),
  CONSTRAINT `paket_mitra_ibfk_2` FOREIGN KEY (`id_mitra`) REFERENCES `mitra` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `paket_tahap_aktif`;
CREATE TABLE `paket_tahap_aktif` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_paket` int(11) NOT NULL,
  `id_tahap` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_paket` (`id_paket`),
  KEY `id_tahap` (`id_tahap`),
  CONSTRAINT `paket_tahap_aktif_ibfk_1` FOREIGN KEY (`id_paket`) REFERENCES `paket` (`id`),
  CONSTRAINT `paket_tahap_aktif_ibfk_2` FOREIGN KEY (`id_tahap`) REFERENCES `tahap` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `paket_tahap_jadwal`;
CREATE TABLE `paket_tahap_jadwal` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_paket` int(11) NOT NULL,
  `id_tahap` int(11) NOT NULL,
  `tanggal_mulai` datetime DEFAULT NULL,
  `tanggal_selesai` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id_paket` (`id_paket`),
  KEY `id_tahap` (`id_tahap`),
  CONSTRAINT `paket_tahap_jadwal_ibfk_1` FOREIGN KEY (`id_paket`) REFERENCES `paket` (`id`),
  CONSTRAINT `paket_tahap_jadwal_ibfk_2` FOREIGN KEY (`id_tahap`) REFERENCES `tahap` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `paket_tenaga_ahli`;
CREATE TABLE `paket_tenaga_ahli` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_paket` int(11) NOT NULL,
  `nama` varchar(128) NOT NULL,
  `tanggal_lahir` date NOT NULL,
  `jenis` enum('Tetap','Tidak Tetap') NOT NULL,
  `kewarganegaraan` enum('WNI','WNA') NOT NULL,
  `id_penugasan` int(11) NOT NULL,
  `periode_mulai` date NOT NULL,
  `periode_selesai` date NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_paket` (`id_paket`),
  KEY `id_penugasan` (`id_penugasan`),
  CONSTRAINT `paket_tenaga_ahli_ibfk_1` FOREIGN KEY (`id_paket`) REFERENCES `paket` (`id`),
  CONSTRAINT `paket_tenaga_ahli_ibfk_2` FOREIGN KEY (`id_penugasan`) REFERENCES `penugasan` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `paket_tenaga_ahli_keahlian`;
CREATE TABLE `paket_tenaga_ahli_keahlian` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_paket_tenaga_ahli` int(11) NOT NULL,
  `keahlian` varchar(64) NOT NULL,
  `level` enum('Muda','Madya','Utama') DEFAULT NULL,
  `sertifikat` varchar(64) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_paket_tenaga_ahli` (`id_paket_tenaga_ahli`),
  CONSTRAINT `paket_tenaga_ahli_keahlian_ibfk_1` FOREIGN KEY (`id_paket_tenaga_ahli`) REFERENCES `paket_tenaga_ahli` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `paket_tenaga_ahli_pendidikan`;
CREATE TABLE `paket_tenaga_ahli_pendidikan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_paket_tenaga_ahli` int(11) NOT NULL,
  `jenjang` enum('S1','S2','S3','SMA/SMK','D3','D4') NOT NULL,
  `jurusan` varchar(64) NOT NULL,
  `id_pendidikan_rumpun` int(11) NOT NULL,
  `tahun_lulus` year(4) NOT NULL,
  `nomor_ijazah` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id_paket_tenaga_ahli` (`id_paket_tenaga_ahli`),
  KEY `id_pendidikan_rumpun` (`id_pendidikan_rumpun`),
  CONSTRAINT `paket_tenaga_ahli_pendidikan_ibfk_1` FOREIGN KEY (`id_paket_tenaga_ahli`) REFERENCES `paket_tenaga_ahli` (`id`),
  CONSTRAINT `paket_tenaga_ahli_pendidikan_ibfk_2` FOREIGN KEY (`id_pendidikan_rumpun`) REFERENCES `pendidikan_rumpun` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `pendidikan_rumpun`;
CREATE TABLE `pendidikan_rumpun` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rumpun` varchar(128) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `pendidikan_rumpun` (`id`, `rumpun`) VALUES
(1, 'Teknik Sipil'),
(2, 'Teknik Informatika');

DROP TABLE IF EXISTS `pengguna_jasa`;
CREATE TABLE `pengguna_jasa` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama_pengguna_jasa` varchar(64) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `pengguna_jasa` (`id`, `nama_pengguna_jasa`) VALUES
(1, 'Kementrian PUPERA'),
(2, 'Kementrian Non PUPERA'),
(3, 'Pemkot / Pemkab'),
(4, 'BUMN / BUMD'),
(5, 'Investo / Swasta');

DROP TABLE IF EXISTS `penugasan`;
CREATE TABLE `penugasan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama_penugasan` varchar(64) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `penugasan` (`id`, `nama_penugasan`) VALUES
(1, 'Anggota Tim'),
(2, 'Ketua Tim');

DROP TABLE IF EXISTS `rab`;
CREATE TABLE `rab` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `paket_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `rab_data`;
CREATE TABLE `rab_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rab_id` int(11) NOT NULL,
  `rab_item_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `rab_detail`;
CREATE TABLE `rab_detail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rab_data_id` int(11) NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `value` json DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `rab_item`;
CREATE TABLE `rab_item` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `detail_structure` json DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `satker`;
CREATE TABLE `satker` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama_satker` varchar(64) NOT NULL,
  `id_satminkal` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_satminkal` (`id_satminkal`),
  CONSTRAINT `satker_ibfk_2` FOREIGN KEY (`id_satminkal`) REFERENCES `satminkal` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `satminkal`;
CREATE TABLE `satminkal` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama_satminkal` varchar(64) NOT NULL,
  `id_instansi` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_instansi` (`id_instansi`),
  CONSTRAINT `satminkal_ibfk_1` FOREIGN KEY (`id_instansi`) REFERENCES `instansi` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `sub_bidang_jasa`;
CREATE TABLE `sub_bidang_jasa` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama_sub_bidang_jasa` varchar(128) NOT NULL,
  `kode` varchar(8) NOT NULL,
  `id_bidang_jasa` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_bidang_jasa` (`id_bidang_jasa`),
  CONSTRAINT `sub_bidang_jasa_ibfk_1` FOREIGN KEY (`id_bidang_jasa`) REFERENCES `bidang_jasa` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `sub_bidang_jasa` (`id`, `nama_sub_bidang_jasa`, `kode`, `id_bidang_jasa`) VALUES
(1, 'Jasa Nasehat dan Pra Desain Arsitektural', 'AR101',  1),
(2, 'Jasa Desain Arsitektur', 'AR102',  1),
(3, 'Jasa Penilaian Peawatan dan Kelayakan Gedung', 'AR103',  1),
(4, 'Jasa Desain Interior', 'AR104',  1),
(5, 'Jasa Pelaksana Kontruksi Bangunan Hiburan Publik', 'AR105',  1),
(6, 'Jasa Nasehat dan Konsultasi Rekayasa Teknik',  'RE101',  2),
(7, 'Jasa Desain Rekayasa untuk konstruksi Pondasi serta Struktur Bangunan',  'RE102',  2),
(8, 'Jasa Desain Rekayasa untuk Pekerjaan Teknik Sipil Air',  'RE103',  2),
(9, 'Jasa Desain Rekayasa untuk Pekerjaan Teknik Sipil Transportasi', 'RE104',  2),
(10,  'Jasa Desain Rekayasa untuk Pekerjaan Mekanikal dan Elektrikal Dalam Bangunan', 'RE105',  2),
(11,  'Jasa Desain Rekayasa untuk Proses Industrial dan Produksi',  'RE106',  2),
(12,  'Jasa Nasehat dan Konsultasi jasa Rekayasa Konstruksi', 'RE107',  2),
(13,  'Jasa Desain Rekayasa Lainnya', 'RE108',  2),
(14,  'Jasa Perencana dan Perancang Perkotaan', 'PR101',  3),
(15,  'Jasa Perencana Wilayah', 'PR102',  3),
(16,  'Jasa Perencana dan Perencang Lingkungan banguan dan lansekap', 'PR103',  3),
(17,  'Jasa Perencana pemanfaatan Ruang', 'PR104',  3),
(18,  'Jasa Pengawas Administrasi Kontrak', 'AR201',  4),
(19,  'Jasa Pengawas Pekerjaan Kontruksi Bangunan Gedung',  'RE201',  5),
(20,  'Jasa Pengawas Pekerjaan Kontruksi Teknik Sipil Transportasi',  'RE202',  5),
(21,  'Jasa Pengawas Pekerjaan Teknik Sipil Air', 'RE203',  5),
(22,  'Jasa Pengawas Pekerjaan Kontruksi dan Instalasi Proses dan Fasilitas Proses dan Fasilitas industri', 'RE204',  5),
(23,  'Jasa Pengawas dan Pengendali Penataan Ruang',  'PR201',  6),
(24,  'Jasa Pembuatan Prospektus Geologi dan Geofisika  ',  'SP301',  7),
(25,  'Jasa Survey Bawa Tanah', 'SP302',  7),
(26,  'Jasa Survey Pemikaan Tanah  ', 'SP303',  7),
(27,  'Jasa Survey Pembuatan Peta  ', 'SP304',  7),
(28,  'Jasa Pengujian dan Analisa Komposisi  dan Tingkat Kemurnian  ',  'SP305',  7),
(29,  'Jasa Pengujian dan Analisa Parameter Fisikal  ', 'SP306',  7),
(30,  'Jasa Pengujian dan Analisa Sistem Mekanikal dan Elektrikal ',  'SP307',  7),
(31,  'Jasa Inspeksi Teknikal  ', 'SP308',  7),
(32,  'Jasa Konsultasi Lingkungan', 'KL401',  8),
(33,  'Jasa Konsultasi Estimasi Nilai Lahan dan Bangunan',  'KL402',  8),
(34,  'Jasa Manajemen Proyek Terkait Konstruksi Bangunan',  'KL403',  8),
(35,  'Jasa Manajemen Proyek Terkait Konstruksi Pekerjaan Teknik Sipil Transportasi', 'KL404',  8),
(36,  'Jasa Manajemen Proyek Terkait Konstruksi Pekerjaan Teknik Sipil Keairan',  'KL405',  8),
(37,  'Jasa Manajemen Proyek Terkait Konstruksi Pekerjaan Teknik Sipil Lainnya',  'KL406',  8),
(38,  'Jasa Manajemen Proyek Terkait Konstruksi Pekerjaan Konstruksi Proses dan Fasilitas Industrial',  'KL407',  8),
(39,  'Jasa Manajemen Proyek Terkait Konstruksi Pekerjaan Sistem Kendali Lalu Lintas',  'KL408',  8);

DROP TABLE IF EXISTS `tahap`;
CREATE TABLE `tahap` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama_tahap` varchar(128) NOT NULL,
  `status` enum('Info Paket','Prakualifikasi','Tidak Shortlist','Proposal','Kontrak','Kalah','Mundur') NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `tahap` (`id`, `nama_tahap`, `status`) VALUES
(1, 'Info Awal',  'Info Paket'),
(2, 'Pengumuman Prakualifikasi',  'Prakualifikasi'),
(3, 'Download Dokumen Kualifikasi', 'Prakualifikasi'),
(4, 'Penjelasan Dokumen Prakualifikasi',  'Prakualifikasi'),
(5, 'Upload Dokumen Prakualifikasi',  'Prakualifikasi'),
(6, 'Evaluasi Dokumen Kualifikasi', 'Prakualifikasi'),
(7, 'Pembuktian Kualifikasi', 'Prakualifikasi'),
(8, 'Penetapan Hasil Kualifikasi',  'Prakualifikasi'),
(9, 'Pengumuman Hasil Prakualifikasi',  'Prakualifikasi'),
(10,  'Download Dokumen Pemilihan', 'Proposal'),
(11,  'Pemberian Penjelasan', 'Proposal'),
(12,  'Upload Dokumen Penawaran', 'Proposal'),
(13,  'Pembukaan dan Evaluasi Penawaran File I : Administrasi dan Teknis',  'Proposal'),
(14,  'Paparan Teknis', 'Proposal'),
(15,  'Penetapan Peringkat Teknis', 'Proposal'),
(16,  'Pemberitahuan/Pengumuman Peringkat Teknis',  'Proposal'),
(17,  'Pembukaan dan Evaluasi Penawaran File II : Harga', 'Proposal'),
(18,  'Penetapan pemenang', 'Proposal'),
(19,  'Pengumuman Pemenang',  'Proposal'),
(20,  'Masa Sanggah Hasil Lelang',  'Proposal'),
(21,  'Klarifikasi dan Negosiasi Teknis dan Biaya', 'Proposal'),
(22,  'Upload Berita Acara Hasil Pelelangan', 'Kontrak'),
(23,  'Surat Penunjukan Penyedia Barang/Jasa',  'Kontrak'),
(24,  'Penandatanganan Kontrak',  'Kontrak');

DROP TABLE IF EXISTS `tenaga_ahli`;
CREATE TABLE `tenaga_ahli` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(128) NOT NULL,
  `tanggal_lahir` date NOT NULL,
  `jenis` enum('Tetap','Tidak Tetap') NOT NULL,
  `kewarganegaraan` enum('WNI','WNA') NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `tenaga_ahli_keahlian`;
CREATE TABLE `tenaga_ahli_keahlian` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_tenaga_ahli` int(11) NOT NULL,
  `keahlian` varchar(64) NOT NULL,
  `level` enum('Muda','Madya','Utama') DEFAULT NULL,
  `sertifikat` varchar(64) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_tenaga_ahli` (`id_tenaga_ahli`),
  CONSTRAINT `tenaga_ahli_keahlian_ibfk_1` FOREIGN KEY (`id_tenaga_ahli`) REFERENCES `tenaga_ahli` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `tenaga_ahli_pendidikan`;
CREATE TABLE `tenaga_ahli_pendidikan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_tenaga_ahli` int(11) NOT NULL,
  `jenjang` enum('S1','S2','S3','SMA/SMK','D3','D4') NOT NULL,
  `jurusan` varchar(64) NOT NULL,
  `id_pendidikan_rumpun` int(11) NOT NULL,
  `tahun_lulus` year(4) NOT NULL,
  `nomor_ijazah` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id_tenaga_ahli` (`id_tenaga_ahli`),
  KEY `id_pendidikan_rumpun` (`id_pendidikan_rumpun`),
  CONSTRAINT `tenaga_ahli_pendidikan_ibfk_1` FOREIGN KEY (`id_tenaga_ahli`) REFERENCES `tenaga_ahli` (`id`),
  CONSTRAINT `tenaga_ahli_pendidikan_ibfk_2` FOREIGN KEY (`id_pendidikan_rumpun`) REFERENCES `pendidikan_rumpun` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `unit_kerja`;
CREATE TABLE `unit_kerja` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama_unit_kerja` varchar(64) NOT NULL,
  `type` enum('divisi','cabang') NOT NULL,
  `alamat` text,
  `nomor_telepon` varchar(16) DEFAULT NULL,
  `nomor_fax` varchar(16) DEFAULT NULL,
  `email` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `unit_kerja` (`id`, `nama_unit_kerja`, `type`, `alamat`, `nomor_telepon`, `nomor_fax`, `email`) VALUES
(1, 'Divisi Kimtaru', 'divisi', NULL, NULL, NULL, NULL),
(2, 'Divisi Transportasi',  'divisi', NULL, NULL, NULL, NULL),
(3, 'Divisi SDA', 'divisi', NULL, NULL, NULL, NULL),
(4, 'Divisi Gedung',  'divisi', NULL, NULL, NULL, NULL),
(5, 'Cabang Padang',  'cabang', NULL, NULL, NULL, NULL),
(6, 'Cabang Semarang',  'cabang', NULL, NULL, NULL, NULL),
(7, 'Cabang Surabaya',  'cabang', NULL, NULL, NULL, NULL),
(8, 'Cabang Makasar', 'cabang', NULL, NULL, NULL, NULL),
(9, 'Cabang Kalimantan',  'cabang', NULL, NULL, NULL, NULL);

CREATE TABLE `timeline` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `paket_id` int(11) DEFAULT NULL,
  `index` int(11) DEFAULT NULL,
  `parent_index` int(11) DEFAULT NULL,
  `task` varchar(255) DEFAULT NULL,
  `month_start` int(11) DEFAULT NULL,
  `month_end` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

