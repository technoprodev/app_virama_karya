<?php

use yii\db\Migration;

class m171215_095321_app extends Migration
{
    public function safeUp()
    {
        $this->execute(file_get_contents('app.sql', true));
    }

    public function safeDown()
    {
        $this->execute(file_get_contents('app-down.sql', true));
    }
}
