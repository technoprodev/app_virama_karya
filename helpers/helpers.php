<?php

/** Debugger */

function ddx() {
    return call_user_func_array([Yii::$app->d, "ddx"], func_get_args());
}

function dd() {
    return call_user_func_array([Yii::$app->d, "dd"], func_get_args());
}

function updateStatus() {
    Yii::$app->db->createCommand('
        UPDATE paket p 
        INNER JOIN (
            SELECT ptj.id_paket, group_concat(t.status) AS status FROM paket_tahap_jadwal ptj
            INNER JOIN tahap t ON ptj.id_tahap = t.id
            WHERE now() BETWEEN ptj.tanggal_mulai AND ptj.tanggal_selesai
            GROUP BY ptj.id_paket
        ) AS t ON t.id_paket = p.id
        SET p.status=t.status
        WHERE p.auto_forward = 1
    ')->execute();
}

function spell_amount($value)
{
    $dasar = [
        1 => 'satu', 'dua', 'tiga', 'empat', 'lima', 
        'enam', 'tujuh', 'delapan', 'sembilan',
    ];
    $angka = [1000000000, 1000000, 1000, 100, 10, 1];
    $satuan = ['milyar', 'juta', 'ribu', 'ratus', 'puluh', ''];
    
    $i = 0;
    $str = '';
    $pecahan = '';
    $separator = (preg_match('/[^.,]*?([.,]{1}?)/', $value, $regs)) 
        ? $regs[1] 
        : false;

    if ($separator) {
        $p = explode($separator, sprintf("%s", $value));
        $pecahan = isset($p[1]) && $p[1] > 0 
            ? ' koma '.spell_amount($p[1]) 
            : '';

        $value = $p[0];
    }

    while ($value != 0) {
        $count = (int)($value / $angka[$i]);

        if ($count >= 10) {
            $str .= spell_amount($count)." ".$satuan[$i]." ";
        }
        else if ($count > 0 && $count < 10) {
            $str .= $dasar[$count]." ".$satuan[$i]." ";
        }

        $value -= $angka[$i] * $count;
        $i++;
    }

    $str = preg_replace("/satu puluh (\w+)/i", "\\1 belas", $str);
    $str = preg_replace("/satu (ribu|ratus|puluh|belas)/i", "se\\1", $str);

    return $str.$pecahan;
}