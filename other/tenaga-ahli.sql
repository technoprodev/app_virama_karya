SELECT 
`ta`.`id`
, `ta`.`nama`
, `ta`.`tanggal_lahir`
, `ta`.`jenis`
, `ta`.`kewarganegaraan`
, tap.*
, tak.*
FROM `tenaga_ahli` `ta` 
left join (
	select 
		tap.id_tenaga_ahli
		, GROUP_CONCAT((CONCAT("• ", tap.jenjang))) as `jenjang`
		, GROUP_CONCAT((CONCAT("• ", tap.jurusan))) as `jurusan`
		, GROUP_CONCAT((CONCAT("• ", tap.id_pendidikan_rumpun))) as `id_pendidikan_rumpun`
		, GROUP_CONCAT((CONCAT("• ", tap.tahun_lulus))) as `tahun_lulus`
		, GROUP_CONCAT((CONCAT("• ", tap.nomor_ijazah))) as `nomor_ijazah`
	from `tenaga_ahli_pendidikan` `tap`
	group by tap.id_tenaga_ahli
) as tap on tap.id_tenaga_ahli = ta.id
left join (
	select 
		tak.id_tenaga_ahli
		, GROUP_CONCAT((CONCAT("• ", tak.keahlian, " (", tak.sertifikat, ")"))) as `keahlian`
	from `tenaga_ahli_keahlian` `tak`
	group by tak.id_tenaga_ahli
) as tak on tak.id_tenaga_ahli = ta.id
