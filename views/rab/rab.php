<?php

use yii\widgets\ActiveForm;
use yii\helpers\Html;

$this->registerJsFile('@web/app/rab/rab.js', ['depends' => 'app_virama_karya\assets_manager\RequiredAsset']);
$this->registerJs('var paket_id='.$paket_id, 2);

?>

<div id="rab" class="table-responsive">
    <?php ActiveForm::begin([
        'method' => 'post',
        'action' => ['rab/index/', 'id' => $paket_id],
    ]); ?>
        <table class="table table-bordered">
            <colgroup>
                <col width="5%" />
                <col width="*" />
                <col width="20%" />
            </colgroup>
        	<thead>
        		<tr>
        			<th class="text-center">No.</th>
        			<th class="text-center">Uraian</th>
        			<th class="text-center">Biaya (Rp.)</th>
        		</tr>
        	</thead>
        	<tbody v-for="(category, i) in rab">
        		<tr class="bg-azure">
        			<td class="text-center f-bold">{{ String.fromCharCode('A'.charCodeAt() + i) }}</td>
                    <td class="f-bold" v-if="readonly">
                        {{ category.name }}
        			</td>
                    <td class="f-bold" v-else>
                        <div class="input-group">
                            <input type="hidden" v-if="!_.isUndefined(category.id)" name="category_ids[]" v-model="category.id" />
                            <input class="form-control bg-lightest" name="category[]" v-model="category.name" />

                            <div class="input-group-btn">
                                <button type="button" class="btn btn-danger" @click="removeCategory(i)">
                                    <i class="fa fa-remove"></i>
                                </button>
                            </div>
                        </div>
                    </td>
        			<td class="text-right f-bold">{{ category.value | currency }}</td>
        		</tr>
        		<tr v-for="(rab_item, j) in category.items">
        			<td></td>
                    <td v-if="readonly">
                        {{ j + 1 }}.
                        {{ options[rab_item.rab_item] }}

                        <a :href="fn.urlTo('rab/detail?id='+rab_item.id)" 
                            class="btn btn-xs btn-warning pull-right"
                            target="_blank">
                            <i class="fa fa-forward"></i>
                        </a>
                    </td>
                    <td v-else>
                        <div class="input-group">
                            <select :name="'rab_item['+i+'][]'" v-model="rab_item.rab_item" class="form-control" 
                                :disabled="rab_item.hasOwnProperty('id')">
                                <option v-for="(label, value) in options" :value="value">
                                    {{ label }}
                                </option>
                            </select>
                            <div class="input-group-btn">
                                <button type="button" class="btn btn-danger" @click="removeItem(category, j)">
                                    <i class="fa fa-remove"></i>
                                </button>
                            </div>
                        </div>
        			</td>
        			<td class="text-right">{{ rab_item.value | currency }}</td>
        		</tr>
                <tr v-if="readonly == false">
                    <td></td>
                    <td>
                        <div class="text-right" v-if="readonly == false">
                            <button type="button" class="btn btn-info" @click="addItem(category)" data-toggle="tooltip" title="Tambah Item">
                                <i class="fa fa-plus"></i>
                            </button>
                        </div>
                    </td>
                </tr>
            </tbody>
            <tbody v-if="readonly == false">
                <tr>
                    <td colspan="3" class="text-right">
                        <button type="button" class="btn btn-success" @click="addCategory()" data-toggle="tooltip" title="Tambah Kategori">
                            <i class="fa fa-plus"></i>
                        </button>
                    </td>
                </tr>
        	</tbody>
        	<tfoot>
        		<tr>
        			<th colspan="2" class="text-right">TOTAL AWAL</th>
        			<th class="text-right">{{ total | currency }}</th>
        		</tr>
        		<tr>
        			<th colspan="2" class="text-right">PPN 10%</th>
        			<th class="text-right">{{ ppn | currency }}</th>
                </tr>
                <tr>
                    <th colspan="2" class="text-right">TOTAL AKHIR</th>
                    <th class="text-right">{{ total - ppn | currency }}</th>
                </tr>
                <tr> 
                    <th colspan="3">
                        Terbilang: <span v-if="total - ppn > 0">{{ spell_amount(total - ppn) }} rupiah</span>
                    </th> 
                </tr>
            </tfoot>
        </table>
        <div class="margin-top-10" v-if="readonly == false">
            <button type="submit" class="btn btn-primary">
                <i class="fa fa-check"></i> Simpan
            </button>
            <button type="button" class="btn btn-default margin-left-10" @click="readonly = true">
                <i class="fa fa-remove"></i> Batal
            </button>
        </div>
        <div class="margin-top-10" v-else>
            <button type="button" class="btn btn-warning" @click="setModeEdit()">
                <i class="fa fa-pencil"></i> Edit
            </button>
            <?= Html::a('Download PDF', ['rab/print-rab', 'id' => $model['paket']->id], ['class' => 'btn bg-rose border-rose margin-left-10']) ?>
        </div>
    <?php ActiveForm::end(); ?>
</div>