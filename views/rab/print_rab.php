<div id="rab" class="table-responsive">
    <table class="table table-bordered">
        <colgroup>
            <col width="5%" />
            <col width="*" />
            <col width="20%" />
        </colgroup>
    	<thead>
    		<tr>
    			<th class="text-center">No.</th>
    			<th class="text-center">Uraian</th>
    			<th class="text-center">Biaya (Rp.)</th>
    		</tr>
    	</thead>

        <?php 
            $i='A'; 
            $total = 0;
        ?>
        <?php foreach($data as $category) : ?>
        	<tbody>
        		<tr class="bg-azure">
        			<td class="text-center f-bold"><?= $i=='A' ? $i : $i++ ?></td>
                    <td class="f-bold"><?= $category["name"] ?></td>
        			<td class="text-right f-bold"><?= number_format($category["value"]) ?></td>
        		</tr>
                <?php foreach($category["items"] as $j => $item) : ?>
            		<tr>
            			<td></td>
                        <td>
                            <?= $j + 1 ?>.
                            <?= $item["name"] ?>
                        </td>
            			<td class="text-right"><?= number_format($item["value"]) ?></td>
            		</tr>

                    <?php $total += $item["value"]; ?>
                <?php endforeach; ?>
            </tbody>
        <?php endforeach; ?>

    	<tfoot>
    		<tr>
    			<th colspan="2" class="text-right">TOTAL AWAL</th>
                <td class="text-right"><?= number_format($total) ?></td>
    		</tr>
    		<tr>
                <?php $ppn = $total * 10/100; ?>
    			<th colspan="2" class="text-right">PPN 10%</th>
                <td class="text-right"><?= number_format($ppn) ?></td>
            </tr>
            <tr>
                <th colspan="2" class="text-right">TOTAL AKHIR</th>
                <td class="text-right"><?= number_format($total - $ppn) ?></td>
            </tr>
            <tr> 
                <th colspan="3">
                    Terbilang: <?= ucwords(spell_amount($total - $ppn)) ?> Rupiah
                </th> 
            </tr>
        </tfoot>
    </table>
</div>