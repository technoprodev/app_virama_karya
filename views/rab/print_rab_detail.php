<h6 class="text-rose padding-bottom-5 border-bottom f-italic"><?= $rab["name"] ?></h6>

<div id="rab-detail" class="table-responsive">
    <table class="table table-bordered">
		<thead>
			<tr> 
				<?php foreach($column as $col) : ?>
					<th><?= $col->name ?></th> 
				<?php endforeach; ?>
			</tr>
		</thead>
		<tbody>
		<?php $total = 0; ?>
		<?php foreach($data as $i => $item) : ?>
			<tr>
			<?php foreach($column as $col) : ?>
				<td>
				<?php switch($col->type): 
					case "increment": ?>
						<?= $i + 1 ?>
						<?php break; ?>

					<?php case "pattern": ?>
					<?php case "numeric": ?>
						<div class="text-right"> <?= $item[$col->id] ?> </div>
						<?php break; ?>

					<?php case "string": ?>
						<?= $item[$col->id] ?>
						<?php break; ?>
				<?php endswitch; ?>
				</td>
			<?php endforeach; ?>
			</tr>

			<?php $total += $item[$rab["struct_def"]->_config->total]; ?>
		<?php endforeach; ?>
		</tbody>
		<tfoot>
			<tr>
				<th colspan="<?= count($column) - 1 ?>" class="text-right">Jumlah</th>
				<th class="text-right"><?= $total ?></th>
			</tr>
		</tfoot>
	</table>
</div>