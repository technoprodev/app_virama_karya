<?php

use yii\widgets\ActiveForm;

$this->title = $title;
$this->params['breadcrumbs'][] = "Home";
$this->params['breadcrumbs'][] = $this->title;

$this->registerJsFile('@web/app/rab/rab_detail.js', ['depends' => 'app_virama_karya\assets_manager\RequiredAsset']);
// $this->registerJs("v.readonly=".$readonly, 3);

?>

<h6 class="text-rose padding-bottom-5 border-bottom f-italic"><?= $subtitle ?></h6>

<div id="rab-detail" class="table-responsive">
    <?php ActiveForm::begin(); ?>
        <div class="text-right" v-if="readonly == false">
            <button type="submit" class="btn btn-primary">
                <i class="fa fa-check"></i> Simpan
            </button>
        </div>
        <div class="text-right" v-else>
            <button type="button" class="btn btn-warning" @click="setModeEdit()">
                <i class="fa fa-pencil"></i> Edit
            </button>
        </div>

        <table class="table table-bordered">
			<thead>
				<tr> 
					<th v-for="col in config.column">{{ col.name }}</th> 
					<th v-if="!readonly"></th>
				</tr>
			</thead>
			<tbody>
				<tr v-for="(item, i) in data">
					<td v-for="(col, order) in config.column">
						<!-- Increment -->
						<div v-if="col.type=='increment'">{{ i + 1 }}</div>
						
						<!-- Pattern -->
						<div v-if="col.type=='pattern'" class="text-right">
							{{ item[col.id] | currency }}

							<input v-if="!readonly" type="hidden" :name="'data['+i+']['+col.id+']'" v-model="item[col.id]" />
						</div>

						<!-- Numeric -->
						<div v-if="readonly && col.type=='numeric'" class="text-right">
							{{ item[col.id] | currency }}
						</div>
						<div v-if="!readonly && col.type=='numeric'">
							<input type="text" :name="'data['+i+']['+col.id+']'" @keyUp="handleUpdate(item, col.id)" v-model="item[col.id]" class="form-control" />
						</div>
						
						<!-- String -->
						<div v-if="readonly && col.type=='string'">
							{{ item[col.id] }}
						</div>
						<div v-if="!readonly && col.type=='string'">
							<input type="text" :name="'data['+i+']['+col.id+']'" @keyUp="handleUpdate(item, col.id)" v-model="item[col.id]" class="form-control" />
						</div>
					</td>
					<td v-if="readonly == false">
						<button type="button" class="btn btn-danger" @click="removeItem(i)" v-show="i > 0">
							<i class="fa fa-remove"></i>
						</button>
					</td>
				</tr>
                <tr v-if="readonly == false">
                    <td :colspan="config.col_length + 1" class="text-right">
                        <button type="button" class="btn btn-info" @click="addItem()" data-toggle="tooltip" title="Tambah Item">
                            <i class="fa fa-plus"></i>
                        </button>
                    </td>
                </tr>
			</tbody>
			<tfoot>
				<tr>
					<th :colspan="config.col_length - 1" class="text-right">Jumlah</th>
					<th class="text-right">{{ total }}</th>
					<th v-if="!readonly"></th>
				</tr>
			</tfoot>
		</table>
    <?php ActiveForm::end(); ?>
</div>