<?php

use yii\helpers\Html;

if (isset($title)) $this->title = $title;
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;

$this->registerJsFile('@web/app/tenaga-ahli/list.js', ['depends' => 'technosmart\assets_manager\DatatablesAsset']);
?>

<table class="datatables display nowrap table table-striped table-hover table-condensed">
    <thead>
        <tr>
            <th class="text-dark f-normal" style="border-bottom: 1px">Action</th>
            <th class="text-dark f-normal" style="border-bottom: 1px">Nama</th>
            <th class="text-dark f-normal" style="border-bottom: 1px">Tanggal Lahir</th>
            <th class="text-dark f-normal" style="border-bottom: 1px">Jenis</th>
            <th class="text-dark f-normal" style="border-bottom: 1px">Kewarganegaraan</th>
            <th class="text-dark f-normal" style="border-bottom: 1px">Jenjang</th>
            <th class="text-dark f-normal" style="border-bottom: 1px">Jurusan</th>
            <th class="text-dark f-normal" style="border-bottom: 1px">Rumpun</th>
            <th class="text-dark f-normal" style="border-bottom: 1px">Tahun Lulus</th>
            <th class="text-dark f-normal" style="border-bottom: 1px">Nomor Sertifikat</th>
            <th class="text-dark f-normal" style="border-bottom: 1px">Keahlian</th>
            <th class="text-dark f-normal" style="border-bottom: 1px">Level</th>
            <th class="text-dark f-normal" style="border-bottom: 1px">Sertifikat</th>
        </tr>
        <tr class="dt-search">
            <th class="padding-0"></th>
            <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Search nama" class="form-control border-none f-normal padding-x-5"/></th>
            <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Search tanggal lahir" class="form-control border-none f-normal padding-x-5"/></th>
            <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Search jenis" class="form-control border-none f-normal padding-x-5"/></th>
            <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Search kewarganegaraan" class="form-control border-none f-normal padding-x-5"/></th>
            <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Search jenjang" class="form-control border-none f-normal padding-x-5"/></th>
            <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Search jurusan" class="form-control border-none f-normal padding-x-5"/></th>
            <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Search rumpun" class="form-control border-none f-normal padding-x-5"/></th>
            <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Search tahun lulus" class="form-control border-none f-normal padding-x-5"/></th>
            <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Search sertifikat" class="form-control border-none f-normal padding-x-5"/></th>
            <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Search keahlian" class="form-control border-none f-normal padding-x-5"/></th>
            <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Search level" class="form-control border-none f-normal padding-x-5"/></th>
            <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Search sertifikat" class="form-control border-none f-normal padding-x-5"/></th>
        </tr>
    </thead>
</table>