<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

if (isset($title)) $this->title = $title;
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;

//
$tenagaAhliPendidikans = [];
if (isset($model['tenaga_ahli_pendidikan']))
    foreach ($model['tenaga_ahli_pendidikan'] as $key => $tenagaAhliPendidikan)
        $tenagaAhliPendidikans[] = $tenagaAhliPendidikan->attributes;

$tenagaAhliKeahlians = [];
if (isset($model['tenaga_ahli_keahlian']))
    foreach ($model['tenaga_ahli_keahlian'] as $key => $tenagaAhliKeahlian)
        $tenagaAhliKeahlians[] = $tenagaAhliKeahlian->attributes;

$this->registerJs(
    'vm.$data.tenaga_ahli.tenagaAhliKeahlians = vm.$data.tenaga_ahli.tenagaAhliKeahlians.concat(' . json_encode($tenagaAhliKeahlians) . ');' .
    'vm.$data.tenaga_ahli.tenagaAhliPendidikans = vm.$data.tenaga_ahli.tenagaAhliPendidikans.concat(' . json_encode($tenagaAhliPendidikans) . ');',
    3
);

//
$error = false;
$errorMessage = '';
$errorVue = false;
if ($model['tenaga_ahli']->hasErrors()) {
    $error = true; 
    $errorMessage .= Html::errorSummary($model['tenaga_ahli'], ['class' => '']);
}

if (isset($model['tenaga_ahli_keahlian'])) foreach ($model['tenaga_ahli_keahlian'] as $key => $tenagaAhliKeahlian) {
    if ($tenagaAhliKeahlian->hasErrors()) {
        $error = true;
        $errorMessage .= Html::errorSummary($tenagaAhliKeahlian, ['class' => '']);
        $errorVue = true; 
    }
}
if (isset($model['tenaga_ahli_pendidikan'])) foreach ($model['tenaga_ahli_pendidikan'] as $key => $tenagaAhliPendidikan) {
    if ($tenagaAhliPendidikan->hasErrors()) {
        $error = true;
        $errorMessage .= Html::errorSummary($tenagaAhliPendidikan, ['class' => '']);
        $errorVue = true; 
    }
}
if ($errorVue) {
    $this->registerJs(
        '$.each($("#app").data("yiiActiveForm").attributes, function() {
            this.status = 3;
        });
        $("#app").yiiActiveForm("validate");',
        5
    );
}
?>

<?php if (!Yii::$app->request->isAjax) : ?>
<div class="box box-break-sm margin-left-30 m-margin-left-0">
    <div class="box-10">
<?php endif; ?>

<?php $form = ActiveForm::begin(['options' => ['id' => 'app', 'class' => 'margin-top-30 margin-bottom-80']]); ?>
  
    <?php if ($error) : ?>
        <div class="alert alert-danger">
            <?= $errorMessage ?>
        </div>
    <?php endif; ?>

    <?= $form->field($model['tenaga_ahli'], 'nama', ['options' => ['class' => 'form-group form-group-sm box box-break-sm margin-bottom-10']])->begin(); ?>
        <div class="box-2 padding-x-0 text-right m-text-left padding-y-5">
            <?= Html::activeLabel($model['tenaga_ahli'], 'nama', ['class' => 'control-label']); ?>
        </div>
        <div class="box-10 m-padding-x-0">
            <?= Html::activeTextInput($model['tenaga_ahli'], 'nama', ['class' => 'form-control', 'maxlength' => true]) ?>
            <?= Html::error($model['tenaga_ahli'], 'nama', ['class' => 'help-block']); ?>
        </div>
    <?= $form->field($model['tenaga_ahli'], 'nama')->end(); ?>

    <?= $form->field($model['tenaga_ahli'], 'tanggal_lahir', ['options' => ['class' => 'form-group form-group-sm box box-break-sm margin-bottom-10']])->begin(); ?>
        <div class="box-2 padding-x-0 text-right m-text-left padding-y-5">
            <?= Html::activeLabel($model['tenaga_ahli'], 'tanggal_lahir', ['class' => 'control-label']); ?>
        </div>
        <div class="box-10 m-padding-x-0">
            <?= Html::activeTextInput($model['tenaga_ahli'], 'tanggal_lahir', ['class' => 'form-control']) ?>
            <?= Html::error($model['tenaga_ahli'], 'tanggal_lahir', ['class' => 'help-block']); ?>
        </div>
    <?= $form->field($model['tenaga_ahli'], 'tanggal_lahir')->end(); ?>

    <?= $form->field($model['tenaga_ahli'], 'jenis', ['options' => ['class' => 'form-group form-group-sm box box-break-sm margin-bottom-10']])->begin(); ?>
        <div class="box-2 padding-x-0 text-right m-text-left padding-y-5">
            <?= Html::activeLabel($model['tenaga_ahli'], 'jenis', ['class' => 'control-label']); ?>
        </div>
        <div class="box-10 m-padding-x-0">
            <?= Html::activeDropDownList($model['tenaga_ahli'], 'jenis', [ 'Tetap' => 'Tetap', 'Tidak Tetap' => 'Tidak Tetap', ], ['prompt' => 'Choose one please', 'class' => 'form-control']) ?>
            <?= Html::error($model['tenaga_ahli'], 'jenis', ['class' => 'help-block']); ?>
        </div>
    <?= $form->field($model['tenaga_ahli'], 'jenis')->end(); ?>

    <?= $form->field($model['tenaga_ahli'], 'kewarganegaraan', ['options' => ['class' => 'form-group form-group-sm box box-break-sm margin-bottom-10']])->begin(); ?>
        <div class="box-2 padding-x-0 text-right m-text-left padding-y-5">
            <?= Html::activeLabel($model['tenaga_ahli'], 'kewarganegaraan', ['class' => 'control-label']); ?>
        </div>
        <div class="box-10 m-padding-x-0">
            <?= Html::activeDropDownList($model['tenaga_ahli'], 'kewarganegaraan', [ 'WNI' => 'WNI', 'WNA' => 'WNA', ], ['prompt' => 'Choose one please', 'class' => 'form-control']) ?>
            <?= Html::error($model['tenaga_ahli'], 'kewarganegaraan', ['class' => 'help-block']); ?>
        </div>
    <?= $form->field($model['tenaga_ahli'], 'kewarganegaraan')->end(); ?>

    <h6 class="text-rose padding-bottom-5 border-bottom f-italic">Pendidikan</h6>

    <?php if (isset($model['tenaga_ahli_pendidikan'])) foreach ($model['tenaga_ahli_pendidikan'] as $key => $value): ?>
        <?= $form->field($model['tenaga_ahli_pendidikan'][$key], "[$key]jenjang")->begin(); ?>
        <?= $form->field($model['tenaga_ahli_pendidikan'][$key], "[$key]jenjang")->end(); ?>
        <?= $form->field($model['tenaga_ahli_pendidikan'][$key], "[$key]jurusan")->begin(); ?>
        <?= $form->field($model['tenaga_ahli_pendidikan'][$key], "[$key]jurusan")->end(); ?>
        <?= $form->field($model['tenaga_ahli_pendidikan'][$key], "[$key]id_pendidikan_rumpun")->begin(); ?>
        <?= $form->field($model['tenaga_ahli_pendidikan'][$key], "[$key]id_pendidikan_rumpun")->end(); ?>
        <?= $form->field($model['tenaga_ahli_pendidikan'][$key], "[$key]tahun_lulus")->begin(); ?>
        <?= $form->field($model['tenaga_ahli_pendidikan'][$key], "[$key]tahun_lulus")->end(); ?>
        <?= $form->field($model['tenaga_ahli_pendidikan'][$key], "[$key]nomor_ijazah")->begin(); ?>
        <?= $form->field($model['tenaga_ahli_pendidikan'][$key], "[$key]nomor_ijazah")->end(); ?>
    <?php endforeach; ?>

    <template v-if="typeof tenaga_ahli.tenagaAhliPendidikans == 'object'">
        <div class="box box-break-sm margin-bottom-5 hidden-sm-less">
            <div class="box-1 padding-x-0 padding-y-5 text-center">
                No
            </div>
            <div class="box-1 padding-x-0 padding-y-5 text-center">
                Jenjang
            </div>
            <div class="box-2 padding-x-0 padding-y-5 text-center">
                Jurusan
            </div>
            <div class="box-2 padding-x-0 padding-y-5 text-center">
                Rumpun
            </div>
            <div class="box-1 padding-x-0 padding-y-5 text-center">
                Tahun Lulus
            </div>
            <div class="box-4 padding-x-0 padding-y-5 text-center">
                Nomor Ijazah
            </div>
            <div class="box-1 padding-x-0">
                <a v-on:click="addTenagaAhliPendidikan" class="btn btn-default btn-sm margin-left-5 bg-azure hover-pointer"><i class="fa fa-plus"></i></a>
            </div>
        </div>
        <template v-for="(value, key, index) in tenaga_ahli.tenagaAhliPendidikans">
            <div v-show="!(value.id < 0)">
                <div class="box box-break-sm">
                    <div class="box-1 padding-x-0 padding-y-5 text-center m-text-left">
                        <input type="hidden" v-bind:id="'tenagaahlipendidikan-' + key + '-id'" v-bind:name="'TenagaAhliPendidikan[' + key + '][id]'" class="form-control" type="text" v-model="tenaga_ahli.tenagaAhliPendidikans[key].id">
                        {{key + 1}}
                    </div>
                    <div class="box-1 padding-x-0">
                        <div v-bind:class="'form-group form-group-sm field-tenagaahlipendidikan-' + key + '-jenjang'">
                            <select v-bind:id="'tenagaahlipendidikan-' + key + '-jenjang'" v-bind:name="'TenagaAhliPendidikan[' + key + '][jenjang]'" class="form-control" v-model="tenaga_ahli.tenagaAhliPendidikans[key].jenjang">
                                <option value="">Pilih Jenjang</option>
                                <?php foreach ((new \app_virama_karya\models\TenagaAhliPendidikan())->getEnum('jenjang') as $key => $value) : ?>
                                    <option value="<?= $value ?>"><?= $value ?></option>
                                <?php endforeach; ?>
                            </select>
                            <div class="help-block"></div>
                        </div>
                    </div>
                    <div class="box-2 padding-x-0">
                        <div v-bind:class="'form-group form-group-sm field-tenagaahlipendidikan-' + key + '-jurusan'">
                            <input placeholder="jurusan" v-bind:id="'tenagaahlipendidikan-' + key + '-jurusan'" v-bind:name="'TenagaAhliPendidikan[' + key + '][jurusan]'" class="form-control" type="text" v-model="tenaga_ahli.tenagaAhliPendidikans[key].jurusan">
                            <div class="help-block"></div>
                        </div>
                    </div>
                    <div class="box-2 padding-x-0">
                        <div v-bind:class="'form-group form-group-sm field-tenagaahlipendidikan-' + key + '-id_pendidikan_rumpun'">
                            <select v-bind:id="'tenagaahlipendidikan-' + key + '-id_pendidikan_rumpun'" v-bind:name="'TenagaAhliPendidikan[' + key + '][id_pendidikan_rumpun]'" class="form-control" v-model="tenaga_ahli.tenagaAhliPendidikans[key].id_pendidikan_rumpun">
                                <option value="">Pilih Rumpun</option>
                                <?php foreach (ArrayHelper::map(\app_virama_karya\models\PendidikanRumpun::find()->asArray()->all(), 'id', 'rumpun') as $key => $value) : ?>
                                    <option value="<?= $key ?>"><?= $value ?></option>
                                <?php endforeach; ?>
                            </select>
                            <div class="help-block"></div>
                        </div>
                    </div>
                    <div class="box-1 padding-x-0">
                        <div v-bind:class="'form-group form-group-sm field-tenagaahlipendidikan-' + key + '-tahun_lulus'">
                            <input placeholder="tahun" v-bind:id="'tenagaahlipendidikan-' + key + '-tahun_lulus'" v-bind:name="'TenagaAhliPendidikan[' + key + '][tahun_lulus]'" class="form-control" type="text" v-model="tenaga_ahli.tenagaAhliPendidikans[key].tahun_lulus">
                            <div class="help-block"></div>
                        </div>
                    </div>
                    <div class="box-4 padding-x-0">
                        <div v-bind:class="'form-group form-group-sm field-tenagaahlipendidikan-' + key + '-nomor_ijazah'">
                            <input placeholder="nomor" v-bind:id="'tenagaahlipendidikan-' + key + '-nomor_ijazah'" v-bind:name="'TenagaAhliPendidikan[' + key + '][nomor_ijazah]'" class="form-control" type="text" v-model="tenaga_ahli.tenagaAhliPendidikans[key].nomor_ijazah">
                            <div class="help-block"></div>
                        </div>
                    </div>
                    <div class="box-1 padding-x-0">
                        <div v-on:click="removeTenagaAhliPendidikan(key)" class="btn btn-default btn-sm margin-left-5 bg-light-red hover-pointer"><i class="fa fa-close"></i></div>
                    </div>
                </div>
            </div>
        </template>
    </template>

    <h6 class="text-rose padding-bottom-5 border-bottom f-italic">Keahlian</h6>

    <?php if (isset($model['tenaga_ahli_keahlian'])) foreach ($model['tenaga_ahli_keahlian'] as $key => $value): ?>
        <?= $form->field($model['tenaga_ahli_keahlian'][$key], "[$key]jenjang")->begin(); ?>
        <?= $form->field($model['tenaga_ahli_keahlian'][$key], "[$key]jenjang")->end(); ?>
        <?= $form->field($model['tenaga_ahli_keahlian'][$key], "[$key]jurusan")->begin(); ?>
        <?= $form->field($model['tenaga_ahli_keahlian'][$key], "[$key]jurusan")->end(); ?>
        <?= $form->field($model['tenaga_ahli_keahlian'][$key], "[$key]id_pendidikan_rumpun")->begin(); ?>
        <?= $form->field($model['tenaga_ahli_keahlian'][$key], "[$key]id_pendidikan_rumpun")->end(); ?>
        <?= $form->field($model['tenaga_ahli_keahlian'][$key], "[$key]tahun_lulus")->begin(); ?>
        <?= $form->field($model['tenaga_ahli_keahlian'][$key], "[$key]tahun_lulus")->end(); ?>
        <?= $form->field($model['tenaga_ahli_keahlian'][$key], "[$key]nomor_ijazah")->begin(); ?>
        <?= $form->field($model['tenaga_ahli_keahlian'][$key], "[$key]nomor_ijazah")->end(); ?>
    <?php endforeach; ?>

    <template v-if="typeof tenaga_ahli.tenagaAhliKeahlians == 'object'">
        <div class="box box-break-sm margin-bottom-5 hidden-sm-less">
            <div class="box-1 padding-x-0 padding-y-5 text-center">
                No
            </div>
            <div class="box-4 padding-x-0 padding-y-5 text-center">
                Keahlian
            </div>
            <div class="box-1 padding-x-0 padding-y-5 text-center">
                Level
            </div>
            <div class="box-5 padding-x-0 padding-y-5 text-center">
                Sertifikat
            </div>
            <div class="box-1 padding-x-0">
                <a v-on:click="addTenagaAhliKeahlian" class="btn btn-default btn-sm margin-left-5 bg-azure hover-pointer"><i class="fa fa-plus"></i></a>
            </div>
        </div>
        <template v-for="(value, key, index) in tenaga_ahli.tenagaAhliKeahlians">
            <div v-show="!(value.id < 0)">
                <div class="box box-break-sm">
                    <div class="box-1 padding-x-0 padding-y-5 text-center m-text-left">
                        <input type="hidden" v-bind:id="'tenagaahlikeahlian-' + key + '-id'" v-bind:name="'TenagaAhliKeahlian[' + key + '][id]'" class="form-control" type="text" v-model="tenaga_ahli.tenagaAhliKeahlians[key].id">
                        {{key + 1}}
                    </div>
                    <div class="box-4 padding-x-0">
                        <div v-bind:class="'form-group form-group-sm field-tenagaahlikeahlian-' + key + '-keahlian'">
                            <input placeholder="keahlian" v-bind:id="'tenagaahlikeahlian-' + key + '-keahlian'" v-bind:name="'TenagaAhliKeahlian[' + key + '][keahlian]'" class="form-control" type="text" v-model="tenaga_ahli.tenagaAhliKeahlians[key].keahlian">
                            <div class="help-block"></div>
                        </div>
                    </div>
                    <div class="box-1 padding-x-0">
                        <div v-bind:class="'form-group form-group-sm field-tenagaahlikeahlian-' + key + '-level'">
                            <select v-bind:id="'tenagaahlikeahlian-' + key + '-level'" v-bind:name="'TenagaAhliKeahlian[' + key + '][level]'" class="form-control" v-model="tenaga_ahli.tenagaAhliKeahlians[key].level">
                                <option value="">Pilih Jenjang</option>
                                <?php foreach ((new \app_virama_karya\models\TenagaAhliKeahlian())->getEnum('level') as $key => $value) : ?>
                                    <option value="<?= $value ?>"><?= $value ?></option>
                                <?php endforeach; ?>
                            </select>
                            <div class="help-block"></div>
                        </div>
                    </div>
                    <div class="box-5 padding-x-0">
                        <div v-bind:class="'form-group form-group-sm field-tenagaahlikeahlian-' + key + '-sertifikat'">
                            <input placeholder="sertifikat" v-bind:id="'tenagaahlikeahlian-' + key + '-sertifikat'" v-bind:name="'TenagaAhliKeahlian[' + key + '][sertifikat]'" class="form-control" type="text" v-model="tenaga_ahli.tenagaAhliKeahlians[key].sertifikat">
                            <div class="help-block"></div>
                        </div>
                    </div>
                    <div class="box-1 padding-x-0">
                        <div v-on:click="removeTenagaAhliKeahlian(key)" class="btn btn-default btn-sm margin-left-5 bg-light-red hover-pointer"><i class="fa fa-close"></i></div>
                    </div>
                </div>
            </div>
        </template>
    </template>

    <hr class="margin-y-15">

    <?php if ($error) : ?>
        <div class="alert alert-danger">
            <?= $errorMessage ?>
        </div>
    <?php endif; ?>
    
    <div class="form-group clearfix">
        <?= Html::submitButton('Submit', ['class' => 'btn btn-default bg-azure rounded-xs border-azure']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default bg-lighter rounded-xs']); ?> 
        <?= Html::a('Back to list', ['index'], ['class' => 'btn btn-default bg-lightest rounded-xs pull-right']) ?>
    </div>
    
<?php ActiveForm::end(); ?>

<?php if (!Yii::$app->request->isAjax) : ?>
    </div>
</div>
<?php endif; ?>