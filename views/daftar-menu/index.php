<?php

use technosmart\yii\widgets\Menu as MenuWidget;
use technosmart\models\Menu;

$this->title = 'Menu';
$this->params['breadcrumbs'][] = $this->title;
?>

<?php
    $cache = Yii::$app->cache;
    $key = 'menu-sidebar';
    if ($cache->exists($key)) {
        $cacheData = $cache->get($key);
    } else {
        $cacheData = [
            'sidebar' => technosmart\models\Menu::find()->where(['code' => 'sidebar'])->asArray()->all(),
        ];
        $cache->set($key, $cacheData);
    }
    // Yii::warning($cacheData['sidebar']);
?>

<?php
    $menuStarter = MenuWidget::widget([
        'items' => Menu::menuTree($cacheData['sidebar'], null, true),
        'options' => [
            'class' => 'menu-y menu-xs menu-active-bold submenu-active-bold menu-active-bg-light-blue menu-hover-darker-10 submenu-active-bg-light-blue',
        ],
        'activateItems' => true,
        'openParents' => true,
        'parentsCssClass' => 'has-submenu',
        'encodeLabels' => false,
        'labelTemplate' => '<a>{label}</a>',
        'hideEmptyItems' => true,
    ]);
?>

<?php if ($menuStarter): ?>
    <!-- <div class="f-bold padding-left-15 fs-13">Menu</div> -->
    <?= $menuStarter ?>
    <hr class="margin-y-15">
<?php endif; ?>