<?php

use yii\widgets\ActiveForm;

$this->registerJsFile('@web/app/timeline/timeline.js', ['depends' => 'app_virama_karya\assets_manager\RequiredAsset']);
$this->registerJs("var paket_id=$paket_id; var period_length=$period_length;", 2);

?>

<style type="text/css">
    .timeline-item {
        display:table;
        width:100%;
        padding-left: 5px;
    }
    .timeline-item .item-row {
        display: table-row;
    }
    .timeline-item .item-row > * {
        display: table-cell;
    }
    .timeline-item .control-label {
        padding: 3px;
    }
    .timeline-item input[type='text'] {
        width: 100%;
        border: none;
    }
    .timeline-item .btn-danger {
        display: none;
    }
    .timeline-item:hover .btn-danger {
        display: block;
    }
</style>

<div id="timeline" class="table-responsive">
    <?php ActiveForm::begin(["method" => "post", "action" => ["timeline/index", "id" => $paket_id]]); ?>
        <div class="text-right">
            <button type="submit" class="btn btn-primary">
                <i class="fa fa-check"></i> Simpan
            </button>
        </div>

        <table class="table table-bordered table-hover">
            <colgroup>
                <col width="50%" />
            </colgroup>
        	<thead>
                <tr>
                    <th rowspan="2">Uraian Pekerjaan</th>
                    <th :colspan="config.period_length" class="text-center">Bulan Ke</th>
                </tr>
        		<tr> <th v-for="(i, index) in config.period_length">{{ i }}</th> </tr>
        	</thead>
            <tbody>
                <tr v-for="(item, i) in timeline">
                    <td class="timeline-item">
                        <div class="item-row">
                            <label :for="'tl'+i" class="control-label text-right" 
                                :style="{'width': (item.level+1)*25 + 'px'}">
                                {{ getIncrement(item) }}.
                            </label>
                            <input :id="'tl'+i" type="text" :name="'timeline['+i+'][task]'" v-model="item.task" />

                            <div class="text-right">
                                <div class="btn-group">
                                    <button v-if="i > 0" type="button" class="btn btn-xs btn-danger" @click="removeItem(i)">
                                        <i class="fa fa-remove"></i>
                                    </button>
                                    <button type="button" class="btn btn-xs btn-success" @click="setAsParent(i)">
                                        <i class="fa fa-chevron-left"></i>
                                    </button>
                                    <button type="button" class="btn btn-xs btn-success" @click="setAsChild(i)">
                                        <i class="fa fa-chevron-right"></i>
                                    </button>
                                </div>
                            </div>
                        </div>

                        <input type="hidden" :name="'timeline['+i+'][month_start]'" v-model="item.month_start" />
                        <input type="hidden" :name="'timeline['+i+'][month_end]'" v-model="item.month_end" />
                        <input type="hidden" :name="'timeline['+i+'][parent_index]'" v-model="item.parent_index" />
                        <input type="hidden" :name="'timeline['+i+'][index]'" v-model="i" />
                    </td>
                    <td v-for="(period, index) in config.period_length" 
                        :class="{'bg-primary': (item.month_start <= period && item.month_end >= period) || item.month_start==period}"
                        @click="setTimelineRange(item, period)">
                    </td>
                </tr>
            </tbody>
            <tfoot>
                <tr>
                    <td class="text-right">
                        <button type="button" class="btn btn-xs btn-primary" @click="addItem()">
                            <i class="fa fa-plus"></i> Add Task
                        </button>
                    </td>
                </tr>
        	</tfoot>
        </table>
    <?php ActiveForm::end(); ?>
</div>