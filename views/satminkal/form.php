<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

if (isset($title)) $this->title = $title;
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;

$error = false;
$errorMessage = '';
if ($model['satminkal']->hasErrors()) {
    $error = true; 
    $errorMessage .= Html::errorSummary($model['satminkal'], ['class' => '']);
}
?>

<?php if (!Yii::$app->request->isAjax) : ?>
<div class="box box-break-sm margin-left-30 m-margin-left-0">
    <div class="box-8">
<?php endif; ?>

<?php $form = ActiveForm::begin(['enableClientValidation' => true, 'options' => ['id' => 'app']]); ?>
  
    <?php if ($error) : ?>
        <div class="alert alert-danger">
            <?= $errorMessage ?>
        </div>
    <?php endif; ?>

    <?= $form->field($model['satminkal'], 'nama_satminkal')->begin(); ?>
        <?= Html::activeLabel($model['satminkal'], 'nama_satminkal', ['class' => 'control-label']); ?>
        <?= Html::activeTextInput($model['satminkal'], 'nama_satminkal', ['class' => 'form-control', 'maxlength' => true]) ?>
        <?= Html::error($model['satminkal'], 'nama_satminkal', ['class' => 'help-block']); ?>
    <?= $form->field($model['satminkal'], 'nama_satminkal')->end(); ?>

    <?= $form->field($model['satminkal'], 'id_instansi')->begin(); ?>
        <?= Html::activeLabel($model['satminkal'], 'id_instansi', ['class' => 'control-label']); ?>
        <?= Html::activeTextInput($model['satminkal'], 'id_instansi', ['class' => 'form-control']) ?>
        <?= Html::error($model['satminkal'], 'id_instansi', ['class' => 'help-block']); ?>
    <?= $form->field($model['satminkal'], 'id_instansi')->end(); ?>


    <hr class="margin-y-15">

    <?php if ($error) : ?>
        <div class="alert alert-danger">
            <?= $errorMessage ?>
        </div>
    <?php endif; ?>
    
    <div class="form-group clearfix">
        <?= Html::submitButton($model['satminkal']->isNewRecord ? 'Create' : 'Update', ['class' => 'btn btn-default bg-azure rounded-xs border-azure']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default bg-lighter rounded-xs']); ?> 
        <?= Html::a('Back to list', ['index'], ['class' => 'btn btn-default bg-lightest rounded-xs pull-right']) ?>
    </div>
    
<?php ActiveForm::end(); ?>

<?php if (!Yii::$app->request->isAjax) : ?>
    </div>
</div>
<?php endif; ?>