<?php

use app_virama_karya\assets_manager\RequiredAsset;
use yii\helpers\Html;
use technosmart\yii\widgets\Menu as MenuWidget;
use yii\widgets\Breadcrumbs;
use technosmart\models\Menu;

RequiredAsset::register($this);
$this->beginPage();

if (Yii::$app->session->hasFlash('success'))
    $this->registerJs(
        'fn.alert("Success", "' . Yii::$app->session->getFlash('success') . '", "succes");',
        3
    );
?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>">
        <meta http-equiv="x-ua-compatible" content="ie=edge,chrome=1">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <meta name="description" content="<?= Yii::$app->params['app.description'] ?>">
        <meta name="keywords" content="<?= Yii::$app->params['app.keywords'] ?>">
        <meta name="author" content="<?= Yii::$app->params['app.author'] ?>">
        <?= Html::csrfMetaTags() ?>
        <meta name="base-url" content="<?= yii\helpers\Url::home(true) ?>">
        <title><?= $this->title ? strip_tags($this->title) . ' | ' : null ?><?= Yii::$app->params['app.name'] ?><?= Yii::$app->params['app.description'] ? ' - ' . Yii::$app->params['app.description'] : null ?></title>
        <link rel="shortcut icon" href="<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/favicon.ico" type="image/x-icon" />
        <link rel="apple-touch-icon" href="<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/favicon.ico">
        <?php $this->head() ?>
    </head>

    <body>
    <?php $this->beginBody() ?>
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        <!-- START @ALERT & CONFIRM -->
        <div class="modal fade" id="modal-alert" tabindex="-1" role="dialog" aria-labelledby="title-alert">
            <div class="modal-dialog modal-sm" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="title-alert"></h4>
                    </div>
                    <div class="modal-body">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="modal-confirm" tabindex="-1" role="dialog" aria-labelledby="title-confirm">
            <div class="modal-dialog modal-sm" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="title-confirm"></h4>
                    </div>
                    <div class="modal-body">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default modal-yes" data-dismiss="modal">Yes</button>
                        <button type="button" class="btn btn-default modal-no" data-dismiss="modal">No</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- /END @ALERT & CONFIRM -->

        <!-- START @WRAPPER -->
        <section id="wrapper">
            <!-- START @HEADER -->
            <header id="header" class="border-bottom border-light-azure">
                <div class="padding-y-10 fs-18 pull-left bg-lightest padding-x-15" style="width: 230px;">
                    <a href="<?= Yii::$app->urlManager->createUrl("daftar-menu/index") ?>" class="underline-none margin-right-10 visible-sm-inline-block visible-xs-inline-block">
                        <i class="fa fa-bars"></i>
                    </a>
                    <a href="<?= Yii::$app->urlManager->createUrl("site/index") ?>" class="underline-none">
                        <img src="<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/logo.jpg" style="height: 30px; width: auto;">
                    </a>
                </div>
                <div class="fs-18 pull-left padding-top-15 padding-x-20 hidden-sm-less">
                    <span class="f-italic text-azure"><?= Yii::$app->params['app.name'] ?></span>
                </div>
                <?php if (!Yii::$app->user->isGuest) : ?>
                    <div class="pull-right padding-top-15">
                        <a class="btn btn-sm btn-default margin-right-15 rounded-xs padding-y-2" data-method="post" href="<?= Yii::$app->urlManager->createUrl("site/logout") ?>">Logout</a>
                    </div>
                <?php endif; ?>
            </header>
            <!-- /END @HEADER -->

            <!-- START @BODY -->
            <section id="body" class="has-sidebar-left">
                <!-- START @SIDEBAR LEFT -->
                <aside id="sidebar-left" class="border-right border-light-azure padding-top-15">
                    <?php
                        $cache = Yii::$app->cache;
                        $key = 'menu-sidebar';
                        if ($cache->exists($key)) {
                            $cacheData = $cache->get($key);
                        } else {
                            $cacheData = [
                                'sidebar' => technosmart\models\Menu::find()->where(['code' => 'sidebar'])->asArray()->all(),
                            ];
                            $cache->set($key, $cacheData);
                        }
                        // Yii::warning($cacheData['sidebar']);
                    ?>

                    <?php
                        $menuStarter = MenuWidget::widget([
                            'items' => Menu::menuTree($cacheData['sidebar'], null, true),
                            'options' => [
                                'class' => 'menu-y menu-xs menu-active-bold submenu-active-bold menu-active-bg-light-blue menu-hover-darker-10 submenu-active-bg-light-blue',
                            ],
                            'activateItems' => true,
                            'openParents' => true,
                            'parentsCssClass' => 'has-submenu',
                            'encodeLabels' => false,
                            'labelTemplate' => '<a>{label}</a>',
                            'hideEmptyItems' => true,
                        ]);
                    ?>

                    <?php if ($menuStarter): ?>
                        <!-- <div class="f-bold padding-left-15 fs-13">Menu</div> -->
                        <?= $menuStarter ?>
                        <hr class="margin-y-15">
                    <?php endif; ?>
                </aside>
                <!-- /END @SIDEBAR LEFT -->

                <!-- START @PAGE WRAPPER -->
                <section id="page-wrapper">
                    <!-- START @PAGE SLIDER -->
                    <!-- <section id="page-slider">
                    </section> -->
                    <!-- /END @PAGE SLIDER -->

                    <!-- START @PAGE HEADER -->
                    <section id="page-header" class="border-bottom border-lighter">
                        <div class="fs-20"><?= Html::decode($this->title) ?></div>
                    </section>
                    <!-- /END @PAGE HEADER -->

                    <!-- START @PAGE BODY -->
                    <section id="page-body">
                        <?= $content ?>
                    </section>
                    <!-- /END @PAGE BODY -->
                </section>
                <!-- /END @PAGE WRAPPER -->

                <!-- START @SIDEBAR RIGHT -->
                <!-- <aside id="sidebar-right" class="border-left">
                </aside> -->
                <!-- /END @SIDEBAR RIGHT -->
            </section>
            <!-- /END @BODY -->

            <!-- START @FOOTER -->
            <footer id="footer" class="border-top border-light-azure bg-light-azure">
                <div class="container text-center fs-18"><?= Yii::$app->params['app.owner'] ?> © <?= date('Y') ?></div>
            </footer>
            <!-- /END @FOOTER -->
        </section>
        <!-- /END @WRAPPER -->

        <!-- START @BACK TO TOP -->
        <!-- <div id="back-to-top">
            <div class="padding-10 bg-gray fs-18 border">
                <span class="glyphicon glyphicon-arrow-up"></span>
            </div>
        </div> -->
        <!-- /END @BACK TO TOP -->

        <?php $this->endBody() ?>
    </body>
</html>
<?php $this->endPage() ?>