<?php

use yii\helpers\Html;

if (isset($title)) $this->title = $title;
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;

$this->registerJsFile('@web/app/tender/kalah.js', ['depends' => 'technosmart\assets_manager\DatatablesAsset']);
?>

<h6>Filter</h6>
<table class="datatables display nowrap table table-striped table-hover">
    <thead>
        <tr>
            <th class="text-dark f-normal" style="border-bottom: 1px">Unit Kerja</th>
            <th class="text-dark f-normal" style="border-bottom: 1px">Hps</th>
        </tr>
        <tr class="dt-search">
            <th class="padding-0">
                <select class="form-control border-none f-normal padding-x-5 search fs-12">
                    <option value="">Semua Unit Kerja</option>
                    <option value="1">Divisi Kimtaru</option>
                    <option value="2">Divisi Transportasi</option>
                    <option value="3">Divisi SDA</option>
                    <option value="4">Divisi Gedung</option>
                    <option value="5">Cabang Padang</option>
                    <option value="6">Cabang Semarang</option>
                    <option value="7">Cabang Surabaya</option>
                    <option value="8">Cabang Makasar</option>
                    <option value="9">Cabang Kalimantan</option>
                </select>
            </th>
            <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Filter hps diatas" class="form-control border-none f-normal padding-x-5 search"/></th>
        </tr>
    </thead>
</table>