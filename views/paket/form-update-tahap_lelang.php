<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

if (isset($title)) $this->title = $title;
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;

technosmart\assets_manager\BootstrapDatepickerAsset::register($this);
technosmart\assets_manager\JqueryMaskedInputAsset::register($this);

$error = false;
$errorMessage = '';
if ($model['paket']->hasErrors()) {
    $error = true; 
    $errorMessage .= Html::errorSummary($model['paket'], ['class' => '']);
}
?>

<?php if (!Yii::$app->request->isAjax) : ?>
<div class="box box-break-sm margin-left-30 m-margin-left-0">
    <div class="box-8 m-padding-x-0">
<?php endif; ?>

<h6 class="text-rose padding-bottom-5 border-bottom f-italic">Tahap Lelang</h6>

<?php $form = ActiveForm::begin(['options' => ['id' => 'app', 'class' => 'margin-top-30 margin-bottom-80']]); ?>
  
    <?php if ($error) : ?>
        <div class="alert alert-danger">
            <?= $errorMessage ?>
        </div>
    <?php endif; ?>

    <div class="box box-break-sm margin-bottom-15 hidden-sm-less">
        <div class="box-4 padding-y-5">
            
        </div>
        
        <div class="box-4">
            Tanggal Mulai
        </div>

        <div class="box-4">
            Tanggal Selesai
        </div>
    </div>

    <?php if (isset($model['paket_tahap_jadwal'])) foreach ($model['paket_tahap_jadwal'] as $key => $value): ?>
        <div class="box box-break-sm">
            <div class="box-4 padding-x-0 text-right m-text-left padding-y-5">
                <?= $model['paket_tahap_jadwal'][$key]->tahap->nama_tahap; ?>
                <?= Html::activeHiddenInput($model['paket_tahap_jadwal'][$key], "[$key]id"); ?>
            </div>
            
            <div class="box-4 m-padding-x-0">
                <?= $form->field($model['paket_tahap_jadwal'][$key], "[$key]tanggal_mulai", ['options' => ['class' => 'form-group form-group-sm box box-break-sm margin-bottom-5']])->begin(); ?>
                    <?= Html::activeTextInput($model['paket_tahap_jadwal'][$key], "[$key]tanggal_mulai", ['class' => 'form-control input-mask-date input-datepicker', 'maxlength' => true]); ?>
                    <?= Html::error($model['paket_tahap_jadwal'][$key], "[$key]tanggal_mulai", ['class' => 'help-block']); ?>
                <?= $form->field($model['paket_tahap_jadwal'][$key], "[$key]tanggal_mulai")->end(); ?>
            </div>

            <div class="box-4 m-padding-x-0">
                <?= $form->field($model['paket_tahap_jadwal'][$key], "[$key]tanggal_selesai", ['options' => ['class' => 'form-group form-group-sm box box-break-sm margin-bottom-5']])->begin(); ?>
                    <?= Html::activeTextInput($model['paket_tahap_jadwal'][$key], "[$key]tanggal_selesai", ['class' => 'form-control input-mask-date input-datepicker', 'maxlength' => true]); ?>
                    <?= Html::error($model['paket_tahap_jadwal'][$key], "[$key]tanggal_selesai", ['class' => 'help-block']); ?>
                <?= $form->field($model['paket_tahap_jadwal'][$key], "[$key]tanggal_selesai")->end(); ?>
            </div>
        </div>
    <?php endforeach; ?>

    <hr class="margin-y-15">

    <?php if ($error) : ?>
        <div class="alert alert-danger">
            <?= $errorMessage ?>
        </div>
    <?php endif; ?>
    
    <div class="form-group clearfix">
        <?= Html::submitButton('Submit', ['class' => 'btn btn-default bg-azure rounded-xs border-azure']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default bg-lighter rounded-xs']); ?> 
        <?= Html::a('Kembali ke detail', ['index', 'id' => $model['paket']->id], ['class' => 'btn btn-default bg-lightest rounded-xs pull-right']) ?>
    </div>
    
<?php ActiveForm::end(); ?>

<?php if (!Yii::$app->request->isAjax) : ?>
    </div>
</div>
<?php endif; ?>