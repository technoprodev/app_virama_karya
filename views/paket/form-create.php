<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

if (isset($title)) $this->title = $title;
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;

//
$paketMitras = [];
if (isset($model['paket_mitra']))
    foreach ($model['paket_mitra'] as $key => $paketMitra)
        $paketMitras[] = $paketMitra->attributes;

$this->registerJs(
    'vm.$data.paket.paketMitras = vm.$data.paket.paketMitras.concat(' . json_encode($paketMitras) . ');' .
    'vm.$data.paket.is_kso = "' . $model['paket']->is_kso . '";',
    3
);

$error = false;
$errorMessage = '';
$errorVue = false;
if ($model['paket']->hasErrors()) {
    $error = true; 
    $errorMessage .= Html::errorSummary($model['paket'], ['class' => '']);
}

if (isset($model['paket_mitra'])) foreach ($model['paket_mitra'] as $key => $paketMitra) {
    if ($paketMitra->hasErrors()) {
        $error = true;
        $errorMessage .= Html::errorSummary($paketMitra, ['class' => '']);
        $errorVue = true; 
    }
}
if ($errorVue) {
    $this->registerJs(
        '$.each($("#app").data("yiiActiveForm").attributes, function() {
            this.status = 3;
        });
        $("#app").yiiActiveForm("validate");',
        5
    );
}
?>

<?php if (!Yii::$app->request->isAjax) : ?>
<div class="box box-break-sm margin-left-30 m-margin-left-0">
    <div class="box-8 m-padding-x-0">
<?php endif; ?>

<?php $form = ActiveForm::begin(['options' => ['id' => 'app', 'class' => 'margin-top-30 margin-bottom-80']]); ?>
  
    <?php if ($error) : ?>
        <div class="alert alert-danger">
            <?= $errorMessage ?>
        </div>
    <?php endif; ?>

    <?= $form->field($model['paket'], 'nama_paket', ['options' => ['class' => 'form-group form-group-sm box box-break-sm margin-bottom-10']])->begin(); ?>
        <div class="box-2 padding-x-0 text-right m-text-left padding-y-5">
            <?= Html::activeLabel($model['paket'], 'nama_paket', ['class' => 'control-label']); ?>
        </div>
        <div class="box-10 m-padding-x-0">
            <?= Html::activeTextInput($model['paket'], 'nama_paket', ['class' => 'form-control', 'maxlength' => true]) ?>
            <?= Html::error($model['paket'], 'nama_paket', ['class' => 'help-block fs-11 margin-0']); ?>
        </div>
    <?= $form->field($model['paket'], 'nama_paket')->end(); ?>

    <?= $form->field($model['paket'], 'lokasi', ['options' => ['class' => 'form-group form-group-sm box box-break-sm margin-bottom-10']])->begin(); ?>
        <div class="box-2 padding-x-0 text-right m-text-left padding-y-5">
            <?= Html::activeLabel($model['paket'], 'lokasi', ['class' => 'control-label']); ?>
        </div>
        <div class="box-10 m-padding-x-0">
            <?= Html::activeTextInput($model['paket'], 'lokasi', ['class' => 'form-control', 'maxlength' => true]) ?>
            <?= Html::error($model['paket'], 'lokasi', ['class' => 'help-block fs-11 margin-0']); ?>
        </div>
    <?= $form->field($model['paket'], 'lokasi')->end(); ?>

    <?= $form->field($model['paket'], 'id_unit_kerja', ['options' => ['class' => 'form-group form-group-sm box box-break-sm margin-bottom-10']])->begin(); ?>
        <div class="box-2 padding-x-0 text-right m-text-left padding-y-5">
            <?= Html::activeLabel($model['paket'], 'id_unit_kerja', ['class' => 'control-label']); ?>
        </div>
        <div class="box-10 m-padding-x-0">
            <?= Html::activeDropDownList($model['paket'], 'id_unit_kerja', ArrayHelper::map(\app_virama_karya\models\UnitKerja::find()->indexBy('id')->asArray()->all(), 'id', 'nama_unit_kerja'), ['prompt' => 'Pilih data', 'class' => 'form-control']); ?>
            <?= Html::error($model['paket'], 'id_unit_kerja', ['class' => 'help-block fs-11 margin-0']); ?>
        </div>
    <?= $form->field($model['paket'], 'id_unit_kerja')->end(); ?>

    <?= $form->field($model['paket'], 'id_bidang_unit_kerja', ['options' => ['class' => 'form-group form-group-sm box box-break-sm margin-bottom-10']])->begin(); ?>
        <div class="box-2 padding-x-0 text-right m-text-left padding-y-5">
            <?= Html::activeLabel($model['paket'], 'id_bidang_unit_kerja', ['class' => 'control-label']); ?>
        </div>
        <div class="box-10 m-padding-x-0">
            <?= Html::activeDropDownList($model['paket'], 'id_bidang_unit_kerja', ArrayHelper::map(\app_virama_karya\models\BidangUnitKerja::find()->indexBy('id')->asArray()->all(), 'id', 'nama_bidang_unit_kerja'), ['prompt' => 'Pilih data', 'class' => 'form-control']); ?>
            <?= Html::error($model['paket'], 'id_bidang_unit_kerja', ['class' => 'help-block fs-11 margin-0']); ?>
        </div>
    <?= $form->field($model['paket'], 'id_bidang_unit_kerja')->end(); ?>

    <?= $form->field($model['paket'], 'id_pengguna_jasa', ['options' => ['class' => 'form-group form-group-sm box box-break-sm margin-bottom-10']])->begin(); ?>
        <div class="box-2 padding-x-0 text-right m-text-left padding-y-5">
            <?= Html::activeLabel($model['paket'], 'id_pengguna_jasa', ['class' => 'control-label']); ?>
        </div>
        <div class="box-10 m-padding-x-0">
            <?= Html::activeDropDownList($model['paket'], 'id_pengguna_jasa', ArrayHelper::map(\app_virama_karya\models\PenggunaJasa::find()->indexBy('id')->asArray()->all(), 'id', 'nama_pengguna_jasa'), ['prompt' => 'Pilih data', 'class' => 'form-control']); ?>
            <?= Html::error($model['paket'], 'id_pengguna_jasa', ['class' => 'help-block fs-11 margin-0']); ?>
        </div>
    <?= $form->field($model['paket'], 'id_pengguna_jasa')->end(); ?>

    <?= $form->field($model['paket'], 'id_instansi', ['options' => ['class' => 'form-group form-group-sm box box-break-sm margin-bottom-10']])->begin(); ?>
        <div class="box-2 padding-x-0 text-right m-text-left padding-y-5">
            <?= Html::activeLabel($model['paket'], 'id_instansi', ['class' => 'control-label']); ?>
        </div>
        <div class="box-10 m-padding-x-0">
            <?= Html::activeDropDownList($model['paket'], 'id_instansi', ArrayHelper::map(\app_virama_karya\models\Instansi::find()->indexBy('id')->asArray()->all(), 'id', 'nama_instansi'), ['prompt' => 'Pilih data', 'class' => 'form-control']); ?>
            <?= Html::error($model['paket'], 'id_instansi', ['class' => 'help-block fs-11 margin-0']); ?>
        </div>
    <?= $form->field($model['paket'], 'id_instansi')->end(); ?>

    <?= $form->field($model['paket'], 'id_satminkal', ['options' => ['class' => 'form-group form-group-sm box box-break-sm margin-bottom-10']])->begin(); ?>
        <div class="box-2 padding-x-0 text-right m-text-left padding-y-5">
            <?= Html::activeLabel($model['paket'], 'id_satminkal', ['class' => 'control-label']); ?>
        </div>
        <div class="box-10 m-padding-x-0">
            <?= Html::activeDropDownList($model['paket'], 'id_satminkal', ArrayHelper::map(\app_virama_karya\models\Satminkal::find()->indexBy('id')->asArray()->all(), 'id', 'nama_satminkal'), ['prompt' => 'Pilih data', 'class' => 'form-control']); ?>
            <?= Html::error($model['paket'], 'id_satminkal', ['class' => 'help-block fs-11 margin-0']); ?>
        </div>
    <?= $form->field($model['paket'], 'id_satminkal')->end(); ?>

    <?= $form->field($model['paket'], 'id_satker', ['options' => ['class' => 'form-group form-group-sm box box-break-sm margin-bottom-10']])->begin(); ?>
        <div class="box-2 padding-x-0 text-right m-text-left padding-y-5">
            <?= Html::activeLabel($model['paket'], 'id_satker', ['class' => 'control-label']); ?>
        </div>
        <div class="box-10 m-padding-x-0">
            <?= Html::activeDropDownList($model['paket'], 'id_satker', ArrayHelper::map(\app_virama_karya\models\Satker::find()->indexBy('id')->asArray()->all(), 'id', 'nama_satker'), ['prompt' => 'Pilih data', 'class' => 'form-control']); ?>
            <?= Html::error($model['paket'], 'id_satker', ['class' => 'help-block fs-11 margin-0']); ?>
        </div>
    <?= $form->field($model['paket'], 'id_satker')->end(); ?>

    <?= $form->field($model['paket'], 'id_bidang_jasa', ['options' => ['class' => 'form-group form-group-sm box box-break-sm margin-bottom-10']])->begin(); ?>
        <div class="box-2 padding-x-0 text-right m-text-left padding-y-5">
            <?= Html::activeLabel($model['paket'], 'id_bidang_jasa', ['class' => 'control-label']); ?>
        </div>
        <div class="box-10 m-padding-x-0">
            <?= Html::activeDropDownList($model['paket'], 'id_bidang_jasa', ArrayHelper::map(\app_virama_karya\models\BidangJasa::find()->indexBy('id')->asArray()->all(), 'id', 'nama_bidang_jasa'), ['prompt' => 'Pilih data', 'class' => 'form-control']); ?>
            <?= Html::error($model['paket'], 'id_bidang_jasa', ['class' => 'help-block fs-11 margin-0']); ?>
        </div>
    <?= $form->field($model['paket'], 'id_bidang_jasa')->end(); ?>

    <?= $form->field($model['paket'], 'id_sub_bidang_jasa', ['options' => ['class' => 'form-group form-group-sm box box-break-sm margin-bottom-10']])->begin(); ?>
        <div class="box-2 padding-x-0 text-right m-text-left padding-y-5">
            <?= Html::activeLabel($model['paket'], 'id_sub_bidang_jasa', ['class' => 'control-label']); ?>
        </div>
        <div class="box-10 m-padding-x-0">
            <?= Html::activeDropDownList($model['paket'], 'id_sub_bidang_jasa', ArrayHelper::map(\app_virama_karya\models\SubBidangJasa::find()->indexBy('id')->asArray()->all(), 'id', 'nama_sub_bidang_jasa'), ['prompt' => 'Pilih data', 'class' => 'form-control']); ?>
            <?= Html::error($model['paket'], 'id_sub_bidang_jasa', ['class' => 'help-block fs-11 margin-0']); ?>
        </div>
    <?= $form->field($model['paket'], 'id_sub_bidang_jasa')->end(); ?>

    <?= $form->field($model['paket'], 'tahun_anggaran', ['options' => ['class' => 'form-group form-group-sm box box-break-sm margin-bottom-10']])->begin(); ?>
        <div class="box-2 padding-x-0 text-right m-text-left padding-y-5">
            <?= Html::activeLabel($model['paket'], 'tahun_anggaran', ['class' => 'control-label']); ?>
        </div>
        <div class="box-10 m-padding-x-0">
            <?= Html::activeDropDownList($model['paket'], 'tahun_anggaran',
            [
                '1990' => '1990',
                '1991' => '1991',
                '1992' => '1992',
                '1993' => '1993',
                '1994' => '1994',
                '1995' => '1995',
                '1996' => '1996',
                '1997' => '1997',
                '1998' => '1998',
                '1999' => '1999',
                '2000' => '2000',
                '2001' => '2001',
                '2002' => '2002',
                '2003' => '2003',
                '2004' => '2004',
                '2005' => '2005',
                '2006' => '2006',
                '2007' => '2007',
                '2008' => '2008',
                '2009' => '2009',
                '2010' => '2010',
                '2011' => '2011',
                '2012' => '2012',
                '2013' => '2013',
                '2014' => '2014',
                '2015' => '2015',
                '2016' => '2016',
                '2017' => '2017',
                '2018' => '2018',
                '2019' => '2019',
                '2020' => '2020',
                '2021' => '2021',
                '2022' => '2022',
                '2023' => '2023',
                '2024' => '2024',
                '2025' => '2025',
                '2026' => '2026',
                '2027' => '2027',
                '2028' => '2028',
                '2029' => '2029',
                '2030' => '2030',
            ],
            ['prompt' => 'Pilih data', 'class' => 'form-control']); ?>
            <?= Html::error($model['paket'], 'tahun_anggaran', ['class' => 'help-block fs-11 margin-0']); ?>
        </div>
    <?= $form->field($model['paket'], 'tahun_anggaran')->end(); ?>

    <?= $form->field($model['paket'], 'sumber_anggaran', ['options' => ['class' => 'form-group form-group-sm box box-break-sm margin-bottom-10']])->begin(); ?>
        <div class="box-2 padding-x-0 text-right m-text-left padding-y-5">
            <?= Html::activeLabel($model['paket'], 'sumber_anggaran', ['class' => 'control-label']); ?>
        </div>
        <div class="box-10 m-padding-x-0">
            <?= Html::activeTextInput($model['paket'], 'sumber_anggaran', ['class' => 'form-control', 'maxlength' => true]) ?>
            <?= Html::error($model['paket'], 'sumber_anggaran', ['class' => 'help-block fs-11 margin-0']); ?>
        </div>
    <?= $form->field($model['paket'], 'sumber_anggaran')->end(); ?>

    <?= $form->field($model['paket'], 'pagu', ['options' => ['class' => 'form-group form-group-sm box box-break-sm margin-bottom-10']])->begin(); ?>
        <div class="box-2 padding-x-0 text-right m-text-left padding-y-5">
            <?= Html::activeLabel($model['paket'], 'pagu', ['class' => 'control-label']); ?>
        </div>
        <div class="box-10 m-padding-x-0">
            <?= Html::activeTextInput($model['paket'], 'pagu', ['class' => 'form-control']) ?>
            <?= Html::error($model['paket'], 'pagu', ['class' => 'help-block fs-11 margin-0']); ?>
        </div>
    <?= $form->field($model['paket'], 'pagu')->end(); ?>

    <?= $form->field($model['paket'], 'hps', ['options' => ['class' => 'form-group form-group-sm box box-break-sm margin-bottom-10']])->begin(); ?>
        <div class="box-2 padding-x-0 text-right m-text-left padding-y-5">
            <?= Html::activeLabel($model['paket'], 'hps', ['class' => 'control-label']); ?>
        </div>
        <div class="box-10 m-padding-x-0">
            <?= Html::activeTextInput($model['paket'], 'hps', ['class' => 'form-control']) ?>
            <?= Html::error($model['paket'], 'hps', ['class' => 'help-block fs-11 margin-0']); ?>
        </div>
    <?= $form->field($model['paket'], 'hps')->end(); ?>

    <?= $form->field($model['paket'], 'bobot_teknis', ['options' => ['class' => 'form-group form-group-sm box box-break-sm margin-bottom-10']])->begin(); ?>
        <div class="box-2 padding-x-0 text-right m-text-left padding-y-5">
            <?= Html::activeLabel($model['paket'], 'bobot_teknis', ['class' => 'control-label']); ?>
        </div>
        <div class="box-10 m-padding-x-0">
            <?= Html::activeTextInput($model['paket'], 'bobot_teknis', ['class' => 'form-control']) ?>
            <?= Html::error($model['paket'], 'bobot_teknis', ['class' => 'help-block fs-11 margin-0']); ?>
        </div>
    <?= $form->field($model['paket'], 'bobot_teknis')->end(); ?>

    <?= $form->field($model['paket'], 'bobot_biaya', ['options' => ['class' => 'form-group form-group-sm box box-break-sm margin-bottom-10']])->begin(); ?>
        <div class="box-2 padding-x-0 text-right m-text-left padding-y-5">
            <?= Html::activeLabel($model['paket'], 'bobot_biaya', ['class' => 'control-label']); ?>
        </div>
        <div class="box-10 m-padding-x-0">
            <?= Html::activeTextInput($model['paket'], 'bobot_biaya', ['class' => 'form-control']) ?>
            <?= Html::error($model['paket'], 'bobot_biaya', ['class' => 'help-block fs-11 margin-0']); ?>
        </div>
    <?= $form->field($model['paket'], 'bobot_biaya')->end(); ?>

    <?= $form->field($model['paket'], 'is_kso', ['options' => ['class' => 'form-group form-group-sm box box-break-sm margin-bottom-10']])->begin(); ?>
        <div class="box-2 padding-x-0 text-right m-text-left padding-y-5">
            <?= Html::activeLabel($model['paket'], 'is_kso', ['class' => 'control-label']); ?>
        </div>
        <div class="box-10 m-padding-x-0">
            <?= Html::activeRadioList($model['paket'], 'is_kso', $model['paket']->getEnum('is_kso'), ['class' => 'row row-radio', 'unselect' => null,
            'item' => function($index, $label, $name, $checked, $value){
                $checked = $checked ? 'checked' : '';
                $disabled = in_array($value, []) ? 'disabled' : '';
                return "<div class='radio col-xs-1'><label><input type='radio' name='$name' value='$value' v-model='paket.is_kso' $checked $disabled><i></i>$label</label></div>";
            }]); ?>
            <?= Html::error($model['paket'], 'is_kso', ['class' => 'help-block fs-11 margin-0']); ?>
        </div>
    <?= $form->field($model['paket'], 'is_kso')->end(); ?>

    <?php if (isset($model['paket_mitra'])) foreach ($model['paket_mitra'] as $key => $value): ?>
        <?= $form->field($model['paket_mitra'][$key], "[$key]id_mitra")->begin(); ?>
        <?= $form->field($model['paket_mitra'][$key], "[$key]id_mitra")->end(); ?>
        <?= $form->field($model['paket_mitra'][$key], "[$key]porsi")->begin(); ?>
        <?= $form->field($model['paket_mitra'][$key], "[$key]porsi")->end(); ?>
    <?php endforeach; ?>

    <template v-if="paket.is_kso == 'Ya' && typeof paket.paketMitras == 'object'">
        <template v-for="(value, key, index) in paket.paketMitras">
            <div v-show="!(value.id < 0)">
                <input type="hidden" v-bind:id="'paketmitra-' + key + '-id'" v-bind:name="'PaketMitra[' + key + '][id]'" class="form-control" type="text" v-model="paket.paketMitras[key].id">
                <div class="box box-break-sm margin-bottom-10">
                    <div class="box-2 padding-x-0 text-right m-text-left padding-y-5">
                        Mitra {{key + 1}}
                    </div>
                    <div v-bind:class="'form-group form-group-sm field-paketmitra-' + key + '-id_mitra box-5 m-padding-x-0'">
                        <select v-bind:id="'paketmitra-' + key + '-id_mitra'" v-bind:name="'PaketMitra[' + key + '][id_mitra]'" class="form-control" v-model="paket.paketMitras[key].id_mitra">
                            <option value="">Pilih Mitra</option>
                            <?php foreach (ArrayHelper::map(\app_virama_karya\models\Mitra::find()->asArray()->all(), 'id', 'nama_perusahaan') as $key => $value) : ?>
                                <option value="<?= $key ?>"><?= $value ?></option>
                            <?php endforeach; ?>
                        </select>
                        <div class="help-block"></div>
                    </div>
                    <div v-bind:class="'form-group form-group-sm field-paketmitra-' + key + '-porsi box-4 padding-x-0'">
                        <input placeholder="porsi" v-bind:id="'paketmitra-' + key + '-porsi'" v-bind:name="'PaketMitra[' + key + '][porsi]'" class="form-control" type="text" v-model="paket.paketMitras[key].porsi">
                        <div class="help-block"></div>
                    </div>
                    <div class="box-1 m-padding-x-0">
                        <div v-on:click="removePaketMitra(key)" class="btn btn-default btn-sm bg-light-red hover-pointer"><i class="fa fa-close"></i></div>
                        <div class="help-block"></div>
                    </div>
                </div>
            </div>
        </template>
        <div class="form-group form-group-sm box box-break-sm margin-bottom-10">
            <div class="box-2 padding-x-0 text-right m-text-left padding-y-5">
                
            </div>
            <div class="box-10 m-padding-x-0">
                <a v-on:click="addPaketMitra" class="btn btn-default btn-sm bg-azure hover-pointer">Tambah mitra</a>
            </div>
        </div>
    </template>

    <hr class="margin-y-15">

    <?= $form->field($model['paket'], 'peluang', ['options' => ['class' => 'form-group form-group-sm box box-break-sm margin-bottom-10']])->begin(); ?>
        <div class="box-2 padding-x-0 text-right m-text-left padding-y-5">
            <?= Html::activeLabel($model['paket'], 'peluang', ['class' => 'control-label']); ?>
        </div>
        <div class="box-10 m-padding-x-0">
            <?= Html::activeTextInput($model['paket'], 'peluang', ['class' => 'form-control', 'disabled' => '']) ?>
            <?= Html::error($model['paket'], 'peluang', ['class' => 'help-block fs-11 margin-0']); ?>
        </div>
    <?= $form->field($model['paket'], 'peluang')->end(); ?>

    <?= $form->field($model['paket'], 'tindak_lanjut_25', ['options' => ['class' => 'form-group form-group-sm box box-break-sm margin-bottom-10']])->begin(); ?>
        <div class="box-2 padding-x-0 text-right m-text-left padding-y-5">
            <?= Html::activeLabel($model['paket'], 'tindak_lanjut_25', ['class' => 'control-label']); ?>
        </div>
        <div class="box-10 m-padding-x-0">
            <?= Html::activeTextArea($model['paket'], 'tindak_lanjut_25', ['class' => 'form-control', 'rows' => 6]) ?>
            <?= Html::error($model['paket'], 'tindak_lanjut_25', ['class' => 'help-block fs-11 margin-0']); ?>
        </div>
    <?= $form->field($model['paket'], 'tindak_lanjut_25')->end(); ?>


    <hr class="margin-y-15">

    <?php if ($error) : ?>
        <div class="alert alert-danger">
            <?= $errorMessage ?>
        </div>
    <?php endif; ?>
    
    <div class="form-group clearfix">
        <?= Html::submitButton('Submit', ['class' => 'btn btn-default bg-azure rounded-xs border-azure']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default bg-lighter rounded-xs']); ?> 
        <?= Html::a('Back to list', ['index'], ['class' => 'btn btn-default bg-lightest rounded-xs pull-right']) ?>
    </div>
    
<?php ActiveForm::end(); ?>

<?php if (!Yii::$app->request->isAjax) : ?>
    </div>
</div>
<?php endif; ?>
        </div>