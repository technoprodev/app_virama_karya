<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

if (isset($title)) $this->title = $title;
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;

$error = false;
$errorMessage = '';
if ($model['paket']->hasErrors()) {
    $error = true; 
    $errorMessage .= Html::errorSummary($model['paket'], ['class' => '']);
}
?>

<?php if (!Yii::$app->request->isAjax) : ?>
<div class="box box-break-sm margin-left-30 m-margin-left-0">
    <div class="box-8 m-padding-x-0">
<?php endif; ?>

<h6 class="text-rose padding-bottom-5 border-bottom f-italic">Info Paket</h6>

<?php $form = ActiveForm::begin(['options' => ['id' => 'app', 'class' => 'margin-top-30 margin-bottom-80']]); ?>
  
    <?php if ($error) : ?>
        <div class="alert alert-danger">
            <?= $errorMessage ?>
        </div>
    <?php endif; ?>

    <?= $form->field($model['paket'], 'nama_paket', ['options' => ['class' => 'form-group form-group-sm box box-break-sm margin-bottom-10']])->begin(); ?>
        <div class="box-2 padding-x-0 text-right m-text-left padding-y-5">
            <?= Html::activeLabel($model['paket'], 'nama_paket', ['class' => 'control-label']); ?>
        </div>
        <div class="box-10 m-padding-x-0">
            <?= Html::activeTextInput($model['paket'], 'nama_paket', ['class' => 'form-control', 'maxlength' => true]) ?>
            <?= Html::error($model['paket'], 'nama_paket', ['class' => 'help-block fs-11 margin-0']); ?>
        </div>
    <?= $form->field($model['paket'], 'nama_paket')->end(); ?>

    <?= $form->field($model['paket'], 'lokasi', ['options' => ['class' => 'form-group form-group-sm box box-break-sm margin-bottom-10']])->begin(); ?>
        <div class="box-2 padding-x-0 text-right m-text-left padding-y-5">
            <?= Html::activeLabel($model['paket'], 'lokasi', ['class' => 'control-label']); ?>
        </div>
        <div class="box-10 m-padding-x-0">
            <?= Html::activeTextInput($model['paket'], 'lokasi', ['class' => 'form-control', 'maxlength' => true]) ?>
            <?= Html::error($model['paket'], 'lokasi', ['class' => 'help-block fs-11 margin-0']); ?>
        </div>
    <?= $form->field($model['paket'], 'lokasi')->end(); ?>

    <?= $form->field($model['paket'], 'id_unit_kerja', ['options' => ['class' => 'form-group form-group-sm box box-break-sm margin-bottom-10']])->begin(); ?>
        <div class="box-2 padding-x-0 text-right m-text-left padding-y-5">
            <?= Html::activeLabel($model['paket'], 'id_unit_kerja', ['class' => 'control-label']); ?>
        </div>
        <div class="box-10 m-padding-x-0">
            <?= Html::activeDropDownList($model['paket'], 'id_unit_kerja', ArrayHelper::map(\app_virama_karya\models\UnitKerja::find()->indexBy('id')->asArray()->all(), 'id', 'nama_unit_kerja'), ['prompt' => 'Pilih data', 'class' => 'form-control']); ?>
            <?= Html::error($model['paket'], 'id_unit_kerja', ['class' => 'help-block fs-11 margin-0']); ?>
        </div>
    <?= $form->field($model['paket'], 'id_unit_kerja')->end(); ?>

    <?= $form->field($model['paket'], 'id_bidang_unit_kerja', ['options' => ['class' => 'form-group form-group-sm box box-break-sm margin-bottom-10']])->begin(); ?>
        <div class="box-2 padding-x-0 text-right m-text-left padding-y-5">
            <?= Html::activeLabel($model['paket'], 'id_bidang_unit_kerja', ['class' => 'control-label']); ?>
        </div>
        <div class="box-10 m-padding-x-0">
            <?= Html::activeDropDownList($model['paket'], 'id_bidang_unit_kerja', ArrayHelper::map(\app_virama_karya\models\BidangUnitKerja::find()->indexBy('id')->asArray()->all(), 'id', 'nama_bidang_unit_kerja'), ['prompt' => 'Pilih data', 'class' => 'form-control']); ?>
            <?= Html::error($model['paket'], 'id_bidang_unit_kerja', ['class' => 'help-block fs-11 margin-0']); ?>
        </div>
    <?= $form->field($model['paket'], 'id_bidang_unit_kerja')->end(); ?>

    <?= $form->field($model['paket'], 'id_pengguna_jasa', ['options' => ['class' => 'form-group form-group-sm box box-break-sm margin-bottom-10']])->begin(); ?>
        <div class="box-2 padding-x-0 text-right m-text-left padding-y-5">
            <?= Html::activeLabel($model['paket'], 'id_pengguna_jasa', ['class' => 'control-label']); ?>
        </div>
        <div class="box-10 m-padding-x-0">
            <?= Html::activeDropDownList($model['paket'], 'id_pengguna_jasa', ArrayHelper::map(\app_virama_karya\models\PenggunaJasa::find()->indexBy('id')->asArray()->all(), 'id', 'nama_pengguna_jasa'), ['prompt' => 'Pilih data', 'class' => 'form-control']); ?>
            <?= Html::error($model['paket'], 'id_pengguna_jasa', ['class' => 'help-block fs-11 margin-0']); ?>
        </div>
    <?= $form->field($model['paket'], 'id_pengguna_jasa')->end(); ?>

    <?= $form->field($model['paket'], 'id_instansi', ['options' => ['class' => 'form-group form-group-sm box box-break-sm margin-bottom-10']])->begin(); ?>
        <div class="box-2 padding-x-0 text-right m-text-left padding-y-5">
            <?= Html::activeLabel($model['paket'], 'id_instansi', ['class' => 'control-label']); ?>
        </div>
        <div class="box-10 m-padding-x-0">
            <?= Html::activeDropDownList($model['paket'], 'id_instansi', ArrayHelper::map(\app_virama_karya\models\Instansi::find()->indexBy('id')->asArray()->all(), 'id', 'nama_instansi'), ['prompt' => 'Pilih data', 'class' => 'form-control']); ?>
            <?= Html::error($model['paket'], 'id_instansi', ['class' => 'help-block fs-11 margin-0']); ?>
        </div>
    <?= $form->field($model['paket'], 'id_instansi')->end(); ?>

    <?= $form->field($model['paket'], 'id_satminkal', ['options' => ['class' => 'form-group form-group-sm box box-break-sm margin-bottom-10']])->begin(); ?>
        <div class="box-2 padding-x-0 text-right m-text-left padding-y-5">
            <?= Html::activeLabel($model['paket'], 'id_satminkal', ['class' => 'control-label']); ?>
        </div>
        <div class="box-10 m-padding-x-0">
            <?= Html::activeDropDownList($model['paket'], 'id_satminkal', ArrayHelper::map(\app_virama_karya\models\Satminkal::find()->indexBy('id')->asArray()->all(), 'id', 'nama_satminkal'), ['prompt' => 'Pilih data', 'class' => 'form-control']); ?>
            <?= Html::error($model['paket'], 'id_satminkal', ['class' => 'help-block fs-11 margin-0']); ?>
        </div>
    <?= $form->field($model['paket'], 'id_satminkal')->end(); ?>

    <?= $form->field($model['paket'], 'id_satker', ['options' => ['class' => 'form-group form-group-sm box box-break-sm margin-bottom-10']])->begin(); ?>
        <div class="box-2 padding-x-0 text-right m-text-left padding-y-5">
            <?= Html::activeLabel($model['paket'], 'id_satker', ['class' => 'control-label']); ?>
        </div>
        <div class="box-10 m-padding-x-0">
            <?= Html::activeDropDownList($model['paket'], 'id_satker', ArrayHelper::map(\app_virama_karya\models\Satker::find()->indexBy('id')->asArray()->all(), 'id', 'nama_satker'), ['prompt' => 'Pilih data', 'class' => 'form-control']); ?>
            <?= Html::error($model['paket'], 'id_satker', ['class' => 'help-block fs-11 margin-0']); ?>
        </div>
    <?= $form->field($model['paket'], 'id_satker')->end(); ?>

    <?= $form->field($model['paket'], 'id_bidang_jasa', ['options' => ['class' => 'form-group form-group-sm box box-break-sm margin-bottom-10']])->begin(); ?>
        <div class="box-2 padding-x-0 text-right m-text-left padding-y-5">
            <?= Html::activeLabel($model['paket'], 'id_bidang_jasa', ['class' => 'control-label']); ?>
        </div>
        <div class="box-10 m-padding-x-0">
            <?= Html::activeDropDownList($model['paket'], 'id_bidang_jasa', ArrayHelper::map(\app_virama_karya\models\BidangJasa::find()->indexBy('id')->asArray()->all(), 'id', 'nama_bidang_jasa'), ['prompt' => 'Pilih data', 'class' => 'form-control']); ?>
            <?= Html::error($model['paket'], 'id_bidang_jasa', ['class' => 'help-block fs-11 margin-0']); ?>
        </div>
    <?= $form->field($model['paket'], 'id_bidang_jasa')->end(); ?>

    <?= $form->field($model['paket'], 'id_sub_bidang_jasa', ['options' => ['class' => 'form-group form-group-sm box box-break-sm margin-bottom-10']])->begin(); ?>
        <div class="box-2 padding-x-0 text-right m-text-left padding-y-5">
            <?= Html::activeLabel($model['paket'], 'id_sub_bidang_jasa', ['class' => 'control-label']); ?>
        </div>
        <div class="box-10 m-padding-x-0">
            <?= Html::activeDropDownList($model['paket'], 'id_sub_bidang_jasa', ArrayHelper::map(\app_virama_karya\models\SubBidangJasa::find()->indexBy('id')->asArray()->all(), 'id', 'nama_sub_bidang_jasa'), ['prompt' => 'Pilih data', 'class' => 'form-control']); ?>
            <?= Html::error($model['paket'], 'id_sub_bidang_jasa', ['class' => 'help-block fs-11 margin-0']); ?>
        </div>
    <?= $form->field($model['paket'], 'id_sub_bidang_jasa')->end(); ?>

    <?= $form->field($model['paket'], 'tahun_anggaran', ['options' => ['class' => 'form-group form-group-sm box box-break-sm margin-bottom-10']])->begin(); ?>
        <div class="box-2 padding-x-0 text-right m-text-left padding-y-5">
            <?= Html::activeLabel($model['paket'], 'tahun_anggaran', ['class' => 'control-label']); ?>
        </div>
        <div class="box-10 m-padding-x-0">
            <?php for ($i=1990; $i <= 2030; $i++) $year[$i] = $i; ?>
            <?= Html::activeDropDownList($model['paket'], 'tahun_anggaran', $year, ['prompt' => 'Pilih data', 'class' => 'form-control']); ?>
            <?= Html::error($model['paket'], 'tahun_anggaran', ['class' => 'help-block fs-11 margin-0']); ?>
        </div>
    <?= $form->field($model['paket'], 'tahun_anggaran')->end(); ?>

    <?= $form->field($model['paket'], 'sumber_anggaran', ['options' => ['class' => 'form-group form-group-sm box box-break-sm margin-bottom-10']])->begin(); ?>
        <div class="box-2 padding-x-0 text-right m-text-left padding-y-5">
            <?= Html::activeLabel($model['paket'], 'sumber_anggaran', ['class' => 'control-label']); ?>
        </div>
        <div class="box-10 m-padding-x-0">
            <?= Html::activeTextInput($model['paket'], 'sumber_anggaran', ['class' => 'form-control', 'maxlength' => true]) ?>
            <?= Html::error($model['paket'], 'sumber_anggaran', ['class' => 'help-block fs-11 margin-0']); ?>
        </div>
    <?= $form->field($model['paket'], 'sumber_anggaran')->end(); ?>

    <?= $form->field($model['paket'], 'pagu', ['options' => ['class' => 'form-group form-group-sm box box-break-sm margin-bottom-10']])->begin(); ?>
        <div class="box-2 padding-x-0 text-right m-text-left padding-y-5">
            <?= Html::activeLabel($model['paket'], 'pagu', ['class' => 'control-label']); ?>
        </div>
        <div class="box-10 m-padding-x-0">
            <?= Html::activeTextInput($model['paket'], 'pagu', ['class' => 'form-control']) ?>
            <?= Html::error($model['paket'], 'pagu', ['class' => 'help-block fs-11 margin-0']); ?>
        </div>
    <?= $form->field($model['paket'], 'pagu')->end(); ?>

    <?= $form->field($model['paket'], 'hps', ['options' => ['class' => 'form-group form-group-sm box box-break-sm margin-bottom-10']])->begin(); ?>
        <div class="box-2 padding-x-0 text-right m-text-left padding-y-5">
            <?= Html::activeLabel($model['paket'], 'hps', ['class' => 'control-label']); ?>
        </div>
        <div class="box-10 m-padding-x-0">
            <?= Html::activeTextInput($model['paket'], 'hps', ['class' => 'form-control']) ?>
            <?= Html::error($model['paket'], 'hps', ['class' => 'help-block fs-11 margin-0']); ?>
        </div>
    <?= $form->field($model['paket'], 'hps')->end(); ?>

    <?= $form->field($model['paket'], 'bobot_teknis', ['options' => ['class' => 'form-group form-group-sm box box-break-sm margin-bottom-10']])->begin(); ?>
        <div class="box-2 padding-x-0 text-right m-text-left padding-y-5">
            <?= Html::activeLabel($model['paket'], 'bobot_teknis', ['class' => 'control-label']); ?>
        </div>
        <div class="box-10 m-padding-x-0">
            <?= Html::activeTextInput($model['paket'], 'bobot_teknis', ['class' => 'form-control']) ?>
            <?= Html::error($model['paket'], 'bobot_teknis', ['class' => 'help-block fs-11 margin-0']); ?>
        </div>
    <?= $form->field($model['paket'], 'bobot_teknis')->end(); ?>

    <?= $form->field($model['paket'], 'bobot_biaya', ['options' => ['class' => 'form-group form-group-sm box box-break-sm margin-bottom-10']])->begin(); ?>
        <div class="box-2 padding-x-0 text-right m-text-left padding-y-5">
            <?= Html::activeLabel($model['paket'], 'bobot_biaya', ['class' => 'control-label']); ?>
        </div>
        <div class="box-10 m-padding-x-0">
            <?= Html::activeTextInput($model['paket'], 'bobot_biaya', ['class' => 'form-control']) ?>
            <?= Html::error($model['paket'], 'bobot_biaya', ['class' => 'help-block fs-11 margin-0']); ?>
        </div>
    <?= $form->field($model['paket'], 'bobot_biaya')->end(); ?>

    <?= $form->field($model['paket'], 'is_kso', ['options' => ['class' => 'form-group form-group-sm box box-break-sm margin-bottom-10']])->begin(); ?>
        <div class="box-2 padding-x-0 text-right m-text-left padding-y-5">
            <?= Html::activeLabel($model['paket'], 'is_kso', ['class' => 'control-label']); ?>
        </div>
        <div class="box-10 m-padding-x-0">
            <?= Html::activeRadioList($model['paket'], 'is_kso', $model['paket']->getEnum('is_kso'), ['class' => 'row row-radio', 'unselect' => null,
            'item' => function($index, $label, $name, $checked, $value){
                $checked = $checked ? 'checked' : '';
                $disabled = in_array($value, []) ? 'disabled' : '';
                return "<div class='radio col-xs-1'><label><input type='radio' name='$name' value='$value' $checked $disabled><i></i>$label</label></div>";
            }]); ?>
            <?= Html::error($model['paket'], 'is_kso', ['class' => 'help-block fs-11 margin-0']); ?>
        </div>
    <?= $form->field($model['paket'], 'is_kso')->end(); ?>

    Mitra

    <hr class="margin-y-15">

    <?php if ($error) : ?>
        <div class="alert alert-danger">
            <?= $errorMessage ?>
        </div>
    <?php endif; ?>
    
    <div class="form-group clearfix">
        <?= Html::submitButton('Submit', ['class' => 'btn btn-default bg-azure rounded-xs border-azure']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default bg-lighter rounded-xs']); ?> 
        <?= Html::a('Kembali ke detail', ['index', 'id' => $model['paket']->id], ['class' => 'btn btn-default bg-lightest rounded-xs pull-right']) ?>
    </div>
    
<?php ActiveForm::end(); ?>

<?php if (!Yii::$app->request->isAjax) : ?>
    </div>
</div>
<?php endif; ?>