<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

if (isset($title)) $this->title = $title;
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;

$this->registerJs(
    'vm.$data.paket.status = vm.$data.paket.status.concat(' . json_encode($model['paket']->status) . ');' .
    'vm.$data.paket.is_kso = "' . $model['paket']->is_kso . '";',
    3
);

?>
    
<div class="margin-top-30">
    <div class="box box-break-sm margin-bottom-10">
        <div class="box-1 padding-x-0 text-right m-text-left"><?= $model['paket']->attributeLabels()['nama_paket'] ?></div>
        <div class="box-11 m-padding-x-0 f-bold"><?= $model['paket']->nama_paket ? $model['paket']->nama_paket : '<span class="f-light f-italic">(kosong)</span>' ?></div>
    </div>

    <div class="box box-break-sm margin-bottom-10">
        <div class="box-1 padding-x-0 text-right m-text-left"><?= $model['paket']->attributeLabels()['status'] ?></div>
        <div class="box-11 m-padding-x-0 f-bold"><?= $model['paket']->status ? implode(', ', $model['paket']->status) : '<span class="f-light f-italic">(kosong)</span>' ?></div>
    </div>

    <?php $form = ActiveForm::begin([
        'options' => ['id' => 'app', 'class' => ''],
        'method' => 'post',
        'action' => ['paket/alasan'],
    ]); ?>
    <div>
        <?= $form->field($model['paket'], 'status', ['options' => ['class' => 'form-group form-group-sm box box-break-sm margin-bottom-10']])->begin(); ?>
            <div class="box-1 padding-x-0 text-right m-text-left">
                Alasan
            </div>
            <div class="box-11 m-padding-x-0">
                <div id="paket-status">
                    <?php if (in_array('Prakualifikasi', $model['paket']->status)) : ?>
                        <div class="radio-inline"><label><input name="Paket[status]" value="Prakualifikasi" checked="checked" type="radio"><i></i>Prakualifikasi</label></div> 
                        <div class="radio-inline"><label><input name="Paket[status]" value="Tidak Shortlist" type="radio"><i></i>Tidak Shortlist</label></div> 
                    <?php elseif (in_array('Proposal', $model['paket']->status)) : ?>
                        <div class="radio-inline"><label><input name="Paket[status]" value="Proposal" type="radio"><i></i>Proposal</label></div> 
                        <div class="radio-inline"><label><input name="Paket[status]" value="Kontrak" type="radio"><i></i>Kontrak</label></div> 
                        <div class="radio-inline"><label><input name="Paket[status]" value="Kalah" type="radio"><i></i>Kalah</label></div> 
                        <div class="radio-inline"><label><input name="Paket[status]" value="Mundur" type="radio"><i></i>Mundur</label></div>
                    <?php endif; ?>
                </div>
                <?= Html::error($model['paket'], 'status', ['class' => 'help-block fs-11 margin-0']); ?>
            </div>
        <?= $form->field($model['paket'], 'status')->end(); ?>
    </div>
    <!-- {{vm.paket.status}} -->
    <template v-show="vm.paket.status.indexOf('Tidak Shortlist')">
        <?= $form->field($model['paket'], 'alasan', ['options' => ['class' => 'form-group form-group-sm box box-break-sm margin-bottom-10']])->begin(); ?>
            <div class="box-1 padding-x-0 text-right m-text-left">
                Tindak Lanjut
            </div>
            <div class="box-11 m-padding-x-0">
                <?= Html::activeTextArea($model['paket'], 'alasan', ['class' => 'form-control', 'rows' => 6]) ?>
                <?= Html::error($model['paket'], 'alasan', ['class' => 'help-block fs-11 margin-0']); ?>
            </div>
        <?= $form->field($model['paket'], 'alasan')->end(); ?>
    </template>
    <?php ActiveForm::end(); ?>
    
    <div class="margin-y-30">
        <ul class="nav nav-tabs f-bold">
            <li class="active"><a href="#tab-info_paket" data-toggle="tab">Info Paket</a></li>
            <li <?= !count(array_intersect($model['paket']->status, ['Prakualifikasi', 'Tidak Shortlist', 'Proposal', 'Kontrak', 'Kalah', 'Mundur'])) > 0 ? 'class="disabled f-normal"' : '' ?>>
                <a <?= count(array_intersect($model['paket']->status, ['Prakualifikasi', 'Tidak Shortlist', 'Proposal', 'Kontrak', 'Kalah', 'Mundur'])) > 0 ? 'href="#tab-pq" data-toggle="tab"' : '' ?>>PQ</a>
            </li>
            <li <?= !count(array_intersect($model['paket']->status, ['Tidak Shortlist'])) > 0 ? 'class="disabled f-normal"' : '' ?>>
                <a <?= count(array_intersect($model['paket']->status, ['Tidak Shortlist'])) > 0 ? 'href="#tab-tidak_shortlist" data-toggle="tab"' : '' ?>>Tidak Shortlist</a>
            </li>
            <li <?= !count(array_intersect($model['paket']->status, ['Proposal', 'Kontrak', 'Kalah', 'Mundur'])) > 0 ? 'class="disabled f-normal"' : '' ?>>
                <a <?= count(array_intersect($model['paket']->status, ['Proposal', 'Kontrak', 'Kalah', 'Mundur'])) > 0 ? 'href="#tab-proposal" data-toggle="tab"' : '' ?>>Proposal</a>
            </li>
            <li <?= !count(array_intersect($model['paket']->status, ['Kontrak', 'Kalah', 'Mundur'])) > 0 ? 'class="disabled f-normal"' : '' ?>>
                <a <?= count(array_intersect($model['paket']->status, ['Kontrak', 'Kalah', 'Mundur'])) > 0 ? 'href="#tab-kontrak" data-toggle="tab"' : '' ?>>Kontrak</a>
            </li>
            <li <?= !count(array_intersect($model['paket']->status, ['Kalah'])) > 0 ? 'class="disabled f-normal"' : '' ?>>
                <a <?= count(array_intersect($model['paket']->status, ['Kalah'])) > 0 ? 'href="#tab-kalah" data-toggle="tab"' : '' ?>>Kalah</a>
            </li>
            <li <?= !count(array_intersect($model['paket']->status, ['Mundur'])) > 0 ? 'class="disabled f-normal"' : '' ?>>
                <a <?= count(array_intersect($model['paket']->status, ['Mundur'])) > 0 ? 'href="#tab-mundur" data-toggle="tab"' : '' ?>>Mundur</a>
            </li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane padding-top-15 margin-left-50 fade active in" id="tab-info_paket">
                <ul class="nav nav-pills">
                    <li class="active"><a href="#tab-info_paket_detail" data-toggle="tab">Info Paket</a></li>
                    <li><a href="#tab-tahap_lelang" data-toggle="tab">Tahap Lelang</a></li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane padding-top-15 fade active in" id="tab-info_paket_detail">
                        <?= $this->render('one/info-paket/_one-info_paket_detail', ['model' => $model]); ?>
                        <?= Html::a('Edit', ['paket/update-info-paket-detail', 'id' => $model['paket']->id], ['class' => 'btn btn-default bg-azure rounded-xs border-azure margin-top-15']) ?>
                        <?= Html::a('Download PDF', ['download/info-paket-detail', 'id' => $model['paket']->id], ['class' => 'btn bg-rose rounded-xs border-rose margin-top-15 margin-left-10']) ?>
                    </div>
                    <div class="tab-pane padding-top-15 fade" id="tab-tahap_lelang">
                        <?= $this->render('one/info-paket/_one-tahap_lelang', ['model' => $model]); ?>
                        <?= Html::a('Edit', ['paket/update-tahap-lelang', 'id' => $model['paket']->id], ['class' => 'btn btn-default bg-azure rounded-xs border-azure margin-top-15']) ?>
                        <?= Html::a('Download PDF', ['download/tahap-lelang', 'id' => $model['paket']->id], ['class' => 'btn bg-rose rounded-xs border-rose margin-top-15 margin-left-10']) ?>
                    </div>
                </div>
            </div>
            <div class="tab-pane padding-top-15 margin-left-50 fade" id="tab-pq">
                <ul class="nav nav-pills">
                    <li class="active"><a href="#tab-pakta_integritas" data-toggle="tab">Pakta Integritas</a></li>
                    <li><a href="#tab-surat_form_isian" data-toggle="tab">Surat Form Isian</a></li>
                    <li><a href="#tab-form_isian_ag" data-toggle="tab">Form Isian (A-G)</a></li>
                    <li><a href="#tab-surat_minat" data-toggle="tab">Surat Minat</a></li>
                    <li><a href="#tab-surat_pernyataan" data-toggle="tab">Surat Pernyataan</a></li>
                    <li><a href="#tab-surat_kso" data-toggle="tab">Surat KSO</a></li>
                    <li><a href="#tab-tenaga_ahli" data-toggle="tab">Tenaga Ahli</a></li>
                    <li><a href="#tab-peralatan" data-toggle="tab">Peralatan</a></li>
                    <li><a href="#tab-pengalaman_10_tahun" data-toggle="tab">Pengalaman 10 Tahun</a></li>
                    <li><a href="#tab-pengalaman_sejenis" data-toggle="tab">Pengalaman Sejenis</a></li>
                    <li><a href="#tab-pengalaman_4_tahun" data-toggle="tab">Pengalaman 4 Tahun</a></li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane padding-top-15 fade active in" id="tab-pakta_integritas">
                        <?= $this->render('one/prakualifikasi/_one-pakta_integritas', ['model' => $model]); ?>
                        <?= Html::a('Edit', ['paket/update-pakta-integritas', 'id' => $model['paket']->id], ['class' => 'btn btn-default bg-azure rounded-xs border-azure margin-top-15']) ?>
                        <?= Html::a('Download PDF', ['download/pakta-integritas', 'id' => $model['paket']->id], ['class' => 'btn bg-rose rounded-xs border-rose margin-top-15 margin-left-10']) ?>
                    </div>
                    <div class="tab-pane padding-top-15 fade" id="tab-surat_form_isian">
                        <?= $this->render('one/prakualifikasi/_one-surat_form_isian', ['model' => $model]); ?>
                        <?= Html::a('Edit', ['paket/update-surat-form-isian', 'id' => $model['paket']->id], ['class' => 'btn btn-default bg-azure rounded-xs border-azure margin-top-15']) ?>
                        <?= Html::a('Download PDF', ['download/surat-form-isian', 'id' => $model['paket']->id], ['class' => 'btn bg-rose rounded-xs border-rose margin-top-15 margin-left-10']) ?>
                    </div>
                    <div class="tab-pane padding-top-15 fade" id="tab-form_isian_ag">
                        <?= $this->render('one/prakualifikasi/_one-form_isian_ag', ['model' => $model]); ?>
                        <?= Html::a('Edit', ['paket/update-form-isian-ag', 'id' => $model['paket']->id], ['class' => 'btn btn-default bg-azure rounded-xs border-azure margin-top-15']) ?>
                        <?= Html::a('Download PDF', ['download/form-isian-ag', 'id' => $model['paket']->id], ['class' => 'btn bg-rose rounded-xs border-rose margin-top-15 margin-left-10']) ?>
                    </div>
                    <div class="tab-pane padding-top-15 fade" id="tab-surat_minat">
                        <?= $this->render('one/prakualifikasi/_one-surat_minat', ['model' => $model]); ?>
                        <?= Html::a('Edit', ['paket/update-surat-minat', 'id' => $model['paket']->id], ['class' => 'btn btn-default bg-azure rounded-xs border-azure margin-top-15']) ?>
                        <?= Html::a('Download PDF', ['download/surat-minat', 'id' => $model['paket']->id], ['class' => 'btn bg-rose rounded-xs border-rose margin-top-15 margin-left-10']) ?>
                    </div>
                    <div class="tab-pane padding-top-15 fade" id="tab-surat_pernyataan">
                        <?= $this->render('one/prakualifikasi/_one-surat_pernyataan', ['model' => $model]); ?>
                        <?= Html::a('Edit', ['paket/update-surat-pernyataan', 'id' => $model['paket']->id], ['class' => 'btn btn-default bg-azure rounded-xs border-azure margin-top-15']) ?>
                        <?= Html::a('Download PDF', ['download/surat-pernyataan', 'id' => $model['paket']->id], ['class' => 'btn bg-rose rounded-xs border-rose margin-top-15 margin-left-10']) ?>
                    </div>
                    <div class="tab-pane padding-top-15 fade" id="tab-surat_kso">
                        <?= $this->render('one/prakualifikasi/_one-surat_kso', ['model' => $model]); ?>
                        <?= Html::a('Edit', ['paket/update-surat-kso', 'id' => $model['paket']->id], ['class' => 'btn btn-default bg-azure rounded-xs border-azure margin-top-15']) ?>
                        <?= Html::a('Download PDF', ['download/surat-kso', 'id' => $model['paket']->id], ['class' => 'btn bg-rose rounded-xs border-rose margin-top-15 margin-left-10']) ?>
                    </div>
                    <div class="tab-pane padding-top-15 fade" id="tab-tenaga_ahli">
                        <?= $this->render('one/prakualifikasi/_one-tenaga_ahli', ['model' => $model]); ?>
                        <?= Html::a('Edit', ['paket/update-tenaga-ahli', 'id' => $model['paket']->id], ['class' => 'btn btn-default bg-azure rounded-xs border-azure margin-top-15']) ?>
                        <?= Html::a('Download PDF', ['download/tenaga-ahli', 'id' => $model['paket']->id], ['class' => 'btn bg-rose rounded-xs border-rose margin-top-15 margin-left-10']) ?>
                    </div>
                    <div class="tab-pane padding-top-15 fade" id="tab-peralatan">
                        <?= $this->render('one/prakualifikasi/_one-peralatan', ['model' => $model]); ?>
                        <?= Html::a('Edit', ['paket/update-peralatan', 'id' => $model['paket']->id], ['class' => 'btn btn-default bg-azure rounded-xs border-azure margin-top-15']) ?>
                        <?= Html::a('Download PDF', ['download/peralatan', 'id' => $model['paket']->id], ['class' => 'btn bg-rose rounded-xs border-rose margin-top-15 margin-left-10']) ?>
                    </div>
                    <div class="tab-pane padding-top-15 fade" id="tab-pengalaman_10_tahun">
                        <?= $this->render('one/prakualifikasi/_one-pengalaman_10_tahun', ['model' => $model]); ?>
                        <?= Html::a('Edit', ['paket/update-pengalaman-10-tahun', 'id' => $model['paket']->id], ['class' => 'btn btn-default bg-azure rounded-xs border-azure margin-top-15']) ?>
                        <?= Html::a('Download PDF', ['download/pengalaman-10-tahun', 'id' => $model['paket']->id], ['class' => 'btn bg-rose rounded-xs border-rose margin-top-15 margin-left-10']) ?>
                    </div>
                    <div class="tab-pane padding-top-15 fade" id="tab-pengalaman_sejenis">
                        <?= $this->render('one/prakualifikasi/_one-pengalaman_sejenis', ['model' => $model]); ?>
                        <?= Html::a('Edit', ['paket/update-pengalaman-sejenis', 'id' => $model['paket']->id], ['class' => 'btn btn-default bg-azure rounded-xs border-azure margin-top-15']) ?>
                        <?= Html::a('Download PDF', ['download/pengalaman-sejenis', 'id' => $model['paket']->id], ['class' => 'btn bg-rose rounded-xs border-rose margin-top-15 margin-left-10']) ?>
                    </div>
                    <div class="tab-pane padding-top-15 fade" id="tab-pengalaman_4_tahun">
                        <?= $this->render('one/prakualifikasi/_one-pengalaman_4_tahun', ['model' => $model]); ?>
                        <?= Html::a('Edit', ['paket/update-pengalaman-4-tahun', 'id' => $model['paket']->id], ['class' => 'btn btn-default bg-azure rounded-xs border-azure margin-top-15']) ?>
                        <?= Html::a('Download PDF', ['download/pengalaman-4-tahun', 'id' => $model['paket']->id], ['class' => 'btn bg-rose rounded-xs border-rose margin-top-15 margin-left-10']) ?>
                    </div>
                </div>
            </div>
            <div class="tab-pane padding-top-15 margin-left-50 fade" id="tab-tidak_shortlist">
                
            </div>
            <div class="tab-pane padding-top-15 margin-left-50 fade" id="tab-proposal">
                <ul class="nav nav-pills">
                    <li class="active"><a href="#tab-surat_kuasa" data-toggle="tab">Surat Kuasa</a></li>
                    <li><a href="#tab-penawaran_administrasi_dan_teknis_kerjaan" data-toggle="tab">Penawaran Administrasi & Teknis Pekerjaan</a></li>
                    <li><a href="#tab-penawaran_biaya_paket_pekerjaan" data-toggle="tab">Penawaran Biaya Paket Pekerjaan</a></li>
                    <li><a href="#tab-rab" data-toggle="tab">RAB</a></li>
                    <li><a href="#tab-jadwal_pelaksanaan_pekerjaan" data-toggle="tab">Jadwal Pelaksanaan Pekerjaan</a></li>
                    <li><a href="#tab-pds" data-toggle="tab">PDS</a></li>
                    <li><a href="#tab-cv_tenaga_ahli" data-toggle="tab">CV Tenaga Ahli</a></li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane padding-top-15 fade active in" id="tab-surat_kuasa">
                        <?= $this->render('one/proposal/_one-surat_kuasa', ['model' => $model]); ?>
                        <?= Html::a('Edit', ['paket/update-surat-kuasa', 'id' => $model['paket']->id], ['class' => 'btn btn-default bg-azure rounded-xs border-azure margin-top-15']) ?>
                        <?= Html::a('Download PDF', ['download/surat-kuasa', 'id' => $model['paket']->id], ['class' => 'btn bg-rose rounded-xs border-rose margin-top-15 margin-left-10']) ?>
                    </div>
                    <div class="tab-pane padding-top-15 fade" id="tab-penawaran_administrasi_dan_teknis_kerjaan">
                        <?= $this->render('one/proposal/_one-penawaran_administrasi_dan_teknis_kerjaan', ['model' => $model]); ?>
                        <?= Html::a('Edit', ['paket/update-penawaran-administrasi-dan-teknis-kerjaan', 'id' => $model['paket']->id], ['class' => 'btn btn-default bg-azure rounded-xs border-azure margin-top-15']) ?>
                        <?= Html::a('Download PDF', ['download/penawaran-administrasi-dan-teknis-kerjaan', 'id' => $model['paket']->id], ['class' => 'btn bg-rose rounded-xs border-rose margin-top-15 margin-left-10']) ?>
                    </div>
                    <div class="tab-pane padding-top-15 fade" id="tab-penawaran_biaya_paket_pekerjaan">
                        <?= $this->render('one/proposal/_one-penawaran_biaya_paket_pekerjaan', ['model' => $model]); ?>
                        <?= Html::a('Edit', ['paket/update-penawaran-biaya-paket-pekerjaan', 'id' => $model['paket']->id], ['class' => 'btn btn-default bg-azure rounded-xs border-azure margin-top-15']) ?>
                        <?= Html::a('Download PDF', ['download/penawaran-biaya-paket-pekerjaan', 'id' => $model['paket']->id], ['class' => 'btn bg-rose rounded-xs border-rose margin-top-15 margin-left-10']) ?>
                    </div>
                    <div class="tab-pane padding-top-15 fade" id="tab-rab">
                        <?= $this->render('/rab/rab', ['paket_id' => $model['paket']->id, 'model' => $model]); ?>
                    </div>
                    <div class="tab-pane padding-top-15 fade" id="tab-jadwal_pelaksanaan_pekerjaan">
                        <?= $this->render('/timeline/timeline', [
                            'paket_id' => $model['paket']->id,
                            "period_length" => 24,
                        ]); ?>
                    </div>
                    <div class="tab-pane padding-top-15 fade" id="tab-pds">
                        <?= $this->render('one/proposal/_one-pds', ['model' => $model]); ?>
                        <?= Html::a('Edit', ['paket/update-pds', 'id' => $model['paket']->id], ['class' => 'btn btn-default bg-azure rounded-xs border-azure margin-top-15']) ?>
                        <?= Html::a('Download PDF', ['download/pds', 'id' => $model['paket']->id], ['class' => 'btn bg-rose rounded-xs border-rose margin-top-15 margin-left-10']) ?>
                    </div>
                    <div class="tab-pane padding-top-15 fade" id="tab-cv_tenaga_ahli">
                        <?= $this->render('one/proposal/_one-cv_tenaga_ahli', ['model' => $model]); ?>
                        <?= Html::a('Edit', ['paket/update-cv-tenaga-ahli', 'id' => $model['paket']->id], ['class' => 'btn btn-default bg-azure rounded-xs border-azure margin-top-15']) ?>
                        <?= Html::a('Download PDF', ['download/cv-tenaga-ahli', 'id' => $model['paket']->id], ['class' => 'btn bg-rose rounded-xs border-rose margin-top-15 margin-left-10']) ?>
                    </div>
                </div>
            </div>
            <div class="tab-pane padding-top-15 margin-left-50 fade" id="tab-kontrak">
                <ul class="nav nav-pills">
                    <li class="active"><a href="#tab-data_kontrak" data-toggle="tab">Data Kontrak</a></li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane padding-top-15 fade active in" id="tab-data_kontrak">
                        <?= $this->render('one/kontrak/_one-data_kontrak', ['model' => $model]); ?>
                        <?= Html::a('Edit', ['paket/update-data-kontrak', 'id' => $model['paket']->id], ['class' => 'btn btn-default bg-azure rounded-xs border-azure margin-top-15']) ?>
                        <?= Html::a('Download PDF', ['download/data-kontrak', 'id' => $model['paket']->id], ['class' => 'btn bg-rose rounded-xs border-rose margin-top-15 margin-left-10']) ?>
                    </div>
                </div>
            </div>
            <div class="tab-pane padding-top-15 margin-left-50 fade" id="tab-menang">
                
            </div>
            <div class="tab-pane padding-top-15 margin-left-50 fade" id="tab-kalah">
                
            </div>
            <div class="tab-pane padding-top-15 margin-left-50 fade" id="tab-mundur">
                
            </div>
        </div>
    </div>
    
    <hr class="margin-y-15">
</div>