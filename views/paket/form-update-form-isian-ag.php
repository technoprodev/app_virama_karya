<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

if (isset($title)) $this->title = $title;
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;

//
$paketFormKomisarises = [];
if (isset($model['paket_form_komisaris']))
    foreach ($model['paket_form_komisaris'] as $key => $paketFormKomisaris)
        $paketFormKomisarises[] = $paketFormKomisaris->attributes;
$paketFormDireksis = [];
if (isset($model['paket_form_direksi']))
    foreach ($model['paket_form_direksi'] as $key => $paketFormDireksi)
        $paketFormDireksis[] = $paketFormDireksi->attributes;
$paketFormSbuks = [];
if (isset($model['paket_form_sbuk']))
    foreach ($model['paket_form_sbuk'] as $key => $paketFormSbuk)
        $paketFormSbuks[] = $paketFormSbuk->attributes;
$paketFormSbunks = [];
if (isset($model['paket_form_sbunk']))
    foreach ($model['paket_form_sbunk'] as $key => $paketFormSbunk)
        $paketFormSbunks[] = $paketFormSbunk->attributes;
$paketFormIjinLainnyas = [];
if (isset($model['paket_form_ijin_lainnya']))
    foreach ($model['paket_form_ijin_lainnya'] as $key => $paketFormIjinLainnya)
        $paketFormIjinLainnyas[] = $paketFormIjinLainnya->attributes;
$paketFormPemilikSahams = [];
if (isset($model['paket_form_pemilik_saham']))
    foreach ($model['paket_form_pemilik_saham'] as $key => $paketFormPemilikSaham)
        $paketFormPemilikSahams[] = $paketFormPemilikSaham->attributes;
$paketFormPajaks = [];
if (isset($model['paket_form_pajak']))
    foreach ($model['paket_form_pajak'] as $key => $paketFormPajak)
        $paketFormPajaks[] = $paketFormPajak->attributes;
$paketFormBuktiPajaks = [];
if (isset($model['paket_form_bukti_pajak']))
    foreach ($model['paket_form_bukti_pajak'] as $key => $paketFormBuktiPajak)
        $paketFormBuktiPajaks[] = $paketFormBuktiPajak->attributes;

$this->registerJs(
    'vm.$data.paket_form.paketFormKomisarises = vm.$data.paket_form.paketFormKomisarises.concat(' . json_encode($paketFormKomisarises) . ');' .
    'vm.$data.paket_form.paketFormDireksis = vm.$data.paket_form.paketFormDireksis.concat(' . json_encode($paketFormDireksis) . ');' .
    'vm.$data.paket_form.paketFormSbuks = vm.$data.paket_form.paketFormSbuks.concat(' . json_encode($paketFormSbuks) . ');' .
    'vm.$data.paket_form.paketFormSbunks = vm.$data.paket_form.paketFormSbunks.concat(' . json_encode($paketFormSbunks) . ');' .
    'vm.$data.paket_form.paketFormIjinLainnyas = vm.$data.paket_form.paketFormIjinLainnyas.concat(' . json_encode($paketFormIjinLainnyas) . ');' .
    'vm.$data.paket_form.paketFormPemilikSahams = vm.$data.paket_form.paketFormPemilikSahams.concat(' . json_encode($paketFormPemilikSahams) . ');' .
    'vm.$data.paket_form.paketFormPajaks = vm.$data.paket_form.paketFormPajaks.concat(' . json_encode($paketFormPajaks) . ');' .
    'vm.$data.paket_form.paketFormBuktiPajaks = vm.$data.paket_form.paketFormBuktiPajaks.concat(' . json_encode($paketFormBuktiPajaks) . ');' .
    '',
    3
);

$error = false;
$errorMessage = '';
$errorVue = false;
if ($model['paket_form']->hasErrors()) {
    $error = true; 
    $errorMessage .= Html::errorSummary($model['paket_form'], ['class' => '']);
}

if (isset($model['paket_form_komisaris'])) foreach ($model['paket_form_komisaris'] as $key => $paketFormKomisaris) {
    if ($paketFormKomisaris->hasErrors()) {
        $error = true;
        $errorMessage .= Html::errorSummary($paketFormKomisaris, ['class' => '']);
        $errorVue = true; 
    }
}
if (isset($model['paket_form_direksi'])) foreach ($model['paket_form_direksi'] as $key => $paketFormDireksi) {
    if ($paketFormDireksi->hasErrors()) {
        $error = true;
        $errorMessage .= Html::errorSummary($paketFormDireksi, ['class' => '']);
        $errorVue = true; 
    }
}
if (isset($model['paket_form_sbuk'])) foreach ($model['paket_form_sbuk'] as $key => $paketFormSbuk) {
    if ($paketFormSbuk->hasErrors()) {
        $error = true;
        $errorMessage .= Html::errorSummary($paketFormSbuk, ['class' => '']);
        $errorVue = true; 
    }
}
if (isset($model['paket_form_sbunk'])) foreach ($model['paket_form_sbunk'] as $key => $paketFormSbunk) {
    if ($paketFormSbunk->hasErrors()) {
        $error = true;
        $errorMessage .= Html::errorSummary($paketFormSbunk, ['class' => '']);
        $errorVue = true; 
    }
}
if (isset($model['paket_form_ijin_lainnya'])) foreach ($model['paket_form_ijin_lainnya'] as $key => $paketFormIjinLainnya) {
    if ($paketFormIjinLainnya->hasErrors()) {
        $error = true;
        $errorMessage .= Html::errorSummary($paketFormIjinLainnya, ['class' => '']);
        $errorVue = true; 
    }
}
if (isset($model['paket_form_pemilik_saham'])) foreach ($model['paket_form_pemilik_saham'] as $key => $paketFormPemilikSaham) {
    if ($paketFormPemilikSaham->hasErrors()) {
        $error = true;
        $errorMessage .= Html::errorSummary($paketFormPemilikSaham, ['class' => '']);
        $errorVue = true; 
    }
}
if (isset($model['paket_form_pajak'])) foreach ($model['paket_form_pajak'] as $key => $paketFormPajak) {
    if ($paketFormPajak->hasErrors()) {
        $error = true;
        $errorMessage .= Html::errorSummary($paketFormPajak, ['class' => '']);
        $errorVue = true; 
    }
}
if (isset($model['paket_form_bukti_pajak'])) foreach ($model['paket_form_bukti_pajak'] as $key => $paketFormBuktiPajak) {
    if ($paketFormBuktiPajak->hasErrors()) {
        $error = true;
        $errorMessage .= Html::errorSummary($paketFormBuktiPajak, ['class' => '']);
        $errorVue = true; 
    }
}
if ($errorVue) {
    $this->registerJs(
        '$.each($("#app").data("yiiActiveForm").attributes, function() {
            this.status = 3;
        });
        $("#app").yiiActiveForm("validate");',
        5
    );
}
?>

<?php if (!Yii::$app->request->isAjax) : ?>
<div class="box box-break-sm margin-left-30 m-margin-left-0">
    <div class="box-11">
<?php endif; ?>

<h6 class="text-rose padding-bottom-5 border-bottom f-italic">Formulir Isian (A-G)</h6>

<?php $form = ActiveForm::begin(['enableClientValidation' => true, 'options' => ['id' => 'app']]); ?>
  
    <?php if ($error) : ?>
        <div class="alert alert-danger">
            <?= $errorMessage ?>
        </div>
    <?php endif; ?>

<div class="border-azure margin-bottom-30">
    <div class="bg-azure padding-x-15 padding-y-5">
        <span class="fs-16">Landasan Hukum Pendirian Badan Usaha</span>
    </div>
    <div class="padding-x-15 padding-y-10">
        <h6 class="text-rose padding-bottom-5 border-bottom f-italic">Akta Pendirian Perusahaan</h6>

        <?= $form->field($model['paket_form'], 'app_nomor_akta', ['options' => ['class' => 'form-group form-group-sm box box-break-sm margin-bottom-10']])->begin(); ?>
            <div class="box-2 padding-x-0 text-right m-text-left padding-y-5">
                <?= Html::activeLabel($model['paket_form'], 'app_nomor_akta', ['class' => 'control-label']); ?>
            </div>
            <div class="box-10 m-padding-x-0">
                <?= Html::activeTextInput($model['paket_form'], 'app_nomor_akta', ['class' => 'form-control']) ?>
                <?= Html::error($model['paket_form'], 'app_nomor_akta', ['class' => 'help-block']); ?>
            </div>
        <?= $form->field($model['paket_form'], 'app_nomor_akta')->end(); ?>

        <?= $form->field($model['paket_form'], 'app_tanggal', ['options' => ['class' => 'form-group form-group-sm box box-break-sm margin-bottom-10']])->begin(); ?>
            <div class="box-2 padding-x-0 text-right m-text-left padding-y-5">
                <?= Html::activeLabel($model['paket_form'], 'app_tanggal', ['class' => 'control-label']); ?>
            </div>
            <div class="box-10 m-padding-x-0">
                <?= Html::activeTextInput($model['paket_form'], 'app_tanggal', ['class' => 'form-control']) ?>
                <?= Html::error($model['paket_form'], 'app_tanggal', ['class' => 'help-block']); ?>
            </div>
        <?= $form->field($model['paket_form'], 'app_tanggal')->end(); ?>

        <?= $form->field($model['paket_form'], 'app_nama_notaris', ['options' => ['class' => 'form-group form-group-sm box box-break-sm margin-bottom-10']])->begin(); ?>
            <div class="box-2 padding-x-0 text-right m-text-left padding-y-5">
                <?= Html::activeLabel($model['paket_form'], 'app_nama_notaris', ['class' => 'control-label']); ?>
            </div>
            <div class="box-10 m-padding-x-0">
                <?= Html::activeTextInput($model['paket_form'], 'app_nama_notaris', ['class' => 'form-control', 'maxlength' => true]) ?>
                <?= Html::error($model['paket_form'], 'app_nama_notaris', ['class' => 'help-block']); ?>
            </div>
        <?= $form->field($model['paket_form'], 'app_nama_notaris')->end(); ?>

        <?= $form->field($model['paket_form'], 'app_nomor_pengesahan', ['options' => ['class' => 'form-group form-group-sm box box-break-sm margin-bottom-10']])->begin(); ?>
            <div class="box-2 padding-x-0 text-right m-text-left padding-y-5">
                <?= Html::activeLabel($model['paket_form'], 'app_nomor_pengesahan', ['class' => 'control-label']); ?>
            </div>
            <div class="box-10 m-padding-x-0">
                <?= Html::activeTextInput($model['paket_form'], 'app_nomor_pengesahan', ['class' => 'form-control', 'maxlength' => true]) ?>
                <?= Html::error($model['paket_form'], 'app_nomor_pengesahan', ['class' => 'help-block']); ?>
            </div>
        <?= $form->field($model['paket_form'], 'app_nomor_pengesahan')->end(); ?>

        <?= $form->field($model['paket_form'], 'app_tanggal_pengesahan', ['options' => ['class' => 'form-group form-group-sm box box-break-sm margin-bottom-10']])->begin(); ?>
            <div class="box-2 padding-x-0 text-right m-text-left padding-y-5">
                <?= Html::activeLabel($model['paket_form'], 'app_tanggal_pengesahan', ['class' => 'control-label']); ?>
            </div>
            <div class="box-10 m-padding-x-0">
                <?= Html::activeTextInput($model['paket_form'], 'app_tanggal_pengesahan', ['class' => 'form-control']) ?>
                <?= Html::error($model['paket_form'], 'app_tanggal_pengesahan', ['class' => 'help-block']); ?>
            </div>
        <?= $form->field($model['paket_form'], 'app_tanggal_pengesahan')->end(); ?>

        <h6 class="text-rose padding-bottom-5 border-bottom f-italic">Akta Perubahan Terakhir</h6>

        <?= $form->field($model['paket_form'], 'apt_nomor_akta', ['options' => ['class' => 'form-group form-group-sm box box-break-sm margin-bottom-10']])->begin(); ?>
            <div class="box-2 padding-x-0 text-right m-text-left padding-y-5">
                <?= Html::activeLabel($model['paket_form'], 'apt_nomor_akta', ['class' => 'control-label']); ?>
            </div>
            <div class="box-10 m-padding-x-0">
                <?= Html::activeTextInput($model['paket_form'], 'apt_nomor_akta', ['class' => 'form-control']) ?>
                <?= Html::error($model['paket_form'], 'apt_nomor_akta', ['class' => 'help-block']); ?>
            </div>
        <?= $form->field($model['paket_form'], 'apt_nomor_akta')->end(); ?>

        <?= $form->field($model['paket_form'], 'apt_tanggal', ['options' => ['class' => 'form-group form-group-sm box box-break-sm margin-bottom-10']])->begin(); ?>
            <div class="box-2 padding-x-0 text-right m-text-left padding-y-5">
                <?= Html::activeLabel($model['paket_form'], 'apt_tanggal', ['class' => 'control-label']); ?>
            </div>
            <div class="box-10 m-padding-x-0">
                <?= Html::activeTextInput($model['paket_form'], 'apt_tanggal', ['class' => 'form-control']) ?>
                <?= Html::error($model['paket_form'], 'apt_tanggal', ['class' => 'help-block']); ?>
            </div>
        <?= $form->field($model['paket_form'], 'apt_tanggal')->end(); ?>

        <?= $form->field($model['paket_form'], 'apt_nama_notaris', ['options' => ['class' => 'form-group form-group-sm box box-break-sm margin-bottom-10']])->begin(); ?>
            <div class="box-2 padding-x-0 text-right m-text-left padding-y-5">
                <?= Html::activeLabel($model['paket_form'], 'apt_nama_notaris', ['class' => 'control-label']); ?>
            </div>
            <div class="box-10 m-padding-x-0">
                <?= Html::activeTextInput($model['paket_form'], 'apt_nama_notaris', ['class' => 'form-control', 'maxlength' => true]) ?>
                <?= Html::error($model['paket_form'], 'apt_nama_notaris', ['class' => 'help-block']); ?>
            </div>
        <?= $form->field($model['paket_form'], 'apt_nama_notaris')->end(); ?>

        <?= $form->field($model['paket_form'], 'apt_nomor_pengesahan', ['options' => ['class' => 'form-group form-group-sm box box-break-sm margin-bottom-10']])->begin(); ?>
            <div class="box-2 padding-x-0 text-right m-text-left padding-y-5">
                <?= Html::activeLabel($model['paket_form'], 'apt_nomor_pengesahan', ['class' => 'control-label']); ?>
            </div>
            <div class="box-10 m-padding-x-0">
                <?= Html::activeTextInput($model['paket_form'], 'apt_nomor_pengesahan', ['class' => 'form-control', 'maxlength' => true]) ?>
                <?= Html::error($model['paket_form'], 'apt_nomor_pengesahan', ['class' => 'help-block']); ?>
            </div>
        <?= $form->field($model['paket_form'], 'apt_nomor_pengesahan')->end(); ?>

        <?= $form->field($model['paket_form'], 'apt_tanggal_pengesahan', ['options' => ['class' => 'form-group form-group-sm box box-break-sm margin-bottom-10']])->begin(); ?>
            <div class="box-2 padding-x-0 text-right m-text-left padding-y-5">
                <?= Html::activeLabel($model['paket_form'], 'apt_tanggal_pengesahan', ['class' => 'control-label']); ?>
            </div>
            <div class="box-10 m-padding-x-0">
                <?= Html::activeTextInput($model['paket_form'], 'apt_tanggal_pengesahan', ['class' => 'form-control']) ?>
                <?= Html::error($model['paket_form'], 'apt_tanggal_pengesahan', ['class' => 'help-block']); ?>
            </div>
        <?= $form->field($model['paket_form'], 'apt_tanggal_pengesahan')->end(); ?>
    </div>
</div>

<div class="border-azure margin-bottom-30">
    <div class="bg-azure padding-x-15 padding-y-5">
        <span class="fs-16">Pengurus Badan Usaha</span>
    </div>
    <div class="padding-x-15 padding-y-10">
        <h6 class="text-rose padding-bottom-5 border-bottom f-italic">Komisaris</h6>
        
        <?php if (isset($model['paket_form_komisaris'])) foreach ($model['paket_form_komisaris'] as $key => $value): ?>
            <?= $form->field($model['paket_form_komisaris'][$key], "[$key]nama")->begin(); ?>
            <?= $form->field($model['paket_form_komisaris'][$key], "[$key]nama")->end(); ?>
            <?= $form->field($model['paket_form_komisaris'][$key], "[$key]nomor_ktp")->begin(); ?>
            <?= $form->field($model['paket_form_komisaris'][$key], "[$key]nomor_ktp")->end(); ?>
            <?= $form->field($model['paket_form_komisaris'][$key], "[$key]jabatan")->begin(); ?>
            <?= $form->field($model['paket_form_komisaris'][$key], "[$key]jabatan")->end(); ?>
        <?php endforeach; ?>

        <template v-if="typeof paket_form.paketFormKomisarises == 'object'">
            <div class="box box-break-sm margin-bottom-5 hidden-sm-less">
                <div class="box-1 padding-x-0 padding-y-5 text-center">
                    No
                </div>
                <div class="box-4 padding-x-0 padding-y-5 text-center">
                    Nama
                </div>
                <div class="box-3 padding-x-0 padding-y-5 text-center">
                    No. KTP
                </div>
                <div class="box-3 padding-x-0 padding-y-5 text-center">
                    Jabatan
                </div>
                <div class="box-1 padding-x-0">
                    <a v-on:click="addPaketFormKomisaris" class="btn btn-default btn-sm margin-left-5 bg-azure hover-pointer"><i class="fa fa-plus"></i></a>
                </div>
            </div>
            <template v-for="(value, key, index) in paket_form.paketFormKomisarises">
                <div v-show="!(value.id < 0)">
                    <div class="box box-break-sm">
                        <div class="box-1 padding-x-0 padding-y-5 text-center m-text-left">
                            <input type="hidden" v-bind:id="'paketformkomisaris-' + key + '-id'" v-bind:name="'PaketFormKomisaris[' + key + '][id]'" class="form-control" type="text" v-model="paket_form.paketFormKomisarises[key].id">
                            {{key + 1}}
                        </div>
                        <div class="box-4 padding-x-0">
                            <div v-bind:class="'form-group form-group-sm field-paketformkomisaris-' + key + '-nama'">
                                <input placeholder="nama" v-bind:id="'paketformkomisaris-' + key + '-nama'" v-bind:name="'PaketFormKomisaris[' + key + '][nama]'" class="form-control" type="text" v-model="paket_form.paketFormKomisarises[key].nama">
                                <div class="help-block"></div>
                            </div>
                        </div>
                        <div class="box-3 padding-x-0">
                            <div v-bind:class="'form-group form-group-sm field-paketformkomisaris-' + key + '-nomor_ktp'">
                                <input placeholder="nomor_ktp" v-bind:id="'paketformkomisaris-' + key + '-nomor_ktp'" v-bind:name="'PaketFormKomisaris[' + key + '][nomor_ktp]'" class="form-control" type="text" v-model="paket_form.paketFormKomisarises[key].nomor_ktp">
                                <div class="help-block"></div>
                            </div>
                        </div>
                        <div class="box-3 padding-x-0">
                            <div v-bind:class="'form-group form-group-sm field-paketformkomisaris-' + key + '-jabatan'">
                                <select v-bind:id="'paketformkomisaris-' + key + '-jabatan'" v-bind:name="'PaketFormKomisaris[' + key + '][jabatan]'" class="form-control" v-model="paket_form.paketFormKomisarises[key].jabatan">
                                    <option value="">Pilih jabatan</option>
                                    <?php foreach ((new \app_virama_karya\models\FormKomisaris())->getEnum('jabatan') as $key => $value) : ?>
                                        <option value="<?= $value ?>"><?= $value ?></option>
                                    <?php endforeach; ?>
                                </select>
                                <div class="help-block"></div>
                            </div>
                        </div>
                        <div class="box-1 padding-x-0">
                            <div v-on:click="removePaketFormKomisaris(key)" class="btn btn-default btn-sm margin-left-5 bg-light-red hover-pointer"><i class="fa fa-close"></i></div>
                        </div>
                    </div>
                </div>
            </template>
        </template>

        <h6 class="text-rose padding-bottom-5 border-bottom f-italic">Direksi</h6>

        <?php if (isset($model['paket_form_direksi'])) foreach ($model['paket_form_direksi'] as $key => $value): ?>
            <?= $form->field($model['paket_form_direksi'][$key], "[$key]nama")->begin(); ?>
            <?= $form->field($model['paket_form_direksi'][$key], "[$key]nama")->end(); ?>
            <?= $form->field($model['paket_form_direksi'][$key], "[$key]nomor_ktp")->begin(); ?>
            <?= $form->field($model['paket_form_direksi'][$key], "[$key]nomor_ktp")->end(); ?>
            <?= $form->field($model['paket_form_direksi'][$key], "[$key]jabatan")->begin(); ?>
            <?= $form->field($model['paket_form_direksi'][$key], "[$key]jabatan")->end(); ?>
        <?php endforeach; ?>

        <template v-if="typeof paket_form.paketFormDireksis == 'object'">
            <div class="box box-break-sm margin-bottom-5 hidden-sm-less">
                <div class="box-1 padding-x-0 padding-y-5 text-center">
                    No
                </div>
                <div class="box-4 padding-x-0 padding-y-5 text-center">
                    Nama
                </div>
                <div class="box-3 padding-x-0 padding-y-5 text-center">
                    No. KTP
                </div>
                <div class="box-3 padding-x-0 padding-y-5 text-center">
                    Jabatan
                </div>
                <div class="box-1 padding-x-0">
                    <a v-on:click="addPaketFormDireksi" class="btn btn-default btn-sm margin-left-5 bg-azure hover-pointer"><i class="fa fa-plus"></i></a>
                </div>
            </div>
            <template v-for="(value, key, index) in paket_form.paketFormDireksis">
                <div v-show="!(value.id < 0)">
                    <div class="box box-break-sm">
                        <div class="box-1 padding-x-0 padding-y-5 text-center m-text-left">
                            <input type="hidden" v-bind:id="'paketformdireksi-' + key + '-id'" v-bind:name="'PaketFormDireksi[' + key + '][id]'" class="form-control" type="text" v-model="paket_form.paketFormDireksis[key].id">
                            {{key + 1}}
                        </div>
                        <div class="box-4 padding-x-0">
                            <div v-bind:class="'form-group form-group-sm field-paketformdireksi-' + key + '-nama'">
                                <input placeholder="nama" v-bind:id="'paketformdireksi-' + key + '-nama'" v-bind:name="'PaketFormDireksi[' + key + '][nama]'" class="form-control" type="text" v-model="paket_form.paketFormDireksis[key].nama">
                                <div class="help-block"></div>
                            </div>
                        </div>
                        <div class="box-3 padding-x-0">
                            <div v-bind:class="'form-group form-group-sm field-paketformdireksi-' + key + '-nomor_ktp'">
                                <input placeholder="nomor_ktp" v-bind:id="'paketformdireksi-' + key + '-nomor_ktp'" v-bind:name="'PaketFormDireksi[' + key + '][nomor_ktp]'" class="form-control" type="text" v-model="paket_form.paketFormDireksis[key].nomor_ktp">
                                <div class="help-block"></div>
                            </div>
                        </div>
                        <div class="box-3 padding-x-0">
                            <div v-bind:class="'form-group form-group-sm field-paketformdireksi-' + key + '-jabatan'">
                                <select v-bind:id="'paketformdireksi-' + key + '-jabatan'" v-bind:name="'PaketFormDireksi[' + key + '][jabatan]'" class="form-control" v-model="paket_form.paketFormDireksis[key].jabatan">
                                    <option value="">Pilih jabatan</option>
                                    <?php foreach ((new \app_virama_karya\models\FormDireksi())->getEnum('jabatan') as $key => $value) : ?>
                                        <option value="<?= $value ?>"><?= $value ?></option>
                                    <?php endforeach; ?>
                                </select>
                                <div class="help-block"></div>
                            </div>
                        </div>
                        <div class="box-1 padding-x-0">
                            <div v-on:click="removePaketFormDireksi(key)" class="btn btn-default btn-sm margin-left-5 bg-light-red hover-pointer"><i class="fa fa-close"></i></div>
                        </div>
                    </div>
                </div>
            </template>
        </template>
    </div>
</div>

<div class="border-azure margin-bottom-30">
    <div class="bg-azure padding-x-15 padding-y-5">
        <span class="fs-16">Ijin Usaha</span>
    </div>
    <div class="padding-x-15 padding-y-10">
        <h6 class="text-rose padding-bottom-5 border-bottom f-italic">Ijin Usaha Jasa Konstruksi/Konsultan (IUJK)</h6>

        <?= $form->field($model['paket_form'], 'iujk_berlaku_mulai', ['options' => ['class' => 'form-group form-group-sm box box-break-sm margin-bottom-10']])->begin(); ?>
            <div class="box-2 padding-x-0 text-right m-text-left padding-y-5">
                <?= Html::activeLabel($model['paket_form'], 'iujk_berlaku_mulai', ['class' => 'control-label']); ?>
            </div>
            <div class="box-10 m-padding-x-0">
                <?= Html::activeTextInput($model['paket_form'], 'iujk_berlaku_mulai', ['class' => 'form-control']) ?>
                <?= Html::error($model['paket_form'], 'iujk_berlaku_mulai', ['class' => 'help-block']); ?>
            </div>
        <?= $form->field($model['paket_form'], 'iujk_berlaku_mulai')->end(); ?>

        <?= $form->field($model['paket_form'], 'iujk_berlaku_sampai', ['options' => ['class' => 'form-group form-group-sm box box-break-sm margin-bottom-10']])->begin(); ?>
            <div class="box-2 padding-x-0 text-right m-text-left padding-y-5">
                <?= Html::activeLabel($model['paket_form'], 'iujk_berlaku_sampai', ['class' => 'control-label']); ?>
            </div>
            <div class="box-10 m-padding-x-0">
                <?= Html::activeTextInput($model['paket_form'], 'iujk_berlaku_sampai', ['class' => 'form-control']) ?>
                <?= Html::error($model['paket_form'], 'iujk_berlaku_sampai', ['class' => 'help-block']); ?>
            </div>
        <?= $form->field($model['paket_form'], 'iujk_berlaku_sampai')->end(); ?>

        <?= $form->field($model['paket_form'], 'iujk_pemberi_izin', ['options' => ['class' => 'form-group form-group-sm box box-break-sm margin-bottom-10']])->begin(); ?>
            <div class="box-2 padding-x-0 text-right m-text-left padding-y-5">
                <?= Html::activeLabel($model['paket_form'], 'iujk_pemberi_izin', ['class' => 'control-label']); ?>
            </div>
            <div class="box-10 m-padding-x-0">
                <?= Html::activeTextInput($model['paket_form'], 'iujk_pemberi_izin', ['class' => 'form-control', 'maxlength' => true]) ?>
                <?= Html::error($model['paket_form'], 'iujk_pemberi_izin', ['class' => 'help-block']); ?>
            </div>
        <?= $form->field($model['paket_form'], 'iujk_pemberi_izin')->end(); ?>

        <?= $form->field($model['paket_form'], 'iujk_perencanaan', ['options' => ['class' => 'form-group form-group-sm box box-break-sm margin-bottom-10']])->begin(); ?>
            <div class="box-2 padding-x-0 text-right m-text-left padding-y-5">
                <?= Html::activeLabel($model['paket_form'], 'iujk_perencanaan', ['class' => 'control-label']); ?>
            </div>
            <div class="box-10 m-padding-x-0">
                <?= Html::activeTextInput($model['paket_form'], 'iujk_perencanaan', ['class' => 'form-control', 'maxlength' => true]) ?>
                <?= Html::error($model['paket_form'], 'iujk_perencanaan', ['class' => 'help-block']); ?>
            </div>
        <?= $form->field($model['paket_form'], 'iujk_perencanaan')->end(); ?>

        <?= $form->field($model['paket_form'], 'iujk_pengawasan', ['options' => ['class' => 'form-group form-group-sm box box-break-sm margin-bottom-10']])->begin(); ?>
            <div class="box-2 padding-x-0 text-right m-text-left padding-y-5">
                <?= Html::activeLabel($model['paket_form'], 'iujk_pengawasan', ['class' => 'control-label']); ?>
            </div>
            <div class="box-10 m-padding-x-0">
                <?= Html::activeTextInput($model['paket_form'], 'iujk_pengawasan', ['class' => 'form-control', 'maxlength' => true]) ?>
                <?= Html::error($model['paket_form'], 'iujk_pengawasan', ['class' => 'help-block']); ?>
            </div>
        <?= $form->field($model['paket_form'], 'iujk_pengawasan')->end(); ?>

        <?= $form->field($model['paket_form'], 'iujk_konsultansi', ['options' => ['class' => 'form-group form-group-sm box box-break-sm margin-bottom-10']])->begin(); ?>
            <div class="box-2 padding-x-0 text-right m-text-left padding-y-5">
                <?= Html::activeLabel($model['paket_form'], 'iujk_konsultansi', ['class' => 'control-label']); ?>
            </div>
            <div class="box-10 m-padding-x-0">
                <?= Html::activeTextInput($model['paket_form'], 'iujk_konsultansi', ['class' => 'form-control', 'maxlength' => true]) ?>
                <?= Html::error($model['paket_form'], 'iujk_konsultansi', ['class' => 'help-block']); ?>
            </div>
        <?= $form->field($model['paket_form'], 'iujk_konsultansi')->end(); ?>

        <h6 class="text-rose padding-bottom-5 border-bottom f-italic">Sertifikat Badan Usaha (SBU) Konstruksi</h6>

        <?= $form->field($model['paket_form'], 'sbuk_berlaku_mulai', ['options' => ['class' => 'form-group form-group-sm box box-break-sm margin-bottom-10']])->begin(); ?>
            <div class="box-2 padding-x-0 text-right m-text-left padding-y-5">
                <?= Html::activeLabel($model['paket_form'], 'sbuk_berlaku_mulai', ['class' => 'control-label']); ?>
            </div>
            <div class="box-10 m-padding-x-0">
                <?= Html::activeTextInput($model['paket_form'], 'sbuk_berlaku_mulai', ['class' => 'form-control']) ?>
                <?= Html::error($model['paket_form'], 'sbuk_berlaku_mulai', ['class' => 'help-block']); ?>
            </div>
        <?= $form->field($model['paket_form'], 'sbuk_berlaku_mulai')->end(); ?>

        <?= $form->field($model['paket_form'], 'sbuk_berlaku_sampai', ['options' => ['class' => 'form-group form-group-sm box box-break-sm margin-bottom-10']])->begin(); ?>
            <div class="box-2 padding-x-0 text-right m-text-left padding-y-5">
                <?= Html::activeLabel($model['paket_form'], 'sbuk_berlaku_sampai', ['class' => 'control-label']); ?>
            </div>
            <div class="box-10 m-padding-x-0">
                <?= Html::activeTextInput($model['paket_form'], 'sbuk_berlaku_sampai', ['class' => 'form-control']) ?>
                <?= Html::error($model['paket_form'], 'sbuk_berlaku_sampai', ['class' => 'help-block']); ?>
            </div>
        <?= $form->field($model['paket_form'], 'sbuk_berlaku_sampai')->end(); ?>

        <?= $form->field($model['paket_form'], 'sbuk_pemberi_izin', ['options' => ['class' => 'form-group form-group-sm box box-break-sm margin-bottom-10']])->begin(); ?>
            <div class="box-2 padding-x-0 text-right m-text-left padding-y-5">
                <?= Html::activeLabel($model['paket_form'], 'sbuk_pemberi_izin', ['class' => 'control-label']); ?>
            </div>
            <div class="box-10 m-padding-x-0">
                <?= Html::activeTextInput($model['paket_form'], 'sbuk_pemberi_izin', ['class' => 'form-control', 'maxlength' => true]) ?>
                <?= Html::error($model['paket_form'], 'sbuk_pemberi_izin', ['class' => 'help-block']); ?>
            </div>
        <?= $form->field($model['paket_form'], 'sbuk_pemberi_izin')->end(); ?>

        <?php if (isset($model['sbuk'])) foreach ($model['sbuk'] as $key => $value): ?>
            <?= $form->field($model['sbuk'][$key], "[$key]sertifikat")->begin(); ?>
            <?= $form->field($model['sbuk'][$key], "[$key]sertifikat")->end(); ?>
            <?= $form->field($model['sbuk'][$key], "[$key]nomor_sertifikat")->begin(); ?>
            <?= $form->field($model['sbuk'][$key], "[$key]nomor_sertifikat")->end(); ?>
        <?php endforeach; ?>

        <template v-if="typeof paket_form.paketFormSbuks == 'object'">
            <div class="box box-break-sm margin-bottom-5 hidden-sm-less">
                <div class="box-1 padding-x-0 padding-y-5 text-center">
                    No
                </div>
                <div class="box-3 padding-x-0 padding-y-5 text-center">
                    Sertifikat
                </div>
                <div class="box-7 padding-x-0 padding-y-5 text-center">
                    Nomor
                </div>
                <div class="box-1 padding-x-0">
                    <a v-on:click="addPaketFormSbuk" class="btn btn-default btn-sm margin-left-5 bg-azure hover-pointer"><i class="fa fa-plus"></i></a>
                </div>
            </div>
            <template v-for="(value, key, index) in paket_form.paketFormSbuks">
                <div v-show="!(value.id < 0)">
                    <div class="box box-break-sm">
                        <div class="box-1 padding-x-0 padding-y-5 text-center m-text-left">
                            <input type="hidden" v-bind:id="'paketformsbuk-' + key + '-id'" v-bind:name="'PaketFormSbuk[' + key + '][id]'" class="form-control" type="text" v-model="paket_form.paketFormSbuks[key].id">
                            {{key + 1}}
                        </div>
                        <div class="box-3 padding-x-0">
                            <div v-bind:class="'form-group form-group-sm field-paketformsbuk-' + key + '-sertifikat'">
                                <input placeholder="sertifikat" v-bind:id="'paketformsbuk-' + key + '-sertifikat'" v-bind:name="'PaketFormSbuk[' + key + '][sertifikat]'" class="form-control" type="text" v-model="paket_form.paketFormSbuks[key].sertifikat">
                                <div class="help-block"></div>
                            </div>
                        </div>
                        <div class="box-7 padding-x-0">
                            <div v-bind:class="'form-group form-group-sm field-paketformsbuk-' + key + '-nomor_sertifikat'">
                                <input placeholder="nomor_sertifikat" v-bind:id="'paketformsbuk-' + key + '-nomor_sertifikat'" v-bind:name="'PaketFormSbuk[' + key + '][nomor_sertifikat]'" class="form-control" type="text" v-model="paket_form.paketFormSbuks[key].nomor_sertifikat">
                                <div class="help-block"></div>
                            </div>
                        </div>
                        <div class="box-1 padding-x-0">
                            <div v-on:click="removePaketFormSbuk(key)" class="btn btn-default btn-sm margin-left-5 bg-light-red hover-pointer"><i class="fa fa-close"></i></div>
                        </div>
                    </div>
                </div>
            </template>
        </template>

        <h6 class="text-rose padding-bottom-5 border-bottom f-italic">Sertifikat Badan Usaha (SBU) Non Konstruksi</h6>

        <?php if (isset($model['sbunk'])) foreach ($model['sbunk'] as $key => $value): ?>
            <?= $form->field($model['sbunk'][$key], "[$key]sertifikat")->begin(); ?>
            <?= $form->field($model['sbunk'][$key], "[$key]sertifikat")->end(); ?>
            <?= $form->field($model['sbunk'][$key], "[$key]nomor_sertifikat")->begin(); ?>
            <?= $form->field($model['sbunk'][$key], "[$key]nomor_sertifikat")->end(); ?>
            <?= $form->field($model['sbunk'][$key], "[$key]berlaku_mulai")->begin(); ?>
            <?= $form->field($model['sbunk'][$key], "[$key]berlaku_mulai")->end(); ?>
            <?= $form->field($model['sbunk'][$key], "[$key]berlaku_sampai")->begin(); ?>
            <?= $form->field($model['sbunk'][$key], "[$key]berlaku_sampai")->end(); ?>
            <?= $form->field($model['sbunk'][$key], "[$key]pemberi_izin")->begin(); ?>
            <?= $form->field($model['sbunk'][$key], "[$key]pemberi_izin")->end(); ?>
        <?php endforeach; ?>

        <template v-if="typeof paket_form.paketFormSbunks == 'object'">
            <div class="box box-break-sm margin-bottom-5 hidden-sm-less">
                <div class="box-1 padding-x-0 padding-y-5 text-center">
                    No
                </div>
                <div class="box-4 padding-x-0 padding-y-5 text-center">
                    Sertifikat
                </div>
                <div class="box-2 padding-x-0 padding-y-5 text-center">
                    Nomor
                </div>
                <div class="box-2 padding-x-0 padding-y-5 text-center">
                    Pemberi Izin
                </div>
                <div class="box-2 padding-x-0 padding-y-5 text-center">
                    Berlaku
                </div>
                <div class="box-1 padding-x-0">
                    <a v-on:click="addPaketFormSbunk" class="btn btn-default btn-sm margin-left-5 bg-azure hover-pointer"><i class="fa fa-plus"></i></a>
                </div>
            </div>
            <template v-for="(value, key, index) in paket_form.paketFormSbunks">
                <div v-show="!(value.id < 0)">
                    <div class="box box-break-sm">
                        <div class="box-1 padding-x-0 padding-y-5 text-center m-text-left">
                            <input type="hidden" v-bind:id="'paketformsbunk-' + key + '-id'" v-bind:name="'PaketFormSbunk[' + key + '][id]'" class="form-control" type="text" v-model="paket_form.paketFormSbunks[key].id">
                            {{key + 1}}
                        </div>
                        <div class="box-4 padding-x-0">
                            <div v-bind:class="'form-group form-group-sm field-paketformsbunk-' + key + '-sertifikat'">
                                <input placeholder="sertifikat" v-bind:id="'paketformsbunk-' + key + '-sertifikat'" v-bind:name="'PaketFormSbunk[' + key + '][sertifikat]'" class="form-control" type="text" v-model="paket_form.paketFormSbunks[key].sertifikat">
                                <div class="help-block"></div>
                            </div>
                        </div>
                        <div class="box-2 padding-x-0">
                            <div v-bind:class="'form-group form-group-sm field-paketformsbunk-' + key + '-nomor_sertifikat'">
                                <input placeholder="nomor_sertifikat" v-bind:id="'paketformsbunk-' + key + '-nomor_sertifikat'" v-bind:name="'PaketFormSbunk[' + key + '][nomor_sertifikat]'" class="form-control" type="text" v-model="paket_form.paketFormSbunks[key].nomor_sertifikat">
                                <div class="help-block"></div>
                            </div>
                        </div>
                        <div class="box-2 padding-x-0">
                            <div v-bind:class="'form-group form-group-sm field-paketformsbunk-' + key + '-pemberi_izin'">
                                <input placeholder="pemberi_izin" v-bind:id="'paketformsbunk-' + key + '-pemberi_izin'" v-bind:name="'PaketFormSbunk[' + key + '][pemberi_izin]'" class="form-control" type="text" v-model="paket_form.paketFormSbunks[key].pemberi_izin">
                                <div class="help-block"></div>
                            </div>
                        </div>
                        <div class="box-1 padding-x-0">
                            <div v-bind:class="'form-group form-group-sm field-paketformsbunk-' + key + '-berlaku_mulai'">
                                <input placeholder="mulai" v-bind:id="'paketformsbunk-' + key + '-berlaku_mulai'" v-bind:name="'PaketFormSbunk[' + key + '][berlaku_mulai]'" class="form-control" type="text" v-model="paket_form.paketFormSbunks[key].berlaku_mulai">
                                <div class="help-block"></div>
                            </div>
                        </div>
                        <div class="box-1 padding-x-0">
                            <div v-bind:class="'form-group form-group-sm field-paketformsbunk-' + key + '-berlaku_sampai'">
                                <input placeholder="sampai" v-bind:id="'paketformsbunk-' + key + '-berlaku_sampai'" v-bind:name="'PaketFormSbunk[' + key + '][berlaku_sampai]'" class="form-control" type="text" v-model="paket_form.paketFormSbunks[key].berlaku_sampai">
                                <div class="help-block"></div>
                            </div>
                        </div>
                        <div class="box-1 padding-x-0">
                            <div v-on:click="removePaketFormSbunk(key)" class="btn btn-default btn-sm margin-left-5 bg-light-red hover-pointer"><i class="fa fa-close"></i></div>
                        </div>
                    </div>
                </div>
            </template>
        </template>
    </div>
</div>

<div class="border-azure margin-bottom-30">
    <div class="bg-azure padding-x-15 padding-y-5">
        <span class="fs-16">Ijin Usaha Lainnya</span>
    </div>
    <div class="padding-x-15 padding-y-10">
        <?php if (isset($model['ijin_lainnya'])) foreach ($model['ijin_lainnya'] as $key => $value): ?>
            <?= $form->field($model['ijin_lainnya'][$key], "[$key]sertifikat")->begin(); ?>
            <?= $form->field($model['ijin_lainnya'][$key], "[$key]sertifikat")->end(); ?>
            <?= $form->field($model['ijin_lainnya'][$key], "[$key]nomor_sertifikat")->begin(); ?>
            <?= $form->field($model['ijin_lainnya'][$key], "[$key]nomor_sertifikat")->end(); ?>
            <?= $form->field($model['ijin_lainnya'][$key], "[$key]berlaku_mulai")->begin(); ?>
            <?= $form->field($model['ijin_lainnya'][$key], "[$key]berlaku_mulai")->end(); ?>
            <?= $form->field($model['ijin_lainnya'][$key], "[$key]berlaku_sampai")->begin(); ?>
            <?= $form->field($model['ijin_lainnya'][$key], "[$key]berlaku_sampai")->end(); ?>
            <?= $form->field($model['ijin_lainnya'][$key], "[$key]pemberi_izin")->begin(); ?>
            <?= $form->field($model['ijin_lainnya'][$key], "[$key]pemberi_izin")->end(); ?>
        <?php endforeach; ?>

        <template v-if="typeof paket_form.paketFormIjinLainnyas == 'object'">
            <div class="box box-break-sm margin-bottom-5 hidden-sm-less">
                <div class="box-1 padding-x-0 padding-y-5 text-center">
                    No
                </div>
                <div class="box-3 padding-x-0 padding-y-5 text-center">
                    Sertifikat
                </div>
                <div class="box-2 padding-x-0 padding-y-5 text-center">
                    Nomor
                </div>
                <div class="box-3 padding-x-0 padding-y-5 text-center">
                    Pemberi Izin
                </div>
                <div class="box-2 padding-x-0 padding-y-5 text-center">
                    Berlaku
                </div>
                <div class="box-1 padding-x-0">
                    <a v-on:click="addPaketFormIjinLainnya" class="btn btn-default btn-sm margin-left-5 bg-azure hover-pointer"><i class="fa fa-plus"></i></a>
                </div>
            </div>
            <template v-for="(value, key, index) in paket_form.paketFormIjinLainnyas">
                <div v-show="!(value.id < 0)">
                    <div class="box box-break-sm">
                        <div class="box-1 padding-x-0 padding-y-5 text-center m-text-left">
                            <input type="hidden" v-bind:id="'paketformijinlainnya-' + key + '-id'" v-bind:name="'PaketFormIjinLainnya[' + key + '][id]'" class="form-control" type="text" v-model="paket_form.paketFormIjinLainnyas[key].id">
                            {{key + 1}}
                        </div>
                        <div class="box-3 padding-x-0">
                            <div v-bind:class="'form-group form-group-sm field-paketformijinlainnya-' + key + '-sertifikat'">
                                <input placeholder="sertifikat" v-bind:id="'paketformijinlainnya-' + key + '-sertifikat'" v-bind:name="'PaketFormIjinLainnya[' + key + '][sertifikat]'" class="form-control" type="text" v-model="paket_form.paketFormIjinLainnyas[key].sertifikat">
                                <div class="help-block"></div>
                            </div>
                        </div>
                        <div class="box-2 padding-x-0">
                            <div v-bind:class="'form-group form-group-sm field-paketformijinlainnya-' + key + '-nomor_sertifikat'">
                                <input placeholder="nomor_sertifikat" v-bind:id="'paketformijinlainnya-' + key + '-nomor_sertifikat'" v-bind:name="'PaketFormIjinLainnya[' + key + '][nomor_sertifikat]'" class="form-control" type="text" v-model="paket_form.paketFormIjinLainnyas[key].nomor_sertifikat">
                                <div class="help-block"></div>
                            </div>
                        </div>
                        <div class="box-3 padding-x-0">
                            <div v-bind:class="'form-group form-group-sm field-paketformijinlainnya-' + key + '-pemberi_izin'">
                                <input placeholder="pemberi_izin" v-bind:id="'paketformijinlainnya-' + key + '-pemberi_izin'" v-bind:name="'PaketFormIjinLainnya[' + key + '][pemberi_izin]'" class="form-control" type="text" v-model="paket_form.paketFormIjinLainnyas[key].pemberi_izin">
                                <div class="help-block"></div>
                            </div>
                        </div>
                        <div class="box-1 padding-x-0">
                            <div v-bind:class="'form-group form-group-sm field-paketformijinlainnya-' + key + '-berlaku_mulai'">
                                <input placeholder="mulai" v-bind:id="'paketformijinlainnya-' + key + '-berlaku_mulai'" v-bind:name="'PaketFormIjinLainnya[' + key + '][berlaku_mulai]'" class="form-control" type="text" v-model="paket_form.paketFormIjinLainnyas[key].berlaku_mulai">
                                <div class="help-block"></div>
                            </div>
                        </div>
                        <div class="box-1 padding-x-0">
                            <div v-bind:class="'form-group form-group-sm field-paketformijinlainnya-' + key + '-berlaku_sampai'">
                                <input placeholder="sampai" v-bind:id="'paketformijinlainnya-' + key + '-berlaku_sampai'" v-bind:name="'PaketFormIjinLainnya[' + key + '][berlaku_sampai]'" class="form-control" type="text" v-model="paket_form.paketFormIjinLainnyas[key].berlaku_sampai">
                                <div class="help-block"></div>
                            </div>
                        </div>
                        <div class="box-1 padding-x-0">
                            <div v-on:click="removePaketFormIjinLainnya(key)" class="btn btn-default btn-sm margin-left-5 bg-light-red hover-pointer"><i class="fa fa-close"></i></div>
                        </div>
                    </div>
                </div>
            </template>
        </template>
    </div>
</div>

<div class="border-azure margin-bottom-30">
    <div class="bg-azure padding-x-15 padding-y-5">
        <span class="fs-16">Susunan Kepemilikan Saham</span>
    </div>
    <div class="padding-x-15 padding-y-10">
        <?php if (isset($model['paket_form_pemilik_saham'])) foreach ($model['paket_form_pemilik_saham'] as $key => $value): ?>
            <?= $form->field($model['paket_form_pemilik_saham'][$key], "[$key]nama")->begin(); ?>
            <?= $form->field($model['paket_form_pemilik_saham'][$key], "[$key]nama")->end(); ?>
            <?= $form->field($model['paket_form_pemilik_saham'][$key], "[$key]nomor_ktp")->begin(); ?>
            <?= $form->field($model['paket_form_pemilik_saham'][$key], "[$key]nomor_ktp")->end(); ?>
            <?= $form->field($model['paket_form_pemilik_saham'][$key], "[$key]alamat")->begin(); ?>
            <?= $form->field($model['paket_form_pemilik_saham'][$key], "[$key]alamat")->end(); ?>
            <?= $form->field($model['paket_form_pemilik_saham'][$key], "[$key]persentase")->begin(); ?>
            <?= $form->field($model['paket_form_pemilik_saham'][$key], "[$key]persentase")->end(); ?>
        <?php endforeach; ?>

        <template v-if="typeof paket_form.paketFormPemilikSahams == 'object'">
            <div class="box box-break-sm margin-bottom-5 hidden-sm-less">
                <div class="box-1 padding-x-0 padding-y-5 text-center">
                    No
                </div>
                <div class="box-2 padding-x-0 padding-y-5 text-center">
                    Nama
                </div>
                <div class="box-3 padding-x-0 padding-y-5 text-center">
                    No. KTP
                </div>
                <div class="box-3 padding-x-0 padding-y-5 text-center">
                    Alamat
                </div>
                <div class="box-2 padding-x-0 padding-y-5 text-center">
                    Persentase (%)
                </div>
                <div class="box-1 padding-x-0">
                    <a v-on:click="addPaketFormPemilikSaham" class="btn btn-default btn-sm margin-left-5 bg-azure hover-pointer"><i class="fa fa-plus"></i></a>
                </div>
            </div>
            <template v-for="(value, key, index) in paket_form.paketFormPemilikSahams">
                <div v-show="!(value.id < 0)">
                    <div class="box box-break-sm">
                        <div class="box-1 padding-x-0 padding-y-5 text-center m-text-left">
                            <input type="hidden" v-bind:id="'paketformpemiliksaham-' + key + '-id'" v-bind:name="'PaketFormPemilikSaham[' + key + '][id]'" class="form-control" type="text" v-model="paket_form.paketFormPemilikSahams[key].id">
                            {{key + 1}}
                        </div>
                        <div class="box-3 padding-x-0">
                            <div v-bind:class="'form-group form-group-sm field-paketformpemiliksaham-' + key + '-nama'">
                                <input placeholder="nama" v-bind:id="'paketformpemiliksaham-' + key + '-nama'" v-bind:name="'PaketFormPemilikSaham[' + key + '][nama]'" class="form-control" type="text" v-model="paket_form.paketFormPemilikSahams[key].nama">
                                <div class="help-block"></div>
                            </div>
                        </div>
                        <div class="box-2 padding-x-0">
                            <div v-bind:class="'form-group form-group-sm field-paketformpemiliksaham-' + key + '-nomor_ktp'">
                                <input placeholder="nomor_ktp" v-bind:id="'paketformpemiliksaham-' + key + '-nomor_ktp'" v-bind:name="'PaketFormPemilikSaham[' + key + '][nomor_ktp]'" class="form-control" type="text" v-model="paket_form.paketFormPemilikSahams[key].nomor_ktp">
                                <div class="help-block"></div>
                            </div>
                        </div>
                        <div class="box-3 padding-x-0">
                            <div v-bind:class="'form-group form-group-sm field-paketformpemiliksaham-' + key + '-alamat'">
                                <input placeholder="alamat" v-bind:id="'paketformpemiliksaham-' + key + '-alamat'" v-bind:name="'PaketFormPemilikSaham[' + key + '][alamat]'" class="form-control" type="text" v-model="paket_form.paketFormPemilikSahams[key].alamat">
                                <div class="help-block"></div>
                            </div>
                        </div>
                        <div class="box-2 padding-x-0">
                            <div v-bind:class="'form-group form-group-sm field-paketformpemiliksaham-' + key + '-persentase'">
                                <input placeholder="persentase" v-bind:id="'paketformpemiliksaham-' + key + '-persentase'" v-bind:name="'PaketFormPemilikSaham[' + key + '][persentase]'" class="form-control" type="text" v-model="paket_form.paketFormPemilikSahams[key].persentase">
                                <div class="help-block"></div>
                            </div>
                        </div>
                        <div class="box-1 padding-x-0">
                            <div v-on:click="removePaketFormPemilikSaham(key)" class="btn btn-default btn-sm margin-left-5 bg-light-red hover-pointer"><i class="fa fa-close"></i></div>
                        </div>
                    </div>
                </div>
            </template>
        </template>
    </div>
</div>

<div class="border-azure margin-bottom-30">
    <div class="bg-azure padding-x-15 padding-y-5">
        <span class="fs-16">Pajak</span>
    </div>
    <div class="padding-x-15 padding-y-10">
        <h6 class="text-rose padding-bottom-5 border-bottom f-italic">Dokumen</h6>

        <?php if (isset($model['paket_form_pajak'])) foreach ($model['paket_form_pajak'] as $key => $value): ?>
            <?= $form->field($model['paket_form_pajak'][$key], "[$key]dokumen")->begin(); ?>
            <?= $form->field($model['paket_form_pajak'][$key], "[$key]dokumen")->end(); ?>
            <?= $form->field($model['paket_form_pajak'][$key], "[$key]nomor_dokumen")->begin(); ?>
            <?= $form->field($model['paket_form_pajak'][$key], "[$key]nomor_dokumen")->end(); ?>
            <?= $form->field($model['paket_form_pajak'][$key], "[$key]tanggal")->begin(); ?>
            <?= $form->field($model['paket_form_pajak'][$key], "[$key]tanggal")->end(); ?>
        <?php endforeach; ?>

        <template v-if="typeof paket_form.paketFormPajaks == 'object'">
            <div class="box box-break-sm margin-bottom-5 hidden-sm-less">
                <div class="box-1 padding-x-0 padding-y-5 text-center">
                    No
                </div>
                <div class="box-4 padding-x-0 padding-y-5 text-center">
                    Dokumen
                </div>
                <div class="box-5 padding-x-0 padding-y-5 text-center">
                    Nomor
                </div>
                <div class="box-1 padding-x-0 padding-y-5 text-center">
                    Tanggal
                </div>
                <div class="box-1 padding-x-0">
                    <a v-on:click="addPaketFormPajak" class="btn btn-default btn-sm margin-left-5 bg-azure hover-pointer"><i class="fa fa-plus"></i></a>
                </div>
            </div>
            <template v-for="(value, key, index) in paket_form.paketFormPajaks">
                <div v-show="!(value.id < 0)">
                    <div class="box box-break-sm">
                        <div class="box-1 padding-x-0 padding-y-5 text-center m-text-left">
                            <input type="hidden" v-bind:id="'paketformpajak-' + key + '-id'" v-bind:name="'PaketFormPajak[' + key + '][id]'" class="form-control" type="text" v-model="paket_form.paketFormPajaks[key].id">
                            {{key + 1}}
                        </div>
                        <div class="box-4 padding-x-0">
                            <div v-bind:class="'form-group form-group-sm field-paketformpajak-' + key + '-dokumen'">
                                <input placeholder="dokumen" v-bind:id="'paketformpajak-' + key + '-dokumen'" v-bind:name="'PaketFormPajak[' + key + '][dokumen]'" class="form-control" type="text" v-model="paket_form.paketFormPajaks[key].dokumen">
                                <div class="help-block"></div>
                            </div>
                        </div>
                        <div class="box-5 padding-x-0">
                            <div v-bind:class="'form-group form-group-sm field-paketformpajak-' + key + '-nomor_dokumen'">
                                <input placeholder="nomor_dokumen" v-bind:id="'paketformpajak-' + key + '-nomor_dokumen'" v-bind:name="'PaketFormPajak[' + key + '][nomor_dokumen]'" class="form-control" type="text" v-model="paket_form.paketFormPajaks[key].nomor_dokumen">
                                <div class="help-block"></div>
                            </div>
                        </div>
                        <div class="box-1 padding-x-0">
                            <div v-bind:class="'form-group form-group-sm field-paketformpajak-' + key + '-tanggal'">
                                <input placeholder="tanggal" v-bind:id="'paketformpajak-' + key + '-tanggal'" v-bind:name="'PaketFormPajak[' + key + '][tanggal]'" class="form-control" type="text" v-model="paket_form.paketFormPajaks[key].tanggal">
                                <div class="help-block"></div>
                            </div>
                        </div>
                        <div class="box-1 padding-x-0">
                            <div v-on:click="removePaketFormPajak(key)" class="btn btn-default btn-sm margin-left-5 bg-light-red hover-pointer"><i class="fa fa-close"></i></div>
                        </div>
                    </div>
                </div>
            </template>
        </template>

        <h6 class="text-rose padding-bottom-5 border-bottom f-italic">Bukti Pelunasan Pajak</h6>

        <?php if (isset($model['paket_form_bukti_pajak'])) foreach ($model['paket_form_bukti_pajak'] as $key => $value): ?>
            <?= $form->field($model['paket_form_bukti_pajak'][$key], "[$key]bulan")->begin(); ?>
            <?= $form->field($model['paket_form_bukti_pajak'][$key], "[$key]bulan")->end(); ?>
            <?= $form->field($model['paket_form_bukti_pajak'][$key], "[$key]tahun")->begin(); ?>
            <?= $form->field($model['paket_form_bukti_pajak'][$key], "[$key]tahun")->end(); ?>
            <?= $form->field($model['paket_form_bukti_pajak'][$key], "[$key]nomor_pasal21_26")->begin(); ?>
            <?= $form->field($model['paket_form_bukti_pajak'][$key], "[$key]nomor_pasal21_26")->end(); ?>
            <?= $form->field($model['paket_form_bukti_pajak'][$key], "[$key]tanggal_pasal21_26")->begin(); ?>
            <?= $form->field($model['paket_form_bukti_pajak'][$key], "[$key]tanggal_pasal21_26")->end(); ?>
            <?= $form->field($model['paket_form_bukti_pajak'][$key], "[$key]nomor_pasal23_26")->begin(); ?>
            <?= $form->field($model['paket_form_bukti_pajak'][$key], "[$key]nomor_pasal23_26")->end(); ?>
            <?= $form->field($model['paket_form_bukti_pajak'][$key], "[$key]tanggal_pasal23_26")->begin(); ?>
            <?= $form->field($model['paket_form_bukti_pajak'][$key], "[$key]tanggal_pasal23_26")->end(); ?>
            <?= $form->field($model['paket_form_bukti_pajak'][$key], "[$key]nomor_pasal4ayat2")->begin(); ?>
            <?= $form->field($model['paket_form_bukti_pajak'][$key], "[$key]nomor_pasal4ayat2")->end(); ?>
            <?= $form->field($model['paket_form_bukti_pajak'][$key], "[$key]tanggal_pasal4ayat2")->begin(); ?>
            <?= $form->field($model['paket_form_bukti_pajak'][$key], "[$key]tanggal_pasal4ayat2")->end(); ?>
            <?= $form->field($model['paket_form_bukti_pajak'][$key], "[$key]nomor_pasal25")->begin(); ?>
            <?= $form->field($model['paket_form_bukti_pajak'][$key], "[$key]nomor_pasal25")->end(); ?>
            <?= $form->field($model['paket_form_bukti_pajak'][$key], "[$key]tanggal_pasal25")->begin(); ?>
            <?= $form->field($model['paket_form_bukti_pajak'][$key], "[$key]tanggal_pasal25")->end(); ?>
            <?= $form->field($model['paket_form_bukti_pajak'][$key], "[$key]nomor_ppnbm")->begin(); ?>
            <?= $form->field($model['paket_form_bukti_pajak'][$key], "[$key]nomor_ppnbm")->end(); ?>
            <?= $form->field($model['paket_form_bukti_pajak'][$key], "[$key]tanggal_ppnbm")->begin(); ?>
            <?= $form->field($model['paket_form_bukti_pajak'][$key], "[$key]tanggal_ppnbm")->end(); ?>
        <?php endforeach; ?>

        <template v-if="typeof paket_form.paketFormBuktiPajaks == 'object'">
            <template v-for="(value, key, index) in paket_form.paketFormBuktiPajaks">
                <div v-show="!(value.id < 0)">
                    <div class="box box-break-sm">
                        <div class="box-2 padding-y-5 text-right m-text-left f-bold">
                            <input type="hidden" v-bind:id="'paketformbuktipajak-' + key + '-id'" v-bind:name="'PaketFormBuktiPajak[' + key + '][id]'" class="form-control" type="text" v-model="paket_form.paketFormBuktiPajaks[key].id">
                            Pajak {{key + 1}}
                        </div>
                        <div class="box-4 padding-x-0">
                            <div v-bind:class="'form-group form-group-sm field-paketformbuktipajak-' + key + '-bulan'">
                                <select placeholder="bulan" v-bind:id="'paketformbuktipajak-' + key + '-bulan'" v-bind:name="'PaketFormBuktiPajak[' + key + '][bulan]'" class="form-control" v-model="paket_form.paketFormBuktiPajaks[key].bulan">
                                    <option value="">Pilih Bulan</option>
                                    <option value="1">Januari</option>
                                    <option value="2">Februari</option>
                                    <option value="3">Maret</option>
                                    <option value="4">Mei</option>
                                    <option value="5">Juni</option>
                                    <option value="6">Juli</option>
                                    <option value="7">Agustus</option>
                                    <option value="8">September</option>
                                    <option value="9">Oktober</option>
                                    <option value="10">November</option>
                                    <option value="12">Desember</option>
                                </select>
                                <div class="help-block"></div>
                            </div>
                        </div>
                        <div class="box-4 padding-x-0">
                            <div v-bind:class="'form-group form-group-sm field-paketformbuktipajak-' + key + '-tahun'">
                                <input placeholder="tahun" v-bind:id="'paketformbuktipajak-' + key + '-tahun'" v-bind:name="'PaketFormBuktiPajak[' + key + '][tahun]'" class="form-control" type="text" v-model="paket_form.paketFormBuktiPajaks[key].tahun">
                                <div class="help-block"></div>
                            </div>
                        </div>
                        <div class="box-1">
                            <div v-on:click="removePaketFormBuktiPajak(key)" class="btn btn-default btn-sm margin-left-5 bg-light-red hover-pointer"><i class="fa fa-close"></i></div>
                        </div>
                    </div>
                    <div class="box box-break-sm">
                        <div class="box-2 padding-y-5 text-right m-text-left">
                            Pasal 21/26
                        </div>
                        <div class="box-4 padding-x-0">
                            <div v-bind:class="'form-group form-group-sm field-paketformbuktipajak-' + key + '-nomor_pasal21_26'">
                                <input placeholder="nomor" v-bind:id="'paketformbuktipajak-' + key + '-nomor_pasal21_26'" v-bind:name="'PaketFormBuktiPajak[' + key + '][nomor_pasal21_26]'" class="form-control" type="text" v-model="paket_form.paketFormBuktiPajaks[key].nomor_pasal21_26">
                                <div class="help-block"></div>
                            </div>
                        </div>
                        <div class="box-4 padding-x-0">
                            <div v-bind:class="'form-group form-group-sm field-paketformbuktipajak-' + key + '-tanggal_pasal21_26'">
                                <input placeholder="tanggal" v-bind:id="'paketformbuktipajak-' + key + '-tanggal_pasal21_26'" v-bind:name="'PaketFormBuktiPajak[' + key + '][tanggal_pasal21_26]'" class="form-control" type="text" v-model="paket_form.paketFormBuktiPajaks[key].tanggal_pasal21_26">
                                <div class="help-block"></div>
                            </div>
                        </div>
                    </div>
                    <div class="box box-break-sm">
                        <div class="box-2 padding-y-5 text-right m-text-left">
                            Pasal 23/26
                        </div>
                        <div class="box-4 padding-x-0">
                            <div v-bind:class="'form-group form-group-sm field-paketformbuktipajak-' + key + '-nomor_pasal23_26'">
                                <input placeholder="nomor" v-bind:id="'paketformbuktipajak-' + key + '-nomor_pasal23_26'" v-bind:name="'PaketFormBuktiPajak[' + key + '][nomor_pasal23_26]'" class="form-control" type="text" v-model="paket_form.paketFormBuktiPajaks[key].nomor_pasal23_26">
                                <div class="help-block"></div>
                            </div>
                        </div>
                        <div class="box-4 padding-x-0">
                            <div v-bind:class="'form-group form-group-sm field-paketformbuktipajak-' + key + '-tanggal_pasal23_26'">
                                <input placeholder="tanggal" v-bind:id="'paketformbuktipajak-' + key + '-tanggal_pasal23_26'" v-bind:name="'PaketFormBuktiPajak[' + key + '][tanggal_pasal23_26]'" class="form-control" type="text" v-model="paket_form.paketFormBuktiPajaks[key].tanggal_pasal23_26">
                                <div class="help-block"></div>
                            </div>
                        </div>
                    </div>
                    <div class="box box-break-sm">
                        <div class="box-2 padding-y-5 text-right m-text-left">
                            Pasal 4 ayat 2
                        </div>
                        <div class="box-4 padding-x-0">
                            <div v-bind:class="'form-group form-group-sm field-paketformbuktipajak-' + key + '-nomor_pasal4ayat2'">
                                <input placeholder="nomor" v-bind:id="'paketformbuktipajak-' + key + '-nomor_pasal4ayat2'" v-bind:name="'PaketFormBuktiPajak[' + key + '][nomor_pasal4ayat2]'" class="form-control" type="text" v-model="paket_form.paketFormBuktiPajaks[key].nomor_pasal4ayat2">
                                <div class="help-block"></div>
                            </div>
                        </div>
                        <div class="box-4 padding-x-0">
                            <div v-bind:class="'form-group form-group-sm field-paketformbuktipajak-' + key + '-tanggal_pasal4ayat2'">
                                <input placeholder="tanggal" v-bind:id="'paketformbuktipajak-' + key + '-tanggal_pasal4ayat2'" v-bind:name="'PaketFormBuktiPajak[' + key + '][tanggal_pasal4ayat2]'" class="form-control" type="text" v-model="paket_form.paketFormBuktiPajaks[key].tanggal_pasal4ayat2">
                                <div class="help-block"></div>
                            </div>
                        </div>
                    </div>
                    <div class="box box-break-sm">
                        <div class="box-2 padding-y-5 text-right m-text-left">
                            Pasal 25
                        </div>
                        <div class="box-4 padding-x-0">
                            <div v-bind:class="'form-group form-group-sm field-paketformbuktipajak-' + key + '-nomor_pasal25'">
                                <input placeholder="nomor" v-bind:id="'paketformbuktipajak-' + key + '-nomor_pasal25'" v-bind:name="'PaketFormBuktiPajak[' + key + '][nomor_pasal25]'" class="form-control" type="text" v-model="paket_form.paketFormBuktiPajaks[key].nomor_pasal25">
                                <div class="help-block"></div>
                            </div>
                        </div>
                        <div class="box-4 padding-x-0">
                            <div v-bind:class="'form-group form-group-sm field-paketformbuktipajak-' + key + '-tanggal_pasal25'">
                                <input placeholder="tanggal" v-bind:id="'paketformbuktipajak-' + key + '-tanggal_pasal25'" v-bind:name="'PaketFormBuktiPajak[' + key + '][tanggal_pasal25]'" class="form-control" type="text" v-model="paket_form.paketFormBuktiPajaks[key].tanggal_pasal25">
                                <div class="help-block"></div>
                            </div>
                        </div>
                    </div>
                    <div class="box box-break-sm">
                        <div class="box-2 padding-y-5 text-right m-text-left">
                            PPN BM
                        </div>
                        <div class="box-4 padding-x-0">
                            <div v-bind:class="'form-group form-group-sm field-paketformbuktipajak-' + key + '-nomor_ppnbm'">
                                <input placeholder="nomor" v-bind:id="'paketformbuktipajak-' + key + '-nomor_ppnbm'" v-bind:name="'PaketFormBuktiPajak[' + key + '][nomor_ppnbm]'" class="form-control" type="text" v-model="paket_form.paketFormBuktiPajaks[key].nomor_ppnbm">
                                <div class="help-block"></div>
                            </div>
                        </div>
                        <div class="box-4 padding-x-0">
                            <div v-bind:class="'form-group form-group-sm field-paketformbuktipajak-' + key + '-tanggal_ppnbm'">
                                <input placeholder="tanggal" v-bind:id="'paketformbuktipajak-' + key + '-tanggal_ppnbm'" v-bind:name="'PaketFormBuktiPajak[' + key + '][tanggal_ppnbm]'" class="form-control" type="text" v-model="paket_form.paketFormBuktiPajaks[key].tanggal_ppnbm">
                                <div class="help-block"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </template>
            <a v-on:click="addPaketFormBuktiPajak" class="btn btn-default btn-sm margin-left-5 bg-azure hover-pointer"><i class="fa fa-plus"></i></a>
        </template>
    </div>
</div>

    <hr class="margin-y-15">

    <?php if ($error) : ?>
        <div class="alert alert-danger">
            <?= $errorMessage ?>
        </div>
    <?php endif; ?>
    
    <div class="form-group clearfix">
        <?= Html::submitButton('Submit', ['class' => 'btn btn-default bg-azure rounded-xs border-azure']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default bg-lighter rounded-xs']); ?> 
        <?= Html::a('Kembali ke detail', ['index', 'id' => $model['paket']->id], ['class' => 'btn btn-default bg-lightest rounded-xs pull-right']) ?>
    </div>
    
<?php ActiveForm::end(); ?>

<?php if (!Yii::$app->request->isAjax) : ?>
    </div>
</div>
<?php endif; ?>