<?php

use yii\helpers\Html;

if (isset($title)) $this->title = $title;
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;

$this->registerJsFile('@web/app/paket/list.js', ['depends' => 'technosmart\assets_manager\DatatablesAsset']);
?>

<h6>Filter</h6>
<table class="datatables display nowrap table table-striped table-hover">
    <thead>
        <tr>
            <th class="text-dark f-normal" style="border-bottom: 1px">Unit Kerja</th>
            <th class="text-dark f-normal" style="border-bottom: 1px">Nama Paket</th>
            <th class="text-dark f-normal" style="border-bottom: 1px">Instansi</th>
            <th class="text-dark f-normal" style="border-bottom: 1px">Hps</th>
            <th class="text-dark f-normal" style="border-bottom: 1px">Tahap</th>
        </tr>
        <tr class="dt-search">
            <th class="padding-0">
                <select class="form-control border-none f-normal padding-x-5 search fs-12">
                    <option value="">Semua Unit Kerja</option>
                    <option value="1">Divisi Kimtaru</option>
                    <option value="2">Divisi Transportasi</option>
                    <option value="3">Divisi SDA</option>
                    <option value="4">Divisi Gedung</option>
                    <option value="5">Cabang Padang</option>
                    <option value="6">Cabang Semarang</option>
                    <option value="7">Cabang Surabaya</option>
                    <option value="8">Cabang Makasar</option>
                    <option value="9">Cabang Kalimantan</option>
                </select>
            </th>
            <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Search nama paket" class="form-control border-none f-normal padding-x-5 search"/></th>
            <th class="padding-0">
                <select class="form-control border-none f-normal padding-x-5 search fs-12">
                    <option value="">Semua Instansi</option>
                    <option value="1">Kementrian PU</option>
                    <option value="2">Kementrian Tenaga Kerja</option>
                    <option value="3">Provinsi DKI Jakarta</option>
                    <option value="4">Bina Marga</option>
                    <option value="5">Pemkot Bandung</option>
                    <option value="6">Tokopedia</option>
                </select>
            </th>
            <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Filter hps diatas" class="form-control border-none f-normal padding-x-5 search"/></th>
            <th class="padding-0">
                <select class="form-control border-none f-normal padding-x-5 search fs-12">
                    <option value="">Semua Tahap</option>
                    <option value="1">Info Awal</option>
                    <option value="2">Pengumuman Prakualifikasi</option>
                    <option value="3">Download Dokumen Kualifikasi</option>
                    <option value="4">Penjelasan Dokumen Prakualifikasi</option>
                    <option value="5">Upload Dokumen Prakualifikasi</option>
                    <option value="6">Evaluasi Dokumen Kualifikasi</option>
                    <option value="7">Pembuktian Kualifikasi</option>
                    <option value="8">Penetapan Hasil Kualifikasi</option>
                    <option value="9">Pengumuman Hasil Prakualifikasi</option>
                    <option value="10">Download Dokumen Pemilihan</option>
                    <option value="11">Pemberian Penjelasan</option>
                    <option value="12">Upload Dokumen Penawaran</option>
                    <option value="13">Pembukaan dan Evaluasi Penawaran File I : Administrasi dan Teknis</option>
                    <option value="14">Paparan Teknis</option>
                    <option value="15">Penetapan Peringkat Teknis</option>
                    <option value="16">Pemberitahuan/Pengumuman Peringkat Teknis</option>
                    <option value="17">Pembukaan dan Evaluasi Penawaran File II : Harga</option>
                    <option value="18">Penetapan pemenang</option>
                    <option value="19">Pengumuman Pemenang</option>
                    <option value="20">Masa Sanggah Hasil Lelang</option>
                    <option value="21">Klarifikasi dan Negosiasi Teknis dan Biaya</option>
                    <option value="22">Upload Berita Acara Hasil Pelelangan</option>
                    <option value="23">Surat Penunjukan Penyedia Barang/Jasa</option>
                    <option value="24">Penandatanganan Kontrak</option>
                </select>
            </th>
        </tr>
    </thead>
</table>