<?php

use yii\helpers\Html;
?>

<div>

<div class="border-azure margin-bottom-30">
    <div class="bg-azure padding-x-15 padding-y-5">
        <span class="fs-16">Landasan Hukum Pendirian Badan Usaha</span>
    </div>
    <div class="padding-x-15 padding-y-10">
        <h6 class="text-rose padding-bottom-5 border-bottom f-italic">Akta Pendirian Perusahaan</h6>

        <div class="box box-break-sm margin-bottom-10">
            <div class="box-2 padding-x-0 text-right m-text-left">
                <?= Html::activeLabel($model['paket_form'], 'app_tanggal', ['class' => 'control-label']); ?>
            </div>
            <div class="box-10 m-padding-x-0 f-bold">
                <?= $model['paket_form']->app_tanggal ? $model['paket_form']->app_tanggal : '<span class="text-gray f-italic">(kosong)</span>' ?>
            </div>
        </div>

        <div class="box box-break-sm margin-bottom-10">
            <div class="box-2 padding-x-0 text-right m-text-left">
                <?= Html::activeLabel($model['paket_form'], 'app_nama_notaris', ['class' => 'control-label']); ?>
            </div>
            <div class="box-10 m-padding-x-0 f-bold">
                <?= $model['paket_form']->app_nama_notaris ? $model['paket_form']->app_nama_notaris : '<span class="text-gray f-italic">(kosong)</span>' ?>
            </div>
        </div>

        <div class="box box-break-sm margin-bottom-10">
            <div class="box-2 padding-x-0 text-right m-text-left">
                <?= Html::activeLabel($model['paket_form'], 'app_nomor_pengesahan', ['class' => 'control-label']); ?>
            </div>
            <div class="box-10 m-padding-x-0 f-bold">
                <?= $model['paket_form']->app_nomor_pengesahan ? $model['paket_form']->app_nomor_pengesahan : '<span class="text-gray f-italic">(kosong)</span>' ?>
            </div>
        </div>

        <div class="box box-break-sm margin-bottom-10">
            <div class="box-2 padding-x-0 text-right m-text-left">
                <?= Html::activeLabel($model['paket_form'], 'app_tanggal_pengesahan', ['class' => 'control-label']); ?>
            </div>
            <div class="box-10 m-padding-x-0 f-bold">
                <?= $model['paket_form']->app_tanggal_pengesahan ? $model['paket_form']->app_tanggal_pengesahan : '<span class="text-gray f-italic">(kosong)</span>' ?>
            </div>
        </div>

        <h6 class="text-rose padding-bottom-5 border-bottom f-italic">Akta Perubahan Terakhir</h6>

        <div class="box box-break-sm margin-bottom-10">
            <div class="box-2 padding-x-0 text-right m-text-left">
                <?= Html::activeLabel($model['paket_form'], 'apt_nomor_akta', ['class' => 'control-label']); ?>
            </div>
            <div class="box-10 m-padding-x-0 f-bold">
                <?= $model['paket_form']->apt_nomor_akta ? $model['paket_form']->apt_nomor_akta : '<span class="text-gray f-italic">(kosong)</span>' ?>
            </div>
        </div>

        <div class="box box-break-sm margin-bottom-10">
            <div class="box-2 padding-x-0 text-right m-text-left">
                <?= Html::activeLabel($model['paket_form'], 'apt_tanggal', ['class' => 'control-label']); ?>
            </div>
            <div class="box-10 m-padding-x-0 f-bold">
                <?= $model['paket_form']->apt_tanggal ? $model['paket_form']->apt_tanggal : '<span class="text-gray f-italic">(kosong)</span>' ?>
            </div>
        </div>

        <div class="box box-break-sm margin-bottom-10">
            <div class="box-2 padding-x-0 text-right m-text-left">
                <?= Html::activeLabel($model['paket_form'], 'apt_nama_notaris', ['class' => 'control-label']); ?>
            </div>
            <div class="box-10 m-padding-x-0 f-bold">
                <?= $model['paket_form']->apt_nama_notaris ? $model['paket_form']->apt_nama_notaris : '<span class="text-gray f-italic">(kosong)</span>' ?>
            </div>
        </div>

        <div class="box box-break-sm margin-bottom-10">
            <div class="box-2 padding-x-0 text-right m-text-left">
                <?= Html::activeLabel($model['paket_form'], 'apt_nomor_pengesahan', ['class' => 'control-label']); ?>
            </div>
            <div class="box-10 m-padding-x-0 f-bold">
                <?= $model['paket_form']->apt_nomor_pengesahan ? $model['paket_form']->apt_nomor_pengesahan : '<span class="text-gray f-italic">(kosong)</span>' ?>
            </div>
        </div>

        <div class="box box-break-sm margin-bottom-10">
            <div class="box-2 padding-x-0 text-right m-text-left">
                <?= Html::activeLabel($model['paket_form'], 'apt_tanggal_pengesahan', ['class' => 'control-label']); ?>
            </div>
            <div class="box-10 m-padding-x-0 f-bold">
                <?= $model['paket_form']->apt_tanggal_pengesahan ? $model['paket_form']->apt_tanggal_pengesahan : '<span class="text-gray f-italic">(kosong)</span>' ?>
            </div>
        </div>
    </div>
</div>

<div class="border-azure margin-bottom-30">
    <div class="bg-azure padding-x-15 padding-y-5">
        <span class="fs-16">Pengurus Badan Usaha</span>
    </div>
    <div class="padding-x-15 padding-y-10">
        <h6 class="text-rose padding-bottom-5 border-bottom f-italic">Komisaris</h6>

        <table class="table table-condensed">
            <thead>
                <tr role="row">
                    <th class="text-dark f-normal" style="border-bottom: 1px">No</th>
                    <th class="text-dark f-normal" style="border-bottom: 1px">Nama</th>
                    <th class="text-dark f-normal" style="border-bottom: 1px">No. KTP</th>
                    <th class="text-dark f-normal" style="border-bottom: 1px">Jabatan</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($model['paket_form']->paketFormKomisarises as $key => $value): ?>
                    <tr role="row">
                        <td><?= $key+1 ?></td>
                        <td><?= $value->nama ?></td>
                        <td><?= $value->nomor_ktp ?></td>
                        <td><?= $value->jabatan ?></td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>

        <h6 class="text-rose padding-bottom-5 border-bottom f-italic">Direksi</h6>

        <table class="table table-condensed">
            <thead>
                <tr role="row">
                    <th class="text-dark f-normal" style="border-bottom: 1px">No</th>
                    <th class="text-dark f-normal" style="border-bottom: 1px">Nama</th>
                    <th class="text-dark f-normal" style="border-bottom: 1px">No. KTP</th>
                    <th class="text-dark f-normal" style="border-bottom: 1px">Jabatan</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($model['paket_form']->paketFormDireksis as $key => $value): ?>
                    <tr role="row">
                        <td><?= $key+1 ?></td>
                        <td><?= $value->nama ?></td>
                        <td><?= $value->nomor_ktp ?></td>
                        <td><?= $value->jabatan ?></td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>

<div class="border-azure margin-bottom-30">
    <div class="bg-azure padding-x-15 padding-y-5">
        <span class="fs-16">Ijin Usaha</span>
    </div>
    <div class="padding-x-15 padding-y-10">
        <h6 class="text-rose padding-bottom-5 border-bottom f-italic">Ijin Usaha Jasa Konstruksi/Konsultan (IUJK)</h6>

        <div class="box box-break-sm margin-bottom-10">
            <div class="box-2 padding-x-0 text-right m-text-left">
                <?= Html::activeLabel($model['paket_form'], 'iujk_berlaku_mulai', ['class' => 'control-label']); ?>
            </div>
            <div class="box-10 m-padding-x-0 f-bold">
                <?= $model['paket_form']->iujk_berlaku_mulai ? $model['paket_form']->iujk_berlaku_mulai : '<span class="text-gray f-italic">(kosong)</span>' ?>
            </div>
        </div>

        <div class="box box-break-sm margin-bottom-10">
            <div class="box-2 padding-x-0 text-right m-text-left">
                <?= Html::activeLabel($model['paket_form'], 'iujk_berlaku_sampai', ['class' => 'control-label']); ?>
            </div>
            <div class="box-10 m-padding-x-0 f-bold">
                <?= $model['paket_form']->iujk_berlaku_sampai ? $model['paket_form']->iujk_berlaku_sampai : '<span class="text-gray f-italic">(kosong)</span>' ?>
            </div>
        </div>

        <div class="box box-break-sm margin-bottom-10">
            <div class="box-2 padding-x-0 text-right m-text-left">
                <?= Html::activeLabel($model['paket_form'], 'iujk_pemberi_izin', ['class' => 'control-label']); ?>
            </div>
            <div class="box-10 m-padding-x-0 f-bold">
                <?= $model['paket_form']->iujk_pemberi_izin ? $model['paket_form']->iujk_pemberi_izin : '<span class="text-gray f-italic">(kosong)</span>' ?>
            </div>
        </div>

        <div class="box box-break-sm margin-bottom-10">
            <div class="box-2 padding-x-0 text-right m-text-left">
                <?= Html::activeLabel($model['paket_form'], 'iujk_perencanaan', ['class' => 'control-label']); ?>
            </div>
            <div class="box-10 m-padding-x-0 f-bold">
                <?= $model['paket_form']->iujk_perencanaan ? $model['paket_form']->iujk_perencanaan : '<span class="text-gray f-italic">(kosong)</span>' ?>
            </div>
        </div>

        <div class="box box-break-sm margin-bottom-10">
            <div class="box-2 padding-x-0 text-right m-text-left">
                <?= Html::activeLabel($model['paket_form'], 'iujk_pengawasan', ['class' => 'control-label']); ?>
            </div>
            <div class="box-10 m-padding-x-0 f-bold">
                <?= $model['paket_form']->iujk_pengawasan ? $model['paket_form']->iujk_pengawasan : '<span class="text-gray f-italic">(kosong)</span>' ?>
            </div>
        </div>

        <div class="box box-break-sm margin-bottom-10">
            <div class="box-2 padding-x-0 text-right m-text-left">
                <?= Html::activeLabel($model['paket_form'], 'iujk_konsultansi', ['class' => 'control-label']); ?>
            </div>
            <div class="box-10 m-padding-x-0 f-bold">
                <?= $model['paket_form']->iujk_konsultansi ? $model['paket_form']->iujk_konsultansi : '<span class="text-gray f-italic">(kosong)</span>' ?>
            </div>
        </div>

        <h6 class="text-rose padding-bottom-5 border-bottom f-italic">Sertifikat Badan Usaha (SBU) Konstruksi</h6>

        <div class="box box-break-sm margin-bottom-10">
            <div class="box-2 padding-x-0 text-right m-text-left">
                <?= Html::activeLabel($model['paket_form'], 'sbuk_berlaku_mulai', ['class' => 'control-label']); ?>
            </div>
            <div class="box-10 m-padding-x-0 f-bold">
                <?= $model['paket_form']->sbuk_berlaku_mulai ? $model['paket_form']->sbuk_berlaku_mulai : '<span class="text-gray f-italic">(kosong)</span>' ?>
            </div>
        </div>

        <div class="box box-break-sm margin-bottom-10">
            <div class="box-2 padding-x-0 text-right m-text-left">
                <?= Html::activeLabel($model['paket_form'], 'sbuk_berlaku_sampai', ['class' => 'control-label']); ?>
            </div>
            <div class="box-10 m-padding-x-0 f-bold">
                <?= $model['paket_form']->sbuk_berlaku_sampai ? $model['paket_form']->sbuk_berlaku_sampai : '<span class="text-gray f-italic">(kosong)</span>' ?>
            </div>
        </div>

        <div class="box box-break-sm margin-bottom-10">
            <div class="box-2 padding-x-0 text-right m-text-left">
                <?= Html::activeLabel($model['paket_form'], 'sbuk_pemberi_izin', ['class' => 'control-label']); ?>
            </div>
            <div class="box-10 m-padding-x-0 f-bold">
                <?= $model['paket_form']->sbuk_pemberi_izin ? $model['paket_form']->sbuk_pemberi_izin : '<span class="text-gray f-italic">(kosong)</span>' ?>
            </div>
        </div>

        <table class="table table-condensed">
            <thead>
                <tr role="row">
                    <th class="text-dark f-normal" style="border-bottom: 1px">No</th>
                    <th class="text-dark f-normal" style="border-bottom: 1px">Sertifikat</th>
                    <th class="text-dark f-normal" style="border-bottom: 1px">Nomor</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($model['paket_form']->paketFormSbuks as $key => $value): ?>
                    <tr role="row">
                        <td><?= $key+1 ?></td>
                        <td><?= $value->sertifikat ?></td>
                        <td><?= $value->nomor_sertifikat ?></td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>

        <h6 class="text-rose padding-bottom-5 border-bottom f-italic">Sertifikat Badan Usaha (SBU) Non Konstruksi</h6>

        <table class="table table-condensed">
            <thead>
                <tr role="row">
                    <th class="text-dark f-normal" style="border-bottom: 1px">No</th>
                    <th class="text-dark f-normal" style="border-bottom: 1px">Sertifikat</th>
                    <th class="text-dark f-normal" style="border-bottom: 1px">Nomor</th>
                    <th class="text-dark f-normal" style="border-bottom: 1px">Pemberi Izin</th>
                    <th class="text-dark f-normal" style="border-bottom: 1px">Masa Berlaku</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($model['paket_form']->paketFormSbunks as $key => $value): ?>
                    <tr role="row">
                        <td><?= $key+1 ?></td>
                        <td><?= $value->sertifikat ?></td>
                        <td><?= $value->nomor_sertifikat ?></td>
                        <td><?= $value->pemberi_izin ?></td>
                        <td><?= $value->berlaku_mulai . ' - ' . $value->berlaku_sampai ?></td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>

<div class="border-azure margin-bottom-30">
    <div class="bg-azure padding-x-15 padding-y-5">
        <span class="fs-16">Ijin Usaha Lainnya</span>
    </div>
    <div class="padding-x-15 padding-y-10">
        <table class="table table-condensed">
            <thead>
                <tr role="row">
                    <th class="text-dark f-normal" style="border-bottom: 1px">No</th>
                    <th class="text-dark f-normal" style="border-bottom: 1px">Sertifikat</th>
                    <th class="text-dark f-normal" style="border-bottom: 1px">Nomor</th>
                    <th class="text-dark f-normal" style="border-bottom: 1px">Pemberi Izin</th>
                    <th class="text-dark f-normal" style="border-bottom: 1px">Masa Berlaku</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($model['paket_form']->paketFormIjinLainnyas as $key => $value): ?>
                    <tr role="row">
                        <td><?= $key+1 ?></td>
                        <td><?= $value->sertifikat ?></td>
                        <td><?= $value->nomor_sertifikat ?></td>
                        <td><?= $value->pemberi_izin ?></td>
                        <td><?= $value->berlaku_mulai . ' - ' . $value->berlaku_sampai ?></td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>

<div class="border-azure margin-bottom-30">
    <div class="bg-azure padding-x-15 padding-y-5">
        <span class="fs-16">Susunan Kepemilikan Saham</span>
    </div>
    <div class="padding-x-15 padding-y-10">
        <table class="table table-condensed">
            <thead>
                <tr role="row">
                    <th class="text-dark f-normal" style="border-bottom: 1px">No</th>
                    <th class="text-dark f-normal" style="border-bottom: 1px">Nama</th>
                    <th class="text-dark f-normal" style="border-bottom: 1px">No. KTP</th>
                    <th class="text-dark f-normal" style="border-bottom: 1px">Alamat</th>
                    <th class="text-dark f-normal" style="border-bottom: 1px">Persentase (%)</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($model['paket_form']->paketFormPemilikSahams as $key => $value): ?>
                    <tr role="row">
                        <td><?= $key+1 ?></td>
                        <td><?= $value->nama ?></td>
                        <td><?= $value->nomor_ktp ?></td>
                        <td><?= $value->alamat ?></td>
                        <td><?= $value->persentase ?></td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>

<div class="border-azure margin-bottom-30">
    <div class="bg-azure padding-x-15 padding-y-5">
        <span class="fs-16">Pajak</span>
    </div>
    <div class="padding-x-15 padding-y-10">
        <h6 class="text-rose padding-bottom-5 border-bottom f-italic">Dokumen</h6>

        <table class="table table-condensed">
            <thead>
                <tr role="row">
                    <th class="text-dark f-normal" style="border-bottom: 1px">No</th>
                    <th class="text-dark f-normal" style="border-bottom: 1px">Dokumen</th>
                    <th class="text-dark f-normal" style="border-bottom: 1px">No. Dokumen</th>
                    <th class="text-dark f-normal" style="border-bottom: 1px">Tanggal</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($model['paket_form']->paketFormPajaks as $key => $value): ?>
                    <tr role="row">
                        <td><?= $key+1 ?></td>
                        <td><?= $value->dokumen ?></td>
                        <td><?= $value->nomor_dokumen ?></td>
                        <td><?= $value->tanggal ?></td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>

        <h6 class="text-rose padding-bottom-5 border-bottom f-italic">Bukti Pelunasan Pajak</h6>

        <?php $bulan = [
            '1' => 'Januari',
            '2' => 'Februari',
            '3' => 'Maret',
            '4' => 'Mei',
            '5' => 'Juni',
            '6' => 'Juli',
            '7' => 'Agustus',
            '8' => 'September',
            '9' => 'Oktober',
            '10' => 'November',
            '12' => 'Desember',
        ]; ?>
        <?php foreach ($model['paket_form']->paketFormBuktiPajaks as $key => $value): ?>
            <div class="margin-botom-30">
                <div class="f-bold fs-14"><?= 'Pajak ' . $bulan[$value->bulan] . ' ' . $value->tahun ?></div>
                <div class="padding-left-20 margin-top-10">
                    <div class="margin-bottom-10">
                        <div>Pasal 21/26     : <b><?= $value->nomor_pasal21_26 ?></b></div>
                        <div>Tanggal         : <i><?= $value->tanggal_pasal21_26 ?></i></div>
                    </div>
                    <div class="margin-bottom-10">
                        <div>Pasal 23/26     : <b><?= $value->nomor_pasal23_26 ?></b></div>
                        <div>Tanggal         : <i><?= $value->tanggal_pasal23_26 ?></i></div>
                    </div>
                    <div class="margin-bottom-10">
                        <div>Pasal 4 ayat 2  : <b><?= $value->nomor_pasal4ayat2 ?></b></div>
                        <div>Tanggal         : <i><?= $value->tanggal_pasal4ayat2 ?></i></div>
                    </div>
                    <div class="margin-bottom-10">
                        <div>Pasal 25        : <b><?= $value->nomor_pasal25 ?></b></div>
                        <div>Tanggal         : <i><?= $value->tanggal_pasal25 ?></i></div>
                    </div>
                    <div class="margin-bottom-10">
                        <div>PPN BM          : <b><?= $value->nomor_ppnbm ?></b></div>
                        <div>Tanggal         : <i><?= $value->tanggal_ppnbm ?></i></div>
                    </div>
                </div>
            </div>
        <?php endforeach; ?>
    </div>
</div>

</div>