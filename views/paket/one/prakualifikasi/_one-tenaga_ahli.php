<?php

use yii\helpers\Html;
?>

<div>
    <table class="table table-striped table-hover table-condensed">
        <thead>
            <tr role="row">
                <th class="text-dark f-normal sorting" style="border-bottom: 1px">Nama</th>
                <th class="text-dark f-normal sorting" style="border-bottom: 1px">Tanggal Lahir</th>
                <th class="text-dark f-normal sorting" style="border-bottom: 1px">Penugasan</th>
                <th class="text-dark f-normal sorting" style="border-bottom: 1px">jenjang</th>
                <th class="text-dark f-normal sorting" style="border-bottom: 1px">jurusan</th>
                <th class="text-dark f-normal sorting" style="border-bottom: 1px">tahun_lulus</th>
                <th class="text-dark f-normal sorting" style="border-bottom: 1px">keahlian</th>
                <th class="text-dark f-normal sorting" style="border-bottom: 1px">level</th>
                <th class="text-dark f-normal sorting" style="border-bottom: 1px">sertifikat</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($model['paket']->paketTenagaAhlis as $key => $value) : ?>
            <tr role="row">
                <td><?= $value->nama ?></td>
                <td><?= $value->tanggal_lahir ?></td>
                <td><?= $value->penugasan->nama_penugasan ?></td>
                <td><?= $value->jenjang ?></td>
                <td><?= $value->jurusan ?></td>
                <td><?= $value->tahun_lulus ?></td>
                <td><?= $value->keahlian ?></td>
                <td><?= $value->level ?></td>
                <td><?= $value->sertifikat ?></td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
</div>