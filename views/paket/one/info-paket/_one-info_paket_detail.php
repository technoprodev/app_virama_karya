<?php

use yii\helpers\Html;
?>

<div>
    <div class="box box-break-sm margin-bottom-10">
        <div class="box-2 padding-x-0 text-right m-text-left"><?= $model['paket']->attributeLabels()['lokasi'] ?></div>
        <div class="box-10 m-padding-x-0 f-bold"><?= $model['paket']->lokasi ? $model['paket']->lokasi : '<span class="f-light f-italic">(kosong)</span>' ?></div>
    </div>
        
    <div class="box box-break-sm margin-bottom-10">
        <div class="box-2 padding-x-0 text-right m-text-left"><?= $model['paket']->attributeLabels()['id_unit_kerja'] ?></div>
        <div class="box-10 m-padding-x-0 f-bold"><?= $model['paket']->id_unit_kerja ? $model['paket']->unitKerja->nama_unit_kerja : '<span class="f-light f-italic">(kosong)</span>' ?></div>
    </div>
        
    <div class="box box-break-sm margin-bottom-10">
        <div class="box-2 padding-x-0 text-right m-text-left"><?= $model['paket']->attributeLabels()['id_bidang_unit_kerja'] ?></div>
        <div class="box-10 m-padding-x-0 f-bold"><?= $model['paket']->id_bidang_unit_kerja ? $model['paket']->bidangUnitKerja->nama_bidang_unit_kerja : '<span class="f-light f-italic">(kosong)</span>' ?></div>
    </div>
        
    <div class="box box-break-sm margin-bottom-10">
        <div class="box-2 padding-x-0 text-right m-text-left"><?= $model['paket']->attributeLabels()['id_pengguna_jasa'] ?></div>
        <div class="box-10 m-padding-x-0 f-bold"><?= $model['paket']->id_pengguna_jasa ? $model['paket']->penggunaJasa->nama_pengguna_jasa : '<span class="f-light f-italic">(kosong)</span>' ?></div>
    </div>
        
    <div class="box box-break-sm margin-bottom-10">
        <div class="box-2 padding-x-0 text-right m-text-left"><?= $model['paket']->attributeLabels()['id_instansi'] ?></div>
        <div class="box-10 m-padding-x-0 f-bold"><?= $model['paket']->id_instansi ? $model['paket']->instansi->nama_instansi : '<span class="f-light f-italic">(kosong)</span>' ?></div>
    </div>
        
    <div class="box box-break-sm margin-bottom-10">
        <div class="box-2 padding-x-0 text-right m-text-left"><?= $model['paket']->attributeLabels()['id_satminkal'] ?></div>
        <div class="box-10 m-padding-x-0 f-bold"><?= $model['paket']->id_satminkal ? $model['paket']->satminkal->nama_satminkal : '<span class="f-light f-italic">(kosong)</span>' ?></div>
    </div>
        
    <div class="box box-break-sm margin-bottom-10">
        <div class="box-2 padding-x-0 text-right m-text-left"><?= $model['paket']->attributeLabels()['id_satker'] ?></div>
        <div class="box-10 m-padding-x-0 f-bold"><?= $model['paket']->id_satker ? $model['paket']->satker->nama_satker : '<span class="f-light f-italic">(kosong)</span>' ?></div>
    </div>
        
    <div class="box box-break-sm margin-bottom-10">
        <div class="box-2 padding-x-0 text-right m-text-left"><?= $model['paket']->attributeLabels()['id_bidang_jasa'] ?></div>
        <div class="box-10 m-padding-x-0 f-bold"><?= $model['paket']->id_bidang_jasa ? $model['paket']->bidangJasa->nama_bidang_jasa : '<span class="f-light f-italic">(kosong)</span>' ?></div>
    </div>
        
    <div class="box box-break-sm margin-bottom-10">
        <div class="box-2 padding-x-0 text-right m-text-left"><?= $model['paket']->attributeLabels()['id_sub_bidang_jasa'] ?></div>
        <div class="box-10 m-padding-x-0 f-bold"><?= $model['paket']->id_sub_bidang_jasa ? $model['paket']->subBidangJasa->nama_sub_bidang_jasa : '<span class="f-light f-italic">(kosong)</span>' ?></div>
    </div>
        
    <div class="box box-break-sm margin-bottom-10">
        <div class="box-2 padding-x-0 text-right m-text-left"><?= $model['paket']->attributeLabels()['tahun_anggaran'] ?></div>
        <div class="box-10 m-padding-x-0 f-bold"><?= $model['paket']->tahun_anggaran ? $model['paket']->tahun_anggaran : '<span class="f-light f-italic">(kosong)</span>' ?></div>
    </div>
        
    <div class="box box-break-sm margin-bottom-10">
        <div class="box-2 padding-x-0 text-right m-text-left"><?= $model['paket']->attributeLabels()['sumber_anggaran'] ?></div>
        <div class="box-10 m-padding-x-0 f-bold"><?= $model['paket']->sumber_anggaran ? $model['paket']->sumber_anggaran : '<span class="f-light f-italic">(kosong)</span>' ?></div>
    </div>
        
    <div class="box box-break-sm margin-bottom-10">
        <div class="box-2 padding-x-0 text-right m-text-left"><?= $model['paket']->attributeLabels()['pagu'] ?></div>
        <div class="box-10 m-padding-x-0 f-bold"><?= $model['paket']->pagu ? $model['paket']->pagu : '<span class="f-light f-italic">(kosong)</span>' ?></div>
    </div>
        
    <div class="box box-break-sm margin-bottom-10">
        <div class="box-2 padding-x-0 text-right m-text-left"><?= $model['paket']->attributeLabels()['hps'] ?></div>
        <div class="box-10 m-padding-x-0 f-bold"><?= $model['paket']->hps ? $model['paket']->hps : '<span class="f-light f-italic">(kosong)</span>' ?></div>
    </div>
        
    <div class="box box-break-sm margin-bottom-10">
        <div class="box-2 padding-x-0 text-right m-text-left"><?= $model['paket']->attributeLabels()['bobot_teknis'] ?></div>
        <div class="box-10 m-padding-x-0 f-bold"><?= $model['paket']->bobot_teknis ? $model['paket']->bobot_teknis : '<span class="f-light f-italic">(kosong)</span>' ?></div>
    </div>
        
    <div class="box box-break-sm margin-bottom-10">
        <div class="box-2 padding-x-0 text-right m-text-left"><?= $model['paket']->attributeLabels()['bobot_biaya'] ?></div>
        <div class="box-10 m-padding-x-0 f-bold"><?= $model['paket']->bobot_biaya ? $model['paket']->bobot_biaya : '<span class="f-light f-italic">(kosong)</span>' ?></div>
    </div>
        
    <div class="box box-break-sm margin-bottom-10">
        <div class="box-2 padding-x-0 text-right m-text-left"><?= $model['paket']->attributeLabels()['is_kso'] ?></div>
        <div class="box-10 m-padding-x-0 f-bold"><?= $model['paket']->is_kso ? $model['paket']->is_kso : '<span class="f-light f-italic">(kosong)</span>' ?></div>
    </div>

    <?php foreach ($model['paket']->paketMitras as $key => $paketMitra) : ?>
    <div class="box box-break-sm margin-bottom-10">
        <div class="box-2 padding-x-0 text-right m-text-left">Mitra <?= $key+1 ?></div>
        <div class="box-10 m-padding-x-0 f-bold"><?= $paketMitra->mitra->nama_perusahaan ?> <span class="f-normal margin-left-10">Porsi: <?= $paketMitra->porsi ?> %</span></div>
    </div>
    <?php endforeach; ?>
</div>