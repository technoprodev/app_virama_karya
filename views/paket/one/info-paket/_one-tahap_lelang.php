<?php

use yii\helpers\Html;
?>

<div>
    <table class="table table-striped table-hover">
        <thead>
            <tr role="row">
                <th class="text-dark f-normal sorting" style="border-bottom: 1px" rowspan="1" colspan="1">Tahap</th>
                <th class="text-dark f-normal sorting" style="border-bottom: 1px" rowspan="1" colspan="1">Mulai</th>
                <th class="text-dark f-normal sorting" style="border-bottom: 1px" rowspan="1" colspan="1">Sampai</th>
            </tr>
        </thead>
        <tbody>
            <?php if (isset($model['paket_tahap_jadwal'])) foreach ($model['paket_tahap_jadwal'] as $key => $value): ?>
                <?php
                    $bold = '';
                    if (
                        time() >= strtotime(str_replace('/', '-', $model['paket_tahap_jadwal'][$key]->tanggal_mulai)) and
                        time() <= strtotime(str_replace('/', '-', $model['paket_tahap_jadwal'][$key]->tanggal_selesai))
                    ) $bold = 'class="f-bold"';
                ?>
                <tr <?= $bold ?>>
                    <td><?= $model['paket_tahap_jadwal'][$key]->tahap->nama_tahap; ?></td>
                    <td><?= $model['paket_tahap_jadwal'][$key]->tanggal_mulai; ?></td>
                    <td><?= $model['paket_tahap_jadwal'][$key]->tanggal_selesai; ?></td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
</div>