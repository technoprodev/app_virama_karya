<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

if (isset($title)) $this->title = $title;
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;

//
$paketTenagaAhlis = [];
if (isset($model['paket_tenaga_ahli']))
    foreach ($model['paket_tenaga_ahli'] as $key => $paketTenagaAhli) {
        // $paketTenagaAhli->jenjang;
        // $paketTenagaAhli->jurusan;
        // $paketTenagaAhli->tahun_lulus;
        // $paketTenagaAhli->keahlian;
        // $paketTenagaAhli->level;
        $temp['jenjang'] = $paketTenagaAhli->jenjang;
        $temp['jurusan'] = $paketTenagaAhli->jurusan;
        $temp['tahun_lulus'] = $paketTenagaAhli->tahun_lulus;
        $temp['keahlian'] = $paketTenagaAhli->keahlian;
        $temp['level'] = $paketTenagaAhli->level;
        $temp['sertifikat'] = $paketTenagaAhli->sertifikat;
        foreach ($paketTenagaAhli->attributes as $k => $value) {
            $temp[$k] = $value;
        }
        $paketTenagaAhlis[] = $temp;
        // ddx($paketTenagaAhlis);
    }

$this->registerJs(
    // 'vm.$data.paket.virtual_category = ' . json_encode($model['paket']->virtual_category) . ';' .
    'vm.$data.paket.paketTenagaAhlis = vm.$data.paket.paketTenagaAhlis.concat(' . json_encode($paketTenagaAhlis) . ');' .
    '',
    3
);

$error = false;
$errorMessage = '';
if ($model['paket']->hasErrors()) {
    $error = true; 
    $errorMessage .= Html::errorSummary($model['paket'], ['class' => '']);
}

$this->registerJsFile('@web/app/paket/form-update-tenaga_ahli.js', ['depends' => 'technosmart\assets_manager\DatatablesAsset']);
?>

<?php if (!Yii::$app->request->isAjax) : ?>
<div class="box box-break-sm margin-left-30 m-margin-left-0">
    <div class="box-12 m-padding-x-0">
<?php endif; ?>

<h6 class="text-rose padding-bottom-5 border-bottom f-italic">Tenaga Ahli</h6>

<table class="datatables display nowrap table table-striped table-hover table-condensed">
    <thead>
        <tr>
            <th class="text-dark f-normal" style="border-bottom: 1px"></th>
            <th class="text-dark f-normal" style="border-bottom: 1px">Nama</th>
            <th class="text-dark f-normal" style="border-bottom: 1px">Tanggal Lahir</th>
            <th class="text-dark f-normal" style="border-bottom: 1px">Jenis</th>
            <th class="text-dark f-normal" style="border-bottom: 1px">Kewarganegaraan</th>
            <th class="text-dark f-normal" style="border-bottom: 1px">Jenjang</th>
            <th class="text-dark f-normal" style="border-bottom: 1px">Jurusan</th>
            <th class="text-dark f-normal" style="border-bottom: 1px">Rumpun</th>
            <th class="text-dark f-normal" style="border-bottom: 1px">Tahun Lulus</th>
            <th class="text-dark f-normal" style="border-bottom: 1px">Nomor Sertifikat</th>
            <th class="text-dark f-normal" style="border-bottom: 1px">Keahlian</th>
            <th class="text-dark f-normal" style="border-bottom: 1px">Level</th>
            <th class="text-dark f-normal" style="border-bottom: 1px">Sertifikat</th>
        </tr>
        <tr class="dt-search">
            <th class="padding-0"></th>
            <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Search nama" class="form-control border-none f-normal padding-x-5"/></th>
            <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Search tanggal lahir" class="form-control border-none f-normal padding-x-5"/></th>
            <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Search jenis" class="form-control border-none f-normal padding-x-5"/></th>
            <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Search kewarganegaraan" class="form-control border-none f-normal padding-x-5"/></th>
            <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Search jenjang" class="form-control border-none f-normal padding-x-5"/></th>
            <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Search jurusan" class="form-control border-none f-normal padding-x-5"/></th>
            <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Search rumpun" class="form-control border-none f-normal padding-x-5"/></th>
            <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Search tahun lulus" class="form-control border-none f-normal padding-x-5"/></th>
            <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Search sertifikat" class="form-control border-none f-normal padding-x-5"/></th>
            <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Search keahlian" class="form-control border-none f-normal padding-x-5"/></th>
            <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Search level" class="form-control border-none f-normal padding-x-5"/></th>
            <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Search sertifikat" class="form-control border-none f-normal padding-x-5"/></th>
        </tr>
    </thead>
</table>

<div class="margin-y-30"></div>

<h6 class="text-rose padding-bottom-5 border-bottom f-italic">Tenaga Ahli Pilihan</h6>

<?php $form = ActiveForm::begin(['options' => ['id' => 'app', 'class' => 'margin-top-30 margin-bottom-80']]); ?>
  
    <?php if ($error) : ?>
        <div class="alert alert-danger">
            <?= $errorMessage ?>
        </div>
    <?php endif; ?>

    <?php if (isset($model['paket_tenaga_ahli'])) foreach ($model['paket_tenaga_ahli'] as $key => $value): ?>
        <?= $form->field($model['paket_tenaga_ahli'][$key], "[$key]nama")->begin(); ?>
        <?= $form->field($model['paket_tenaga_ahli'][$key], "[$key]nama")->end(); ?>
        <?= $form->field($model['paket_tenaga_ahli'][$key], "[$key]tanggal_lahir")->begin(); ?>
        <?= $form->field($model['paket_tenaga_ahli'][$key], "[$key]tanggal_lahir")->end(); ?>
        <?= $form->field($model['paket_tenaga_ahli'][$key], "[$key]jenis")->begin(); ?>
        <?= $form->field($model['paket_tenaga_ahli'][$key], "[$key]jenis")->end(); ?>
        <?= $form->field($model['paket_tenaga_ahli'][$key], "[$key]kewarganegaraan")->begin(); ?>
        <?= $form->field($model['paket_tenaga_ahli'][$key], "[$key]kewarganegaraan")->end(); ?>
    <?php endforeach; ?>

    <template v-if="typeof paket.paketTenagaAhlis == 'object'">
        <div class="box box-break-sm margin-bottom-5 hidden-sm-less">
            <div class="box-3 padding-left-0 padding-y-5">
                Nama
            </div>
            <div class="box-2 padding-left-0 padding-y-5">
                Inputan
            </div>
            <div class="box-1 padding-left-0 padding-y-5">
                Jenjang
            </div>
            <div class="box-2 padding-left-0 padding-y-5">
                Jurusan
            </div>
            <div class="box-1 padding-left-0 padding-y-5">
                Tahun Lulus
            </div>
            <div class="box-2 padding-left-0 padding-y-5">
                Keahlian
            </div>
            <div class="box-1 padding-left-0 padding-y-5">
                Level
            </div>
        </div>
        <template v-for="(value, key, index) in paket.paketTenagaAhlis">
            <div v-show="!(value.id < 0)">
                <input type="hidden" v-bind:id="'pakettenagaahli-' + key + '-id'" v-bind:name="'PaketTenagaAhli[' + key + '][id]'" class="form-control" type="text" v-model="paket.paketTenagaAhlis[key].id">
                <input type="hidden" v-bind:id="'pakettenagaahli-' + key + '-id_tenaga_ahli'" v-bind:name="'PaketTenagaAhli[' + key + '][id_tenaga_ahli]'" class="form-control" type="text" v-model="paket.paketTenagaAhlis[key].id_tenaga_ahli">
                <div class="box box-break-sm">
                    <div class="box-3 padding-left-0 padding-y-5 f-bold">
                        <!-- <div v-bind:class="'form-group form-group-sm field-pakettenagaahli-' + key + '-nama'">
                            <input placeholder="nama" v-bind:id="'pakettenagaahli-' + key + '-nama'" v-bind:name="'PaketTenagaAhli[' + key + '][nama]'" class="form-control" type="text" v-model="paket.paketTenagaAhlis[key].nama">
                            <div class="help-block"></div>
                        </div> -->
                        
                        <div class="margin-bottom-5">{{key + 1}}. {{paket.paketTenagaAhlis[key].nama}}</div>
                        <div class="margin-bottom-5">{{paket.paketTenagaAhlis[key].tanggal_lahir}}</div>
                        <div class="margin-bottom-5">{{paket.paketTenagaAhlis[key].jenis}}</div>
                        <div class="margin-bottom-5">{{paket.paketTenagaAhlis[key].kewarganegaraan}}</div>
                        <div v-on:click="removePaketTenagaAhli(key)" class="btn btn-default btn-sm bg-light-red hover-pointer"><i class="fa fa-close"></i></div>
                    </div>
                    <div class="box-2 padding-left-0 padding-y-5 f-bold">
                        <div v-bind:class="'form-group form-group-sm field-pakettenagaahli-' + key + '-id_penugasan'">
                            <select v-bind:id="'pakettenagaahli-' + key + '-id_penugasan'" v-bind:name="'PaketTenagaAhli[' + key + '][id_penugasan]'" class="form-control" v-model="paket.paketTenagaAhlis[key].id_penugasan">
                                <option value="">Pilih penugasan</option>
                                <?php foreach (ArrayHelper::map(\app_virama_karya\models\Penugasan::find()->asArray()->all(), 'id', 'nama_penugasan') as $key => $value) : ?>
                                    <option value="<?= $key ?>"><?= $value ?></option>
                                <?php endforeach; ?>
                            </select>
                            <div class="help-block"></div>
                        </div>
                        <div v-bind:class="'form-group form-group-sm field-pakettenagaahli-' + key + '-periode_mulai'">
                            <input placeholder="periode_mulai" v-bind:id="'pakettenagaahli-' + key + '-periode_mulai'" v-bind:name="'PaketTenagaAhli[' + key + '][periode_mulai]'" class="form-control" type="text" v-model="paket.paketTenagaAhlis[key].periode_mulai">
                            <div class="help-block"></div>
                        </div>
                        <div v-bind:class="'form-group form-group-sm field-pakettenagaahli-' + key + '-periode_selesai'">
                            <input placeholder="periode_selesai" v-bind:id="'pakettenagaahli-' + key + '-periode_selesai'" v-bind:name="'PaketTenagaAhli[' + key + '][periode_selesai]'" class="form-control" type="text" v-model="paket.paketTenagaAhlis[key].periode_selesai">
                            <div class="help-block"></div>
                        </div>
                    </div>
                    <div class="box-1 padding-left-0 padding-y-5 f-bold">
                        {{ paket.paketTenagaAhlis[key].jenjang | enter }}
                    </div>
                    <div class="box-2 padding-left-0 padding-y-5 f-bold">
                        {{ paket.paketTenagaAhlis[key].jurusan | enter }}
                    </div>
                    <div class="box-1 padding-left-0 padding-y-5 f-bold">
                        {{ paket.paketTenagaAhlis[key].tahun_lulus | enter }}
                    </div>
                    <div class="box-2 padding-left-0 padding-y-5 f-bold">
                        {{ paket.paketTenagaAhlis[key].keahlian | enter }}
                    </div>
                    <div class="box-1 padding-left-0 padding-y-5 f-bold">
                        {{ paket.paketTenagaAhlis[key].level | enter }}
                    </div>
                </div>
            </div>
        </template>
    </template>

    <hr class="margin-y-15">

    <?php if ($error) : ?>
        <div class="alert alert-danger">
            <?= $errorMessage ?>
        </div>
    <?php endif; ?>
    
    <div class="form-group clearfix">
        <?= Html::submitButton('Submit', ['class' => 'btn btn-default bg-azure rounded-xs border-azure']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default bg-lighter rounded-xs']); ?> 
        <?= Html::a('Kembali ke detail', ['index', 'id' => $model['paket']->id], ['class' => 'btn btn-default bg-lightest rounded-xs pull-right']) ?>
    </div>
    
<?php ActiveForm::end(); ?>

<?php if (!Yii::$app->request->isAjax) : ?>
    </div>
</div>
<?php endif; ?>