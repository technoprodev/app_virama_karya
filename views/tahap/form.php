<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

if (isset($title)) $this->title = $title;
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;

$error = false;
$errorMessage = '';
if ($model['tahap']->hasErrors()) {
    $error = true; 
    $errorMessage .= Html::errorSummary($model['tahap'], ['class' => '']);
}
?>

<?php if (!Yii::$app->request->isAjax) : ?>
<div class="box box-break-sm margin-left-30 m-margin-left-0">
    <div class="box-8">
<?php endif; ?>

<?php $form = ActiveForm::begin(['enableClientValidation' => true, 'options' => ['id' => 'app']]); ?>
  
    <?php if ($error) : ?>
        <div class="alert alert-danger">
            <?= $errorMessage ?>
        </div>
    <?php endif; ?>

    <?= $form->field($model['tahap'], 'nama_tahap')->begin(); ?>
        <?= Html::activeLabel($model['tahap'], 'nama_tahap', ['class' => 'control-label']); ?>
        <?= Html::activeTextInput($model['tahap'], 'nama_tahap', ['class' => 'form-control', 'maxlength' => true]) ?>
        <?= Html::error($model['tahap'], 'nama_tahap', ['class' => 'help-block']); ?>
    <?= $form->field($model['tahap'], 'nama_tahap')->end(); ?>

    <?= $form->field($model['tahap'], 'status')->begin(); ?>
        <?= Html::activeLabel($model['tahap'], 'status', ['class' => 'control-label']); ?>
        <?= Html::activeDropDownList($model['tahap'], 'status', [ 'Info Paket' => 'Info Paket', 'Prakualifikasi' => 'Prakualifikasi', 'Tidak Shortlist' => 'Tidak Shortlist', 'Proposal' => 'Proposal', 'Kontrak' => 'Kontrak', 'Kalah' => 'Kalah', 'Mundur' => 'Mundur', ], ['prompt' => 'Choose one please', 'class' => 'form-control']) ?>
        <?= Html::error($model['tahap'], 'status', ['class' => 'help-block']); ?>
    <?= $form->field($model['tahap'], 'status')->end(); ?>


    <hr class="margin-y-15">

    <?php if ($error) : ?>
        <div class="alert alert-danger">
            <?= $errorMessage ?>
        </div>
    <?php endif; ?>
    
    <div class="form-group clearfix">
        <?= Html::submitButton($model['tahap']->isNewRecord ? 'Create' : 'Update', ['class' => 'btn btn-default bg-azure rounded-xs border-azure']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default bg-lighter rounded-xs']); ?> 
        <?= Html::a('Back to list', ['index'], ['class' => 'btn btn-default bg-lightest rounded-xs pull-right']) ?>
    </div>
    
<?php ActiveForm::end(); ?>

<?php if (!Yii::$app->request->isAjax) : ?>
    </div>
</div>
<?php endif; ?>