<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

if (isset($title)) $this->title = $title;
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;

$error = false;
$errorMessage = '';
if ($model['bidang-unit-kerja']->hasErrors()) {
    $error = true; 
    $errorMessage .= Html::errorSummary($model['bidang-unit-kerja'], ['class' => '']);
}
?>

<?php if (!Yii::$app->request->isAjax) : ?>
<div class="box box-break-sm margin-left-30 m-margin-left-0">
    <div class="box-8">
<?php endif; ?>

<?php $form = ActiveForm::begin(['enableClientValidation' => true, 'options' => ['id' => 'app']]); ?>
  
    <?php if ($error) : ?>
        <div class="alert alert-danger">
            <?= $errorMessage ?>
        </div>
    <?php endif; ?>

    <?= $form->field($model['bidang-unit-kerja'], 'nama_bidang_unit_kerja')->begin(); ?>
        <?= Html::activeLabel($model['bidang-unit-kerja'], 'nama_bidang_unit_kerja', ['class' => 'control-label']); ?>
        <?= Html::activeTextInput($model['bidang-unit-kerja'], 'nama_bidang_unit_kerja', ['class' => 'form-control', 'maxlength' => true]) ?>
        <?= Html::error($model['bidang-unit-kerja'], 'nama_bidang_unit_kerja', ['class' => 'help-block']); ?>
    <?= $form->field($model['bidang-unit-kerja'], 'nama_bidang_unit_kerja')->end(); ?>

    <?= $form->field($model['bidang-unit-kerja'], 'id_unit_kerja')->begin(); ?>
        <?= Html::activeLabel($model['bidang-unit-kerja'], 'id_unit_kerja', ['class' => 'control-label']); ?>
        <?= Html::activeTextInput($model['bidang-unit-kerja'], 'id_unit_kerja', ['class' => 'form-control']) ?>
        <?= Html::error($model['bidang-unit-kerja'], 'id_unit_kerja', ['class' => 'help-block']); ?>
    <?= $form->field($model['bidang-unit-kerja'], 'id_unit_kerja')->end(); ?>


    <hr class="margin-y-15">

    <?php if ($error) : ?>
        <div class="alert alert-danger">
            <?= $errorMessage ?>
        </div>
    <?php endif; ?>
    
    <div class="form-group clearfix">
        <?= Html::submitButton($model['bidang-unit-kerja']->isNewRecord ? 'Create' : 'Update', ['class' => 'btn btn-default bg-azure rounded-xs border-azure']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default bg-lighter rounded-xs']); ?> 
        <?= Html::a('Back to list', ['index'], ['class' => 'btn btn-default bg-lightest rounded-xs pull-right']) ?>
    </div>
    
<?php ActiveForm::end(); ?>

<?php if (!Yii::$app->request->isAjax) : ?>
    </div>
</div>
<?php endif; ?>