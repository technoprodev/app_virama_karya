<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

if (isset($title)) $this->title = $title;
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;

$error = false;
$errorMessage = '';
if ($model['pengguna-jasa']->hasErrors()) {
    $error = true; 
    $errorMessage .= Html::errorSummary($model['pengguna-jasa'], ['class' => '']);
}
?>

<?php if (!Yii::$app->request->isAjax) : ?>
<div class="box box-break-sm margin-left-30 m-margin-left-0">
    <div class="box-8">
<?php endif; ?>

<?php $form = ActiveForm::begin(['enableClientValidation' => true, 'options' => ['id' => 'app']]); ?>
  
    <?php if ($error) : ?>
        <div class="alert alert-danger">
            <?= $errorMessage ?>
        </div>
    <?php endif; ?>

    <?= $form->field($model['pengguna-jasa'], 'nama_pengguna_jasa')->begin(); ?>
        <?= Html::activeLabel($model['pengguna-jasa'], 'nama_pengguna_jasa', ['class' => 'control-label']); ?>
        <?= Html::activeTextInput($model['pengguna-jasa'], 'nama_pengguna_jasa', ['class' => 'form-control', 'maxlength' => true]) ?>
        <?= Html::error($model['pengguna-jasa'], 'nama_pengguna_jasa', ['class' => 'help-block']); ?>
    <?= $form->field($model['pengguna-jasa'], 'nama_pengguna_jasa')->end(); ?>


    <hr class="margin-y-15">

    <?php if ($error) : ?>
        <div class="alert alert-danger">
            <?= $errorMessage ?>
        </div>
    <?php endif; ?>
    
    <div class="form-group clearfix">
        <?= Html::submitButton($model['pengguna-jasa']->isNewRecord ? 'Create' : 'Update', ['class' => 'btn btn-default bg-azure rounded-xs border-azure']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default bg-lighter rounded-xs']); ?> 
        <?= Html::a('Back to list', ['index'], ['class' => 'btn btn-default bg-lightest rounded-xs pull-right']) ?>
    </div>
    
<?php ActiveForm::end(); ?>

<?php if (!Yii::$app->request->isAjax) : ?>
    </div>
</div>
<?php endif; ?>