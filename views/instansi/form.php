<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

if (isset($title)) $this->title = $title;
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;

$error = false;
$errorMessage = '';
if ($model['instansi']->hasErrors()) {
    $error = true; 
    $errorMessage .= Html::errorSummary($model['instansi'], ['class' => '']);
}
?>

<?php if (!Yii::$app->request->isAjax) : ?>
<div class="box box-break-sm margin-left-30 m-margin-left-0">
    <div class="box-8">
<?php endif; ?>

<?php $form = ActiveForm::begin(['enableClientValidation' => true, 'options' => ['id' => 'app']]); ?>
  
    <?php if ($error) : ?>
        <div class="alert alert-danger">
            <?= $errorMessage ?>
        </div>
    <?php endif; ?>

    <?= $form->field($model['instansi'], 'nama_instansi')->begin(); ?>
        <?= Html::activeLabel($model['instansi'], 'nama_instansi', ['class' => 'control-label']); ?>
        <?= Html::activeTextInput($model['instansi'], 'nama_instansi', ['class' => 'form-control', 'maxlength' => true]) ?>
        <?= Html::error($model['instansi'], 'nama_instansi', ['class' => 'help-block']); ?>
    <?= $form->field($model['instansi'], 'nama_instansi')->end(); ?>

    <?= $form->field($model['instansi'], 'alamat')->begin(); ?>
        <?= Html::activeLabel($model['instansi'], 'alamat', ['class' => 'control-label']); ?>
        <?= Html::activeTextArea($model['instansi'], 'alamat', ['class' => 'form-control', 'rows' => 6]) ?>
        <?= Html::error($model['instansi'], 'alamat', ['class' => 'help-block']); ?>
    <?= $form->field($model['instansi'], 'alamat')->end(); ?>

    <?= $form->field($model['instansi'], 'id_pengguna_jasa')->begin(); ?>
        <?= Html::activeLabel($model['instansi'], 'id_pengguna_jasa', ['class' => 'control-label']); ?>
        <?= Html::activeTextInput($model['instansi'], 'id_pengguna_jasa', ['class' => 'form-control']) ?>
        <?= Html::error($model['instansi'], 'id_pengguna_jasa', ['class' => 'help-block']); ?>
    <?= $form->field($model['instansi'], 'id_pengguna_jasa')->end(); ?>


    <hr class="margin-y-15">

    <?php if ($error) : ?>
        <div class="alert alert-danger">
            <?= $errorMessage ?>
        </div>
    <?php endif; ?>
    
    <div class="form-group clearfix">
        <?= Html::submitButton($model['instansi']->isNewRecord ? 'Create' : 'Update', ['class' => 'btn btn-default bg-azure rounded-xs border-azure']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default bg-lighter rounded-xs']); ?> 
        <?= Html::a('Back to list', ['index'], ['class' => 'btn btn-default bg-lightest rounded-xs pull-right']) ?>
    </div>
    
<?php ActiveForm::end(); ?>

<?php if (!Yii::$app->request->isAjax) : ?>
    </div>
</div>
<?php endif; ?>