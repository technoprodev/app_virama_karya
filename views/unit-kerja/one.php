<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

if (isset($title)) $this->title = $title;
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;
?>

<?php if (!Yii::$app->request->isAjax) : ?>
<div class="box box-break-sm margin-top-15">
    <div class="box-6">
<?php endif; ?>

<div class="box box-break-sm margin-bottom-10">
    <div class="box-2 padding-x-0 text-right m-text-left"><?= $model['unit-kerja']->attributeLabels()['id'] ?></div>
    <div class="box-10 m-padding-x-0 text-dark"><?= $model['unit-kerja']->id ? $model['unit-kerja']->id : '<span class="text-gray f-italic">(kosong)</span>' ?></div>
</div>
    
<div class="box box-break-sm margin-bottom-10">
    <div class="box-2 padding-x-0 text-right m-text-left"><?= $model['unit-kerja']->attributeLabels()['nama_unit_kerja'] ?></div>
    <div class="box-10 m-padding-x-0 text-dark"><?= $model['unit-kerja']->nama_unit_kerja ? $model['unit-kerja']->nama_unit_kerja : '<span class="text-gray f-italic">(kosong)</span>' ?></div>
</div>
    
<div class="box box-break-sm margin-bottom-10">
    <div class="box-2 padding-x-0 text-right m-text-left"><?= $model['unit-kerja']->attributeLabels()['type'] ?></div>
    <div class="box-10 m-padding-x-0 text-dark"><?= $model['unit-kerja']->type ? $model['unit-kerja']->type : '<span class="text-gray f-italic">(kosong)</span>' ?></div>
</div>
    
<div class="box box-break-sm margin-bottom-10">
    <div class="box-2 padding-x-0 text-right m-text-left"><?= $model['unit-kerja']->attributeLabels()['alamat'] ?></div>
    <div class="box-10 m-padding-x-0 text-dark"><?= $model['unit-kerja']->alamat ? $model['unit-kerja']->alamat : '<span class="text-gray f-italic">(kosong)</span>' ?></div>
</div>
    
<div class="box box-break-sm margin-bottom-10">
    <div class="box-2 padding-x-0 text-right m-text-left"><?= $model['unit-kerja']->attributeLabels()['nomor_telepon'] ?></div>
    <div class="box-10 m-padding-x-0 text-dark"><?= $model['unit-kerja']->nomor_telepon ? $model['unit-kerja']->nomor_telepon : '<span class="text-gray f-italic">(kosong)</span>' ?></div>
</div>
    
<div class="box box-break-sm margin-bottom-10">
    <div class="box-2 padding-x-0 text-right m-text-left"><?= $model['unit-kerja']->attributeLabels()['nomor_fax'] ?></div>
    <div class="box-10 m-padding-x-0 text-dark"><?= $model['unit-kerja']->nomor_fax ? $model['unit-kerja']->nomor_fax : '<span class="text-gray f-italic">(kosong)</span>' ?></div>
</div>
    
<div class="box box-break-sm margin-bottom-10">
    <div class="box-2 padding-x-0 text-right m-text-left"><?= $model['unit-kerja']->attributeLabels()['email'] ?></div>
    <div class="box-10 m-padding-x-0 text-dark"><?= $model['unit-kerja']->email ? $model['unit-kerja']->email : '<span class="text-gray f-italic">(kosong)</span>' ?></div>
</div>
    
<?php if (!Yii::$app->request->isAjax) : ?>
        <hr class="margin-y-15">
        <div class="form-group clearfix">
            <?= Html::a('Update', ['update', 'id' => $model['unit-kerja']->id], ['class' => 'btn btn-sm btn-default bg-azure rounded-xs border-azure']) ?>&nbsp;
            <?= Html::a('Delete', ['delete', 'id' => $model['unit-kerja']->id], [
                'class' => 'btn btn-sm btn-default bg-lighter rounded-xs',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item ?',
                    'method' => 'post',
                ],
            ]) ?>
            <?= Html::a('Back to list', ['index'], ['class' => 'btn btn-sm btn-default bg-lighter rounded-xs pull-right']) ?>
        </div>
    </div>
</div>
<?php endif; ?>