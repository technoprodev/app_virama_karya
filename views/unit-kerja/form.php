<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

if (isset($title)) $this->title = $title;
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;

$error = false;
$errorMessage = '';
if ($model['unit-kerja']->hasErrors()) {
    $error = true; 
    $errorMessage .= Html::errorSummary($model['unit-kerja'], ['class' => '']);
}
?>

<?php if (!Yii::$app->request->isAjax) : ?>
<div class="box box-break-sm margin-left-30 m-margin-left-0">
    <div class="box-8">
<?php endif; ?>

<?php $form = ActiveForm::begin(['enableClientValidation' => true, 'options' => ['id' => 'app']]); ?>
  
    <?php if ($error) : ?>
        <div class="alert alert-danger">
            <?= $errorMessage ?>
        </div>
    <?php endif; ?>

    <?= $form->field($model['unit-kerja'], 'nama_unit_kerja')->begin(); ?>
        <?= Html::activeLabel($model['unit-kerja'], 'nama_unit_kerja', ['class' => 'control-label']); ?>
        <?= Html::activeTextInput($model['unit-kerja'], 'nama_unit_kerja', ['class' => 'form-control', 'maxlength' => true]) ?>
        <?= Html::error($model['unit-kerja'], 'nama_unit_kerja', ['class' => 'help-block']); ?>
    <?= $form->field($model['unit-kerja'], 'nama_unit_kerja')->end(); ?>

    <?= $form->field($model['unit-kerja'], 'type')->begin(); ?>
        <?= Html::activeLabel($model['unit-kerja'], 'type', ['class' => 'control-label']); ?>
        <?= Html::activeDropDownList($model['unit-kerja'], 'type', [ 'divisi' => 'Divisi', 'cabang' => 'Cabang', ], ['prompt' => 'Choose one please', 'class' => 'form-control']) ?>
        <?= Html::error($model['unit-kerja'], 'type', ['class' => 'help-block']); ?>
    <?= $form->field($model['unit-kerja'], 'type')->end(); ?>

    <?= $form->field($model['unit-kerja'], 'alamat')->begin(); ?>
        <?= Html::activeLabel($model['unit-kerja'], 'alamat', ['class' => 'control-label']); ?>
        <?= Html::activeTextArea($model['unit-kerja'], 'alamat', ['class' => 'form-control', 'rows' => 6]) ?>
        <?= Html::error($model['unit-kerja'], 'alamat', ['class' => 'help-block']); ?>
    <?= $form->field($model['unit-kerja'], 'alamat')->end(); ?>

    <?= $form->field($model['unit-kerja'], 'nomor_telepon')->begin(); ?>
        <?= Html::activeLabel($model['unit-kerja'], 'nomor_telepon', ['class' => 'control-label']); ?>
        <?= Html::activeTextInput($model['unit-kerja'], 'nomor_telepon', ['class' => 'form-control', 'maxlength' => true]) ?>
        <?= Html::error($model['unit-kerja'], 'nomor_telepon', ['class' => 'help-block']); ?>
    <?= $form->field($model['unit-kerja'], 'nomor_telepon')->end(); ?>

    <?= $form->field($model['unit-kerja'], 'nomor_fax')->begin(); ?>
        <?= Html::activeLabel($model['unit-kerja'], 'nomor_fax', ['class' => 'control-label']); ?>
        <?= Html::activeTextInput($model['unit-kerja'], 'nomor_fax', ['class' => 'form-control', 'maxlength' => true]) ?>
        <?= Html::error($model['unit-kerja'], 'nomor_fax', ['class' => 'help-block']); ?>
    <?= $form->field($model['unit-kerja'], 'nomor_fax')->end(); ?>

    <?= $form->field($model['unit-kerja'], 'email')->begin(); ?>
        <?= Html::activeLabel($model['unit-kerja'], 'email', ['class' => 'control-label']); ?>
        <?= Html::activeTextArea($model['unit-kerja'], 'email', ['class' => 'form-control', 'rows' => 6]) ?>
        <?= Html::error($model['unit-kerja'], 'email', ['class' => 'help-block']); ?>
    <?= $form->field($model['unit-kerja'], 'email')->end(); ?>


    <hr class="margin-y-15">

    <?php if ($error) : ?>
        <div class="alert alert-danger">
            <?= $errorMessage ?>
        </div>
    <?php endif; ?>
    
    <div class="form-group clearfix">
        <?= Html::submitButton($model['unit-kerja']->isNewRecord ? 'Create' : 'Update', ['class' => 'btn btn-default bg-azure rounded-xs border-azure']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default bg-lighter rounded-xs']); ?> 
        <?= Html::a('Back to list', ['index'], ['class' => 'btn btn-default bg-lightest rounded-xs pull-right']) ?>
    </div>
    
<?php ActiveForm::end(); ?>

<?php if (!Yii::$app->request->isAjax) : ?>
    </div>
</div>
<?php endif; ?>