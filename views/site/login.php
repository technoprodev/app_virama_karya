<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

if (isset($title)) $this->title = $title;
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;

$error = false;
$errorMessage = '';
if ($model['login']->hasErrors()) {
    $error = true; 
    $errorMessage .= Html::errorSummary($model['login'], ['class' => '']);
}
?>

<div class="text-center margin-top-60 m-margin-top-30">
    <img src="<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/logo-inverse.jpg" width="250px;" class="bg-lightest padding-5">
</div>
<div class="margin-top-20 border-lighter border-thick shadow rounded-xs" style="max-width: 350px; width: 100%; margin-left: auto; margin-right: auto;">
    <div class="clearfix padding-15 border-bottom bg-azure text-center fs-18">
        <span class="f-italic"><?= $this->title ?></span>
    </div>
    <div class="padding-20 bg-lightest">
        <?php $form = ActiveForm::begin(['id' => 'app']); ?>

        <?= $form->field($model['login'], 'login')->begin(); ?>
            <?= Html::activeLabel($model['login'], 'login', ['class' => 'control-label fs-12']); ?>
            <?= Html::activeTextInput($model['login'], 'login', ['class' => 'form-control', 'maxlength' => true]); ?>
            <?= Html::error($model['login'], 'login', ['class' => 'help-block help-block-error']); ?>
        <?= $form->field($model['login'], 'login')->end(); ?>

        <?= $form->field($model['login'], 'password')->begin(); ?>
            <?= Html::activeLabel($model['login'], 'password', ['class' => 'control-label fs-12']); ?>
            <?= Html::activePasswordInput($model['login'], 'password', ['class' => 'form-control', 'maxlength' => true]); ?>
            <?= Html::error($model['login'], 'password', ['class' => 'help-block help-block-error']); ?>
        <?= $form->field($model['login'], 'password')->end(); ?>

        <?= $form->field($model['login'], 'rememberMe')->begin(); ?>
            <div class="checkbox">
                <?= Html::activeCheckbox($model['login'], 'rememberMe', ['uncheck' => 0]); ?>
            </div>
            <?= Html::error($model['login'], 'rememberMe', ['class' => 'help-block help-block-error']); ?>
        <?= $form->field($model['login'], 'rememberMe')->end(); ?>

        <?php if ($error) : ?>
            <div class="alert alert-danger">
                <?= $errorMessage ?>
            </div>
        <?php endif; ?>

        <div class="form-group">
            <?= Html::submitButton('Login', ['class' => 'btn border-azure bg-azure hover-bg-light-azure']) ?>
        </div>

        <?php ActiveForm::end(); ?>
    </div>
    <div class="bg-lighter padding-y-10">
        <div class="text-center text-azure fs-18"><?= Yii::$app->params['app.name'] ?> © <?= date('Y') ?></div>
    </div>
</div>