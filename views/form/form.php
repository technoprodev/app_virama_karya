<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

if (isset($title)) $this->title = $title;
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;

//
$formKomisarises = [];
if (isset($model['form_komisaris']))
    foreach ($model['form_komisaris'] as $key => $formKomisaris)
        $formKomisarises[] = $formKomisaris->attributes;
$formDireksis = [];
if (isset($model['form_direksi']))
    foreach ($model['form_direksi'] as $key => $formDireksi)
        $formDireksis[] = $formDireksi->attributes;
$formSbuks = [];
if (isset($model['form_sbuk']))
    foreach ($model['form_sbuk'] as $key => $formSbuk)
        $formSbuks[] = $formSbuk->attributes;
$formSbunks = [];
if (isset($model['form_sbunk']))
    foreach ($model['form_sbunk'] as $key => $formSbunk)
        $formSbunks[] = $formSbunk->attributes;
$formIjinLainnyas = [];
if (isset($model['form_ijin_lainnya']))
    foreach ($model['form_ijin_lainnya'] as $key => $formIjinLainnya)
        $formIjinLainnyas[] = $formIjinLainnya->attributes;
$formPemilikSahams = [];
if (isset($model['form_pemilik_saham']))
    foreach ($model['form_pemilik_saham'] as $key => $formPemilikSaham)
        $formPemilikSahams[] = $formPemilikSaham->attributes;
$formPajaks = [];
if (isset($model['form_pajak']))
    foreach ($model['form_pajak'] as $key => $formPajak)
        $formPajaks[] = $formPajak->attributes;
$formBuktiPajaks = [];
if (isset($model['form_bukti_pajak']))
    foreach ($model['form_bukti_pajak'] as $key => $formBuktiPajak)
        $formBuktiPajaks[] = $formBuktiPajak->attributes;

$this->registerJs(
    'vm.$data.form.formKomisarises = vm.$data.form.formKomisarises.concat(' . json_encode($formKomisarises) . ');' .
    'vm.$data.form.formDireksis = vm.$data.form.formDireksis.concat(' . json_encode($formDireksis) . ');' .
    'vm.$data.form.formSbuks = vm.$data.form.formSbuks.concat(' . json_encode($formSbuks) . ');' .
    'vm.$data.form.formSbunks = vm.$data.form.formSbunks.concat(' . json_encode($formSbunks) . ');' .
    'vm.$data.form.formIjinLainnyas = vm.$data.form.formIjinLainnyas.concat(' . json_encode($formIjinLainnyas) . ');' .
    'vm.$data.form.formPemilikSahams = vm.$data.form.formPemilikSahams.concat(' . json_encode($formPemilikSahams) . ');' .
    'vm.$data.form.formPajaks = vm.$data.form.formPajaks.concat(' . json_encode($formPajaks) . ');' .
    'vm.$data.form.formBuktiPajaks = vm.$data.form.formBuktiPajaks.concat(' . json_encode($formBuktiPajaks) . ');' .
    '',
    3
);

$error = false;
$errorMessage = '';
$errorVue = false;
if ($model['form']->hasErrors()) {
    $error = true; 
    $errorMessage .= Html::errorSummary($model['form'], ['class' => '']);
}

if (isset($model['form_komisaris'])) foreach ($model['form_komisaris'] as $key => $formKomisaris) {
    if ($formKomisaris->hasErrors()) {
        $error = true;
        $errorMessage .= Html::errorSummary($formKomisaris, ['class' => '']);
        $errorVue = true; 
    }
}
if (isset($model['form_direksi'])) foreach ($model['form_direksi'] as $key => $formDireksi) {
    if ($formDireksi->hasErrors()) {
        $error = true;
        $errorMessage .= Html::errorSummary($formDireksi, ['class' => '']);
        $errorVue = true; 
    }
}
if (isset($model['form_sbuk'])) foreach ($model['form_sbuk'] as $key => $formSbuk) {
    if ($formSbuk->hasErrors()) {
        $error = true;
        $errorMessage .= Html::errorSummary($formSbuk, ['class' => '']);
        $errorVue = true; 
    }
}
if (isset($model['form_sbunk'])) foreach ($model['form_sbunk'] as $key => $formSbunk) {
    if ($formSbunk->hasErrors()) {
        $error = true;
        $errorMessage .= Html::errorSummary($formSbunk, ['class' => '']);
        $errorVue = true; 
    }
}
if (isset($model['form_ijin_lainnya'])) foreach ($model['form_ijin_lainnya'] as $key => $formIjinLainnya) {
    if ($formIjinLainnya->hasErrors()) {
        $error = true;
        $errorMessage .= Html::errorSummary($formIjinLainnya, ['class' => '']);
        $errorVue = true; 
    }
}
if (isset($model['form_pemilik_saham'])) foreach ($model['form_pemilik_saham'] as $key => $formPemilikSaham) {
    if ($formPemilikSaham->hasErrors()) {
        $error = true;
        $errorMessage .= Html::errorSummary($formPemilikSaham, ['class' => '']);
        $errorVue = true; 
    }
}
if (isset($model['form_pajak'])) foreach ($model['form_pajak'] as $key => $formPajak) {
    if ($formPajak->hasErrors()) {
        $error = true;
        $errorMessage .= Html::errorSummary($formPajak, ['class' => '']);
        $errorVue = true; 
    }
}
if (isset($model['form_bukti_pajak'])) foreach ($model['form_bukti_pajak'] as $key => $formBuktiPajak) {
    if ($formBuktiPajak->hasErrors()) {
        $error = true;
        $errorMessage .= Html::errorSummary($formBuktiPajak, ['class' => '']);
        $errorVue = true; 
    }
}
if ($errorVue) {
    $this->registerJs(
        '$.each($("#app").data("yiiActiveForm").attributes, function() {
            this.status = 3;
        });
        $("#app").yiiActiveForm("validate");',
        5
    );
}
?>

<?php if (!Yii::$app->request->isAjax) : ?>
<div class="box box-break-sm margin-left-30 m-margin-left-0">
    <div class="box-11">
<?php endif; ?>

<?php $form = ActiveForm::begin(['enableClientValidation' => true, 'options' => ['id' => 'app']]); ?>
  
    <?php if ($error) : ?>
        <div class="alert alert-danger">
            <?= $errorMessage ?>
        </div>
    <?php endif; ?>

<div class="border-azure margin-bottom-30">
    <div class="bg-azure padding-x-15 padding-y-5">
        <span class="fs-16">Landasan Hukum Pendirian Badan Usaha</span>
    </div>
    <div class="padding-x-15 padding-y-10">
        <h6 class="text-rose padding-bottom-5 border-bottom f-italic">Akta Pendirian Perusahaan</h6>

        <?= $form->field($model['form'], 'app_nomor_akta', ['options' => ['class' => 'form-group form-group-sm box box-break-sm margin-bottom-10']])->begin(); ?>
            <div class="box-2 padding-x-0 text-right m-text-left padding-y-5">
                <?= Html::activeLabel($model['form'], 'app_nomor_akta', ['class' => 'control-label']); ?>
            </div>
            <div class="box-10 m-padding-x-0">
                <?= Html::activeTextInput($model['form'], 'app_nomor_akta', ['class' => 'form-control']) ?>
                <?= Html::error($model['form'], 'app_nomor_akta', ['class' => 'help-block']); ?>
            </div>
        <?= $form->field($model['form'], 'app_nomor_akta')->end(); ?>

        <?= $form->field($model['form'], 'app_tanggal', ['options' => ['class' => 'form-group form-group-sm box box-break-sm margin-bottom-10']])->begin(); ?>
            <div class="box-2 padding-x-0 text-right m-text-left padding-y-5">
                <?= Html::activeLabel($model['form'], 'app_tanggal', ['class' => 'control-label']); ?>
            </div>
            <div class="box-10 m-padding-x-0">
                <?= Html::activeTextInput($model['form'], 'app_tanggal', ['class' => 'form-control']) ?>
                <?= Html::error($model['form'], 'app_tanggal', ['class' => 'help-block']); ?>
            </div>
        <?= $form->field($model['form'], 'app_tanggal')->end(); ?>

        <?= $form->field($model['form'], 'app_nama_notaris', ['options' => ['class' => 'form-group form-group-sm box box-break-sm margin-bottom-10']])->begin(); ?>
            <div class="box-2 padding-x-0 text-right m-text-left padding-y-5">
                <?= Html::activeLabel($model['form'], 'app_nama_notaris', ['class' => 'control-label']); ?>
            </div>
            <div class="box-10 m-padding-x-0">
                <?= Html::activeTextInput($model['form'], 'app_nama_notaris', ['class' => 'form-control', 'maxlength' => true]) ?>
                <?= Html::error($model['form'], 'app_nama_notaris', ['class' => 'help-block']); ?>
            </div>
        <?= $form->field($model['form'], 'app_nama_notaris')->end(); ?>

        <?= $form->field($model['form'], 'app_nomor_pengesahan', ['options' => ['class' => 'form-group form-group-sm box box-break-sm margin-bottom-10']])->begin(); ?>
            <div class="box-2 padding-x-0 text-right m-text-left padding-y-5">
                <?= Html::activeLabel($model['form'], 'app_nomor_pengesahan', ['class' => 'control-label']); ?>
            </div>
            <div class="box-10 m-padding-x-0">
                <?= Html::activeTextInput($model['form'], 'app_nomor_pengesahan', ['class' => 'form-control', 'maxlength' => true]) ?>
                <?= Html::error($model['form'], 'app_nomor_pengesahan', ['class' => 'help-block']); ?>
            </div>
        <?= $form->field($model['form'], 'app_nomor_pengesahan')->end(); ?>

        <?= $form->field($model['form'], 'app_tanggal_pengesahan', ['options' => ['class' => 'form-group form-group-sm box box-break-sm margin-bottom-10']])->begin(); ?>
            <div class="box-2 padding-x-0 text-right m-text-left padding-y-5">
                <?= Html::activeLabel($model['form'], 'app_tanggal_pengesahan', ['class' => 'control-label']); ?>
            </div>
            <div class="box-10 m-padding-x-0">
                <?= Html::activeTextInput($model['form'], 'app_tanggal_pengesahan', ['class' => 'form-control']) ?>
                <?= Html::error($model['form'], 'app_tanggal_pengesahan', ['class' => 'help-block']); ?>
            </div>
        <?= $form->field($model['form'], 'app_tanggal_pengesahan')->end(); ?>

        <h6 class="text-rose padding-bottom-5 border-bottom f-italic">Akta Perubahan Terakhir</h6>

        <?= $form->field($model['form'], 'apt_nomor_akta', ['options' => ['class' => 'form-group form-group-sm box box-break-sm margin-bottom-10']])->begin(); ?>
            <div class="box-2 padding-x-0 text-right m-text-left padding-y-5">
                <?= Html::activeLabel($model['form'], 'apt_nomor_akta', ['class' => 'control-label']); ?>
            </div>
            <div class="box-10 m-padding-x-0">
                <?= Html::activeTextInput($model['form'], 'apt_nomor_akta', ['class' => 'form-control']) ?>
                <?= Html::error($model['form'], 'apt_nomor_akta', ['class' => 'help-block']); ?>
            </div>
        <?= $form->field($model['form'], 'apt_nomor_akta')->end(); ?>

        <?= $form->field($model['form'], 'apt_tanggal', ['options' => ['class' => 'form-group form-group-sm box box-break-sm margin-bottom-10']])->begin(); ?>
            <div class="box-2 padding-x-0 text-right m-text-left padding-y-5">
                <?= Html::activeLabel($model['form'], 'apt_tanggal', ['class' => 'control-label']); ?>
            </div>
            <div class="box-10 m-padding-x-0">
                <?= Html::activeTextInput($model['form'], 'apt_tanggal', ['class' => 'form-control']) ?>
                <?= Html::error($model['form'], 'apt_tanggal', ['class' => 'help-block']); ?>
            </div>
        <?= $form->field($model['form'], 'apt_tanggal')->end(); ?>

        <?= $form->field($model['form'], 'apt_nama_notaris', ['options' => ['class' => 'form-group form-group-sm box box-break-sm margin-bottom-10']])->begin(); ?>
            <div class="box-2 padding-x-0 text-right m-text-left padding-y-5">
                <?= Html::activeLabel($model['form'], 'apt_nama_notaris', ['class' => 'control-label']); ?>
            </div>
            <div class="box-10 m-padding-x-0">
                <?= Html::activeTextInput($model['form'], 'apt_nama_notaris', ['class' => 'form-control', 'maxlength' => true]) ?>
                <?= Html::error($model['form'], 'apt_nama_notaris', ['class' => 'help-block']); ?>
            </div>
        <?= $form->field($model['form'], 'apt_nama_notaris')->end(); ?>

        <?= $form->field($model['form'], 'apt_nomor_pengesahan', ['options' => ['class' => 'form-group form-group-sm box box-break-sm margin-bottom-10']])->begin(); ?>
            <div class="box-2 padding-x-0 text-right m-text-left padding-y-5">
                <?= Html::activeLabel($model['form'], 'apt_nomor_pengesahan', ['class' => 'control-label']); ?>
            </div>
            <div class="box-10 m-padding-x-0">
                <?= Html::activeTextInput($model['form'], 'apt_nomor_pengesahan', ['class' => 'form-control', 'maxlength' => true]) ?>
                <?= Html::error($model['form'], 'apt_nomor_pengesahan', ['class' => 'help-block']); ?>
            </div>
        <?= $form->field($model['form'], 'apt_nomor_pengesahan')->end(); ?>

        <?= $form->field($model['form'], 'apt_tanggal_pengesahan', ['options' => ['class' => 'form-group form-group-sm box box-break-sm margin-bottom-10']])->begin(); ?>
            <div class="box-2 padding-x-0 text-right m-text-left padding-y-5">
                <?= Html::activeLabel($model['form'], 'apt_tanggal_pengesahan', ['class' => 'control-label']); ?>
            </div>
            <div class="box-10 m-padding-x-0">
                <?= Html::activeTextInput($model['form'], 'apt_tanggal_pengesahan', ['class' => 'form-control']) ?>
                <?= Html::error($model['form'], 'apt_tanggal_pengesahan', ['class' => 'help-block']); ?>
            </div>
        <?= $form->field($model['form'], 'apt_tanggal_pengesahan')->end(); ?>
    </div>
</div>

<div class="border-azure margin-bottom-30">
    <div class="bg-azure padding-x-15 padding-y-5">
        <span class="fs-16">Pengurus Badan Usaha</span>
    </div>
    <div class="padding-x-15 padding-y-10">
        <h6 class="text-rose padding-bottom-5 border-bottom f-italic">Komisaris</h6>
        
        <?php if (isset($model['form_komisaris'])) foreach ($model['form_komisaris'] as $key => $value): ?>
            <?= $form->field($model['form_komisaris'][$key], "[$key]nama")->begin(); ?>
            <?= $form->field($model['form_komisaris'][$key], "[$key]nama")->end(); ?>
            <?= $form->field($model['form_komisaris'][$key], "[$key]nomor_ktp")->begin(); ?>
            <?= $form->field($model['form_komisaris'][$key], "[$key]nomor_ktp")->end(); ?>
            <?= $form->field($model['form_komisaris'][$key], "[$key]jabatan")->begin(); ?>
            <?= $form->field($model['form_komisaris'][$key], "[$key]jabatan")->end(); ?>
        <?php endforeach; ?>

        <template v-if="typeof form.formKomisarises == 'object'">
            <div class="box box-break-sm margin-bottom-5 hidden-sm-less">
                <div class="box-1 padding-x-0 padding-y-5 text-center">
                    No
                </div>
                <div class="box-4 padding-x-0 padding-y-5 text-center">
                    Nama
                </div>
                <div class="box-3 padding-x-0 padding-y-5 text-center">
                    No. KTP
                </div>
                <div class="box-3 padding-x-0 padding-y-5 text-center">
                    Jabatan
                </div>
                <div class="box-1 padding-x-0">
                    <a v-on:click="addFormKomisaris" class="btn btn-default btn-sm margin-left-5 bg-azure hover-pointer"><i class="fa fa-plus"></i></a>
                </div>
            </div>
            <template v-for="(value, key, index) in form.formKomisarises">
                <div v-show="!(value.id < 0)">
                    <div class="box box-break-sm">
                        <div class="box-1 padding-x-0 padding-y-5 text-center m-text-left">
                            <input type="hidden" v-bind:id="'formkomisaris-' + key + '-id'" v-bind:name="'FormKomisaris[' + key + '][id]'" class="form-control" type="text" v-model="form.formKomisarises[key].id">
                            {{key + 1}}
                        </div>
                        <div class="box-4 padding-x-0">
                            <div v-bind:class="'form-group form-group-sm field-formkomisaris-' + key + '-nama'">
                                <input placeholder="nama" v-bind:id="'formkomisaris-' + key + '-nama'" v-bind:name="'FormKomisaris[' + key + '][nama]'" class="form-control" type="text" v-model="form.formKomisarises[key].nama">
                                <div class="help-block"></div>
                            </div>
                        </div>
                        <div class="box-3 padding-x-0">
                            <div v-bind:class="'form-group form-group-sm field-formkomisaris-' + key + '-nomor_ktp'">
                                <input placeholder="nomor_ktp" v-bind:id="'formkomisaris-' + key + '-nomor_ktp'" v-bind:name="'FormKomisaris[' + key + '][nomor_ktp]'" class="form-control" type="text" v-model="form.formKomisarises[key].nomor_ktp">
                                <div class="help-block"></div>
                            </div>
                        </div>
                        <div class="box-3 padding-x-0">
                            <div v-bind:class="'form-group form-group-sm field-formkomisaris-' + key + '-jabatan'">
                                <select v-bind:id="'formkomisaris-' + key + '-jabatan'" v-bind:name="'FormKomisaris[' + key + '][jabatan]'" class="form-control" v-model="form.formKomisarises[key].jabatan">
                                    <option value="">Pilih jabatan</option>
                                    <?php foreach ((new \app_virama_karya\models\FormKomisaris())->getEnum('jabatan') as $key => $value) : ?>
                                        <option value="<?= $value ?>"><?= $value ?></option>
                                    <?php endforeach; ?>
                                </select>
                                <div class="help-block"></div>
                            </div>
                        </div>
                        <div class="box-1 padding-x-0">
                            <div v-on:click="removeFormKomisaris(key)" class="btn btn-default btn-sm margin-left-5 bg-light-red hover-pointer"><i class="fa fa-close"></i></div>
                        </div>
                    </div>
                </div>
            </template>
        </template>

        <h6 class="text-rose padding-bottom-5 border-bottom f-italic">Direksi</h6>

        <?php if (isset($model['form_direksi'])) foreach ($model['form_direksi'] as $key => $value): ?>
            <?= $form->field($model['form_direksi'][$key], "[$key]nama")->begin(); ?>
            <?= $form->field($model['form_direksi'][$key], "[$key]nama")->end(); ?>
            <?= $form->field($model['form_direksi'][$key], "[$key]nomor_ktp")->begin(); ?>
            <?= $form->field($model['form_direksi'][$key], "[$key]nomor_ktp")->end(); ?>
            <?= $form->field($model['form_direksi'][$key], "[$key]jabatan")->begin(); ?>
            <?= $form->field($model['form_direksi'][$key], "[$key]jabatan")->end(); ?>
        <?php endforeach; ?>

        <template v-if="typeof form.formDireksis == 'object'">
            <div class="box box-break-sm margin-bottom-5 hidden-sm-less">
                <div class="box-1 padding-x-0 padding-y-5 text-center">
                    No
                </div>
                <div class="box-4 padding-x-0 padding-y-5 text-center">
                    Nama
                </div>
                <div class="box-3 padding-x-0 padding-y-5 text-center">
                    No. KTP
                </div>
                <div class="box-3 padding-x-0 padding-y-5 text-center">
                    Jabatan
                </div>
                <div class="box-1 padding-x-0">
                    <a v-on:click="addFormDireksi" class="btn btn-default btn-sm margin-left-5 bg-azure hover-pointer"><i class="fa fa-plus"></i></a>
                </div>
            </div>
            <template v-for="(value, key, index) in form.formDireksis">
                <div v-show="!(value.id < 0)">
                    <div class="box box-break-sm">
                        <div class="box-1 padding-x-0 padding-y-5 text-center m-text-left">
                            <input type="hidden" v-bind:id="'formdireksi-' + key + '-id'" v-bind:name="'FormDireksi[' + key + '][id]'" class="form-control" type="text" v-model="form.formDireksis[key].id">
                            {{key + 1}}
                        </div>
                        <div class="box-4 padding-x-0">
                            <div v-bind:class="'form-group form-group-sm field-formdireksi-' + key + '-nama'">
                                <input placeholder="nama" v-bind:id="'formdireksi-' + key + '-nama'" v-bind:name="'FormDireksi[' + key + '][nama]'" class="form-control" type="text" v-model="form.formDireksis[key].nama">
                                <div class="help-block"></div>
                            </div>
                        </div>
                        <div class="box-3 padding-x-0">
                            <div v-bind:class="'form-group form-group-sm field-formdireksi-' + key + '-nomor_ktp'">
                                <input placeholder="nomor_ktp" v-bind:id="'formdireksi-' + key + '-nomor_ktp'" v-bind:name="'FormDireksi[' + key + '][nomor_ktp]'" class="form-control" type="text" v-model="form.formDireksis[key].nomor_ktp">
                                <div class="help-block"></div>
                            </div>
                        </div>
                        <div class="box-3 padding-x-0">
                            <div v-bind:class="'form-group form-group-sm field-formdireksi-' + key + '-jabatan'">
                                <select v-bind:id="'formdireksi-' + key + '-jabatan'" v-bind:name="'FormDireksi[' + key + '][jabatan]'" class="form-control" v-model="form.formDireksis[key].jabatan">
                                    <option value="">Pilih jabatan</option>
                                    <?php foreach ((new \app_virama_karya\models\FormDireksi())->getEnum('jabatan') as $key => $value) : ?>
                                        <option value="<?= $value ?>"><?= $value ?></option>
                                    <?php endforeach; ?>
                                </select>
                                <div class="help-block"></div>
                            </div>
                        </div>
                        <div class="box-1 padding-x-0">
                            <div v-on:click="removeFormDireksi(key)" class="btn btn-default btn-sm margin-left-5 bg-light-red hover-pointer"><i class="fa fa-close"></i></div>
                        </div>
                    </div>
                </div>
            </template>
        </template>
    </div>
</div>

<div class="border-azure margin-bottom-30">
    <div class="bg-azure padding-x-15 padding-y-5">
        <span class="fs-16">Ijin Usaha</span>
    </div>
    <div class="padding-x-15 padding-y-10">
        <h6 class="text-rose padding-bottom-5 border-bottom f-italic">Ijin Usaha Jasa Konstruksi/Konsultan (IUJK)</h6>

        <?= $form->field($model['form'], 'iujk_berlaku_mulai', ['options' => ['class' => 'form-group form-group-sm box box-break-sm margin-bottom-10']])->begin(); ?>
            <div class="box-2 padding-x-0 text-right m-text-left padding-y-5">
                <?= Html::activeLabel($model['form'], 'iujk_berlaku_mulai', ['class' => 'control-label']); ?>
            </div>
            <div class="box-10 m-padding-x-0">
                <?= Html::activeTextInput($model['form'], 'iujk_berlaku_mulai', ['class' => 'form-control']) ?>
                <?= Html::error($model['form'], 'iujk_berlaku_mulai', ['class' => 'help-block']); ?>
            </div>
        <?= $form->field($model['form'], 'iujk_berlaku_mulai')->end(); ?>

        <?= $form->field($model['form'], 'iujk_berlaku_sampai', ['options' => ['class' => 'form-group form-group-sm box box-break-sm margin-bottom-10']])->begin(); ?>
            <div class="box-2 padding-x-0 text-right m-text-left padding-y-5">
                <?= Html::activeLabel($model['form'], 'iujk_berlaku_sampai', ['class' => 'control-label']); ?>
            </div>
            <div class="box-10 m-padding-x-0">
                <?= Html::activeTextInput($model['form'], 'iujk_berlaku_sampai', ['class' => 'form-control']) ?>
                <?= Html::error($model['form'], 'iujk_berlaku_sampai', ['class' => 'help-block']); ?>
            </div>
        <?= $form->field($model['form'], 'iujk_berlaku_sampai')->end(); ?>

        <?= $form->field($model['form'], 'iujk_pemberi_izin', ['options' => ['class' => 'form-group form-group-sm box box-break-sm margin-bottom-10']])->begin(); ?>
            <div class="box-2 padding-x-0 text-right m-text-left padding-y-5">
                <?= Html::activeLabel($model['form'], 'iujk_pemberi_izin', ['class' => 'control-label']); ?>
            </div>
            <div class="box-10 m-padding-x-0">
                <?= Html::activeTextInput($model['form'], 'iujk_pemberi_izin', ['class' => 'form-control', 'maxlength' => true]) ?>
                <?= Html::error($model['form'], 'iujk_pemberi_izin', ['class' => 'help-block']); ?>
            </div>
        <?= $form->field($model['form'], 'iujk_pemberi_izin')->end(); ?>

        <?= $form->field($model['form'], 'iujk_perencanaan', ['options' => ['class' => 'form-group form-group-sm box box-break-sm margin-bottom-10']])->begin(); ?>
            <div class="box-2 padding-x-0 text-right m-text-left padding-y-5">
                <?= Html::activeLabel($model['form'], 'iujk_perencanaan', ['class' => 'control-label']); ?>
            </div>
            <div class="box-10 m-padding-x-0">
                <?= Html::activeTextInput($model['form'], 'iujk_perencanaan', ['class' => 'form-control', 'maxlength' => true]) ?>
                <?= Html::error($model['form'], 'iujk_perencanaan', ['class' => 'help-block']); ?>
            </div>
        <?= $form->field($model['form'], 'iujk_perencanaan')->end(); ?>

        <?= $form->field($model['form'], 'iujk_pengawasan', ['options' => ['class' => 'form-group form-group-sm box box-break-sm margin-bottom-10']])->begin(); ?>
            <div class="box-2 padding-x-0 text-right m-text-left padding-y-5">
                <?= Html::activeLabel($model['form'], 'iujk_pengawasan', ['class' => 'control-label']); ?>
            </div>
            <div class="box-10 m-padding-x-0">
                <?= Html::activeTextInput($model['form'], 'iujk_pengawasan', ['class' => 'form-control', 'maxlength' => true]) ?>
                <?= Html::error($model['form'], 'iujk_pengawasan', ['class' => 'help-block']); ?>
            </div>
        <?= $form->field($model['form'], 'iujk_pengawasan')->end(); ?>

        <?= $form->field($model['form'], 'iujk_konsultansi', ['options' => ['class' => 'form-group form-group-sm box box-break-sm margin-bottom-10']])->begin(); ?>
            <div class="box-2 padding-x-0 text-right m-text-left padding-y-5">
                <?= Html::activeLabel($model['form'], 'iujk_konsultansi', ['class' => 'control-label']); ?>
            </div>
            <div class="box-10 m-padding-x-0">
                <?= Html::activeTextInput($model['form'], 'iujk_konsultansi', ['class' => 'form-control', 'maxlength' => true]) ?>
                <?= Html::error($model['form'], 'iujk_konsultansi', ['class' => 'help-block']); ?>
            </div>
        <?= $form->field($model['form'], 'iujk_konsultansi')->end(); ?>

        <h6 class="text-rose padding-bottom-5 border-bottom f-italic">Sertifikat Badan Usaha (SBU) Konstruksi</h6>

        <?= $form->field($model['form'], 'sbuk_berlaku_mulai', ['options' => ['class' => 'form-group form-group-sm box box-break-sm margin-bottom-10']])->begin(); ?>
            <div class="box-2 padding-x-0 text-right m-text-left padding-y-5">
                <?= Html::activeLabel($model['form'], 'sbuk_berlaku_mulai', ['class' => 'control-label']); ?>
            </div>
            <div class="box-10 m-padding-x-0">
                <?= Html::activeTextInput($model['form'], 'sbuk_berlaku_mulai', ['class' => 'form-control']) ?>
                <?= Html::error($model['form'], 'sbuk_berlaku_mulai', ['class' => 'help-block']); ?>
            </div>
        <?= $form->field($model['form'], 'sbuk_berlaku_mulai')->end(); ?>

        <?= $form->field($model['form'], 'sbuk_berlaku_sampai', ['options' => ['class' => 'form-group form-group-sm box box-break-sm margin-bottom-10']])->begin(); ?>
            <div class="box-2 padding-x-0 text-right m-text-left padding-y-5">
                <?= Html::activeLabel($model['form'], 'sbuk_berlaku_sampai', ['class' => 'control-label']); ?>
            </div>
            <div class="box-10 m-padding-x-0">
                <?= Html::activeTextInput($model['form'], 'sbuk_berlaku_sampai', ['class' => 'form-control']) ?>
                <?= Html::error($model['form'], 'sbuk_berlaku_sampai', ['class' => 'help-block']); ?>
            </div>
        <?= $form->field($model['form'], 'sbuk_berlaku_sampai')->end(); ?>

        <?= $form->field($model['form'], 'sbuk_pemberi_izin', ['options' => ['class' => 'form-group form-group-sm box box-break-sm margin-bottom-10']])->begin(); ?>
            <div class="box-2 padding-x-0 text-right m-text-left padding-y-5">
                <?= Html::activeLabel($model['form'], 'sbuk_pemberi_izin', ['class' => 'control-label']); ?>
            </div>
            <div class="box-10 m-padding-x-0">
                <?= Html::activeTextInput($model['form'], 'sbuk_pemberi_izin', ['class' => 'form-control', 'maxlength' => true]) ?>
                <?= Html::error($model['form'], 'sbuk_pemberi_izin', ['class' => 'help-block']); ?>
            </div>
        <?= $form->field($model['form'], 'sbuk_pemberi_izin')->end(); ?>

        <?php if (isset($model['sbuk'])) foreach ($model['sbuk'] as $key => $value): ?>
            <?= $form->field($model['sbuk'][$key], "[$key]sertifikat")->begin(); ?>
            <?= $form->field($model['sbuk'][$key], "[$key]sertifikat")->end(); ?>
            <?= $form->field($model['sbuk'][$key], "[$key]nomor_sertifikat")->begin(); ?>
            <?= $form->field($model['sbuk'][$key], "[$key]nomor_sertifikat")->end(); ?>
        <?php endforeach; ?>

        <template v-if="typeof form.formSbuks == 'object'">
            <div class="box box-break-sm margin-bottom-5 hidden-sm-less">
                <div class="box-1 padding-x-0 padding-y-5 text-center">
                    No
                </div>
                <div class="box-3 padding-x-0 padding-y-5 text-center">
                    Sertifikat
                </div>
                <div class="box-7 padding-x-0 padding-y-5 text-center">
                    Nomor
                </div>
                <div class="box-1 padding-x-0">
                    <a v-on:click="addFormSbuk" class="btn btn-default btn-sm margin-left-5 bg-azure hover-pointer"><i class="fa fa-plus"></i></a>
                </div>
            </div>
            <template v-for="(value, key, index) in form.formSbuks">
                <div v-show="!(value.id < 0)">
                    <div class="box box-break-sm">
                        <div class="box-1 padding-x-0 padding-y-5 text-center m-text-left">
                            <input type="hidden" v-bind:id="'formsbuk-' + key + '-id'" v-bind:name="'FormSbuk[' + key + '][id]'" class="form-control" type="text" v-model="form.formSbuks[key].id">
                            {{key + 1}}
                        </div>
                        <div class="box-3 padding-x-0">
                            <div v-bind:class="'form-group form-group-sm field-formsbuk-' + key + '-sertifikat'">
                                <input placeholder="sertifikat" v-bind:id="'formsbuk-' + key + '-sertifikat'" v-bind:name="'FormSbuk[' + key + '][sertifikat]'" class="form-control" type="text" v-model="form.formSbuks[key].sertifikat">
                                <div class="help-block"></div>
                            </div>
                        </div>
                        <div class="box-7 padding-x-0">
                            <div v-bind:class="'form-group form-group-sm field-formsbuk-' + key + '-nomor_sertifikat'">
                                <input placeholder="nomor_sertifikat" v-bind:id="'formsbuk-' + key + '-nomor_sertifikat'" v-bind:name="'FormSbuk[' + key + '][nomor_sertifikat]'" class="form-control" type="text" v-model="form.formSbuks[key].nomor_sertifikat">
                                <div class="help-block"></div>
                            </div>
                        </div>
                        <div class="box-1 padding-x-0">
                            <div v-on:click="removeFormSbuk(key)" class="btn btn-default btn-sm margin-left-5 bg-light-red hover-pointer"><i class="fa fa-close"></i></div>
                        </div>
                    </div>
                </div>
            </template>
        </template>

        <h6 class="text-rose padding-bottom-5 border-bottom f-italic">Sertifikat Badan Usaha (SBU) Non Konstruksi</h6>

        <?php if (isset($model['sbunk'])) foreach ($model['sbunk'] as $key => $value): ?>
            <?= $form->field($model['sbunk'][$key], "[$key]sertifikat")->begin(); ?>
            <?= $form->field($model['sbunk'][$key], "[$key]sertifikat")->end(); ?>
            <?= $form->field($model['sbunk'][$key], "[$key]nomor_sertifikat")->begin(); ?>
            <?= $form->field($model['sbunk'][$key], "[$key]nomor_sertifikat")->end(); ?>
            <?= $form->field($model['sbunk'][$key], "[$key]berlaku_mulai")->begin(); ?>
            <?= $form->field($model['sbunk'][$key], "[$key]berlaku_mulai")->end(); ?>
            <?= $form->field($model['sbunk'][$key], "[$key]berlaku_sampai")->begin(); ?>
            <?= $form->field($model['sbunk'][$key], "[$key]berlaku_sampai")->end(); ?>
            <?= $form->field($model['sbunk'][$key], "[$key]pemberi_izin")->begin(); ?>
            <?= $form->field($model['sbunk'][$key], "[$key]pemberi_izin")->end(); ?>
        <?php endforeach; ?>

        <template v-if="typeof form.formSbunks == 'object'">
            <div class="box box-break-sm margin-bottom-5 hidden-sm-less">
                <div class="box-1 padding-x-0 padding-y-5 text-center">
                    No
                </div>
                <div class="box-4 padding-x-0 padding-y-5 text-center">
                    Sertifikat
                </div>
                <div class="box-2 padding-x-0 padding-y-5 text-center">
                    Nomor
                </div>
                <div class="box-2 padding-x-0 padding-y-5 text-center">
                    Pemberi Izin
                </div>
                <div class="box-2 padding-x-0 padding-y-5 text-center">
                    Berlaku
                </div>
                <div class="box-1 padding-x-0">
                    <a v-on:click="addFormSbunk" class="btn btn-default btn-sm margin-left-5 bg-azure hover-pointer"><i class="fa fa-plus"></i></a>
                </div>
            </div>
            <template v-for="(value, key, index) in form.formSbunks">
                <div v-show="!(value.id < 0)">
                    <div class="box box-break-sm">
                        <div class="box-1 padding-x-0 padding-y-5 text-center m-text-left">
                            <input type="hidden" v-bind:id="'formsbunk-' + key + '-id'" v-bind:name="'FormSbunk[' + key + '][id]'" class="form-control" type="text" v-model="form.formSbunks[key].id">
                            {{key + 1}}
                        </div>
                        <div class="box-4 padding-x-0">
                            <div v-bind:class="'form-group form-group-sm field-formsbunk-' + key + '-sertifikat'">
                                <input placeholder="sertifikat" v-bind:id="'formsbunk-' + key + '-sertifikat'" v-bind:name="'FormSbunk[' + key + '][sertifikat]'" class="form-control" type="text" v-model="form.formSbunks[key].sertifikat">
                                <div class="help-block"></div>
                            </div>
                        </div>
                        <div class="box-2 padding-x-0">
                            <div v-bind:class="'form-group form-group-sm field-formsbunk-' + key + '-nomor_sertifikat'">
                                <input placeholder="nomor_sertifikat" v-bind:id="'formsbunk-' + key + '-nomor_sertifikat'" v-bind:name="'FormSbunk[' + key + '][nomor_sertifikat]'" class="form-control" type="text" v-model="form.formSbunks[key].nomor_sertifikat">
                                <div class="help-block"></div>
                            </div>
                        </div>
                        <div class="box-2 padding-x-0">
                            <div v-bind:class="'form-group form-group-sm field-formsbunk-' + key + '-pemberi_izin'">
                                <input placeholder="pemberi_izin" v-bind:id="'formsbunk-' + key + '-pemberi_izin'" v-bind:name="'FormSbunk[' + key + '][pemberi_izin]'" class="form-control" type="text" v-model="form.formSbunks[key].pemberi_izin">
                                <div class="help-block"></div>
                            </div>
                        </div>
                        <div class="box-1 padding-x-0">
                            <div v-bind:class="'form-group form-group-sm field-formsbunk-' + key + '-berlaku_mulai'">
                                <input placeholder="mulai" v-bind:id="'formsbunk-' + key + '-berlaku_mulai'" v-bind:name="'FormSbunk[' + key + '][berlaku_mulai]'" class="form-control" type="text" v-model="form.formSbunks[key].berlaku_mulai">
                                <div class="help-block"></div>
                            </div>
                        </div>
                        <div class="box-1 padding-x-0">
                            <div v-bind:class="'form-group form-group-sm field-formsbunk-' + key + '-berlaku_sampai'">
                                <input placeholder="sampai" v-bind:id="'formsbunk-' + key + '-berlaku_sampai'" v-bind:name="'FormSbunk[' + key + '][berlaku_sampai]'" class="form-control" type="text" v-model="form.formSbunks[key].berlaku_sampai">
                                <div class="help-block"></div>
                            </div>
                        </div>
                        <div class="box-1 padding-x-0">
                            <div v-on:click="removeFormSbunk(key)" class="btn btn-default btn-sm margin-left-5 bg-light-red hover-pointer"><i class="fa fa-close"></i></div>
                        </div>
                    </div>
                </div>
            </template>
        </template>
    </div>
</div>

<div class="border-azure margin-bottom-30">
    <div class="bg-azure padding-x-15 padding-y-5">
        <span class="fs-16">Ijin Usaha Lainnya</span>
    </div>
    <div class="padding-x-15 padding-y-10">
        <?php if (isset($model['ijin_lainnya'])) foreach ($model['ijin_lainnya'] as $key => $value): ?>
            <?= $form->field($model['ijin_lainnya'][$key], "[$key]sertifikat")->begin(); ?>
            <?= $form->field($model['ijin_lainnya'][$key], "[$key]sertifikat")->end(); ?>
            <?= $form->field($model['ijin_lainnya'][$key], "[$key]nomor_sertifikat")->begin(); ?>
            <?= $form->field($model['ijin_lainnya'][$key], "[$key]nomor_sertifikat")->end(); ?>
            <?= $form->field($model['ijin_lainnya'][$key], "[$key]berlaku_mulai")->begin(); ?>
            <?= $form->field($model['ijin_lainnya'][$key], "[$key]berlaku_mulai")->end(); ?>
            <?= $form->field($model['ijin_lainnya'][$key], "[$key]berlaku_sampai")->begin(); ?>
            <?= $form->field($model['ijin_lainnya'][$key], "[$key]berlaku_sampai")->end(); ?>
            <?= $form->field($model['ijin_lainnya'][$key], "[$key]pemberi_izin")->begin(); ?>
            <?= $form->field($model['ijin_lainnya'][$key], "[$key]pemberi_izin")->end(); ?>
        <?php endforeach; ?>

        <template v-if="typeof form.formIjinLainnyas == 'object'">
            <div class="box box-break-sm margin-bottom-5 hidden-sm-less">
                <div class="box-1 padding-x-0 padding-y-5 text-center">
                    No
                </div>
                <div class="box-3 padding-x-0 padding-y-5 text-center">
                    Sertifikat
                </div>
                <div class="box-2 padding-x-0 padding-y-5 text-center">
                    Nomor
                </div>
                <div class="box-3 padding-x-0 padding-y-5 text-center">
                    Pemberi Izin
                </div>
                <div class="box-2 padding-x-0 padding-y-5 text-center">
                    Berlaku
                </div>
                <div class="box-1 padding-x-0">
                    <a v-on:click="addFormIjinLainnya" class="btn btn-default btn-sm margin-left-5 bg-azure hover-pointer"><i class="fa fa-plus"></i></a>
                </div>
            </div>
            <template v-for="(value, key, index) in form.formIjinLainnyas">
                <div v-show="!(value.id < 0)">
                    <div class="box box-break-sm">
                        <div class="box-1 padding-x-0 padding-y-5 text-center m-text-left">
                            <input type="hidden" v-bind:id="'formijinlainnya-' + key + '-id'" v-bind:name="'FormIjinLainnya[' + key + '][id]'" class="form-control" type="text" v-model="form.formIjinLainnyas[key].id">
                            {{key + 1}}
                        </div>
                        <div class="box-3 padding-x-0">
                            <div v-bind:class="'form-group form-group-sm field-formijinlainnya-' + key + '-sertifikat'">
                                <input placeholder="sertifikat" v-bind:id="'formijinlainnya-' + key + '-sertifikat'" v-bind:name="'FormIjinLainnya[' + key + '][sertifikat]'" class="form-control" type="text" v-model="form.formIjinLainnyas[key].sertifikat">
                                <div class="help-block"></div>
                            </div>
                        </div>
                        <div class="box-2 padding-x-0">
                            <div v-bind:class="'form-group form-group-sm field-formijinlainnya-' + key + '-nomor_sertifikat'">
                                <input placeholder="nomor_sertifikat" v-bind:id="'formijinlainnya-' + key + '-nomor_sertifikat'" v-bind:name="'FormIjinLainnya[' + key + '][nomor_sertifikat]'" class="form-control" type="text" v-model="form.formIjinLainnyas[key].nomor_sertifikat">
                                <div class="help-block"></div>
                            </div>
                        </div>
                        <div class="box-3 padding-x-0">
                            <div v-bind:class="'form-group form-group-sm field-formijinlainnya-' + key + '-pemberi_izin'">
                                <input placeholder="pemberi_izin" v-bind:id="'formijinlainnya-' + key + '-pemberi_izin'" v-bind:name="'FormIjinLainnya[' + key + '][pemberi_izin]'" class="form-control" type="text" v-model="form.formIjinLainnyas[key].pemberi_izin">
                                <div class="help-block"></div>
                            </div>
                        </div>
                        <div class="box-1 padding-x-0">
                            <div v-bind:class="'form-group form-group-sm field-formijinlainnya-' + key + '-berlaku_mulai'">
                                <input placeholder="mulai" v-bind:id="'formijinlainnya-' + key + '-berlaku_mulai'" v-bind:name="'FormIjinLainnya[' + key + '][berlaku_mulai]'" class="form-control" type="text" v-model="form.formIjinLainnyas[key].berlaku_mulai">
                                <div class="help-block"></div>
                            </div>
                        </div>
                        <div class="box-1 padding-x-0">
                            <div v-bind:class="'form-group form-group-sm field-formijinlainnya-' + key + '-berlaku_sampai'">
                                <input placeholder="sampai" v-bind:id="'formijinlainnya-' + key + '-berlaku_sampai'" v-bind:name="'FormIjinLainnya[' + key + '][berlaku_sampai]'" class="form-control" type="text" v-model="form.formIjinLainnyas[key].berlaku_sampai">
                                <div class="help-block"></div>
                            </div>
                        </div>
                        <div class="box-1 padding-x-0">
                            <div v-on:click="removeFormIjinLainnya(key)" class="btn btn-default btn-sm margin-left-5 bg-light-red hover-pointer"><i class="fa fa-close"></i></div>
                        </div>
                    </div>
                </div>
            </template>
        </template>
    </div>
</div>

<div class="border-azure margin-bottom-30">
    <div class="bg-azure padding-x-15 padding-y-5">
        <span class="fs-16">Susunan Kepemilikan Saham</span>
    </div>
    <div class="padding-x-15 padding-y-10">
        <?php if (isset($model['form_pemilik_saham'])) foreach ($model['form_pemilik_saham'] as $key => $value): ?>
            <?= $form->field($model['form_pemilik_saham'][$key], "[$key]nama")->begin(); ?>
            <?= $form->field($model['form_pemilik_saham'][$key], "[$key]nama")->end(); ?>
            <?= $form->field($model['form_pemilik_saham'][$key], "[$key]nomor_ktp")->begin(); ?>
            <?= $form->field($model['form_pemilik_saham'][$key], "[$key]nomor_ktp")->end(); ?>
            <?= $form->field($model['form_pemilik_saham'][$key], "[$key]alamat")->begin(); ?>
            <?= $form->field($model['form_pemilik_saham'][$key], "[$key]alamat")->end(); ?>
            <?= $form->field($model['form_pemilik_saham'][$key], "[$key]persentase")->begin(); ?>
            <?= $form->field($model['form_pemilik_saham'][$key], "[$key]persentase")->end(); ?>
        <?php endforeach; ?>

        <template v-if="typeof form.formPemilikSahams == 'object'">
            <div class="box box-break-sm margin-bottom-5 hidden-sm-less">
                <div class="box-1 padding-x-0 padding-y-5 text-center">
                    No
                </div>
                <div class="box-2 padding-x-0 padding-y-5 text-center">
                    Nama
                </div>
                <div class="box-3 padding-x-0 padding-y-5 text-center">
                    No. KTP
                </div>
                <div class="box-3 padding-x-0 padding-y-5 text-center">
                    Alamat
                </div>
                <div class="box-2 padding-x-0 padding-y-5 text-center">
                    Persentase (%)
                </div>
                <div class="box-1 padding-x-0">
                    <a v-on:click="addFormPemilikSaham" class="btn btn-default btn-sm margin-left-5 bg-azure hover-pointer"><i class="fa fa-plus"></i></a>
                </div>
            </div>
            <template v-for="(value, key, index) in form.formPemilikSahams">
                <div v-show="!(value.id < 0)">
                    <div class="box box-break-sm">
                        <div class="box-1 padding-x-0 padding-y-5 text-center m-text-left">
                            <input type="hidden" v-bind:id="'formpemiliksaham-' + key + '-id'" v-bind:name="'FormPemilikSaham[' + key + '][id]'" class="form-control" type="text" v-model="form.formPemilikSahams[key].id">
                            {{key + 1}}
                        </div>
                        <div class="box-3 padding-x-0">
                            <div v-bind:class="'form-group form-group-sm field-formpemiliksaham-' + key + '-nama'">
                                <input placeholder="nama" v-bind:id="'formpemiliksaham-' + key + '-nama'" v-bind:name="'FormPemilikSaham[' + key + '][nama]'" class="form-control" type="text" v-model="form.formPemilikSahams[key].nama">
                                <div class="help-block"></div>
                            </div>
                        </div>
                        <div class="box-2 padding-x-0">
                            <div v-bind:class="'form-group form-group-sm field-formpemiliksaham-' + key + '-nomor_ktp'">
                                <input placeholder="nomor_ktp" v-bind:id="'formpemiliksaham-' + key + '-nomor_ktp'" v-bind:name="'FormPemilikSaham[' + key + '][nomor_ktp]'" class="form-control" type="text" v-model="form.formPemilikSahams[key].nomor_ktp">
                                <div class="help-block"></div>
                            </div>
                        </div>
                        <div class="box-3 padding-x-0">
                            <div v-bind:class="'form-group form-group-sm field-formpemiliksaham-' + key + '-alamat'">
                                <input placeholder="alamat" v-bind:id="'formpemiliksaham-' + key + '-alamat'" v-bind:name="'FormPemilikSaham[' + key + '][alamat]'" class="form-control" type="text" v-model="form.formPemilikSahams[key].alamat">
                                <div class="help-block"></div>
                            </div>
                        </div>
                        <div class="box-2 padding-x-0">
                            <div v-bind:class="'form-group form-group-sm field-formpemiliksaham-' + key + '-persentase'">
                                <input placeholder="persentase" v-bind:id="'formpemiliksaham-' + key + '-persentase'" v-bind:name="'FormPemilikSaham[' + key + '][persentase]'" class="form-control" type="text" v-model="form.formPemilikSahams[key].persentase">
                                <div class="help-block"></div>
                            </div>
                        </div>
                        <div class="box-1 padding-x-0">
                            <div v-on:click="removeFormPemilikSaham(key)" class="btn btn-default btn-sm margin-left-5 bg-light-red hover-pointer"><i class="fa fa-close"></i></div>
                        </div>
                    </div>
                </div>
            </template>
        </template>
    </div>
</div>

<div class="border-azure margin-bottom-30">
    <div class="bg-azure padding-x-15 padding-y-5">
        <span class="fs-16">Pajak</span>
    </div>
    <div class="padding-x-15 padding-y-10">
        <h6 class="text-rose padding-bottom-5 border-bottom f-italic">Dokumen</h6>

        <?php if (isset($model['form_pajak'])) foreach ($model['form_pajak'] as $key => $value): ?>
            <?= $form->field($model['form_pajak'][$key], "[$key]dokumen")->begin(); ?>
            <?= $form->field($model['form_pajak'][$key], "[$key]dokumen")->end(); ?>
            <?= $form->field($model['form_pajak'][$key], "[$key]nomor_dokumen")->begin(); ?>
            <?= $form->field($model['form_pajak'][$key], "[$key]nomor_dokumen")->end(); ?>
            <?= $form->field($model['form_pajak'][$key], "[$key]tanggal")->begin(); ?>
            <?= $form->field($model['form_pajak'][$key], "[$key]tanggal")->end(); ?>
        <?php endforeach; ?>

        <template v-if="typeof form.formPajaks == 'object'">
            <div class="box box-break-sm margin-bottom-5 hidden-sm-less">
                <div class="box-1 padding-x-0 padding-y-5 text-center">
                    No
                </div>
                <div class="box-4 padding-x-0 padding-y-5 text-center">
                    Dokumen
                </div>
                <div class="box-5 padding-x-0 padding-y-5 text-center">
                    Nomor
                </div>
                <div class="box-1 padding-x-0 padding-y-5 text-center">
                    Tanggal
                </div>
                <div class="box-1 padding-x-0">
                    <a v-on:click="addFormPajak" class="btn btn-default btn-sm margin-left-5 bg-azure hover-pointer"><i class="fa fa-plus"></i></a>
                </div>
            </div>
            <template v-for="(value, key, index) in form.formPajaks">
                <div v-show="!(value.id < 0)">
                    <div class="box box-break-sm">
                        <div class="box-1 padding-x-0 padding-y-5 text-center m-text-left">
                            <input type="hidden" v-bind:id="'formpajak-' + key + '-id'" v-bind:name="'FormPajak[' + key + '][id]'" class="form-control" type="text" v-model="form.formPajaks[key].id">
                            {{key + 1}}
                        </div>
                        <div class="box-4 padding-x-0">
                            <div v-bind:class="'form-group form-group-sm field-formpajak-' + key + '-dokumen'">
                                <input placeholder="dokumen" v-bind:id="'formpajak-' + key + '-dokumen'" v-bind:name="'FormPajak[' + key + '][dokumen]'" class="form-control" type="text" v-model="form.formPajaks[key].dokumen">
                                <div class="help-block"></div>
                            </div>
                        </div>
                        <div class="box-5 padding-x-0">
                            <div v-bind:class="'form-group form-group-sm field-formpajak-' + key + '-nomor_dokumen'">
                                <input placeholder="nomor_dokumen" v-bind:id="'formpajak-' + key + '-nomor_dokumen'" v-bind:name="'FormPajak[' + key + '][nomor_dokumen]'" class="form-control" type="text" v-model="form.formPajaks[key].nomor_dokumen">
                                <div class="help-block"></div>
                            </div>
                        </div>
                        <div class="box-1 padding-x-0">
                            <div v-bind:class="'form-group form-group-sm field-formpajak-' + key + '-tanggal'">
                                <input placeholder="tanggal" v-bind:id="'formpajak-' + key + '-tanggal'" v-bind:name="'FormPajak[' + key + '][tanggal]'" class="form-control" type="text" v-model="form.formPajaks[key].tanggal">
                                <div class="help-block"></div>
                            </div>
                        </div>
                        <div class="box-1 padding-x-0">
                            <div v-on:click="removeFormPajak(key)" class="btn btn-default btn-sm margin-left-5 bg-light-red hover-pointer"><i class="fa fa-close"></i></div>
                        </div>
                    </div>
                </div>
            </template>
        </template>

        <h6 class="text-rose padding-bottom-5 border-bottom f-italic">Bukti Pelunasan Pajak</h6>

        <?php if (isset($model['form_bukti_pajak'])) foreach ($model['form_bukti_pajak'] as $key => $value): ?>
            <?= $form->field($model['form_bukti_pajak'][$key], "[$key]bulan")->begin(); ?>
            <?= $form->field($model['form_bukti_pajak'][$key], "[$key]bulan")->end(); ?>
            <?= $form->field($model['form_bukti_pajak'][$key], "[$key]tahun")->begin(); ?>
            <?= $form->field($model['form_bukti_pajak'][$key], "[$key]tahun")->end(); ?>
            <?= $form->field($model['form_bukti_pajak'][$key], "[$key]nomor_pasal21_26")->begin(); ?>
            <?= $form->field($model['form_bukti_pajak'][$key], "[$key]nomor_pasal21_26")->end(); ?>
            <?= $form->field($model['form_bukti_pajak'][$key], "[$key]tanggal_pasal21_26")->begin(); ?>
            <?= $form->field($model['form_bukti_pajak'][$key], "[$key]tanggal_pasal21_26")->end(); ?>
            <?= $form->field($model['form_bukti_pajak'][$key], "[$key]nomor_pasal23_26")->begin(); ?>
            <?= $form->field($model['form_bukti_pajak'][$key], "[$key]nomor_pasal23_26")->end(); ?>
            <?= $form->field($model['form_bukti_pajak'][$key], "[$key]tanggal_pasal23_26")->begin(); ?>
            <?= $form->field($model['form_bukti_pajak'][$key], "[$key]tanggal_pasal23_26")->end(); ?>
            <?= $form->field($model['form_bukti_pajak'][$key], "[$key]nomor_pasal4ayat2")->begin(); ?>
            <?= $form->field($model['form_bukti_pajak'][$key], "[$key]nomor_pasal4ayat2")->end(); ?>
            <?= $form->field($model['form_bukti_pajak'][$key], "[$key]tanggal_pasal4ayat2")->begin(); ?>
            <?= $form->field($model['form_bukti_pajak'][$key], "[$key]tanggal_pasal4ayat2")->end(); ?>
            <?= $form->field($model['form_bukti_pajak'][$key], "[$key]nomor_pasal25")->begin(); ?>
            <?= $form->field($model['form_bukti_pajak'][$key], "[$key]nomor_pasal25")->end(); ?>
            <?= $form->field($model['form_bukti_pajak'][$key], "[$key]tanggal_pasal25")->begin(); ?>
            <?= $form->field($model['form_bukti_pajak'][$key], "[$key]tanggal_pasal25")->end(); ?>
            <?= $form->field($model['form_bukti_pajak'][$key], "[$key]nomor_ppnbm")->begin(); ?>
            <?= $form->field($model['form_bukti_pajak'][$key], "[$key]nomor_ppnbm")->end(); ?>
            <?= $form->field($model['form_bukti_pajak'][$key], "[$key]tanggal_ppnbm")->begin(); ?>
            <?= $form->field($model['form_bukti_pajak'][$key], "[$key]tanggal_ppnbm")->end(); ?>
        <?php endforeach; ?>

        <template v-if="typeof form.formBuktiPajaks == 'object'">
            <template v-for="(value, key, index) in form.formBuktiPajaks">
                <div v-show="!(value.id < 0)">
                    <div class="box box-break-sm">
                        <div class="box-2 padding-y-5 text-right m-text-left f-bold">
                            <input type="hidden" v-bind:id="'formbuktipajak-' + key + '-id'" v-bind:name="'FormBuktiPajak[' + key + '][id]'" class="form-control" type="text" v-model="form.formBuktiPajaks[key].id">
                            Pajak {{key + 1}}
                        </div>
                        <div class="box-4 padding-x-0">
                            <div v-bind:class="'form-group form-group-sm field-formbuktipajak-' + key + '-bulan'">
                                <select placeholder="bulan" v-bind:id="'formbuktipajak-' + key + '-bulan'" v-bind:name="'FormBuktiPajak[' + key + '][bulan]'" class="form-control" v-model="form.formBuktiPajaks[key].bulan">
                                    <option value="">Pilih Bulan</option>
                                    <option value="1">Januari</option>
                                    <option value="2">Februari</option>
                                    <option value="3">Maret</option>
                                    <option value="4">Mei</option>
                                    <option value="5">Juni</option>
                                    <option value="6">Juli</option>
                                    <option value="7">Agustus</option>
                                    <option value="8">September</option>
                                    <option value="9">Oktober</option>
                                    <option value="10">November</option>
                                    <option value="12">Desember</option>
                                </select>
                                <div class="help-block"></div>
                            </div>
                        </div>
                        <div class="box-4 padding-x-0">
                            <div v-bind:class="'form-group form-group-sm field-formbuktipajak-' + key + '-tahun'">
                                <input placeholder="tahun" v-bind:id="'formbuktipajak-' + key + '-tahun'" v-bind:name="'FormBuktiPajak[' + key + '][tahun]'" class="form-control" type="text" v-model="form.formBuktiPajaks[key].tahun">
                                <div class="help-block"></div>
                            </div>
                        </div>
                        <div class="box-1">
                            <div v-on:click="removeFormBuktiPajak(key)" class="btn btn-default btn-sm margin-left-5 bg-light-red hover-pointer"><i class="fa fa-close"></i></div>
                        </div>
                    </div>
                    <div class="box box-break-sm">
                        <div class="box-2 padding-y-5 text-right m-text-left">
                            Pasal 21/26
                        </div>
                        <div class="box-4 padding-x-0">
                            <div v-bind:class="'form-group form-group-sm field-formbuktipajak-' + key + '-nomor_pasal21_26'">
                                <input placeholder="nomor" v-bind:id="'formbuktipajak-' + key + '-nomor_pasal21_26'" v-bind:name="'FormBuktiPajak[' + key + '][nomor_pasal21_26]'" class="form-control" type="text" v-model="form.formBuktiPajaks[key].nomor_pasal21_26">
                                <div class="help-block"></div>
                            </div>
                        </div>
                        <div class="box-4 padding-x-0">
                            <div v-bind:class="'form-group form-group-sm field-formbuktipajak-' + key + '-tanggal_pasal21_26'">
                                <input placeholder="tanggal" v-bind:id="'formbuktipajak-' + key + '-tanggal_pasal21_26'" v-bind:name="'FormBuktiPajak[' + key + '][tanggal_pasal21_26]'" class="form-control" type="text" v-model="form.formBuktiPajaks[key].tanggal_pasal21_26">
                                <div class="help-block"></div>
                            </div>
                        </div>
                    </div>
                    <div class="box box-break-sm">
                        <div class="box-2 padding-y-5 text-right m-text-left">
                            Pasal 23/26
                        </div>
                        <div class="box-4 padding-x-0">
                            <div v-bind:class="'form-group form-group-sm field-formbuktipajak-' + key + '-nomor_pasal23_26'">
                                <input placeholder="nomor" v-bind:id="'formbuktipajak-' + key + '-nomor_pasal23_26'" v-bind:name="'FormBuktiPajak[' + key + '][nomor_pasal23_26]'" class="form-control" type="text" v-model="form.formBuktiPajaks[key].nomor_pasal23_26">
                                <div class="help-block"></div>
                            </div>
                        </div>
                        <div class="box-4 padding-x-0">
                            <div v-bind:class="'form-group form-group-sm field-formbuktipajak-' + key + '-tanggal_pasal23_26'">
                                <input placeholder="tanggal" v-bind:id="'formbuktipajak-' + key + '-tanggal_pasal23_26'" v-bind:name="'FormBuktiPajak[' + key + '][tanggal_pasal23_26]'" class="form-control" type="text" v-model="form.formBuktiPajaks[key].tanggal_pasal23_26">
                                <div class="help-block"></div>
                            </div>
                        </div>
                    </div>
                    <div class="box box-break-sm">
                        <div class="box-2 padding-y-5 text-right m-text-left">
                            Pasal 4 ayat 2
                        </div>
                        <div class="box-4 padding-x-0">
                            <div v-bind:class="'form-group form-group-sm field-formbuktipajak-' + key + '-nomor_pasal4ayat2'">
                                <input placeholder="nomor" v-bind:id="'formbuktipajak-' + key + '-nomor_pasal4ayat2'" v-bind:name="'FormBuktiPajak[' + key + '][nomor_pasal4ayat2]'" class="form-control" type="text" v-model="form.formBuktiPajaks[key].nomor_pasal4ayat2">
                                <div class="help-block"></div>
                            </div>
                        </div>
                        <div class="box-4 padding-x-0">
                            <div v-bind:class="'form-group form-group-sm field-formbuktipajak-' + key + '-tanggal_pasal4ayat2'">
                                <input placeholder="tanggal" v-bind:id="'formbuktipajak-' + key + '-tanggal_pasal4ayat2'" v-bind:name="'FormBuktiPajak[' + key + '][tanggal_pasal4ayat2]'" class="form-control" type="text" v-model="form.formBuktiPajaks[key].tanggal_pasal4ayat2">
                                <div class="help-block"></div>
                            </div>
                        </div>
                    </div>
                    <div class="box box-break-sm">
                        <div class="box-2 padding-y-5 text-right m-text-left">
                            Pasal 25
                        </div>
                        <div class="box-4 padding-x-0">
                            <div v-bind:class="'form-group form-group-sm field-formbuktipajak-' + key + '-nomor_pasal25'">
                                <input placeholder="nomor" v-bind:id="'formbuktipajak-' + key + '-nomor_pasal25'" v-bind:name="'FormBuktiPajak[' + key + '][nomor_pasal25]'" class="form-control" type="text" v-model="form.formBuktiPajaks[key].nomor_pasal25">
                                <div class="help-block"></div>
                            </div>
                        </div>
                        <div class="box-4 padding-x-0">
                            <div v-bind:class="'form-group form-group-sm field-formbuktipajak-' + key + '-tanggal_pasal25'">
                                <input placeholder="tanggal" v-bind:id="'formbuktipajak-' + key + '-tanggal_pasal25'" v-bind:name="'FormBuktiPajak[' + key + '][tanggal_pasal25]'" class="form-control" type="text" v-model="form.formBuktiPajaks[key].tanggal_pasal25">
                                <div class="help-block"></div>
                            </div>
                        </div>
                    </div>
                    <div class="box box-break-sm">
                        <div class="box-2 padding-y-5 text-right m-text-left">
                            PPN BM
                        </div>
                        <div class="box-4 padding-x-0">
                            <div v-bind:class="'form-group form-group-sm field-formbuktipajak-' + key + '-nomor_ppnbm'">
                                <input placeholder="nomor" v-bind:id="'formbuktipajak-' + key + '-nomor_ppnbm'" v-bind:name="'FormBuktiPajak[' + key + '][nomor_ppnbm]'" class="form-control" type="text" v-model="form.formBuktiPajaks[key].nomor_ppnbm">
                                <div class="help-block"></div>
                            </div>
                        </div>
                        <div class="box-4 padding-x-0">
                            <div v-bind:class="'form-group form-group-sm field-formbuktipajak-' + key + '-tanggal_ppnbm'">
                                <input placeholder="tanggal" v-bind:id="'formbuktipajak-' + key + '-tanggal_ppnbm'" v-bind:name="'FormBuktiPajak[' + key + '][tanggal_ppnbm]'" class="form-control" type="text" v-model="form.formBuktiPajaks[key].tanggal_ppnbm">
                                <div class="help-block"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </template>
            <a v-on:click="addFormBuktiPajak" class="btn btn-default btn-sm margin-left-5 bg-azure hover-pointer"><i class="fa fa-plus"></i></a>
        </template>
    </div>
</div>

    <hr class="margin-y-15">

    <?php if ($error) : ?>
        <div class="alert alert-danger">
            <?= $errorMessage ?>
        </div>
    <?php endif; ?>
    
    <div class="form-group clearfix">
        <?= Html::submitButton('Submit', ['class' => 'btn btn-default bg-azure rounded-xs border-azure']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default bg-lighter rounded-xs']); ?> 
        <?= Html::a('Kembali ke detail', ['index'], ['class' => 'btn btn-default bg-lightest rounded-xs pull-right']) ?>
    </div>
    
<?php ActiveForm::end(); ?>

<?php if (!Yii::$app->request->isAjax) : ?>
    </div>
</div>
<?php endif; ?>