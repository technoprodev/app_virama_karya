<?php
$params['user.passwordResetTokenExpire'] = 3600;
$params['slider'] = false;

$config = [
    'id' => 'app_virama_karya',
    'basePath' => dirname(__DIR__),
    'controllerNamespace' => 'app_virama_karya\controllers',
    'bootstrap' => ['log'],
    'modules' => [],
    'components' => [
        'request' => [
            'csrfParam' => '_csrf-app_virama_karya',
        ],
        'user' => [
            'identityClass' => 'app_virama_karya\models\User',
            'enableAutoLogin' => true,
            'identityCookie' => ['name' => '_identity-app_virama_karya', 'httpOnly' => true],
        ],
        'session' => [
            'name' => 'session-app_virama_karya',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
    ],
    'params' => $params,
];

if (php_sapi_name() != 'cli' && YII_ENV == 'dev') {
    $config['modules']['dev'] = [
        'class' => 'technosmart\modules\dev\Module',
    ];
}

return $config;