<?php

Yii::setAlias('@app_virama_karya', dirname(dirname(__DIR__)) . '/app_virama_karya');
Yii::setAlias('@console', dirname(dirname(__DIR__)) . '/console');
Yii::setAlias('@download-dev-file', 'http://localhost/technosmart/app_virama_karya/web/upload/dev-file');
Yii::setAlias('@upload-dev-file', dirname(dirname(__DIR__)) . '/app_virama_karya/web/upload/dev-file');

require Yii::getAlias("@app_virama_karya/helpers/helpers.php");