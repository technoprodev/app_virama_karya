<?php
namespace app_virama_karya\models;

use Yii;

/**
 * This is the model class for table "sub_bidang_jasa".
 *
 * @property integer $id
 * @property string $nama_sub_bidang_jasa
 * @property string $kode
 * @property integer $id_bidang_jasa
 *
 * @property Paket[] $pakets
 * @property BidangJasa $bidangJasa
 */
class SubBidangJasa extends \technosmart\yii\db\ActiveRecord
{
    public static function tableName()
    {
        return 'sub_bidang_jasa';
    }

    public function rules()
    {
        return [
            //id

            //nama_sub_bidang_jasa
            [['nama_sub_bidang_jasa'], 'required'],
            [['nama_sub_bidang_jasa'], 'string', 'max' => 128],

            //kode
            [['kode'], 'required'],
            [['kode'], 'string', 'max' => 8],

            //id_bidang_jasa
            [['id_bidang_jasa'], 'required'],
            [['id_bidang_jasa'], 'integer'],
            [['id_bidang_jasa'], 'exist', 'skipOnError' => true, 'targetClass' => BidangJasa::className(), 'targetAttribute' => ['id_bidang_jasa' => 'id']],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nama_sub_bidang_jasa' => 'Nama Sub Bidang Jasa',
            'kode' => 'Kode',
            'id_bidang_jasa' => 'Id Bidang Jasa',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPakets()
    {
        return $this->hasMany(Paket::className(), ['id_sub_bidang_jasa' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBidangJasa()
    {
        return $this->hasOne(BidangJasa::className(), ['id' => 'id_bidang_jasa']);
    }
}
