<?php
namespace app_virama_karya\models;

use Yii;

/**
 * This is the model class for table "form".
 *
 * @property integer $id
 * @property string $app_nomor_akta
 * @property string $app_tanggal
 * @property string $app_nama_notaris
 * @property string $app_nomor_pengesahan
 * @property string $app_tanggal_pengesahan
 * @property string $apt_nomor_akta
 * @property string $apt_tanggal
 * @property string $apt_nama_notaris
 * @property string $apt_nomor_pengesahan
 * @property string $apt_tanggal_pengesahan
 * @property string $iujk_berlaku_mulai
 * @property string $iujk_berlaku_sampai
 * @property string $iujk_pemberi_izin
 * @property string $iujk_perencanaan
 * @property string $iujk_pengawasan
 * @property string $iujk_konsultansi
 * @property string $sbuk_berlaku_mulai
 * @property string $sbuk_berlaku_sampai
 * @property string $sbuk_pemberi_izin
 *
 * @property FormBuktiPajak[] $formBuktiPajaks
 * @property FormDireksi[] $formDireksis
 * @property FormIjinLainnya[] $formIjinLainnyas
 * @property FormKomisaris[] $formKomisarises
 * @property FormPajak[] $formPajaks
 * @property FormPemilikSaham[] $formPemilikSahams
 * @property FormSbuk[] $formSbuks
 * @property FormSbunk[] $formSbunks
 */
class Form extends \technosmart\yii\db\ActiveRecord
{
    public static function tableName()
    {
        return 'form';
    }

    public function rules()
    {
        return [
            //id

            //app_nomor_akta
            [['app_nomor_akta'], 'required'],
            [['app_nomor_akta'], 'string', 'max' => 4],

            //app_tanggal
            [['app_tanggal'], 'required'],
            [['app_tanggal'], 'safe'],

            //app_nama_notaris
            [['app_nama_notaris'], 'required'],
            [['app_nama_notaris'], 'string', 'max' => 128],

            //app_nomor_pengesahan
            [['app_nomor_pengesahan'], 'required'],
            [['app_nomor_pengesahan'], 'string', 'max' => 64],

            //app_tanggal_pengesahan
            [['app_tanggal_pengesahan'], 'required'],
            [['app_tanggal_pengesahan'], 'safe'],

            //apt_nomor_akta
            [['apt_nomor_akta'], 'required'],
            [['apt_nomor_akta'], 'string', 'max' => 4],

            //apt_tanggal
            [['apt_tanggal'], 'required'],
            [['apt_tanggal'], 'safe'],

            //apt_nama_notaris
            [['apt_nama_notaris'], 'required'],
            [['apt_nama_notaris'], 'string', 'max' => 128],

            //apt_nomor_pengesahan
            [['apt_nomor_pengesahan'], 'required'],
            [['apt_nomor_pengesahan'], 'string', 'max' => 64],

            //apt_tanggal_pengesahan
            [['apt_tanggal_pengesahan'], 'required'],
            [['apt_tanggal_pengesahan'], 'safe'],

            //iujk_berlaku_mulai
            [['iujk_berlaku_mulai'], 'required'],
            [['iujk_berlaku_mulai'], 'safe'],

            //iujk_berlaku_sampai
            [['iujk_berlaku_sampai'], 'required'],
            [['iujk_berlaku_sampai'], 'safe'],

            //iujk_perencanaan
            [['iujk_pemberi_izin'], 'required'],
            [['iujk_pemberi_izin'], 'string', 'max' => 128],

            //iujk_perencanaan
            [['iujk_perencanaan'], 'required'],
            [['iujk_perencanaan'], 'string', 'max' => 64],

            //iujk_pengawasan
            [['iujk_pengawasan'], 'required'],
            [['iujk_pengawasan'], 'string', 'max' => 64],

            //iujk_konsultansi
            [['iujk_konsultansi'], 'required'],
            [['iujk_konsultansi'], 'string', 'max' => 64],

            //sbuk_berlaku_mulai
            [['sbuk_berlaku_mulai'], 'required'],
            [['sbuk_berlaku_mulai'], 'safe'],

            //sbuk_berlaku_sampai
            [['sbuk_berlaku_sampai'], 'required'],
            [['sbuk_berlaku_sampai'], 'safe'],

            //sbuk_pemberi_izin
            [['sbuk_pemberi_izin'], 'required'],
            [['sbuk_pemberi_izin'], 'string', 'max' => 128],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'app_nomor_akta' => 'Nomor Akta',
            'app_tanggal' => 'Tanggal',
            'app_nama_notaris' => 'Nama Notaris',
            'app_nomor_pengesahan' => 'Nomor Pengesahan',
            'app_tanggal_pengesahan' => 'Tanggal Pengesahan',
            'apt_nomor_akta' => 'Nomor Akta',
            'apt_tanggal' => 'Tanggal',
            'apt_nama_notaris' => 'Nama Notaris',
            'apt_nomor_pengesahan' => 'Nomor Pengesahan',
            'apt_tanggal_pengesahan' => 'Tanggal Pengesahan',
            'iujk_berlaku_mulai' => 'Berlaku Mulai',
            'iujk_berlaku_sampai' => 'Berlaku Sampai',
            'iujk_pemberi_izin' => 'Instansi Pemberi Izin',
            'iujk_perencanaan' => 'IUJK Perencanaan',
            'iujk_pengawasan' => 'IUJK Pengawasan',
            'iujk_konsultansi' => 'IUJK Konsultansi',
            'sbuk_berlaku_mulai' => 'Berlaku Mulai',
            'sbuk_berlaku_sampai' => 'Berlaku Sampai',
            'sbuk_pemberi_izin' => 'Instansi Pemberi Izin',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFormBuktiPajaks()
    {
        return $this->hasMany(FormBuktiPajak::className(), ['id_form' => 'id'])
            ->orderBy(['tahun' => SORT_DESC, 'bulan' => SORT_DESC]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFormDireksis()
    {
        return $this->hasMany(FormDireksi::className(), ['id_form' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFormIjinLainnyas()
    {
        return $this->hasMany(FormIjinLainnya::className(), ['id_form' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFormKomisarises()
    {
        return $this->hasMany(FormKomisaris::className(), ['id_form' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFormPajaks()
    {
        return $this->hasMany(FormPajak::className(), ['id_form' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFormPemilikSahams()
    {
        return $this->hasMany(FormPemilikSaham::className(), ['id_form' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFormSbuks()
    {
        return $this->hasMany(FormSbuk::className(), ['id_form' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFormSbunks()
    {
        return $this->hasMany(FormSbunk::className(), ['id_form' => 'id']);
    }
}
