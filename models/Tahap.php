<?php
namespace app_virama_karya\models;

use Yii;

/**
 * This is the model class for table "tahap".
 *
 * @property integer $id
 * @property string $nama_tahap
 * @property string $status
 *
 * @property PaketTahapAktif[] $paketTahapAktifs
 * @property PaketTahapJadwal[] $paketTahapJadwals
 */
class Tahap extends \technosmart\yii\db\ActiveRecord
{
    public static function tableName()
    {
        return 'tahap';
    }

    public function rules()
    {
        return [
            //id

            //nama_tahap
            [['nama_tahap'], 'required'],
            [['nama_tahap'], 'string', 'max' => 128],

            //status
            [['status'], 'required'],
            [['status'], 'string'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nama_tahap' => 'Nama Tahap',
            'status' => 'Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPaketTahapAktifs()
    {
        return $this->hasMany(PaketTahapAktif::className(), ['id_tahap' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPaketTahapJadwals()
    {
        return $this->hasMany(PaketTahapJadwal::className(), ['id_tahap' => 'id']);
    }
}
