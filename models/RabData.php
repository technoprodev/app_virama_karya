<?php
namespace app_virama_karya\models;

use Yii;

/**
 * This is the model class for table "rab_data".
 *
 * @property integer $id
 * @property integer $parent_id
 * @property string $value
 */
class RabData extends \technosmart\yii\db\ActiveRecord
{
    public static function tableName()
    {
        return 'rab_data';
    }

    public function rules()
    {
        return [
            //id

            //parent_id
            [['rab_item_id'], 'integer'],

            //parent_id
            [['rab_id'], 'integer'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'rab_item_id' => 'Parent ID',
            'rab_id' => 'Rab ID',
        ];
    }
}
