<?php
namespace app_virama_karya\models;

use Yii;

/**
 * This is the model class for table "paket_form_direksi".
 *
 * @property integer $id
 * @property integer $id_paket_form
 * @property string $nama
 * @property string $nomor_ktp
 * @property string $jabatan
 *
 * @property PaketForm $paketForm
 */
class PaketFormDireksi extends \technosmart\yii\db\ActiveRecord
{
    public $isDeleted;

    public static function tableName()
    {
        return 'paket_form_direksi';
    }

    public function rules()
    {
        return [
            //id

            //id_paket_form
            [['id_paket_form'], 'required'],
            [['id_paket_form'], 'integer'],
            [['id_paket_form'], 'exist', 'skipOnError' => true, 'targetClass' => PaketForm::className(), 'targetAttribute' => ['id_paket_form' => 'id_paket']],

            //nama
            [['nama'], 'required'],
            [['nama'], 'string', 'max' => 128],

            //nomor_ktp
            [['nomor_ktp'], 'required'],
            [['nomor_ktp'], 'string', 'max' => 16],

            //jabatan
            [['jabatan'], 'required'],
            [['jabatan'], 'string'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_paket_form' => 'Id Paket Form',
            'nama' => 'Nama',
            'nomor_ktp' => 'Nomor Ktp',
            'jabatan' => 'Jabatan',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPaketForm()
    {
        return $this->hasOne(PaketForm::className(), ['id_paket' => 'id_paket_form']);
    }
}
