<?php
namespace app_virama_karya\models;

use Yii;

/**
 * This is the model class for table "paket_mitra".
 *
 * @property integer $id
 * @property integer $id_paket
 * @property integer $id_mitra
 * @property double $porsi
 *
 * @property Paket $paket
 * @property Mitra $mitra
 */
class PaketMitra extends \technosmart\yii\db\ActiveRecord
{
    public $isDeleted;
    
    public static function tableName()
    {
        return 'paket_mitra';
    }

    public function rules()
    {
        return [
            //id

            //id_paket
            [['id_paket'], 'required'],
            [['id_paket'], 'integer'],
            [['id_paket'], 'exist', 'skipOnError' => true, 'targetClass' => Paket::className(), 'targetAttribute' => ['id_paket' => 'id']],

            //id_mitra
            [['id_mitra'], 'required'],
            [['id_mitra'], 'integer'],
            [['id_mitra'], 'exist', 'skipOnError' => true, 'targetClass' => Mitra::className(), 'targetAttribute' => ['id_mitra' => 'id']],

            //porsi
            [['porsi'], 'number'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_paket' => 'Id Paket',
            'id_mitra' => 'Mitra',
            'porsi' => 'Porsi',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPaket()
    {
        return $this->hasOne(Paket::className(), ['id' => 'id_paket']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMitra()
    {
        return $this->hasOne(Mitra::className(), ['id' => 'id_mitra']);
    }
}
