<?php
namespace app_virama_karya\models;

use Yii;

/**
 * This is the model class for table "paket".
 *
 * @property integer $id
 * @property string $status
 * @property integer $auto_forward
 * @property string $alasan
 * @property string $nama_paket
 * @property string $lokasi
 * @property integer $id_unit_kerja
 * @property integer $id_bidang_unit_kerja
 * @property integer $id_pengguna_jasa
 * @property integer $id_instansi
 * @property integer $id_satminkal
 * @property integer $id_satker
 * @property integer $id_bidang_jasa
 * @property integer $id_sub_bidang_jasa
 * @property string $tahun_anggaran
 * @property string $sumber_anggaran
 * @property integer $pagu
 * @property integer $hps
 * @property double $bobot_teknis
 * @property double $bobot_biaya
 * @property string $is_kso
 * @property string $peluang
 * @property string $tindak_lanjut_25
 * @property string $tindak_lanjut_50
 * @property string $tindak_lanjut_75
 * @property string $tindak_lanjut_90
 * @property string $tindak_lanjut_95
 * @property integer $nilai_kontrak
 * @property string $nomor_kontrak
 * @property string $kontrak_mulai
 * @property string $kontrak_selesai
 *
 * @property UnitKerja $unitKerja
 * @property BidangUnitKerja $bidangUnitKerja
 * @property PenggunaJasa $penggunaJasa
 * @property Instansi $instansi
 * @property Satminkal $satminkal
 * @property Satker $satker
 * @property BidangJasa $bidangJasa
 * @property SubBidangJasa $subBidangJasa
 * @property PaketForm $paketForm
 * @property PaketMitra[] $paketMitras
 * @property PaketTahapAktif[] $paketTahapAktifs
 * @property PaketTahapJadwal[] $paketTahapJadwals
 * @property PaketTenagaAhli[] $paketTenagaAhlis
 */
class Paket extends \technosmart\yii\db\ActiveRecord
{
    public static function tableName()
    {
        return 'paket';
    }

    public function rules()
    {
        return [
            //id

            //status
            [['status'], 'required'],

            //auto_forward
            [['auto_forward'], 'integer'],

            //alasan
            [['alasan'], 'string'],

            //nama_paket
            [['nama_paket'], 'required'],
            [['nama_paket'], 'string', 'max' => 256],

            //lokasi
            [['lokasi'], 'required'],
            [['lokasi'], 'string', 'max' => 256],

            //id_unit_kerja
            [['id_unit_kerja'], 'required'],
            [['id_unit_kerja'], 'integer'],
            [['id_unit_kerja'], 'exist', 'skipOnError' => true, 'targetClass' => UnitKerja::className(), 'targetAttribute' => ['id_unit_kerja' => 'id']],

            //id_bidang_unit_kerja
            [['id_bidang_unit_kerja'], 'required'],
            [['id_bidang_unit_kerja'], 'integer'],
            [['id_bidang_unit_kerja'], 'exist', 'skipOnError' => true, 'targetClass' => BidangUnitKerja::className(), 'targetAttribute' => ['id_bidang_unit_kerja' => 'id']],

            //id_pengguna_jasa
            [['id_pengguna_jasa'], 'required'],
            [['id_pengguna_jasa'], 'integer'],
            [['id_pengguna_jasa'], 'exist', 'skipOnError' => true, 'targetClass' => PenggunaJasa::className(), 'targetAttribute' => ['id_pengguna_jasa' => 'id']],

            //id_instansi
            [['id_instansi'], 'required'],
            [['id_instansi'], 'integer'],
            [['id_instansi'], 'exist', 'skipOnError' => true, 'targetClass' => Instansi::className(), 'targetAttribute' => ['id_instansi' => 'id']],

            //id_satminkal
            [['id_satminkal'], 'required'],
            [['id_satminkal'], 'integer'],
            [['id_satminkal'], 'exist', 'skipOnError' => true, 'targetClass' => Satminkal::className(), 'targetAttribute' => ['id_satminkal' => 'id']],

            //id_satker
            [['id_satker'], 'required'],
            [['id_satker'], 'integer'],
            [['id_satker'], 'exist', 'skipOnError' => true, 'targetClass' => Satker::className(), 'targetAttribute' => ['id_satker' => 'id']],

            //id_bidang_jasa
            [['id_bidang_jasa'], 'required'],
            [['id_bidang_jasa'], 'integer'],
            [['id_bidang_jasa'], 'exist', 'skipOnError' => true, 'targetClass' => BidangJasa::className(), 'targetAttribute' => ['id_bidang_jasa' => 'id']],

            //id_sub_bidang_jasa
            [['id_sub_bidang_jasa'], 'required'],
            [['id_sub_bidang_jasa'], 'integer'],
            [['id_sub_bidang_jasa'], 'exist', 'skipOnError' => true, 'targetClass' => SubBidangJasa::className(), 'targetAttribute' => ['id_sub_bidang_jasa' => 'id']],

            //tahun_anggaran
            [['tahun_anggaran'], 'required'],
            [['tahun_anggaran'], 'safe'],

            //sumber_anggaran
            [['sumber_anggaran'], 'required'],
            [['sumber_anggaran'], 'string', 'max' => 256],

            //pagu
            [['pagu'], 'required'],
            [['pagu'], 'integer'],

            //hps
            [['hps'], 'required'],
            [['hps'], 'integer'],

            //bobot_teknis
            [['bobot_teknis'], 'required'],
            [['bobot_teknis'], 'number'],

            //bobot_biaya
            [['bobot_biaya'], 'required'],
            [['bobot_biaya'], 'number'],

            //is_kso
            [['is_kso'], 'required'],
            [['is_kso'], 'string'],

            //peluang
            [['peluang'], 'required'],
            [['peluang'], 'string'],

            //tindak_lanjut_25
            [['tindak_lanjut_25'], 'string'],

            //tindak_lanjut_50
            [['tindak_lanjut_50'], 'string'],

            //tindak_lanjut_75
            [['tindak_lanjut_75'], 'string'],

            //tindak_lanjut_90
            [['tindak_lanjut_90'], 'string'],

            //tindak_lanjut_95
            [['tindak_lanjut_95'], 'string'],

            //nilai_kontrak
            [['nilai_kontrak'], 'integer'],

            //nomor_kontrak
            [['nomor_kontrak'], 'string', 'max' => 256],

            //kontrak_mulai
            [['kontrak_mulai'], 'safe'],

            //kontrak_selesai
            [['kontrak_selesai'], 'safe'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'status' => 'Status',
            'auto_forward' => 'Auto Forward',
            'alasan' => 'Alasan',
            'nama_paket' => 'Nama Paket',
            'lokasi' => 'Lokasi',
            'id_unit_kerja' => 'Unit Kerja',
            'id_bidang_unit_kerja' => 'Bidang Unit Kerja',
            'id_pengguna_jasa' => 'Pengguna Jasa',
            'id_instansi' => 'Instansi',
            'id_satminkal' => 'Satminkal',
            'id_satker' => 'Satker',
            'id_bidang_jasa' => 'Bidang Jasa',
            'id_sub_bidang_jasa' => 'Sub Bidang Jasa',
            'tahun_anggaran' => 'Tahun Anggaran',
            'sumber_anggaran' => 'Sumber Anggaran',
            'pagu' => 'Pagu',
            'hps' => 'Hps',
            'bobot_teknis' => 'Bobot Teknis',
            'bobot_biaya' => 'Bobot Biaya',
            'is_kso' => 'Kso',
            'peluang' => 'Peluang (%)',
            'tindak_lanjut_25' => 'Tindak Lanjut 25%',
            'tindak_lanjut_50' => 'Tindak Lanjut 50%',
            'tindak_lanjut_75' => 'Tindak Lanjut 75%',
            'tindak_lanjut_90' => 'Tindak Lanjut 90%',
            'tindak_lanjut_95' => 'Tindak Lanjut 95%',
            'nilai_kontrak' => 'Nilai Kontrak',
            'nomor_kontrak' => 'Nomor Kontrak',
            'kontrak_mulai' => 'Kontrak Mulai',
            'kontrak_selesai' => 'Kontrak Selesai',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUnitKerja()
    {
        return $this->hasOne(UnitKerja::className(), ['id' => 'id_unit_kerja']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBidangUnitKerja()
    {
        return $this->hasOne(BidangUnitKerja::className(), ['id' => 'id_bidang_unit_kerja']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPenggunaJasa()
    {
        return $this->hasOne(PenggunaJasa::className(), ['id' => 'id_pengguna_jasa']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInstansi()
    {
        return $this->hasOne(Instansi::className(), ['id' => 'id_instansi']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSatminkal()
    {
        return $this->hasOne(Satminkal::className(), ['id' => 'id_satminkal']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSatker()
    {
        return $this->hasOne(Satker::className(), ['id' => 'id_satker']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBidangJasa()
    {
        return $this->hasOne(BidangJasa::className(), ['id' => 'id_bidang_jasa']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSubBidangJasa()
    {
        return $this->hasOne(SubBidangJasa::className(), ['id' => 'id_sub_bidang_jasa']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPaketForm()
    {
        return $this->hasOne(PaketForm::className(), ['id_paket' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPaketMitras()
    {
        return $this->hasMany(PaketMitra::className(), ['id_paket' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPaketTahapAktifs()
    {
        return $this->hasMany(PaketTahapAktif::className(), ['id_paket' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPaketTahapJadwals()
    {
        return $this->hasMany(PaketTahapJadwal::className(), ['id_paket' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPaketTenagaAhlis()
    {
        return $this->hasMany(PaketTenagaAhli::className(), ['id_paket' => 'id']);
    }
}
