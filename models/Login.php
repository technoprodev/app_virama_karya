<?php
namespace app_virama_karya\models;

use Yii;
use app_virama_karya\models\User;

class Login extends \technosmart\models\Login
{
    protected function getUser()
    {
        if ($this->user === null) {
            switch ($this->scenario) {
                case 'using-login':
                    $this->user = User::findByLogin($this->login);
                    break;
                case 'using-username':
                    $this->user = User::findByUsername($this->username);
                    break;
                case 'using-email':
                    $this->user = User::findByEmail($this->email);
                    break;
                case 'using-phone':
                    $this->user = User::findByPhone($this->phone);
                    break;
                default:
                    $this->user = User::findByLogin($this->login);
            }
        }

        return $this->user;
    }
}