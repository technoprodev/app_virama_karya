<?php
namespace app_virama_karya\models;

use Yii;

/**
 * This is the model class for table "bidang_unit_kerja".
 *
 * @property integer $id
 * @property string $nama_bidang_unit_kerja
 * @property integer $id_unit_kerja
 *
 * @property UnitKerja $unitKerja
 * @property Paket[] $pakets
 */
class BidangUnitKerja extends \technosmart\yii\db\ActiveRecord
{
    public static function tableName()
    {
        return 'bidang_unit_kerja';
    }

    public function rules()
    {
        return [
            //id

            //nama_bidang_unit_kerja
            [['nama_bidang_unit_kerja'], 'required'],
            [['nama_bidang_unit_kerja'], 'string', 'max' => 64],

            //id_unit_kerja
            [['id_unit_kerja'], 'required'],
            [['id_unit_kerja'], 'integer'],
            [['id_unit_kerja'], 'exist', 'skipOnError' => true, 'targetClass' => UnitKerja::className(), 'targetAttribute' => ['id_unit_kerja' => 'id']],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nama_bidang_unit_kerja' => 'Nama Bidang Unit Kerja',
            'id_unit_kerja' => 'Id Unit Kerja',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUnitKerja()
    {
        return $this->hasOne(UnitKerja::className(), ['id' => 'id_unit_kerja']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPakets()
    {
        return $this->hasMany(Paket::className(), ['id_bidang_unit_kerja' => 'id']);
    }
}
