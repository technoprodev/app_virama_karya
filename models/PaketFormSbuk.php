<?php
namespace app_virama_karya\models;

use Yii;

/**
 * This is the model class for table "paket_form_sbuk".
 *
 * @property integer $id
 * @property integer $id_paket_form
 * @property string $sertifikat
 * @property string $nomor_sertifikat
 *
 * @property PaketForm $paketForm
 */
class PaketFormSbuk extends \technosmart\yii\db\ActiveRecord
{
    public $isDeleted;

    public static function tableName()
    {
        return 'paket_form_sbuk';
    }

    public function rules()
    {
        return [
            //id

            //id_paket_form
            [['id_paket_form'], 'required'],
            [['id_paket_form'], 'integer'],
            [['id_paket_form'], 'exist', 'skipOnError' => true, 'targetClass' => PaketForm::className(), 'targetAttribute' => ['id_paket_form' => 'id_paket']],

            //sertifikat
            [['sertifikat'], 'required'],
            [['sertifikat'], 'string', 'max' => 128],

            //nomor_sertifikat
            [['nomor_sertifikat'], 'required'],
            [['nomor_sertifikat'], 'string'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_paket_form' => 'Id Paket Form',
            'sertifikat' => 'Sertifikat',
            'nomor_sertifikat' => 'Nomor Sertifikat',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPaketForm()
    {
        return $this->hasOne(PaketForm::className(), ['id_paket' => 'id_paket_form']);
    }
}
