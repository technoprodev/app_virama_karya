<?php
namespace app_virama_karya\models;

use Yii;

/**
 * This is the model class for table "form_pemilik_saham".
 *
 * @property integer $id
 * @property integer $id_form
 * @property string $nama
 * @property string $nomor_ktp
 * @property string $alamat
 * @property double $persentase
 *
 * @property Form $form
 */
class FormPemilikSaham extends \technosmart\yii\db\ActiveRecord
{
    public $isDeleted;

    public static function tableName()
    {
        return 'form_pemilik_saham';
    }

    public function rules()
    {
        return [
            //id

            //id_form
            [['id_form'], 'required'],
            [['id_form'], 'integer'],
            [['id_form'], 'exist', 'skipOnError' => true, 'targetClass' => Form::className(), 'targetAttribute' => ['id_form' => 'id']],

            //nama
            [['nama'], 'required'],
            [['nama'], 'string', 'max' => 128],

            //nomor_ktp
            [['nomor_ktp'], 'string', 'max' => 16],

            //alamat
            [['alamat'], 'string'],

            //persentase
            [['persentase'], 'number'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_form' => 'Id Form',
            'nama' => 'Nama',
            'nomor_ktp' => 'Nomor Ktp',
            'alamat' => 'Alamat',
            'persentase' => 'Persentase',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getForm()
    {
        return $this->hasOne(Form::className(), ['id' => 'id_form']);
    }
}
