<?php
namespace app_virama_karya\models;

use Yii;

/**
 * This is the model class for table "rab_item".
 *
 * @property integer $id
 * @property string $name
 * @property string $detail_structure
 */
class RabItem extends \technosmart\yii\db\ActiveRecord
{
    public static function tableName()
    {
        return 'rab_item';
    }

    public function rules()
    {
        return [
            //id

            //name
            [['name'], 'required'],
            [['name'], 'string', 'max' => 50],

            //detail_structure
            [['detail_structure'], 'string'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'detail_structure' => 'Detail Structure',
        ];
    }
}
