<?php
namespace app_virama_karya\models;

use Yii;

/**
 * This is the model class for table "paket_form_bukti_pajak".
 *
 * @property integer $id
 * @property integer $id_paket_form
 * @property integer $bulan
 * @property string $tahun
 * @property string $nomor_pasal21_26
 * @property string $tanggal_pasal21_26
 * @property string $nomor_pasal23_26
 * @property string $tanggal_pasal23_26
 * @property string $nomor_pasal4ayat2
 * @property string $tanggal_pasal4ayat2
 * @property string $nomor_pasal25
 * @property string $tanggal_pasal25
 * @property string $nomor_ppnbm
 * @property string $tanggal_ppnbm
 *
 * @property PaketForm $paketForm
 */
class PaketFormBuktiPajak extends \technosmart\yii\db\ActiveRecord
{
    public $isDeleted;

    public static function tableName()
    {
        return 'paket_form_bukti_pajak';
    }

    public function rules()
    {
        return [
            //id

            //id_paket_form
            [['id_paket_form'], 'required'],
            [['id_paket_form'], 'integer'],
            [['id_paket_form'], 'exist', 'skipOnError' => true, 'targetClass' => PaketForm::className(), 'targetAttribute' => ['id_paket_form' => 'id_paket']],

            //bulan
            [['bulan'], 'required'],
            [['bulan'], 'integer'],

            //tahun
            [['tahun'], 'required'],
            [['tahun'], 'safe'],

            //nomor_pasal21_26
            [['nomor_pasal21_26'], 'required'],
            [['nomor_pasal21_26'], 'string', 'max' => 128],

            //tanggal_pasal21_26
            [['tanggal_pasal21_26'], 'required'],
            [['tanggal_pasal21_26'], 'safe'],

            //nomor_pasal23_26
            [['nomor_pasal23_26'], 'required'],
            [['nomor_pasal23_26'], 'string', 'max' => 128],

            //tanggal_pasal23_26
            [['tanggal_pasal23_26'], 'required'],
            [['tanggal_pasal23_26'], 'safe'],

            //nomor_pasal4ayat2
            [['nomor_pasal4ayat2'], 'required'],
            [['nomor_pasal4ayat2'], 'string', 'max' => 128],

            //tanggal_pasal4ayat2
            [['tanggal_pasal4ayat2'], 'required'],
            [['tanggal_pasal4ayat2'], 'safe'],

            //nomor_pasal25
            [['nomor_pasal25'], 'required'],
            [['nomor_pasal25'], 'string', 'max' => 128],

            //tanggal_pasal25
            [['tanggal_pasal25'], 'required'],
            [['tanggal_pasal25'], 'safe'],

            //nomor_ppnbm
            [['nomor_ppnbm'], 'required'],
            [['nomor_ppnbm'], 'string', 'max' => 128],

            //tanggal_ppnbm
            [['tanggal_ppnbm'], 'required'],
            [['tanggal_ppnbm'], 'safe'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_paket_form' => 'Id Paket Form',
            'bulan' => 'Bulan',
            'tahun' => 'Tahun',
            'nomor_pasal21_26' => 'Nomor Pasal21 26',
            'tanggal_pasal21_26' => 'Tanggal Pasal21 26',
            'nomor_pasal23_26' => 'Nomor Pasal23 26',
            'tanggal_pasal23_26' => 'Tanggal Pasal23 26',
            'nomor_pasal4ayat2' => 'Nomor Pasal4ayat2',
            'tanggal_pasal4ayat2' => 'Tanggal Pasal4ayat2',
            'nomor_pasal25' => 'Nomor Pasal25',
            'tanggal_pasal25' => 'Tanggal Pasal25',
            'nomor_ppnbm' => 'Nomor Ppnbm',
            'tanggal_ppnbm' => 'Tanggal Ppnbm',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPaketForm()
    {
        return $this->hasOne(PaketForm::className(), ['id_paket' => 'id_paket_form']);
    }
}
