<?php
namespace app_virama_karya\models;

use Yii;

/**
 * This is the model class for table "paket_tenaga_ahli_keahlian".
 *
 * @property integer $id
 * @property integer $id_paket_tenaga_ahli
 * @property string $keahlian
 * @property string $level
 * @property string $sertifikat
 *
 * @property PaketTenagaAhli $paketTenagaAhli
 */
class PaketTenagaAhliKeahlian extends \technosmart\yii\db\ActiveRecord
{
    public static function tableName()
    {
        return 'paket_tenaga_ahli_keahlian';
    }

    public function rules()
    {
        return [
            //id

            //id_paket_tenaga_ahli
            [['id_paket_tenaga_ahli'], 'required'],
            [['id_paket_tenaga_ahli'], 'integer'],
            [['id_paket_tenaga_ahli'], 'exist', 'skipOnError' => true, 'targetClass' => PaketTenagaAhli::className(), 'targetAttribute' => ['id_paket_tenaga_ahli' => 'id']],

            //keahlian
            [['keahlian'], 'required'],
            [['keahlian'], 'string', 'max' => 64],

            //level
            [['level'], 'string'],

            //sertifikat
            [['sertifikat'], 'required'],
            [['sertifikat'], 'string', 'max' => 64],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_paket_tenaga_ahli' => 'Id Paket Tenaga Ahli',
            'keahlian' => 'Keahlian',
            'level' => 'Level',
            'sertifikat' => 'Sertifikat',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPaketTenagaAhli()
    {
        return $this->hasOne(PaketTenagaAhli::className(), ['id' => 'id_paket_tenaga_ahli']);
    }
}
