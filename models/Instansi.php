<?php
namespace app_virama_karya\models;

use Yii;

/**
 * This is the model class for table "instansi".
 *
 * @property integer $id
 * @property string $nama_instansi
 * @property string $alamat
 * @property integer $id_pengguna_jasa
 *
 * @property PenggunaJasa $penggunaJasa
 * @property Paket[] $pakets
 * @property Satminkal[] $satminkals
 */
class Instansi extends \technosmart\yii\db\ActiveRecord
{
    public static function tableName()
    {
        return 'instansi';
    }

    public function rules()
    {
        return [
            //id

            //nama_instansi
            [['nama_instansi'], 'required'],
            [['nama_instansi'], 'string', 'max' => 64],

            //alamat
            [['alamat'], 'string'],

            //id_pengguna_jasa
            [['id_pengguna_jasa'], 'required'],
            [['id_pengguna_jasa'], 'integer'],
            [['id_pengguna_jasa'], 'exist', 'skipOnError' => true, 'targetClass' => PenggunaJasa::className(), 'targetAttribute' => ['id_pengguna_jasa' => 'id']],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nama_instansi' => 'Nama Instansi',
            'alamat' => 'Alamat',
            'id_pengguna_jasa' => 'Id Pengguna Jasa',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPenggunaJasa()
    {
        return $this->hasOne(PenggunaJasa::className(), ['id' => 'id_pengguna_jasa']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPakets()
    {
        return $this->hasMany(Paket::className(), ['id_instansi' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSatminkals()
    {
        return $this->hasMany(Satminkal::className(), ['id_instansi' => 'id']);
    }
}
