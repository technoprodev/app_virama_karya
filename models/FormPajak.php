<?php
namespace app_virama_karya\models;

use Yii;

/**
 * This is the model class for table "form_pajak".
 *
 * @property integer $id
 * @property integer $id_form
 * @property string $dokumen
 * @property string $nomor_dokumen
 * @property string $tanggal
 *
 * @property Form $form
 */
class FormPajak extends \technosmart\yii\db\ActiveRecord
{
    public $isDeleted;

    public static function tableName()
    {
        return 'form_pajak';
    }

    public function rules()
    {
        return [
            //id

            //id_form
            [['id_form'], 'required'],
            [['id_form'], 'integer'],
            [['id_form'], 'exist', 'skipOnError' => true, 'targetClass' => Form::className(), 'targetAttribute' => ['id_form' => 'id']],

            //dokumen
            [['dokumen'], 'required'],
            [['dokumen'], 'string', 'max' => 128],

            //nomor_dokumen
            [['nomor_dokumen'], 'required'],
            [['nomor_dokumen'], 'string', 'max' => 128],

            //tanggal
            [['tanggal'], 'safe'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_form' => 'Id Form',
            'dokumen' => 'Dokumen',
            'nomor_dokumen' => 'Nomor Dokumen',
            'tanggal' => 'Tanggal',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getForm()
    {
        return $this->hasOne(Form::className(), ['id' => 'id_form']);
    }
}
