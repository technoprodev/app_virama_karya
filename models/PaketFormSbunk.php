<?php
namespace app_virama_karya\models;

use Yii;

/**
 * This is the model class for table "paket_form_sbunk".
 *
 * @property integer $id
 * @property integer $id_paket_form
 * @property string $sertifikat
 * @property string $nomor_sertifikat
 * @property string $berlaku_mulai
 * @property string $berlaku_sampai
 * @property string $pemberi_izin
 *
 * @property PaketForm $paketForm
 */
class PaketFormSbunk extends \technosmart\yii\db\ActiveRecord
{
    public $isDeleted;

    public static function tableName()
    {
        return 'paket_form_sbunk';
    }

    public function rules()
    {
        return [
            //id

            //id_paket_form
            [['id_paket_form'], 'required'],
            [['id_paket_form'], 'integer'],
            [['id_paket_form'], 'exist', 'skipOnError' => true, 'targetClass' => PaketForm::className(), 'targetAttribute' => ['id_paket_form' => 'id_paket']],

            //sertifikat
            [['sertifikat'], 'required'],
            [['sertifikat'], 'string', 'max' => 128],

            //nomor_sertifikat
            [['nomor_sertifikat'], 'required'],
            [['nomor_sertifikat'], 'string', 'max' => 128],

            //berlaku_mulai
            [['berlaku_mulai'], 'safe'],

            //berlaku_sampai
            [['berlaku_sampai'], 'safe'],

            //pemberi_izin
            [['pemberi_izin'], 'string', 'max' => 64],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_paket_form' => 'Id Paket Form',
            'sertifikat' => 'Sertifikat',
            'nomor_sertifikat' => 'Nomor Sertifikat',
            'berlaku_mulai' => 'Berlaku Mulai',
            'berlaku_sampai' => 'Berlaku Sampai',
            'pemberi_izin' => 'Pemberi Izin',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPaketForm()
    {
        return $this->hasOne(PaketForm::className(), ['id_paket' => 'id_paket_form']);
    }
}
