<?php
namespace app_virama_karya\models;

use Yii;

/**
 * This is connector for Yii::$app->user with model User
 */
class User extends \technosmart\models\User
{
    public function rules()
    {
        return array_merge(parent::rules(), [
            //username
            [['username'], 'required'],
            
            //email
            [['email'], 'required'],
        ]);
    }
}