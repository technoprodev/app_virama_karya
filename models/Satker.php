<?php
namespace app_virama_karya\models;

use Yii;

/**
 * This is the model class for table "satker".
 *
 * @property integer $id
 * @property string $nama_satker
 * @property integer $id_satminkal
 *
 * @property Paket[] $pakets
 * @property Satminkal $satminkal
 */
class Satker extends \technosmart\yii\db\ActiveRecord
{
    public static function tableName()
    {
        return 'satker';
    }

    public function rules()
    {
        return [
            //id

            //nama_satker
            [['nama_satker'], 'required'],
            [['nama_satker'], 'string', 'max' => 64],

            //id_satminkal
            [['id_satminkal'], 'required'],
            [['id_satminkal'], 'integer'],
            [['id_satminkal'], 'exist', 'skipOnError' => true, 'targetClass' => Satminkal::className(), 'targetAttribute' => ['id_satminkal' => 'id']],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nama_satker' => 'Nama Satker',
            'id_satminkal' => 'Id Satminkal',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPakets()
    {
        return $this->hasMany(Paket::className(), ['id_satker' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSatminkal()
    {
        return $this->hasOne(Satminkal::className(), ['id' => 'id_satminkal']);
    }
}
