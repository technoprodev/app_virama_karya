<?php
namespace app_virama_karya\models;

use Yii;

/**
 * This is the model class for table "pendidikan_rumpun".
 *
 * @property integer $id
 * @property string $rumpun
 *
 * @property PaketTenagaAhliPendidikan[] $paketTenagaAhliPendidikans
 * @property TenagaAhliPendidikan[] $tenagaAhliPendidikans
 */
class PendidikanRumpun extends \technosmart\yii\db\ActiveRecord
{
    public static function tableName()
    {
        return 'pendidikan_rumpun';
    }

    public function rules()
    {
        return [
            //id

            //rumpun
            [['rumpun'], 'required'],
            [['rumpun'], 'string', 'max' => 128],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'rumpun' => 'Rumpun',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPaketTenagaAhliPendidikans()
    {
        return $this->hasMany(PaketTenagaAhliPendidikan::className(), ['id_pendidikan_rumpun' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTenagaAhliPendidikans()
    {
        return $this->hasMany(TenagaAhliPendidikan::className(), ['id_pendidikan_rumpun' => 'id']);
    }
}
