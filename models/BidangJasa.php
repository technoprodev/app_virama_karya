<?php
namespace app_virama_karya\models;

use Yii;

/**
 * This is the model class for table "bidang_jasa".
 *
 * @property integer $id
 * @property string $nama_bidang_jasa
 *
 * @property Paket[] $pakets
 * @property SubBidangJasa[] $subBidangJasas
 */
class BidangJasa extends \technosmart\yii\db\ActiveRecord
{
    public static function tableName()
    {
        return 'bidang_jasa';
    }

    public function rules()
    {
        return [
            //id

            //nama_bidang_jasa
            [['nama_bidang_jasa'], 'required'],
            [['nama_bidang_jasa'], 'string', 'max' => 64],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nama_bidang_jasa' => 'Nama Bidang Jasa',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPakets()
    {
        return $this->hasMany(Paket::className(), ['id_bidang_jasa' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSubBidangJasas()
    {
        return $this->hasMany(SubBidangJasa::className(), ['id_bidang_jasa' => 'id']);
    }
}
