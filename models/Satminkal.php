<?php
namespace app_virama_karya\models;

use Yii;

/**
 * This is the model class for table "satminkal".
 *
 * @property integer $id
 * @property string $nama_satminkal
 * @property integer $id_instansi
 *
 * @property Paket[] $pakets
 * @property Satker[] $satkers
 * @property Instansi $instansi
 */
class Satminkal extends \technosmart\yii\db\ActiveRecord
{
    public static function tableName()
    {
        return 'satminkal';
    }

    public function rules()
    {
        return [
            //id

            //nama_satminkal
            [['nama_satminkal'], 'required'],
            [['nama_satminkal'], 'string', 'max' => 64],

            //id_instansi
            [['id_instansi'], 'required'],
            [['id_instansi'], 'integer'],
            [['id_instansi'], 'exist', 'skipOnError' => true, 'targetClass' => Instansi::className(), 'targetAttribute' => ['id_instansi' => 'id']],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nama_satminkal' => 'Nama Satminkal',
            'id_instansi' => 'Id Instansi',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPakets()
    {
        return $this->hasMany(Paket::className(), ['id_satminkal' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSatkers()
    {
        return $this->hasMany(Satker::className(), ['id_satminkal' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInstansi()
    {
        return $this->hasOne(Instansi::className(), ['id' => 'id_instansi']);
    }
}
