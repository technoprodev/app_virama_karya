<?php
namespace app_virama_karya\models;

use Yii;

/**
 * This is the model class for table "paket_tahap_aktif".
 *
 * @property integer $id
 * @property integer $id_paket
 * @property integer $id_tahap
 *
 * @property Paket $paket
 * @property Tahap $tahap
 */
class PaketTahapAktif extends \technosmart\yii\db\ActiveRecord
{
    public static function tableName()
    {
        return 'paket_tahap_aktif';
    }

    public function rules()
    {
        return [
            //id

            //id_paket
            [['id_paket'], 'required'],
            [['id_paket'], 'integer'],
            [['id_paket'], 'exist', 'skipOnError' => true, 'targetClass' => Paket::className(), 'targetAttribute' => ['id_paket' => 'id']],

            //id_tahap
            [['id_tahap'], 'required'],
            [['id_tahap'], 'integer'],
            [['id_tahap'], 'exist', 'skipOnError' => true, 'targetClass' => Tahap::className(), 'targetAttribute' => ['id_tahap' => 'id']],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_paket' => 'Id Paket',
            'id_tahap' => 'Id Tahap',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPaket()
    {
        return $this->hasOne(Paket::className(), ['id' => 'id_paket']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTahap()
    {
        return $this->hasOne(Tahap::className(), ['id' => 'id_tahap']);
    }
}
