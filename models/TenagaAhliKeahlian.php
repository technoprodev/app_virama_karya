<?php
namespace app_virama_karya\models;

use Yii;

/**
 * This is the model class for table "tenaga_ahli_keahlian".
 *
 * @property integer $id
 * @property integer $id_tenaga_ahli
 * @property string $keahlian
 * @property string $level
 * @property string $sertifikat
 *
 * @property TenagaAhli $tenagaAhli
 */
class TenagaAhliKeahlian extends \technosmart\yii\db\ActiveRecord
{
    public $isDeleted;
    
    public static function tableName()
    {
        return 'tenaga_ahli_keahlian';
    }

    public function rules()
    {
        return [
            //id

            //id_tenaga_ahli
            [['id_tenaga_ahli'], 'required'],
            [['id_tenaga_ahli'], 'integer'],
            [['id_tenaga_ahli'], 'exist', 'skipOnError' => true, 'targetClass' => TenagaAhli::className(), 'targetAttribute' => ['id_tenaga_ahli' => 'id']],

            //keahlian
            [['keahlian'], 'required'],
            [['keahlian'], 'string', 'max' => 64],

            //level
            [['level'], 'string'],

            //sertifikat
            [['sertifikat'], 'required'],
            [['sertifikat'], 'string', 'max' => 64],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_tenaga_ahli' => 'Id Tenaga Ahli',
            'keahlian' => 'Keahlian',
            'level' => 'Level',
            'sertifikat' => 'Sertifikat',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTenagaAhli()
    {
        return $this->hasOne(TenagaAhli::className(), ['id' => 'id_tenaga_ahli']);
    }
}
