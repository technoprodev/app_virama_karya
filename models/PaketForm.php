<?php
namespace app_virama_karya\models;

use Yii;

/**
 * This is the model class for table "paket_form".
 *
 * @property integer $id_paket
 * @property string $app_nomor_akta
 * @property string $app_tanggal
 * @property string $app_nama_notaris
 * @property string $app_nomor_pengesahan
 * @property string $app_tanggal_pengesahan
 * @property string $apt_nomor_akta
 * @property string $apt_tanggal
 * @property string $apt_nama_notaris
 * @property string $apt_nomor_pengesahan
 * @property string $apt_tanggal_pengesahan
 * @property string $iujk_berlaku_mulai
 * @property string $iujk_berlaku_sampai
 * @property string $iujk_pemberi_izin
 * @property string $iujk_perencanaan
 * @property string $iujk_pengawasan
 * @property string $iujk_konsultansi
 * @property string $sbuk_berlaku_mulai
 * @property string $sbuk_berlaku_sampai
 * @property string $sbuk_pemberi_izin
 *
 * @property Paket $paket
 * @property PaketFormBuktiPajak[] $paketFormBuktiPajaks
 * @property PaketFormDireksi[] $paketFormDireksis
 * @property PaketFormIjinLainnya[] $paketFormIjinLainnyas
 * @property PaketFormKomisaris[] $paketFormKomisarises
 * @property PaketFormPajak[] $paketFormPajaks
 * @property PaketFormPemilikSaham[] $paketFormPemilikSahams
 * @property PaketFormSbuk[] $paketFormSbuks
 * @property PaketFormSbunk[] $paketFormSbunks
 */
class PaketForm extends \technosmart\yii\db\ActiveRecord
{
    public static function tableName()
    {
        return 'paket_form';
    }

    public function rules()
    {
        return [
            //id_paket
            [['id_paket'], 'required'],
            [['id_paket'], 'integer'],
            [['id_paket'], 'exist', 'skipOnError' => true, 'targetClass' => Paket::className(), 'targetAttribute' => ['id_paket' => 'id']],

            //app_nomor_akta
            [['app_nomor_akta'], 'required'],
            [['app_nomor_akta'], 'string', 'max' => 4],

            //app_tanggal
            [['app_tanggal'], 'required'],
            [['app_tanggal'], 'safe'],

            //app_nama_notaris
            [['app_nama_notaris'], 'required'],
            [['app_nama_notaris'], 'string', 'max' => 128],

            //app_nomor_pengesahan
            [['app_nomor_pengesahan'], 'required'],
            [['app_nomor_pengesahan'], 'string', 'max' => 64],

            //app_tanggal_pengesahan
            [['app_tanggal_pengesahan'], 'required'],
            [['app_tanggal_pengesahan'], 'safe'],

            //apt_nomor_akta
            [['apt_nomor_akta'], 'required'],
            [['apt_nomor_akta'], 'string', 'max' => 4],

            //apt_tanggal
            [['apt_tanggal'], 'required'],
            [['apt_tanggal'], 'safe'],

            //apt_nama_notaris
            [['apt_nama_notaris'], 'required'],
            [['apt_nama_notaris'], 'string', 'max' => 128],

            //apt_nomor_pengesahan
            [['apt_nomor_pengesahan'], 'required'],
            [['apt_nomor_pengesahan'], 'string', 'max' => 64],

            //apt_tanggal_pengesahan
            [['apt_tanggal_pengesahan'], 'required'],
            [['apt_tanggal_pengesahan'], 'safe'],

            //iujk_berlaku_mulai
            [['iujk_berlaku_mulai'], 'required'],
            [['iujk_berlaku_mulai'], 'safe'],

            //iujk_berlaku_sampai
            [['iujk_berlaku_sampai'], 'required'],
            [['iujk_berlaku_sampai'], 'safe'],

            //iujk_pemberi_izin
            [['iujk_pemberi_izin'], 'required'],
            [['iujk_pemberi_izin'], 'string', 'max' => 128],

            //iujk_perencanaan
            [['iujk_perencanaan'], 'required'],
            [['iujk_perencanaan'], 'string', 'max' => 64],

            //iujk_pengawasan
            [['iujk_pengawasan'], 'required'],
            [['iujk_pengawasan'], 'string', 'max' => 64],

            //iujk_konsultansi
            [['iujk_konsultansi'], 'required'],
            [['iujk_konsultansi'], 'string', 'max' => 64],

            //sbuk_berlaku_mulai
            [['sbuk_berlaku_mulai'], 'required'],
            [['sbuk_berlaku_mulai'], 'safe'],

            //sbuk_berlaku_sampai
            [['sbuk_berlaku_sampai'], 'required'],
            [['sbuk_berlaku_sampai'], 'safe'],

            //sbuk_pemberi_izin
            [['sbuk_pemberi_izin'], 'required'],
            [['sbuk_pemberi_izin'], 'string', 'max' => 128],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id_paket' => 'Id Paket',
            'app_nomor_akta' => 'App Nomor Akta',
            'app_tanggal' => 'App Tanggal',
            'app_nama_notaris' => 'App Nama Notaris',
            'app_nomor_pengesahan' => 'App Nomor Pengesahan',
            'app_tanggal_pengesahan' => 'App Tanggal Pengesahan',
            'apt_nomor_akta' => 'Apt Nomor Akta',
            'apt_tanggal' => 'Apt Tanggal',
            'apt_nama_notaris' => 'Apt Nama Notaris',
            'apt_nomor_pengesahan' => 'Apt Nomor Pengesahan',
            'apt_tanggal_pengesahan' => 'Apt Tanggal Pengesahan',
            'iujk_berlaku_mulai' => 'Iujk Berlaku Mulai',
            'iujk_berlaku_sampai' => 'Iujk Berlaku Sampai',
            'iujk_pemberi_izin' => 'Iujk Pemberi Izin',
            'iujk_perencanaan' => 'Iujk Perencanaan',
            'iujk_pengawasan' => 'Iujk Pengawasan',
            'iujk_konsultansi' => 'Iujk Konsultansi',
            'sbuk_berlaku_mulai' => 'Sbuk Berlaku Mulai',
            'sbuk_berlaku_sampai' => 'Sbuk Berlaku Sampai',
            'sbuk_pemberi_izin' => 'Sbuk Pemberi Izin',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPaket()
    {
        return $this->hasOne(Paket::className(), ['id' => 'id_paket']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPaketFormBuktiPajaks()
    {
        return $this->hasMany(PaketFormBuktiPajak::className(), ['id_paket_form' => 'id_paket']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPaketFormDireksis()
    {
        return $this->hasMany(PaketFormDireksi::className(), ['id_paket_form' => 'id_paket']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPaketFormIjinLainnyas()
    {
        return $this->hasMany(PaketFormIjinLainnya::className(), ['id_paket_form' => 'id_paket']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPaketFormKomisarises()
    {
        return $this->hasMany(PaketFormKomisaris::className(), ['id_paket_form' => 'id_paket']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPaketFormPajaks()
    {
        return $this->hasMany(PaketFormPajak::className(), ['id_paket_form' => 'id_paket']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPaketFormPemilikSahams()
    {
        return $this->hasMany(PaketFormPemilikSaham::className(), ['id_paket_form' => 'id_paket']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPaketFormSbuks()
    {
        return $this->hasMany(PaketFormSbuk::className(), ['id_paket_form' => 'id_paket']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPaketFormSbunks()
    {
        return $this->hasMany(PaketFormSbunk::className(), ['id_paket_form' => 'id_paket']);
    }
}
