<?php
namespace app_virama_karya\models;

use Yii;

/**
 * This is the model class for table "unit_kerja".
 *
 * @property integer $id
 * @property string $nama_unit_kerja
 * @property string $type
 * @property string $alamat
 * @property string $nomor_telepon
 * @property string $nomor_fax
 * @property string $email
 *
 * @property BidangUnitKerja[] $bidangUnitKerjas
 * @property Paket[] $pakets
 */
class UnitKerja extends \technosmart\yii\db\ActiveRecord
{
    public static function tableName()
    {
        return 'unit_kerja';
    }

    public function rules()
    {
        return [
            //id

            //nama_unit_kerja
            [['nama_unit_kerja'], 'required'],
            [['nama_unit_kerja'], 'string', 'max' => 64],

            //type
            [['type'], 'required'],
            [['type'], 'string'],

            //alamat
            [['alamat'], 'string'],

            //nomor_telepon
            [['nomor_telepon'], 'string', 'max' => 16],

            //nomor_fax
            [['nomor_fax'], 'string', 'max' => 16],

            //email
            [['email'], 'string'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nama_unit_kerja' => 'Nama Unit Kerja',
            'type' => 'Type',
            'alamat' => 'Alamat',
            'nomor_telepon' => 'Nomor Telepon',
            'nomor_fax' => 'Nomor Fax',
            'email' => 'Email',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBidangUnitKerjas()
    {
        return $this->hasMany(BidangUnitKerja::className(), ['id_unit_kerja' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPakets()
    {
        return $this->hasMany(Paket::className(), ['id_unit_kerja' => 'id']);
    }
}
