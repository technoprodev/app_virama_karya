<?php
namespace app_virama_karya\models;

use Yii;

/**
 * This is the model class for table "tenaga_ahli".
 *
 * @property integer $id
 * @property string $nama
 * @property string $tanggal_lahir
 * @property string $jenis
 * @property string $kewarganegaraan
 *
 * @property TenagaAhliKeahlian[] $tenagaAhliKeahlians
 * @property TenagaAhliPendidikan[] $tenagaAhliPendidikans
 */
class TenagaAhli extends \technosmart\yii\db\ActiveRecord
{
    public static function tableName()
    {
        return 'tenaga_ahli';
    }

    public function rules()
    {
        return [
            //id

            //nama
            [['nama'], 'required'],
            [['nama'], 'string', 'max' => 128],

            //tanggal_lahir
            [['tanggal_lahir'], 'required'],
            [['tanggal_lahir'], 'safe'],

            //jenis
            [['jenis'], 'required'],
            [['jenis'], 'string'],

            //kewarganegaraan
            [['kewarganegaraan'], 'required'],
            [['kewarganegaraan'], 'string'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nama' => 'Nama',
            'tanggal_lahir' => 'Tanggal Lahir',
            'jenis' => 'Jenis',
            'kewarganegaraan' => 'Kewarganegaraan',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTenagaAhliKeahlians()
    {
        return $this->hasMany(TenagaAhliKeahlian::className(), ['id_tenaga_ahli' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTenagaAhliPendidikans()
    {
        return $this->hasMany(TenagaAhliPendidikan::className(), ['id_tenaga_ahli' => 'id']);
    }
}
