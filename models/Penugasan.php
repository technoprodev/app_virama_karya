<?php
namespace app_virama_karya\models;

use Yii;

/**
 * This is the model class for table "penugasan".
 *
 * @property integer $id
 * @property string $nama_penugasan
 *
 * @property PaketTenagaAhli[] $paketTenagaAhlis
 */
class Penugasan extends \technosmart\yii\db\ActiveRecord
{
    public static function tableName()
    {
        return 'penugasan';
    }

    public function rules()
    {
        return [
            //id

            //nama_penugasan
            [['nama_penugasan'], 'required'],
            [['nama_penugasan'], 'string', 'max' => 64],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nama_penugasan' => 'Nama Penugasan',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPaketTenagaAhlis()
    {
        return $this->hasMany(PaketTenagaAhli::className(), ['id_penugasan' => 'id']);
    }
}
