<?php
namespace app_virama_karya\models;

use Yii;

/**
 * This is the model class for table "rab".
 *
 * @property integer $id
 * @property integer $parent_id
 * @property integer $paket_id
 * @property integer $name
 */
class Rab extends \technosmart\yii\db\ActiveRecord
{
    public static function tableName()
    {
        return 'rab';
    }

    public function rules()
    {
        return [
            //id

            //paket_id
            [['paket_id'], 'integer'],
            [['paket_id'], 'required'],

            //name
            [['name'], 'required'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'paket_id' => 'Paket ID',
            'name' => 'Rab Item ID',
        ];
    }
}
