<?php
namespace app_virama_karya\models;

use Yii;

/**
 * This is the model class for table "paket_tenaga_ahli_pendidikan".
 *
 * @property integer $id
 * @property integer $id_paket_tenaga_ahli
 * @property string $jenjang
 * @property string $jurusan
 * @property integer $id_pendidikan_rumpun
 * @property string $tahun_lulus
 * @property string $nomor_ijazah
 *
 * @property PaketTenagaAhli $paketTenagaAhli
 * @property PendidikanRumpun $pendidikanRumpun
 */
class PaketTenagaAhliPendidikan extends \technosmart\yii\db\ActiveRecord
{
    public static function tableName()
    {
        return 'paket_tenaga_ahli_pendidikan';
    }

    public function rules()
    {
        return [
            //id

            //id_paket_tenaga_ahli
            [['id_paket_tenaga_ahli'], 'required'],
            [['id_paket_tenaga_ahli'], 'integer'],
            [['id_paket_tenaga_ahli'], 'exist', 'skipOnError' => true, 'targetClass' => PaketTenagaAhli::className(), 'targetAttribute' => ['id_paket_tenaga_ahli' => 'id']],

            //jenjang
            [['jenjang'], 'required'],
            [['jenjang'], 'string'],

            //jurusan
            [['jurusan'], 'required'],
            [['jurusan'], 'string', 'max' => 64],

            //id_pendidikan_rumpun
            [['id_pendidikan_rumpun'], 'required'],
            [['id_pendidikan_rumpun'], 'integer'],
            [['id_pendidikan_rumpun'], 'exist', 'skipOnError' => true, 'targetClass' => PendidikanRumpun::className(), 'targetAttribute' => ['id_pendidikan_rumpun' => 'id']],

            //tahun_lulus
            [['tahun_lulus'], 'required'],
            [['tahun_lulus'], 'safe'],

            //nomor_ijazah
            [['nomor_ijazah'], 'string', 'max' => 128],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_paket_tenaga_ahli' => 'Id Paket Tenaga Ahli',
            'jenjang' => 'Jenjang',
            'jurusan' => 'Jurusan',
            'id_pendidikan_rumpun' => 'Id Pendidikan Rumpun',
            'tahun_lulus' => 'Tahun Lulus',
            'nomor_ijazah' => 'Nomor Ijazah',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPaketTenagaAhli()
    {
        return $this->hasOne(PaketTenagaAhli::className(), ['id' => 'id_paket_tenaga_ahli']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPendidikanRumpun()
    {
        return $this->hasOne(PendidikanRumpun::className(), ['id' => 'id_pendidikan_rumpun']);
    }
}
