<?php
namespace app_virama_karya\models;

use Yii;

/**
 * This is the model class for table "form_komisaris".
 *
 * @property integer $id
 * @property integer $id_form
 * @property string $nama
 * @property string $nomor_ktp
 * @property string $jabatan
 *
 * @property Form $form
 */
class FormKomisaris extends \technosmart\yii\db\ActiveRecord
{
    public $isDeleted;

    public static function tableName()
    {
        return 'form_komisaris';
    }

    public function rules()
    {
        return [
            //id

            //id_form
            [['id_form'], 'required'],
            [['id_form'], 'integer'],
            [['id_form'], 'exist', 'skipOnError' => true, 'targetClass' => Form::className(), 'targetAttribute' => ['id_form' => 'id']],

            //nama
            [['nama'], 'required'],
            [['nama'], 'string', 'max' => 128],

            //nomor_ktp
            [['nomor_ktp'], 'required'],
            [['nomor_ktp'], 'string', 'max' => 16],

            //jabatan
            [['jabatan'], 'required'],
            [['jabatan'], 'string'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_form' => 'Id Form',
            'nama' => 'Nama',
            'nomor_ktp' => 'Nomor Ktp',
            'jabatan' => 'Jabatan',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getForm()
    {
        return $this->hasOne(Form::className(), ['id' => 'id_form']);
    }
}
