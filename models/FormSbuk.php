<?php
namespace app_virama_karya\models;

use Yii;

/**
 * This is the model class for table "form_sbuk".
 *
 * @property integer $id
 * @property integer $id_form
 * @property string $sertifikat
 * @property string $nomor_sertifikat
 *
 * @property Form $form
 */
class FormSbuk extends \technosmart\yii\db\ActiveRecord
{
    public $isDeleted;

    public static function tableName()
    {
        return 'form_sbuk';
    }

    public function rules()
    {
        return [
            //id

            //id_form
            [['id_form'], 'required'],
            [['id_form'], 'integer'],
            [['id_form'], 'exist', 'skipOnError' => true, 'targetClass' => Form::className(), 'targetAttribute' => ['id_form' => 'id']],

            //sertifikat
            [['sertifikat'], 'required'],
            [['sertifikat'], 'string', 'max' => 128],

            //nomor_sertifikat
            [['nomor_sertifikat'], 'required'],
            [['nomor_sertifikat'], 'string'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_form' => 'Id Form',
            'sertifikat' => 'Sertifikat',
            'nomor_sertifikat' => 'Nomor Sertifikat',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getForm()
    {
        return $this->hasOne(Form::className(), ['id' => 'id_form']);
    }
}
