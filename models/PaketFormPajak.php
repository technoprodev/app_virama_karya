<?php
namespace app_virama_karya\models;

use Yii;

/**
 * This is the model class for table "paket_form_pajak".
 *
 * @property integer $id
 * @property integer $id_paket_form
 * @property string $dokumen
 * @property string $nomor_dokumen
 * @property string $tanggal
 *
 * @property PaketForm $paketForm
 */
class PaketFormPajak extends \technosmart\yii\db\ActiveRecord
{
    public $isDeleted;

    public static function tableName()
    {
        return 'paket_form_pajak';
    }

    public function rules()
    {
        return [
            //id

            //id_paket_form
            [['id_paket_form'], 'required'],
            [['id_paket_form'], 'integer'],
            [['id_paket_form'], 'exist', 'skipOnError' => true, 'targetClass' => PaketForm::className(), 'targetAttribute' => ['id_paket_form' => 'id_paket']],

            //dokumen
            [['dokumen'], 'required'],
            [['dokumen'], 'string', 'max' => 128],

            //nomor_dokumen
            [['nomor_dokumen'], 'required'],
            [['nomor_dokumen'], 'string', 'max' => 128],

            //tanggal
            [['tanggal'], 'safe'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_paket_form' => 'Id Paket Form',
            'dokumen' => 'Dokumen',
            'nomor_dokumen' => 'Nomor Dokumen',
            'tanggal' => 'Tanggal',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPaketForm()
    {
        return $this->hasOne(PaketForm::className(), ['id_paket' => 'id_paket_form']);
    }
}
