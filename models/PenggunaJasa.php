<?php
namespace app_virama_karya\models;

use Yii;

/**
 * This is the model class for table "pengguna_jasa".
 *
 * @property integer $id
 * @property string $nama_pengguna_jasa
 *
 * @property Instansi[] $instansis
 * @property Paket[] $pakets
 */
class PenggunaJasa extends \technosmart\yii\db\ActiveRecord
{
    public static function tableName()
    {
        return 'pengguna_jasa';
    }

    public function rules()
    {
        return [
            //id

            //nama_pengguna_jasa
            [['nama_pengguna_jasa'], 'required'],
            [['nama_pengguna_jasa'], 'string', 'max' => 64],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nama_pengguna_jasa' => 'Nama Pengguna Jasa',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInstansis()
    {
        return $this->hasMany(Instansi::className(), ['id_pengguna_jasa' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPakets()
    {
        return $this->hasMany(Paket::className(), ['id_pengguna_jasa' => 'id']);
    }
}
