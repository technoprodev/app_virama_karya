<?php
namespace app_virama_karya\models;

use Yii;

/**
 * This is the model class for table "paket_tenaga_ahli".
 *
 * @property integer $id
 * @property integer $id_paket
 * @property string $nama
 * @property string $tanggal_lahir
 * @property string $jenis
 * @property string $kewarganegaraan
 * @property integer $id_penugasan
 * @property string $periode_mulai
 * @property string $periode_selesai
 *
 * @property Paket $paket
 * @property Penugasan $penugasan
 * @property PaketTenagaAhliKeahlian[] $paketTenagaAhliKeahlians
 * @property PaketTenagaAhliPendidikan[] $paketTenagaAhliPendidikans
 */
class PaketTenagaAhli extends \technosmart\yii\db\ActiveRecord
{
    public $isDeleted;
    public $id_tenaga_ahli;
    public $jenjang;
    public $jurusan;
    public $tahun_lulus;
    public $keahlian;
    public $level;
    public $sertifikat;

    public static function tableName()
    {
        return 'paket_tenaga_ahli';
    }

    public function rules()
    {
        return [
            //id

            //id_paket
            [['id_paket'], 'required'],
            [['id_paket'], 'integer'],
            [['id_paket'], 'exist', 'skipOnError' => true, 'targetClass' => Paket::className(), 'targetAttribute' => ['id_paket' => 'id']],

            //nama
            [['nama'], 'required'],
            [['nama'], 'string', 'max' => 128],

            //tanggal_lahir
            [['tanggal_lahir'], 'required'],
            [['tanggal_lahir'], 'safe'],

            //jenis
            [['jenis'], 'required'],
            [['jenis'], 'string'],

            //kewarganegaraan
            [['kewarganegaraan'], 'required'],
            [['kewarganegaraan'], 'string'],

            //id_penugasan
            [['id_penugasan'], 'required'],
            [['id_penugasan'], 'integer'],
            [['id_penugasan'], 'exist', 'skipOnError' => true, 'targetClass' => Penugasan::className(), 'targetAttribute' => ['id_penugasan' => 'id']],

            //periode_mulai
            [['periode_mulai'], 'required'],
            [['periode_mulai'], 'safe'],

            //periode_selesai
            [['periode_selesai'], 'required'],
            [['periode_selesai'], 'safe'],
            
            //virtual
            [['id_tenaga_ahli'], 'safe'],
            [['jenjang'], 'safe'],
            [['jurusan'], 'safe'],
            [['tahun_lulus'], 'safe'],
            [['keahlian'], 'safe'],
            [['level'], 'safe'],
            [['sertifikat'], 'safe'],
        ];
    }

    public function afterFind()
    {
        parent::afterFind();
        
        foreach ($this->paketTenagaAhliPendidikans as $key => $value) {
            $this->jenjang .= '• ' . $value->jenjang . ' ';
            $this->jurusan .= '• ' . $value->jurusan . ' ';
            $this->tahun_lulus .= '• ' . $value->tahun_lulus . ' ';
        }
        foreach ($this->paketTenagaAhliKeahlians as $key => $value) {
            $this->keahlian .= '• ' . $value->keahlian . ' ';
            $this->level .= '• ' . $value->level . ' ';
            $this->sertifikat .= '• ' . $value->sertifikat . ' ';
        }
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_paket' => 'Id Paket',
            'nama' => 'Nama',
            'tanggal_lahir' => 'Tanggal Lahir',
            'jenis' => 'Jenis',
            'kewarganegaraan' => 'Kewarganegaraan',
            'id_penugasan' => 'Id Penugasan',
            'periode_mulai' => 'Periode Mulai',
            'periode_selesai' => 'Periode Selesai',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPaket()
    {
        return $this->hasOne(Paket::className(), ['id' => 'id_paket']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPenugasan()
    {
        return $this->hasOne(Penugasan::className(), ['id' => 'id_penugasan']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPaketTenagaAhliKeahlians()
    {
        return $this->hasMany(PaketTenagaAhliKeahlian::className(), ['id_paket_tenaga_ahli' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPaketTenagaAhliPendidikans()
    {
        return $this->hasMany(PaketTenagaAhliPendidikan::className(), ['id_paket_tenaga_ahli' => 'id']);
    }
}
