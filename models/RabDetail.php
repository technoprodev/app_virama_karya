<?php
namespace app_virama_karya\models;

use Yii;

/**
 * This is the model class for table "rab_detail".
 *
 * @property integer $id
 * @property integer $parent_id
 * @property string $value
 */
class RabDetail extends \technosmart\yii\db\ActiveRecord
{
    public static function tableName()
    {
        return 'rab_detail';
    }

    public function rules()
    {
        return [
            //id

            //parent_id
            [['parent_id'], 'integer'],

            //parent_id
            [['rab_data_id'], 'integer'],

            //value
            [['value'], 'string'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'parent_id' => 'Parent ID',
            'rab_data_id' => 'Rab Data ID',
            'value' => 'Value',
        ];
    }
}
