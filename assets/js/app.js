if(typeof Vue == 'function') {
    if(typeof VueDefaultValue == 'object') {
        Vue.use(VueDefaultValue);
    }

    if(typeof Vue.http == 'function') {
        Vue.http.headers.common['X-CSRF-Token'] = csrfToken;
    }

    Vue.directive('init', {
        inserted: function(el) {
            pluginInit(el);
        },
        componentUpdated: function(el) {
            pluginInit(el);
        },
    });

    var vm = new Vue({
        el: '#app',
        data: {
            form: {
                formKomisarises: [],
                formDireksis: [],
                formSbuks: [],
                formSbunks: [],
                formIjinLainnyas: [],
                formPemilikSahams: [],
                formPajaks: [],
                formBuktiPajaks: [],
            },
            paket_form: {
                paketFormKomisarises: [],
                paketFormDireksis: [],
                paketFormSbuks: [],
                paketFormSbunks: [],
                paketFormIjinLainnyas: [],
                paketFormPemilikSahams: [],
                paketFormPajaks: [],
                paketFormBuktiPajaks: [],
            },
            tenaga_ahli: {
                tenagaAhliPendidikans: [],
                tenagaAhliKeahlians: [],
            },
            paket: {
                status: [],
                is_kso: 'Tidak',
                paketTenagaAhlis: [],
                paketMitras: [],
            },
        },
        methods: {
            addFormKomisaris: function() {
                this.form.formKomisarises.push({
                    id: null,
                    id_form: null,
                    nama: null,
                    nomor_ktp: null,
                    jabatan: null,
                });
            },
            removeFormKomisaris: function(i, isNew) {
                if (this.form.formKomisarises[i].id == null)
                    this.form.formKomisarises.splice(i, 1);
                else
                    this.form.formKomisarises[i].id*=-1;
            },
            
            addFormDireksi: function() {
                this.form.formDireksis.push({
                    id: null,
                    id_form: null,
                    nama: null,
                    nomor_ktp: null,
                    jabatan: null,
                });
            },
            removeFormDireksi: function(i, isNew) {
                if (this.form.formDireksis[i].id == null)
                    this.form.formDireksis.splice(i, 1);
                else
                    this.form.formDireksis[i].id*=-1;
            },
            
            addFormSbuk: function() {
                this.form.formSbuks.push({
                    id: null,
                    id_form: null,
                    sertifikat: null,
                    nomor_sertifikat: null,
                });
            },
            removeFormSbuk: function(i, isNew) {
                if (this.form.formSbuks[i].id == null)
                    this.form.formSbuks.splice(i, 1);
                else
                    this.form.formSbuks[i].id*=-1;
            },
            
            addFormSbunk: function() {
                this.form.formSbunks.push({
                    id: null,
                    id_form: null,
                    sertifikat: null,
                    nomor_sertifikat: null,
                    berlaku_mulai: null,
                    berlaku_sampai: null,
                    pemberi_izin: null,
                });
            },
            removeFormSbunk: function(i, isNew) {
                if (this.form.formSbunks[i].id == null)
                    this.form.formSbunks.splice(i, 1);
                else
                    this.form.formSbunks[i].id*=-1;
            },
            
            addFormIjinLainnya: function() {
                this.form.formIjinLainnyas.push({
                    id: null,
                    id_form: null,
                    sertifikat: null,
                    nomor_sertifikat: null,
                    berlaku_mulai: null,
                    berlaku_sampai: null,
                    pemberi_izin: null,
                });
            },
            removeFormIjinLainnya: function(i, isNew) {
                if (this.form.formIjinLainnyas[i].id == null)
                    this.form.formIjinLainnyas.splice(i, 1);
                else
                    this.form.formIjinLainnyas[i].id*=-1;
            },
            
            addFormPemilikSaham: function() {
                this.form.formPemilikSahams.push({
                    id: null,
                    id_form: null,
                    nama: null,
                    nomor_ktp: null,
                    alamat: null,
                    persentase: null,
                });
            },
            removeFormPemilikSaham: function(i, isNew) {
                if (this.form.formPemilikSahams[i].id == null)
                    this.form.formPemilikSahams.splice(i, 1);
                else
                    this.form.formPemilikSahams[i].id*=-1;
            },
            
            addFormPajak: function() {
                this.form.formPajaks.push({
                    id: null,
                    id_form: null,
                    dokumen: null,
                    nomor_dokumen: null,
                    tanggal: null,
                });
            },
            removeFormPajak: function(i, isNew) {
                if (this.form.formPajaks[i].id == null)
                    this.form.formPajaks.splice(i, 1);
                else
                    this.form.formPajaks[i].id*=-1;
            },

            addFormBuktiPajak: function() {
                this.form.formBuktiPajaks.push({
                    id: null,
                    id_form: null,
                    bulan: '',
                    tahun: null,

                    nomor_pasal21_26: null,
                    tanggal_pasal21_26: null,

                    nomor_pasal23_26: null,
                    tanggal_pasal23_26: null,

                    nomor_pasal4ayat2: null,
                    tanggal_pasal4ayat2: null,

                    nomor_pasal25: null,
                    tanggal_pasal25: null,
                    
                    nomor_ppnbm: null,
                    tanggal_ppnbm: null,
                });
            },
            removeFormBuktiPajak: function(i, isNew) {
                if (this.form.formBuktiPajaks[i].id == null)
                    this.form.formBuktiPajaks.splice(i, 1);
                else
                    this.form.formBuktiPajaks[i].id*=-1;
            },

            //

            addPaketFormKomisaris: function() {
                this.paket_form.paketFormKomisarises.push({
                    id: null,
                    id_paket_form: null,
                    nama: null,
                    nomor_ktp: null,
                    jabatan: null,
                });
            },
            removePaketFormKomisaris: function(i, isNew) {
                if (this.paket_form.paketFormKomisarises[i].id == null)
                    this.paket_form.paketFormKomisarises.splice(i, 1);
                else
                    this.paket_form.paketFormKomisarises[i].id*=-1;
            },
            
            addPaketFormDireksi: function() {
                this.paket_form.paketFormDireksis.push({
                    id: null,
                    id_paket_form: null,
                    nama: null,
                    nomor_ktp: null,
                    jabatan: null,
                });
            },
            removePaketFormDireksi: function(i, isNew) {
                if (this.paket_form.paketFormDireksis[i].id == null)
                    this.paket_form.paketFormDireksis.splice(i, 1);
                else
                    this.paket_form.paketFormDireksis[i].id*=-1;
            },
            
            addPaketFormSbuk: function() {
                this.paket_form.paketFormSbuks.push({
                    id: null,
                    id_paket_form: null,
                    sertifikat: null,
                    nomor_sertifikat: null,
                });
            },
            removePaketFormSbuk: function(i, isNew) {
                if (this.paket_form.paketFormSbuks[i].id == null)
                    this.paket_form.paketFormSbuks.splice(i, 1);
                else
                    this.paket_form.paketFormSbuks[i].id*=-1;
            },
            
            addPaketFormSbunk: function() {
                this.paket_form.paketFormSbunks.push({
                    id: null,
                    id_paket_form: null,
                    sertifikat: null,
                    nomor_sertifikat: null,
                    berlaku_mulai: null,
                    berlaku_sampai: null,
                    pemberi_izin: null,
                });
            },
            removePaketFormSbunk: function(i, isNew) {
                if (this.paket_form.paketFormSbunks[i].id == null)
                    this.paket_form.paketFormSbunks.splice(i, 1);
                else
                    this.paket_form.paketFormSbunks[i].id*=-1;
            },
            
            addPaketFormIjinLainnya: function() {
                this.paket_form.paketFormIjinLainnyas.push({
                    id: null,
                    id_paket_form: null,
                    sertifikat: null,
                    nomor_sertifikat: null,
                    berlaku_mulai: null,
                    berlaku_sampai: null,
                    pemberi_izin: null,
                });
            },
            removePaketFormIjinLainnya: function(i, isNew) {
                if (this.paket_form.paketFormIjinLainnyas[i].id == null)
                    this.paket_form.paketFormIjinLainnyas.splice(i, 1);
                else
                    this.paket_form.paketFormIjinLainnyas[i].id*=-1;
            },
            
            addPaketFormPemilikSaham: function() {
                this.paket_form.paketFormPemilikSahams.push({
                    id: null,
                    id_paket_form: null,
                    nama: null,
                    nomor_ktp: null,
                    alamat: null,
                    persentase: null,
                });
            },
            removePaketFormPemilikSaham: function(i, isNew) {
                if (this.paket_form.paketFormPemilikSahams[i].id == null)
                    this.paket_form.paketFormPemilikSahams.splice(i, 1);
                else
                    this.paket_form.paketFormPemilikSahams[i].id*=-1;
            },
            
            addPaketFormPajak: function() {
                this.paket_form.paketFormPajaks.push({
                    id: null,
                    id_paket_form: null,
                    dokumen: null,
                    nomor_dokumen: null,
                    tanggal: null,
                });
            },
            removePaketFormPajak: function(i, isNew) {
                if (this.paket_form.paketFormPajaks[i].id == null)
                    this.paket_form.paketFormPajaks.splice(i, 1);
                else
                    this.paket_form.paketFormPajaks[i].id*=-1;
            },

            addPaketFormBuktiPajak: function() {
                this.paket_form.paketFormBuktiPajaks.push({
                    id: null,
                    id_paket_form: null,
                    bulan: '',
                    tahun: null,

                    nomor_pasal21_26: null,
                    tanggal_pasal21_26: null,

                    nomor_pasal23_26: null,
                    tanggal_pasal23_26: null,

                    nomor_pasal4ayat2: null,
                    tanggal_pasal4ayat2: null,

                    nomor_pasal25: null,
                    tanggal_pasal25: null,
                    
                    nomor_ppnbm: null,
                    tanggal_ppnbm: null,
                });
            },
            removePaketFormBuktiPajak: function(i, isNew) {
                if (this.paket_form.paketFormBuktiPajaks[i].id == null)
                    this.paket_form.paketFormBuktiPajaks.splice(i, 1);
                else
                    this.paket_form.paketFormBuktiPajaks[i].id*=-1;
            },

            addTenagaAhliPendidikan: function() {
                pluginInit();
                this.tenaga_ahli.tenagaAhliPendidikans.push({
                    id: null,
                    id_tenaga_ahli: null,
                    jenjang: '',
                    jurusan: null,
                    id_pendidikan_rumpun: '',
                    tahun_lulus: null,
                    nomor_ijazah: null,
                });
            },
            removeTenagaAhliPendidikan: function(i, isNew) {
                pluginInit();
                if (this.tenaga_ahli.tenagaAhliPendidikans[i].id == null)
                    this.tenaga_ahli.tenagaAhliPendidikans.splice(i, 1);
                else
                    this.tenaga_ahli.tenagaAhliPendidikans[i].id*=-1;
            },
            addTenagaAhliKeahlian: function() {
                pluginInit();
                this.tenaga_ahli.tenagaAhliKeahlians.push({
                    id: null,
                    id_tenaga_ahli: null,
                    keahlian: null,
                    level: '',
                    sertifikat: null,
                });
            },
            removeTenagaAhliKeahlian: function(i, isNew) {
                pluginInit();
                if (this.tenaga_ahli.tenagaAhliKeahlians[i].id == null)
                    this.tenaga_ahli.tenagaAhliKeahlians.splice(i, 1);
                else
                    this.tenaga_ahli.tenagaAhliKeahlians[i].id*=-1;
            },

            addPaketTenagaAhli: function(data) {
                pluginInit();
                this.paket.paketTenagaAhlis.push(data);
            },
            removePaketTenagaAhli: function(i, isNew) {
                pluginInit();

                if (typeof this.paket.paketTenagaAhlis[i] === "undefined") {
                    return;
                }

                if (this.paket.paketTenagaAhlis[i].id == null)
                    this.paket.paketTenagaAhlis.splice(i, 1);
                else
                    this.paket.paketTenagaAhlis[i].id*=-1;
            },
            addPaketMitra: function() {
                pluginInit();
                this.paket.paketMitras.push({
                    id: null,
                    id_paket: null,
                    id_mitra: '',
                    porsi: null,
                });
            },
            removePaketMitra: function(i, isNew) {
                pluginInit();
                if (this.paket.paketMitras[i].id == null)
                    this.paket.paketMitras.splice(i, 1);
                else
                    this.paket.paketMitras[i].id*=-1;
            },
            enter: function (value) {
                if (value) {
                    return value.replace(/,/g, '<br>');
                } else {
                    return value;
                }
            },
        },
        filters: {
            enter: function (value) {
                function htmlEncode(value){
                    return $('<div/>').text(value).html();
                }

                function htmlDecode(value){
                    return $('<div/>').html(value).text();
                }

                htmlEncode('<b>test</b>')
                if (value) {
                    return value.replace(/,/g, htmlDecode(' '));
                } else {
                    return value;
                }
            }
        }
    });
}