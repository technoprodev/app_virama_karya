<?php
namespace app_virama_karya\assets_manager;

use yii\web\AssetBundle;

class RequiredAsset extends AssetBundle
{
	public $sourcePath = '@app_virama_karya/assets';

    public $css = [];

    public $js = [
        'js/app.js',
    ];

    public $depends = [
        'technosmart\assets_manager\VueAsset',
        'technosmart\assets_manager\VueResourceAsset',
        'technosmart\assets_manager\RequiredAsset',
        'technosmart\assets_manager\LodashAsset',
    ];
}