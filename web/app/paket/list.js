$(document).ready(function() {
    // datatabless
    if (typeof $.fn.dataTable === 'function') {
        $.fn.dataTableExt.oStdClasses.sWrapper = 'dataTables_wrapper ff-default';
        $.fn.dataTableExt.oStdClasses.sLength = 'dataTables_length form-group form-group-sm pull-left';
        $.fn.dataTableExt.oStdClasses.sLengthSelect = 'form-control';
        $.fn.dataTableExt.oStdClasses.sFilter = 'dataTables_filter form-group form-group-sm pull-right';
        $.fn.dataTableExt.oStdClasses.sFilterInput = 'form-control';
        $.fn.dataTableExt.oStdClasses.sInfo = 'dataTables_info pull-left';
        $.fn.dataTableExt.oStdClasses.sPaging = 'dataTables_paginate pull-right paging_';
        $.fn.dataTableExt.oStdClasses.sPageButtonActive = 'active';
        var el = $('.datatables');
        $.each(el, function() {
            var table = $(this).DataTable({
                "autoWidth": false,
                "deferRender": true,
                "processing": true,
                "serverSide": true,
                "ajax": {
                    "url": fn.urlTo('paket/datatables'),
                    "type": "POST"
                },
                "columns": [
                    {
                        data: {
                            _: 'p.id_unit_kerja',
                            'filter': 'p.id_unit_kerja',
                            'sort': 'p.id_unit_kerja',
                            'display': 'nama_unit_kerja',
                        },
                        'defaultContent':'&nbsp;',
                    },
                    {
                        'data': 'nama_paket',
                        render: function ( data, type, row ) {
                            return '<a href="' + fn.urlTo('paket/' + row.id) + '" class="text-azure">' + data + '</a>';
                        },
                        createdCell: function (td, cellData, rowData, row, col) {
                            pluginInit(td);
                        },
                        'defaultContent':'&nbsp;',
                    },
                    {
                        data: {
                            _: 'p.id_instansi',
                            'filter': 'p.id_instansi',
                            'sort': 'p.id_instansi',
                            'display': 'nama_instansi',
                        },
                        'defaultContent':'&nbsp;',
                    },
                    {
                        data: {
                            _: 'p.hps',
                            'filter': 'p.hps',
                            'sort': 'p.hps',
                            'display': 'hps',
                        },
                        'defaultContent':'&nbsp;',
                    },
                    {
                        data: {
                            _: 't.id',
                            'filter': 't.id',
                            'sort': 't.id',
                            'display': 'tahap',
                        },
                        render: function ( data, type, row ) {
                            if (row.tahap) {
                                return row.tahap.replace(/,/g, ', ');
                            } else {
                                return row.tahap;
                            }
                        },
                        'defaultContent':'&nbsp;',
                    },
                ],
                dom: '<"clearfix"l><"clearfix margin-bottom-10 scroll-x"rt><"clearfix"ip>',
                "lengthMenu": [[10, 25, 50, 100, -1], ['10 data', '25 data', '50 data', '100 data', "All data"]],
                "orderCellsTop": true,
                language: {
                    lengthMenu : "_MENU_",
                    search: "",
                    searchPlaceholder: "Search here",
                    buttons: {
                        copyTitle: 'Title',
                        copyKeys: 'copy keys',
                        copySuccess: {
                            _: '%d rows copied',
                            1: '1 row copied'
                        }
                    }
                },
                colReorder: true
            });

            var dtSearch = $('.dt-search', $(this));
            table.columns().every(function(index, table, column){
                var that = this;
                
                var timer = null;
                dtSearch.find('th:nth-child(' + (index + 1) + ') .search').on('keyup change', function(){
                    var val = {
                        that : that,
                        this : this,
                    };

                    clearTimeout(timer); 
                       timer = setTimeout(function () {
                        if(val.that.search() !== val.this.value){
                            val.that.search( val.this.value ).draw();
                        }
                    }, 500);
                });
            });
        });
    }
});