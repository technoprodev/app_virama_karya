var v = new Vue({
    el: '#timeline',
    data: {
        timeline: [],
        config: {
            period_length: period_length,
        },
        increment: {},
        default: {
            paket_id: paket_id,
        	item: {
    			task: "",
                month_start: 0,
                month_end: 0,
                level: 0,
                parent_index: undefined,
                children: [],
    		},
        }
    },
    methods: {
    	init: function() {
            var self = this;
            
            $.ajax({
                url: fn.urlTo("timeline", {id: self.default.paket_id}),
                dataType: "json",
                success: function(data) {
                    if (data.length > 0) {
                        for (var i in data) {
                            self.timeline.push($.extend(true, {}, self.default.item, data[i]));
                        }

                        self.buildStructure(true);
                    }
                    else {
                        self.addItem();
                    }
                },
            })
    	},
        setTimelineRange: function(item, period) {
            var middle_index = Math.floor((item.month_end - item.month_start) / 2);
            var period_index = period - item.month_start;

            if (
                item.month_start == 0 // first init
                || period < item.month_start // out of range
                || period_index < middle_index // left side
            ) {
                item.month_start = period;
            }
            else if (
                (item.month_start > 0 && item.month_end == 0) // first init
                || period > item.month_end // out of range
                || period_index >= middle_index // right side
            ) {
                item.month_end = period;
            }

            if (item.month_end < item.month_start) {
                item.month_end = item.month_start;
            }
        },
        addItem: function(data) {
            this.timeline.push($.extend(true, {}, this.default.item, data));
            this.buildStructure();
        },
        setAsChild: function(index) {
            var item = this.timeline[index];

            for (var i = index-1; i >= 0; i--) {
                var parent = this.timeline[i];

                if (item.level > parent.level) {
                    break;
                }
                else if (item.level == parent.level) {
                    item.parent_index = i;
                    break;
                }
            }

            this.buildStructure();
        },
        setAsParent: function(index) {
            var item = this.timeline[index];

            if (item.level <= 1) {
                item.parent_index = undefined;
            }
            else {
                for (var i = index-1; i >= 0; i--) {
                    var parent = this.timeline[i];

                    if (item.level-2 == parent.level) {
                        item.parent_index = i;
                        break;
                    }
                }
            }

            this.buildStructure();
        },
        removeItem: function(index) {
            // if (!confirm("Apakah anda yakin ingin menghapus ?")) {
            //     return;
            // }

            this.timeline.splice(index, 1);
    	},
        getIncrement: function(item) {
            var increment_idx = item.level+'.'+(item.parent_index || 'x');

            if (_.isUndefined(this.increment[increment_idx])) {
                this.increment[increment_idx] = 1;
            }

            var increment = this.increment[increment_idx]++;

            switch (item.level) {
                case 0: return String.fromCharCode('A'.charCodeAt() + (increment - 1));
                case 2: return String.fromCharCode('a'.charCodeAt() + (increment - 1));
                default: return increment;
            }
        },
        buildStructure: function(cleanBuild) {
            if (true) {
            // if (cleanBuild) {
                for (var i in this.timeline) {
                    var item = this.timeline[i];

                    // reset
                    item.children = [];
                    item.level = 0;

                    var parent = this.timeline[item.parent_index];

                    if (!_.isUndefined(parent)) {
                        parent.children.push(item);
                    }
                }
            }

            var makeTreeLevel = function(data, level) {
                for (var i in data) {
                    var item = data[i];

                    if (item.level == 0) {
                        item.level = level;
                    }
                    
                    if (item.children.length > 0) {
                        makeTreeLevel(item.children, level + 1);
                    }
                }
            }

            makeTreeLevel(this.timeline, 0);
            pluginInit()
        },
    },
    watch: {
        timeline: {
            deep: true,
            handler: function() {
                this.increment = {};
            },
        },
    },
});

v.init();
