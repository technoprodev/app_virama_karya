var v = new Vue({
    el: '#rab-detail',
    data: {
        readonly: true,
        data: [],
        config: {
            col_length: 0,
            sum_col_name: "",
            struct_def: {},
            column: {},
        },
        default: {
            item: {},
        },
    },
    methods: {
        init: function() {
            var self = this;
            
            $.ajax({
                dataType: "json",
                success: function(res) {
                    // table config
                    self.config.struct_def = res.rab.struct_def;

                    if (_.has(self.config.struct_def, "_config")) {
                        self.config.sum_col_name = self.config.struct_def._config.total;
                    }

                    for (var key in self.config.struct_def) {
                        if (key == "_config") {
                            continue;
                        }
                        
                        var item = self.config.struct_def[key];

                        item.id = key;
                        self.config.column[item.order] = item;
                    }

                    self.config.col_length = Object.entries(self.config.struct_def).length - 1;

                    // default value item
                    self.default.item = res.default;

                    // table rows
                    if (res.data.length > 0) {
                        self.data = res.data;

                        // calculate pattern
                        for (var i in self.data) {
                            for (var key in self.config.struct_def) {
                                if (self.config.struct_def[key].type == "pattern") {
                                    self.handleUpdate(self.data[i], key);
                                }
                            }
                        }
                    }
                    else {
                        self.readonly = false;
                        self.addItem();
                    }
                },
            })
        },
        setModeEdit: function() {
            var x = window.confirm('Apakah anda yakin ingin mengedit ?');

            if (x) {
                this.readonly = false;
            }
        },
        makeItem: function(data) {
            return $.extend(true, {}, this.default.item, data);
        },
        addItem: function(data) {
            this.data.push(this.makeItem(data));
            // rab.value = this.getRabTotal(rab);
        },
        removeItem: function(index) {
            this.data.splice(index, 1);

            // #dev ajax delete
        },
        handleUpdate: function(data, key) {
            for (var key in this.config.struct_def) {
                var col = this.config.struct_def[key];

                switch (col.type) {
                    case "pattern":
                        data[key] = this.calculate(col.pattern, data);
                        break;
                }
            }
        },
        calculate: function(pattern, data) {
            if (typeof data === "undefined") {
                return;
            }

            var regex = /(\$[\w]+)\s*([\+\-\*\/])?/g;
            var m;

            var result = 0;
            var opr = "+";

            while (m = regex.exec(pattern)) {
                var key = m[1].substring(1);

                if (data.hasOwnProperty(key)) {
                    var value = isNaN(data[key]) ? 0 : parseInt(data[key]);

                    switch (opr) {
                        case "+": result += value; break;
                        case "-": result -= value; break;
                        case "*": result *= value; break;
                        case "/": result /= value; break;
                    }
                }

                opr = m[2];
            }

            return result;
        },
    },
    computed: {
        total: function() {
            var total = 0;

            for (var i in this.data) {
                total += !_.isUndefined(this.config.sum_col_name) 
                    ? parseInt(this.data[i][this.config.sum_col_name]) 
                    : 0;
            }

            return total;
        },
    },
});

v.init();
