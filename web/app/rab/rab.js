var v = new Vue({
    el: '#rab',
    data: {
        readonly: true,
        rab: [],
        options: [],
        default: {
            paket_id: paket_id,
            category: {
                name: "",
                value: 0,
                items: [],
            },
        	item: {
    			rab_item: "",
    			value: 0,
    		},
        }
    },
    methods: {
    	init: function() {
            var self = this;
            
            $.ajax({
                dataType: "json",
                url: fn.urlTo("rab", {id: self.default.paket_id}),
                success: function(res) {
                    self.options = res.options;

                    if (res.data.length > 0) {
                        self.rab = res.data;
                    }
                    else {
                        self.readonly = false;
                		self.addCategory();
                    }
                },
            })
    	},
        setModeEdit: function() {
            this.readonly = false;
        },
        addCategory: function(data) {
            data = $.extend(true, {}, this.default.category, data);
            data.value = this.getRabTotal(data);

            this.rab.push(data);
            this.addItem(data);
        },
        addItem: function(rab, data) {
            rab.items.push($.extend(true, {}, this.default.item, data));
            rab.value = this.getRabTotal(rab);
        },
    	removeCategory: function(index) {
            var self = this;
            var id = this.rab[index].id;
            var remove = function() {
                self.rab.splice(index, 1);
            };

            if (_.isUndefined(id)) {
                remove();
                return;
            }

            fn.confirm({
                // title: $(this).attr('confirm-title'),
                body: "Apakah anda yakin ingin menghapus ?",
                yes: function(){
                    $.ajax({
                        url: fn.urlTo("rab/delete-rab?id="+ id),
                        type: "post",
                        success: remove,
                    })
                }
            });
        },
        removeItem: function(rab, index) {
            var id = rab.items[index].id;
            var remove = function() {
                rab.items.splice(index, 1);
            };

            if (_.isUndefined(id)) {
                remove();
                return;
            }

            fn.confirm({
                // title: $(this).attr('confirm-title'),
                body: "Apakah anda yakin ingin menghapus ?",
                yes: function(){
                    $.ajax({
                        url: fn.urlTo("rab/delete-rab-data?id="+ id),
                        type: "post",
                        success: remove,
                    })
                }
            });
    	},
    	getRabTotal: function(rab) {
    		var total = 0;

    		if (typeof rab === "undefined" || !rab.hasOwnProperty("items")) {
    			rab.items = [];
    		}

    		for (var j in rab.items) {
				total += parseInt(rab.items[j].value);
			}

			return total;
    	},
    },
    computed: {
    	total: function() {
    		var total = 0;

    		for (var i in this.rab) {
    			total += this.getRabTotal(this.rab[i]);
			}

			return total;
    	},
    	ppn: function() {
    		return this.total * 10/100;
    	},
    },
});

function spell_amount(value) {
    var dictionary = {
        1: 'satu', 2: 'dua', 3: 'tiga', 4: 'empat', 5: 'lima',
        6: 'enam', 7: 'tujuh', 8: 'delapan', 9: 'sembilan',
    };
    var unit = {
        12: 'triliun', 9: 'milyar', 6: 'juta',
        3: 'ribu', 2: 'ratus', 1: 'puluh', 0: '',
    };
    
    var fraction = '';
    value = new String(value);

    var decimal_regx = /[^.,]*?([.,]{1}?)/.exec(value);
    var separator = decimal_regx ? decimal_regx[1] : false;

    if (separator) {
        var p = value.split(separator);
        var fraction = p.length > 1 && p[1] > 0 ? ' koma '+spell_amount(p[1]) : '';

        value = p[0];
    }

    var str = '';
    var len_digit = Object.keys(unit);

    // for (var i in len_digit) {
    for (var i = len_digit.length-1; i >= 0; i--) {
        if (value.length <= len_digit[i]) {
            continue;
        }
        
        var domain = value.substring(0, value.length - len_digit[i]);
        
        if (domain.length > 1) {
            str += spell_amount(domain);
            str += " " + unit[len_digit[i]] + " ";
        }
        else if (dictionary.hasOwnProperty(domain)) {
            str += dictionary[domain];
            str += " " + unit[len_digit[i]] + " ";
        }
        
        value = value.substring(domain.length);
    }

    str = str.replace(/satu puluh (\w+)/i, "$1 belas")
        .replace(/satu (ribu|ratus|puluh|belas)/i, "se$1")
        .trim();

    return str + fraction;
}

v.init();
