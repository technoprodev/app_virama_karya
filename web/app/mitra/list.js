$(document).ready(function() {
    // datatabless
    if (typeof $.fn.dataTable === 'function') {
        $.fn.dataTableExt.oStdClasses.sWrapper = 'dataTables_wrapper ff-default';
        $.fn.dataTableExt.oStdClasses.sLength = 'dataTables_length form-group form-group-sm pull-left';
        $.fn.dataTableExt.oStdClasses.sLengthSelect = 'form-control';
        $.fn.dataTableExt.oStdClasses.sFilter = 'dataTables_filter form-group form-group-sm pull-right';
        $.fn.dataTableExt.oStdClasses.sFilterInput = 'form-control';
        $.fn.dataTableExt.oStdClasses.sInfo = 'dataTables_info pull-left';
        $.fn.dataTableExt.oStdClasses.sPaging = 'dataTables_paginate pull-right paging_';
        $.fn.dataTableExt.oStdClasses.sPageButtonActive = 'active';
        var el = $('.datatables');
        $.each(el, function() {
            var table = $(this).DataTable({
                "autoWidth": false,
                "deferRender": true,
                "processing": true,
                "serverSide": true,
                "ajax": {
                    "url": fn.urlTo('mitra/datatables'),
                    "type": "POST"
                },
                "columns": [
                    {
                        'data': 'id',
                        'searchable': false,
                        'orderable': false,
                        render: function ( data, type, row ) {
                            return '<a href="' + fn.urlTo('mitra/' + data) + '" class="text-azure" modal-md="" modal-title="Data Keterangan Mitra ' + row.id + '"><i class="fa fa-eye"></i></a>&nbsp;&nbsp;' +
                                '<a href="' + fn.urlTo('mitra/update/' + data) + '" class="text-spring"><i class="fa fa-pencil"></i></a>&nbsp;&nbsp;' +
                                '<a href="' + fn.urlTo('mitra/delete/' + data) + '" class="text-rose" data-confirm="Are you sure you want to delete this item?" data-method="post"><i class="fa fa-trash-o"></i></a>';
                        },
                        createdCell: function (td, cellData, rowData, row, col) {
                            pluginInit(td);
                        },
                        'defaultContent':'&nbsp;',
                    },
                    {'data': 'nama_perusahaan'},
                    {'data': 'alamat_perusahaan'},
                ],
                dom: '<"clearfix"lBf><"clearfix margin-bottom-10 scroll-x"rt><"clearfix"ip>',
                "lengthMenu": [[10, 25, 50, 100, -1], ['10 entries', '25 entries', '50 entries', '100 entries', "All entries"]],
                "orderCellsTop": true,
                // "ordering": false, //
                // "order": [], //
                // "order": [[1, 'asc'], [2, 'asc']], //

                buttons: {
                    dom: {
                        container: {
                            className: 'dt-buttons pull-left hidden-sm-less'
                        },
                        button: {
                            className: 'margin-left-5 margin-bottom-5 btn btn-default btn-sm',
                            active: 'bg-azure border-azure'
                        }
                    },
                    buttons: [
                        {
                            extend: 'colvis',
                            title: 'Data show/hide',
                            text: 'Show/hide <i class="fa fa-angle-down"></i>'
                        },
                        {
                            extend: 'copy',
                            title: 'Data export',
                            text: 'Copy'
                        },
                        {
                            extend: 'csv',
                            title: 'Data export',
                            text: 'Csv'
                        },
                        {
                            extend: 'excel',
                            title: 'Data export',
                            text: 'Excel'
                        },
                        {
                            extend: 'pdf',
                            title: 'Data export',
                            text: 'Pdf'
                        },
                        {
                            extend: 'print',
                            title: 'Data export',
                            text: 'Print'
                        }/*,
                        {
                            text: 'My button',
                            action: function ( e, dt, node, config ) {
                                alert( 'Button activated' );
                            }
                        },
                        {
                            extend: 'collection',
                            text: 'Table control',
                            autoClose: true,
                            buttons: [
                                {
                                    text: 'Toggle start date',
                                    action: function ( e, dt, node, config ) {
                                        dt.column( -2 ).visible( ! dt.column( -2 ).visible() );
                                    }
                                },
                                {
                                    text: 'Toggle salary',
                                    action: function ( e, dt, node, config ) {
                                        dt.column( -1 ).visible( ! dt.column( -1 ).visible() );
                                    }
                                }
                            ]
                        }*/
                    ]
                },
                language: {
                    lengthMenu : "_MENU_",
                    search: "",
                    searchPlaceholder: "Search here",
                    buttons: {
                        copyTitle: 'Title',
                        copyKeys: 'copy keys',
                        copySuccess: {
                            _: '%d rows copied',
                            1: '1 row copied'
                        }
                    }
                },
                colReorder: true
            });

            var dtSearch = $('.dt-search', $(this));
            table.columns().every(function(index, table, column){
                var that = this;
                
                var timer = null;
                dtSearch.find('th:nth-child(' + (index + 1) + ') input').on('keyup change', function(){
                    var val = {
                        that : that,
                        this : this,
                    };

                    clearTimeout(timer); 
                       timer = setTimeout(function () {
                        if(val.that.search() !== val.this.value){
                            val.that.search( val.this.value ).draw();
                        }
                    }, 500);
                });
            });
        });
        // $('th', el).unbind('click.DT'); //
        // $('th', el).remove(); //
    }
});