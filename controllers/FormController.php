<?php
namespace app_virama_karya\controllers;

use Yii;
use app_virama_karya\models\Form;
use app_virama_karya\models\FormKomisaris;
use app_virama_karya\models\FormDireksi;
use app_virama_karya\models\FormSbuk;
use app_virama_karya\models\FormSbunk;
use app_virama_karya\models\FormIjinLainnya;
use app_virama_karya\models\FormPemilikSaham;
use app_virama_karya\models\FormPajak;
use app_virama_karya\models\FormBuktiPajak;
use technosmart\yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\widgets\ActiveForm;

/**
 * FormController implements highly advanced CRUD actions for Form model.
 */
class FormController extends Controller
{
    /*public static $permissions = [
        ['view', 'View Form'], ['create', 'Create Form'], ['update', 'Update Form'], ['delete', 'Delete Form'],
    ];

    public function behaviors()
    {
        return [
            'access' => $this->access([
                [['index'], 'view'],
                [['index', 'create'], 'create'],
                [['index', 'update'], 'update'],
                [['index', 'delete'], 'delete', null, ['POST']],
            ]),
        ];
    }*/

    protected function findModel($id)
    {
        if (($model = Form::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    protected function findModelFormKomisaris($id)
    {
        if (($model = FormKomisaris::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    protected function findModelFormDireksi($id)
    {
        if (($model = FormDireksi::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    protected function findModelFormSbuk($id)
    {
        if (($model = FormSbuk::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    protected function findModelFormSbunk($id)
    {
        if (($model = FormSbunk::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    protected function findModelFormIjinLainnya($id)
    {
        if (($model = FormIjinLainnya::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    protected function findModelFormPemilikSaham($id)
    {
        if (($model = FormPemilikSaham::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    protected function findModelFormPajak($id)
    {
        if (($model = FormPajak::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    protected function findModelFormBuktiPajak($id)
    {
        if (($model = FormBuktiPajak::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionIndex()
    {
        $model['form'] = $this->findModel(1);
        return $this->render('one', [
            'model' => $model,
            'title' => 'Formulir Isian (A-G)',
        ]);
    }

    public function actionUpdate()
    {
        $render = false;

        $model['form'] = $this->findModel(1);

        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();

            $model['form']->load($post);
            if (isset($post['FormKomisaris'])) {
                foreach ($post['FormKomisaris'] as $key => $value) {
                    if ($value['id'] > 0) {
                        $formKomisaris = $this->findModelFormKomisaris($value['id']);
                        $formKomisaris->setAttributes($value);
                    } else if($value['id'] < 0) {
                        $formKomisaris = $this->findModelFormKomisaris(($value['id']*-1));
                        $formKomisaris->isDeleted = true;
                    } else {
                        $formKomisaris = new FormKomisaris();
                        $formKomisaris->setAttributes($value);
                    }
                    $model['form_komisaris'][] = $formKomisaris;
                }
            }
            if (isset($post['FormDireksi'])) {
                foreach ($post['FormDireksi'] as $key => $value) {
                    if ($value['id'] > 0) {
                        $formDireksi = $this->findModelFormDireksi($value['id']);
                        $formDireksi->setAttributes($value);
                    } else if($value['id'] < 0) {
                        $formDireksi = $this->findModelFormDireksi(($value['id']*-1));
                        $formDireksi->isDeleted = true;
                    } else {
                        $formDireksi = new FormDireksi();
                        $formDireksi->setAttributes($value);
                    }
                    $model['form_direksi'][] = $formDireksi;
                }
            }
            if (isset($post['FormSbuk'])) {
                foreach ($post['FormSbuk'] as $key => $value) {
                    if ($value['id'] > 0) {
                        $formSbuk = $this->findModelFormSbuk($value['id']);
                        $formSbuk->setAttributes($value);
                    } else if($value['id'] < 0) {
                        $formSbuk = $this->findModelFormSbuk(($value['id']*-1));
                        $formSbuk->isDeleted = true;
                    } else {
                        $formSbuk = new FormSbuk();
                        $formSbuk->setAttributes($value);
                    }
                    $model['form_sbuk'][] = $formSbuk;
                }
            }
            if (isset($post['FormSbunk'])) {
                foreach ($post['FormSbunk'] as $key => $value) {
                    if ($value['id'] > 0) {
                        $formSbunk = $this->findModelFormSbunk($value['id']);
                        $formSbunk->setAttributes($value);
                    } else if($value['id'] < 0) {
                        $formSbunk = $this->findModelFormSbunk(($value['id']*-1));
                        $formSbunk->isDeleted = true;
                    } else {
                        $formSbunk = new FormSbunk();
                        $formSbunk->setAttributes($value);
                    }
                    $model['form_sbunk'][] = $formSbunk;
                }
            }
            if (isset($post['FormIjinLainnya'])) {
                foreach ($post['FormIjinLainnya'] as $key => $value) {
                    if ($value['id'] > 0) {
                        $formIjinLainnya = $this->findModelFormIjinLainnya($value['id']);
                        $formIjinLainnya->setAttributes($value);
                    } else if($value['id'] < 0) {
                        $formIjinLainnya = $this->findModelFormIjinLainnya(($value['id']*-1));
                        $formIjinLainnya->isDeleted = true;
                    } else {
                        $formIjinLainnya = new FormIjinLainnya();
                        $formIjinLainnya->setAttributes($value);
                    }
                    $model['form_ijin_lainnya'][] = $formIjinLainnya;
                }
            }
            if (isset($post['FormPemilikSaham'])) {
                foreach ($post['FormPemilikSaham'] as $key => $value) {
                    if ($value['id'] > 0) {
                        $formPemilikSaham = $this->findModelFormPemilikSaham($value['id']);
                        $formPemilikSaham->setAttributes($value);
                    } else if($value['id'] < 0) {
                        $formPemilikSaham = $this->findModelFormPemilikSaham(($value['id']*-1));
                        $formPemilikSaham->isDeleted = true;
                    } else {
                        $formPemilikSaham = new FormPemilikSaham();
                        $formPemilikSaham->setAttributes($value);
                    }
                    $model['form_pemilik_saham'][] = $formPemilikSaham;
                }
            }
            if (isset($post['FormPajak'])) {
                foreach ($post['FormPajak'] as $key => $value) {
                    if ($value['id'] > 0) {
                        $formPajak = $this->findModelFormPajak($value['id']);
                        $formPajak->setAttributes($value);
                    } else if($value['id'] < 0) {
                        $formPajak = $this->findModelFormPajak(($value['id']*-1));
                        $formPajak->isDeleted = true;
                    } else {
                        $formPajak = new FormPajak();
                        $formPajak->setAttributes($value);
                    }
                    $model['form_pajak'][] = $formPajak;
                }
            }
            if (isset($post['FormBuktiPajak'])) {
                foreach ($post['FormBuktiPajak'] as $key => $value) {
                    if ($value['id'] > 0) {
                        $formBuktiPajak = $this->findModelFormBuktiPajak($value['id']);
                        $formBuktiPajak->setAttributes($value);
                    } else if($value['id'] < 0) {
                        $formBuktiPajak = $this->findModelFormBuktiPajak(($value['id']*-1));
                        $formBuktiPajak->isDeleted = true;
                    } else {
                        $formBuktiPajak = new FormBuktiPajak();
                        $formBuktiPajak->setAttributes($value);
                    }
                    $model['form_bukti_pajak'][] = $formBuktiPajak;
                }
            }

            $transaction['form'] = Form::getDb()->beginTransaction();

            try {
                if (!$model['form']->save()) {
                    throw new \yii\base\UserException('Data cannot be saved. Please try again.');
                }

                $error = false;
                if (isset($model['form_komisaris']) and is_array($model['form_komisaris'])) {
                    foreach ($model['form_komisaris'] as $key => $formKomisaris) {
                        if ($formKomisaris->isDeleted) {
                            if (!$formKomisaris->delete()) {
                                $error = true;
                            }
                        } else {
                            $formKomisaris->id_form = $model['form']->id;
                            if (!$formKomisaris->save()) {
                                $error = true;
                            }
                        }
                    }
                }
                if (isset($model['form_direksi']) and is_array($model['form_direksi'])) {
                    foreach ($model['form_direksi'] as $key => $formDireksi) {
                        if ($formDireksi->isDeleted) {
                            if (!$formDireksi->delete()) {
                                $error = true;
                            }
                        } else {
                            $formDireksi->id_form = $model['form']->id;
                            if (!$formDireksi->save()) {
                                $error = true;
                            }
                        }
                    }
                }
                if (isset($model['form_sbuk']) and is_array($model['form_sbuk'])) {
                    foreach ($model['form_sbuk'] as $key => $formSbuk) {
                        if ($formSbuk->isDeleted) {
                            if (!$formSbuk->delete()) {
                                $error = true;
                            }
                        } else {
                            $formSbuk->id_form = $model['form']->id;
                            if (!$formSbuk->save()) {
                                $error = true;
                            }
                        }
                    }
                }
                if (isset($model['form_sbunk']) and is_array($model['form_sbunk'])) {
                    foreach ($model['form_sbunk'] as $key => $formSbunk) {
                        if ($formSbunk->isDeleted) {
                            if (!$formSbunk->delete()) {
                                $error = true;
                            }
                        } else {
                            $formSbunk->id_form = $model['form']->id;
                            if (!$formSbunk->save()) {
                                $error = true;
                            }
                        }
                    }
                }
                if (isset($model['form_ijin_lainnya']) and is_array($model['form_ijin_lainnya'])) {
                    foreach ($model['form_ijin_lainnya'] as $key => $formIjinLainnya) {
                        if ($formIjinLainnya->isDeleted) {
                            if (!$formIjinLainnya->delete()) {
                                $error = true;
                            }
                        } else {
                            $formIjinLainnya->id_form = $model['form']->id;
                            if (!$formIjinLainnya->save()) {
                                $error = true;
                            }
                        }
                    }
                }
                if (isset($model['form_pemilik_saham']) and is_array($model['form_pemilik_saham'])) {
                    foreach ($model['form_pemilik_saham'] as $key => $formPemilikSaham) {
                        if ($formPemilikSaham->isDeleted) {
                            if (!$formPemilikSaham->delete()) {
                                $error = true;
                            }
                        } else {
                            $formPemilikSaham->id_form = $model['form']->id;
                            if (!$formPemilikSaham->save()) {
                                $error = true;
                            }
                        }
                    }
                }
                if (isset($model['form_pajak']) and is_array($model['form_pajak'])) {
                    foreach ($model['form_pajak'] as $key => $formPajak) {
                        if ($formPajak->isDeleted) {
                            if (!$formPajak->delete()) {
                                $error = true;
                            }
                        } else {
                            $formPajak->id_form = $model['form']->id;
                            if (!$formPajak->save()) {
                                $error = true;
                            }
                        }
                    }
                }
                if (isset($model['form_bukti_pajak']) and is_array($model['form_bukti_pajak'])) {
                    foreach ($model['form_bukti_pajak'] as $key => $formBuktiPajak) {
                        if ($formBuktiPajak->isDeleted) {
                            if (!$formBuktiPajak->delete()) {
                                $error = true;
                            }
                        } else {
                            $formBuktiPajak->id_form = $model['form']->id;
                            if (!$formBuktiPajak->save()) {
                                $error = true;
                            }
                        }
                    }
                }

                if ($error) {
                    throw new \yii\base\UserException('Data cannot be saved. Please try again.');
                }
                
                $transaction['form']->commit();
                Yii::$app->session->setFlash('success', 'Data has been saved.');
            } catch (\Exception $e) {
                $render = true;
                $transaction['form']->rollBack();
            } catch (\Throwable $e) {
                $render = true;
                $transaction['form']->rollBack();
            }
        } else {
            foreach ($model['form']->formKomisarises as $key => $formKomisaris)
                $model['form_komisaris'][] = $formKomisaris;
            foreach ($model['form']->formDireksis as $key => $formDireksi)
                $model['form_direksi'][] = $formDireksi;
            foreach ($model['form']->formSbuks as $key => $formSbuk)
                $model['form_sbuk'][] = $formSbuk;
            foreach ($model['form']->formSbunks as $key => $formSbunk)
                $model['form_sbunk'][] = $formSbunk;
            foreach ($model['form']->formIjinLainnyas as $key => $formIjinLainnya)
                $model['form_ijin_lainnya'][] = $formIjinLainnya;
            foreach ($model['form']->formPemilikSahams as $key => $formPemilikSaham)
                $model['form_pemilik_saham'][] = $formPemilikSaham;
            foreach ($model['form']->formPajaks as $key => $formPajak)
                $model['form_pajak'][] = $formPajak;
            foreach ($model['form']->formBuktiPajaks as $key => $formBuktiPajak)
                $model['form_bukti_pajak'][] = $formBuktiPajak;

            $render = true;
        }

        if ($render)
            return $this->render('form', [
                'model' => $model,
                'title' => 'Update Form Isian (A-G)',
            ]);
        else
            return $this->redirect(['index']);
    }
}
