<?php
namespace app_virama_karya\controllers;

use Yii;
use app_virama_karya\models\Paket;
use app_virama_karya\models\PaketTahapJadwal;
use app_virama_karya\models\Tahap;
use app_virama_karya\models\PaketForm;
use app_virama_karya\models\PaketFormKomisaris;
use app_virama_karya\models\PaketFormDireksi;
use app_virama_karya\models\PaketFormSbuk;
use app_virama_karya\models\PaketFormSbunk;
use app_virama_karya\models\PaketFormIjinLainnya;
use app_virama_karya\models\PaketFormPemilikSaham;
use app_virama_karya\models\PaketFormPajak;
use app_virama_karya\models\PaketFormBuktiPajak;
use app_virama_karya\models\Form;
use app_virama_karya\models\FormKomisaris;
use app_virama_karya\models\FormDireksi;
use app_virama_karya\models\FormSbuk;
use app_virama_karya\models\FormSbunk;
use app_virama_karya\models\FormIjinLainnya;
use app_virama_karya\models\FormPemilikSaham;
use app_virama_karya\models\FormPajak;
use app_virama_karya\models\FormBuktiPajak;
use technosmart\yii\web\Controller;
use yii\web\NotFoundHttpException;

class TenderController extends Controller
{
    public function beforeAction($action)
    {
        updateStatus();
        return parent::beforeAction($action);
    }

    public function actionDatatablesDiikuti()
    {
        $query = new \yii\db\Query();
        $query
            ->select([
                'uk.nama_unit_kerja',
                'sum(p.hps) as hps',
            ])
            ->from('unit_kerja uk')
            ->join('LEFT JOIN', 'paket p', 'p.id_unit_kerja = uk.id AND p.status <> "Info Paket"')
            ->groupBy('uk.id')
        ;
        
        return $this->datatables($query, Yii::$app->request->post(), Paket::getDb());
    }

    public function actionDiikuti()
    {
        return $this->render('diikuti', [
            'title' => 'Tender Diikuti per Unit Kerja',
        ]);
    }

    public function actionDatatablesDimenangkan()
    {
        $query = new \yii\db\Query();
        $query
            ->select([
                'uk.nama_unit_kerja',
                'sum(p.hps) as hps',
            ])
            ->from('unit_kerja uk')
            ->join('LEFT JOIN', 'paket p', 'p.id_unit_kerja = uk.id AND FIND_IN_SET("Kontrak", p.status)')
            ->groupBy('uk.id')
        ;
        
        return $this->datatables($query, Yii::$app->request->post(), Paket::getDb());
    }

    public function actionDimenangkan()
    {
        return $this->render('dimenangkan', [
            'title' => 'Tender Dimenangkan per Unit Kerja',
        ]);
    }

    public function actionDatatablesKalah()
    {
        $query = new \yii\db\Query();
        $query
            ->select([
                'uk.nama_unit_kerja',
                'sum(p.hps) as hps',
            ])
            ->from('unit_kerja uk')
            ->join('LEFT JOIN', 'paket p', 'p.id_unit_kerja = uk.id AND FIND_IN_SET("Kalah", p.status)')
            ->groupBy('uk.id')
        ;
        
        return $this->datatables($query, Yii::$app->request->post(), Paket::getDb());
    }

    public function actionKalah()
    {
        return $this->render('kalah', [
            'title' => 'Tender yang Kalah per Unit Kerja',
        ]);
    }
}
