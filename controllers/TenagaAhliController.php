<?php
namespace app_virama_karya\controllers;

use Yii;
use app_virama_karya\models\TenagaAhli;
use app_virama_karya\models\TenagaAhliPendidikan;
use app_virama_karya\models\TenagaAhliKeahlian;
use technosmart\yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\widgets\ActiveForm;

/**
 * TenagaAhliController implements highly advanced CRUD actions for TenagaAhli model.
 */
class TenagaAhliController extends Controller
{
    /*public static $permissions = [
        ['view', 'View Tenaga ahli'], ['create', 'Create Tenaga ahli'], ['update', 'Update Tenaga ahli'], ['delete', 'Delete Tenaga ahli'],
    ];

    public function behaviors()
    {
        return [
            'access' => $this->access([
                [['index'], 'view'],
                [['index', 'create'], 'create'],
                [['index', 'update'], 'update'],
                [['index', 'delete'], 'delete', null, ['POST']],
            ]),
        ];
    }*/

    /**
     * Finds the TenagaAhli model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return TenagaAhli the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = TenagaAhli::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    protected function findModelTenagaAhliPendidikan($id)
    {
        if (($model = TenagaAhliPendidikan::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    protected function findModelTenagaAhliKeahlian($id)
    {
        if (($model = TenagaAhliKeahlian::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionDatatables()
    {
        $db = TenagaAhli::getDb();
        $post = Yii::$app->request->post();

        // serve data for datatables
        if (isset($post['draw'])) {
            $subQueryPendidikan = new \yii\db\Query();
            $subQueryPendidikan
                ->select([
                    'tap.id_tenaga_ahli',
                    'GROUP_CONCAT((CONCAT("• ", tap.jenjang))) as jenjang',
                    'GROUP_CONCAT((CONCAT("• ", tap.jurusan))) as jurusan',
                    'GROUP_CONCAT((CONCAT("• ", pr.rumpun))) as rumpun',
                    'GROUP_CONCAT((CONCAT("• ", tap.tahun_lulus))) as tahun_lulus',
                    'GROUP_CONCAT((CONCAT("• ", tap.nomor_ijazah))) as nomor_ijazah',
                ])
                ->from('tenaga_ahli_pendidikan tap')
                ->join('LEFT JOIN', 'pendidikan_rumpun pr', 'pr.id = tap.id_pendidikan_rumpun')
                ->groupBy('tap.id_tenaga_ahli')
                ;

            $subQueryKeahlian = new \yii\db\Query();
            $subQueryKeahlian
                ->select([
                    'tak.id_tenaga_ahli',
                    'GROUP_CONCAT((CONCAT("• ", tak.keahlian))) as keahlian',
                    'GROUP_CONCAT((CONCAT("• ", tak.level))) as level',
                    'GROUP_CONCAT((CONCAT("• ", tak.sertifikat))) as sertifikat',
                ])
                ->from('tenaga_ahli_keahlian tak')
                ->groupBy('tak.id_tenaga_ahli')
                ;

            $query = new \yii\db\Query();
            $query
                ->select('count(*)')
                ->from('tenaga_ahli ta')
                ->join('LEFT JOIN', ['tap' => $subQueryPendidikan], 'ta.id = tap.id_tenaga_ahli')
                ->join('LEFT JOIN', ['tak' => $subQueryKeahlian], 'ta.id = tak.id_tenaga_ahli')
                ;
            $countWhere = count($query->where);

            $total = $query->scalar($db);
            $return['recordsTotal'] = $total;
            $return['recordsFiltered'] = $total;

            $allWhere = ['or'];
            $allSearch = $post['search']['value'];
            foreach ($post['columns'] as $key => $value) {
                if ($value['searchable'] == 'true') {
                    $column = $value['data'];
                    if (is_array($column)) {
                        if ( isset($column['filter']) )
                            $column = $column['filter'];
                        else
                            $column = $column['_'];
                    }

                    if ($value['search']['regex'] == 'false') {
                        $query->andFilterWhere(['like', $column, $value['search']['value']]);
                    } else if ($value['search']['regex'] == 'true') {
                        $query->andFilterWhere(['regexp', $column, $value['search']['value']]);
                    }

                    if ($allSearch) {
                        if ($post['search']['regex'] == 'false') {
                            $allWhere[] = ['like', $column, $allSearch];
                        } else if ($post['search']['regex'] == 'true') {
                            $allWhere[] = ['regexp', $column, $allSearch];
                        }
                    }
                }
            }
            if (count($allWhere) > 1)
                $query->andFilterWhere($allWhere);
            if (count($query->where) > $countWhere)
                $return['recordsFiltered'] = $query->scalar($db);

            $query->select([
                'ta.id',
                'ta.nama',
                'ta.tanggal_lahir',
                'ta.jenis',
                'ta.kewarganegaraan',
                'tap.*',
                'tak.*',
            ]);

            $order = [];
            if (isset($post['order'])) {
                foreach ($post['order'] as $key => $value) {
                    $column = $post['columns'][$value['column']]['data'];
                    if ($post['columns'][$value['column']]['orderable'] == 'false') {
                        continue;
                    }
                    if (is_array($column)) {
                        if ( isset($column['sort']) )
                            $column = $column['sort'];
                        else
                            $column = $column['_'];
                    }

                    if ($value['dir'] == 'asc')
                        $order[$column] = SORT_ASC;
                    else if ($value['dir'] == 'desc')
                        $order[$column] = SORT_DESC;
                }
            }
            count($order) ? $query->orderBy($order) : 0;

            if (isset($post['length']))
                $query->limit(intval($post['length']));

            if (isset($post['start']))
                $query->offset(intval($post['start']));

            $return['draw'] = intval($post['draw']);
            $return['data'] = $query->all($db);
            // Yii::$app->d->ddx($query->createCommand()->getRawSql());
            return $this->json($return);
        }
    }

    /**
     * If param(s) is null, display all datas from models.
     * If all param(s) is not null, display a data from model.
     * @param integer $id
     * @return mixed
     */
    public function actionIndex($id = null)
    {
        // view all data
        if (!$id) {
            return $this->render('list', [
                'title' => 'List of Tenaga ahlis',
            ]);
        }
        
        // view single data
        $model['tenaga_ahli'] = $this->findModel($id);
        return $this->render('one', [
            'model' => $model,
            'title' => 'Detail of Tenaga ahli ' . $model['tenaga_ahli']->id,
        ]);
    }

    /**
     * Creates new data(s) from model(s).
     * If submission is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionCreate()
    {
        $render = false;

        $model['tenaga_ahli'] = isset($id) ? $this->findModel($id) : new TenagaAhli();

        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();

            $model['tenaga_ahli']->load($post);
            if (isset($post['TenagaAhliPendidikan'])) {
                foreach ($post['TenagaAhliPendidikan'] as $key => $value) {
                    if ($value['id'] > 0) {
                        $tenagaAhliPendidikan = $this->findModelTenagaAhliPendidikan($value['id']);
                        $tenagaAhliPendidikan->setAttributes($value);
                    } else if($value['id'] < 0) {
                        $tenagaAhliPendidikan = $this->findModelTenagaAhliPendidikan(($value['id']*-1));
                        $tenagaAhliPendidikan->isDeleted = true;
                    } else {
                        $tenagaAhliPendidikan = new TenagaAhliPendidikan();
                        $tenagaAhliPendidikan->setAttributes($value);
                    }
                    $model['tenaga_ahli_pendidikan'][] = $tenagaAhliPendidikan;
                }
            }
            if (isset($post['TenagaAhliKeahlian'])) {
                foreach ($post['TenagaAhliKeahlian'] as $key => $value) {
                    if ($value['id'] > 0) {
                        $tenagaAhliKeahlian = $this->findModelTenagaAhliKeahlian($value['id']);
                        $tenagaAhliKeahlian->setAttributes($value);
                    } else if($value['id'] < 0) {
                        $tenagaAhliKeahlian = $this->findModelTenagaAhliKeahlian(($value['id']*-1));
                        $tenagaAhliKeahlian->isDeleted = true;
                    } else {
                        $tenagaAhliKeahlian = new TenagaAhliKeahlian();
                        $tenagaAhliKeahlian->setAttributes($value);
                    }
                    $model['tenaga_ahli_keahlian'][] = $tenagaAhliKeahlian;
                }
            }

            $transaction['tenaga_ahli'] = TenagaAhli::getDb()->beginTransaction();

            try {
                if (!$model['tenaga_ahli']->save()) {
                    throw new \yii\base\UserException('Data cannot be saved. Please try again.');
                }

                $error = false;
                if (isset($model['tenaga_ahli_pendidikan']) and is_array($model['tenaga_ahli_pendidikan'])) {
                    foreach ($model['tenaga_ahli_pendidikan'] as $key => $tenagaAhliPendidikan) {
                        if ($tenagaAhliPendidikan->isDeleted) {
                            if (!$tenagaAhliPendidikan->delete()) {
                                $error = true;
                            }
                        } else {
                            $tenagaAhliPendidikan->id_tenaga_ahli = $model['tenaga_ahli']->id;
                            if (!$tenagaAhliPendidikan->save()) {
                                $error = true;
                            }
                        }
                    }
                }

                if (isset($model['tenaga_ahli_keahlian']) and is_array($model['tenaga_ahli_keahlian'])) {
                    foreach ($model['tenaga_ahli_keahlian'] as $key => $tenagaAhliKeahlian) {
                        if ($tenagaAhliKeahlian->isDeleted) {
                            if (!$tenagaAhliKeahlian->delete()) {
                                $error = true;
                            }
                        } else {
                            $tenagaAhliKeahlian->id_tenaga_ahli = $model['tenaga_ahli']->id;
                            if (!$tenagaAhliKeahlian->save()) {
                                $error = true;
                            }
                        }
                    }
                }
                
                if ($error) {
                    throw new \yii\base\UserException('Data cannot be saved. Please try again.');
                }
                
                $transaction['tenaga_ahli']->commit();
                Yii::$app->session->setFlash('success', 'Data berhasil disimpan.');
            } catch (\Exception $e) {
                $render = true;
                $transaction['tenaga_ahli']->rollBack();
            } catch (\Throwable $e) {
                $render = true;
                $transaction['tenaga_ahli']->rollBack();
            }
        } else {
            foreach ($model['tenaga_ahli']->tenagaAhliPendidikans as $key => $tenagaAhliPendidikan)
                $model['tenaga_ahli_pendidikan'][] = $tenagaAhliPendidikan;
            foreach ($model['tenaga_ahli']->tenagaAhliKeahlians as $key => $tenagaAhliKeahlian)
                $model['tenaga_ahli_keahlian'][] = $tenagaAhliKeahlian;

            if ($model['tenaga_ahli']->isNewRecord) {
                $model['tenaga_ahli_pendidikan'][] = new TenagaAhliPendidikan();
                $model['tenaga_ahli_keahlian'][] = new TenagaAhliKeahlian();
            }

            $render = true;
        }

        if ($render)
            return $this->render('form', [
                'model' => $model,
                'title' => 'Daftarkan Tenaga ahli',
            ]);
        else
            return $this->redirect(['index']);
    }

    /**
     * Updates existing data(s) from model(s).
     * If submission is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id = null)
    {
        $render = false;

        $model['tenaga_ahli'] = isset($id) ? $this->findModel($id) : new TenagaAhli();

        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();

            $model['tenaga_ahli']->load($post);
            if (isset($post['TenagaAhliPendidikan'])) {
                foreach ($post['TenagaAhliPendidikan'] as $key => $value) {
                    if ($value['id'] > 0) {
                        $tenagaAhliPendidikan = $this->findModelTenagaAhliPendidikan($value['id']);
                        $tenagaAhliPendidikan->setAttributes($value);
                    } else if($value['id'] < 0) {
                        $tenagaAhliPendidikan = $this->findModelTenagaAhliPendidikan(($value['id']*-1));
                        $tenagaAhliPendidikan->isDeleted = true;
                    } else {
                        $tenagaAhliPendidikan = new TenagaAhliPendidikan();
                        $tenagaAhliPendidikan->setAttributes($value);
                    }
                    $model['tenaga_ahli_pendidikan'][] = $tenagaAhliPendidikan;
                }
            }
            if (isset($post['TenagaAhliKeahlian'])) {
                foreach ($post['TenagaAhliKeahlian'] as $key => $value) {
                    if ($value['id'] > 0) {
                        $tenagaAhliKeahlian = $this->findModelTenagaAhliKeahlian($value['id']);
                        $tenagaAhliKeahlian->setAttributes($value);
                    } else if($value['id'] < 0) {
                        $tenagaAhliKeahlian = $this->findModelTenagaAhliKeahlian(($value['id']*-1));
                        $tenagaAhliKeahlian->isDeleted = true;
                    } else {
                        $tenagaAhliKeahlian = new TenagaAhliKeahlian();
                        $tenagaAhliKeahlian->setAttributes($value);
                    }
                    $model['tenaga_ahli_keahlian'][] = $tenagaAhliKeahlian;
                }
            }

            $transaction['tenaga_ahli'] = TenagaAhli::getDb()->beginTransaction();

            try {
                if (!$model['tenaga_ahli']->save()) {
                    throw new \yii\base\UserException('Data cannot be saved. Please try again.');
                }

                $error = false;
                if (isset($model['tenaga_ahli_pendidikan']) and is_array($model['tenaga_ahli_pendidikan'])) {
                    foreach ($model['tenaga_ahli_pendidikan'] as $key => $tenagaAhliPendidikan) {
                        if ($tenagaAhliPendidikan->isDeleted) {
                            if (!$tenagaAhliPendidikan->delete()) {
                                $error = true;
                            }
                        } else {
                            $tenagaAhliPendidikan->id_tenaga_ahli = $model['tenaga_ahli']->id;
                            if (!$tenagaAhliPendidikan->save()) {
                                $error = true;
                            }
                        }
                    }
                }

                if (isset($model['tenaga_ahli_keahlian']) and is_array($model['tenaga_ahli_keahlian'])) {
                    foreach ($model['tenaga_ahli_keahlian'] as $key => $tenagaAhliKeahlian) {
                        if ($tenagaAhliKeahlian->isDeleted) {
                            if (!$tenagaAhliKeahlian->delete()) {
                                $error = true;
                            }
                        } else {
                            $tenagaAhliKeahlian->id_tenaga_ahli = $model['tenaga_ahli']->id;
                            if (!$tenagaAhliKeahlian->save()) {
                                $error = true;
                            }
                        }
                    }
                }
                
                if ($error) {
                    throw new \yii\base\UserException('Data cannot be saved. Please try again.');
                }
                
                $transaction['tenaga_ahli']->commit();
                Yii::$app->session->setFlash('success', 'Data berhasil disimpan.');
            } catch (\Exception $e) {
                $render = true;
                $transaction['tenaga_ahli']->rollBack();
            } catch (\Throwable $e) {
                $render = true;
                $transaction['tenaga_ahli']->rollBack();
            }
        } else {
            foreach ($model['tenaga_ahli']->tenagaAhliPendidikans as $key => $tenagaAhliPendidikan)
                $model['tenaga_ahli_pendidikan'][] = $tenagaAhliPendidikan;
            foreach ($model['tenaga_ahli']->tenagaAhliKeahlians as $key => $tenagaAhliKeahlian)
                $model['tenaga_ahli_keahlian'][] = $tenagaAhliKeahlian;

            if ($model['tenaga_ahli']->isNewRecord) {
                $model['tenaga_ahli_pendidikan'][] = new TenagaAhliPendidikan();
                $model['tenaga_ahli_keahlian'][] = new TenagaAhliKeahlian();
            }

            $render = true;
        }

        if ($render)
            return $this->render('form', [
                'model' => $model,
                'title' => 'Update Tenaga ahli ' . $model['tenaga_ahli']->nama,
            ]);
        else
            return $this->redirect(['index']);
    }

    /**
     * Deletes an existing TenagaAhli model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }
}
