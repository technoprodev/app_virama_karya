<?php
namespace app_virama_karya\controllers;

use Yii;
use technosmart\yii\web\Controller;
use app_virama_karya\models\Paket;
use Dompdf\Dompdf;

class DownloadController extends Controller
{
	protected function findModel($id)
    {
        if (($model = Paket::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionIndex()
    {
        $dompdf = new Dompdf();
        // $dompdf->loadHtml($this->renderPartial('index'));
        // $dompdf->setPaper('A4', 'landscape');
        // $dompdf->render();
        // $dompdf->stream();

        return $this->renderPartial('index');
    }
    
	public function actionInfoPaketDetail($id)
	{
		$dompdf = new Dompdf();
		$model['paket'] = $this->findModel($id);
        $model['paket_form'] = $model['paket']->paketForm;
        foreach ($model['paket']->paketTahapJadwals as $key => $paketTahapJadwal)
            $model['paket_tahap_jadwal'][] = $paketTahapJadwal;

		Yii::$app->view->params['title'] = \yii\helpers\Inflector::titleize('info_paket_detail', true) . ' ' . $model['paket']->nama_paket;
		$this->layout = 'download';
		$dompdf->loadHtml($this->render('/paket/one/info-paket/_one-info_paket_detail', ['model' => $model]));
        $dompdf->setPaper('A4', 'landscape');
        $dompdf->render();
        $dompdf->stream();

		// return $this->render('/paket/one/info-paket/_one-info_paket_detail', ['model' => $model]);
	}

	public function actionTahapLelang($id)
	{
		$dompdf = new Dompdf();
		$model['paket'] = $this->findModel($id);
        $model['paket_form'] = $model['paket']->paketForm;
        foreach ($model['paket']->paketTahapJadwals as $key => $paketTahapJadwal)
            $model['paket_tahap_jadwal'][] = $paketTahapJadwal;

		Yii::$app->view->params['title'] = \yii\helpers\Inflector::titleize('tahap_lelang', true) . ' ' . $model['paket']->nama_paket;
		$this->layout = 'download';
		$dompdf->loadHtml($this->render('/paket/one/info-paket/_one-tahap_lelang', ['model' => $model]));
        $dompdf->setPaper('A4', 'landscape');
        $dompdf->render();
        $dompdf->stream();

		// return $this->render('/paket/one/info-paket/_one-tahap_lelang', ['model' => $model]);
	}

	public function actionPaktaIntegritas($id)
	{
		$dompdf = new Dompdf();
		$model['paket'] = $this->findModel($id);
        $model['paket_form'] = $model['paket']->paketForm;
        foreach ($model['paket']->paketTahapJadwals as $key => $paketTahapJadwal)
            $model['paket_tahap_jadwal'][] = $paketTahapJadwal;

		Yii::$app->view->params['title'] = \yii\helpers\Inflector::titleize('pakta_integritas', true) . ' ' . $model['paket']->nama_paket;
		$this->layout = 'download';
		$dompdf->loadHtml($this->render('/paket/one/prakualifikasi/_one-pakta_integritas', ['model' => $model]));
        $dompdf->setPaper('A4', 'landscape');
        $dompdf->render();
        $dompdf->stream();

		// return $this->render('/paket/one/prakualifikasi/_one-pakta_integritas', ['model' => $model]);
	}

	public function actionSuratFormIsian($id)
	{
		$dompdf = new Dompdf();
		$model['paket'] = $this->findModel($id);
        $model['paket_form'] = $model['paket']->paketForm;
        foreach ($model['paket']->paketTahapJadwals as $key => $paketTahapJadwal)
            $model['paket_tahap_jadwal'][] = $paketTahapJadwal;

		Yii::$app->view->params['title'] = \yii\helpers\Inflector::titleize('surat_form_isian', true) . ' ' . $model['paket']->nama_paket;
		$this->layout = 'download';
		$dompdf->loadHtml($this->render('/paket/one/prakualifikasi/_one-surat_form_isian', ['model' => $model]));
        $dompdf->setPaper('A4', 'landscape');
        $dompdf->render();
        $dompdf->stream();

		// return $this->render('/paket/one/prakualifikasi/_one-surat_form_isian', ['model' => $model]);
	}

	public function actionFormIsianAg($id)
	{
		$dompdf = new Dompdf();
		$model['paket'] = $this->findModel($id);
        $model['paket_form'] = $model['paket']->paketForm;
        foreach ($model['paket']->paketTahapJadwals as $key => $paketTahapJadwal)
            $model['paket_tahap_jadwal'][] = $paketTahapJadwal;

		Yii::$app->view->params['title'] = \yii\helpers\Inflector::titleize('form_isian_ag', true) . ' ' . $model['paket']->nama_paket;
		$this->layout = 'download';
		$dompdf->loadHtml($this->render('/paket/one/prakualifikasi/_one-form_isian_ag', ['model' => $model]));
        $dompdf->setPaper('A4', 'landscape');
        $dompdf->render();
        $dompdf->stream();

		// return $this->render('/paket/one/prakualifikasi/_one-form_isian_ag', ['model' => $model]);
	}

	public function actionSuratMinat($id)
	{
		$dompdf = new Dompdf();
		$model['paket'] = $this->findModel($id);
        $model['paket_form'] = $model['paket']->paketForm;
        foreach ($model['paket']->paketTahapJadwals as $key => $paketTahapJadwal)
            $model['paket_tahap_jadwal'][] = $paketTahapJadwal;

		Yii::$app->view->params['title'] = \yii\helpers\Inflector::titleize('surat_minat', true) . ' ' . $model['paket']->nama_paket;
		$this->layout = 'download';
		$dompdf->loadHtml($this->render('/paket/one/prakualifikasi/_one-surat_minat', ['model' => $model]));
        $dompdf->setPaper('A4', 'landscape');
        $dompdf->render();
        $dompdf->stream();

		// return $this->render('/paket/one/prakualifikasi/_one-surat_minat', ['model' => $model]);
	}

	public function actionSuratPernyataan($id)
	{
		$dompdf = new Dompdf();
		$model['paket'] = $this->findModel($id);
        $model['paket_form'] = $model['paket']->paketForm;
        foreach ($model['paket']->paketTahapJadwals as $key => $paketTahapJadwal)
            $model['paket_tahap_jadwal'][] = $paketTahapJadwal;

		Yii::$app->view->params['title'] = \yii\helpers\Inflector::titleize('surat_pernyataan', true) . ' ' . $model['paket']->nama_paket;
		$this->layout = 'download';
		$dompdf->loadHtml($this->render('/paket/one/prakualifikasi/_one-surat_pernyataan', ['model' => $model]));
        $dompdf->setPaper('A4', 'landscape');
        $dompdf->render();
        $dompdf->stream();

		// return $this->render('/paket/one/prakualifikasi/_one-surat_pernyataan', ['model' => $model]);
	}

	public function actionSuratKso($id)
	{
		$dompdf = new Dompdf();
		$model['paket'] = $this->findModel($id);
        $model['paket_form'] = $model['paket']->paketForm;
        foreach ($model['paket']->paketTahapJadwals as $key => $paketTahapJadwal)
            $model['paket_tahap_jadwal'][] = $paketTahapJadwal;

		Yii::$app->view->params['title'] = \yii\helpers\Inflector::titleize('surat_kso', true) . ' ' . $model['paket']->nama_paket;
		$this->layout = 'download';
		$dompdf->loadHtml($this->render('/paket/one/prakualifikasi/_one-surat_kso', ['model' => $model]));
        $dompdf->setPaper('A4', 'landscape');
        $dompdf->render();
        $dompdf->stream();

		// return $this->render('/paket/one/prakualifikasi/_one-surat_kso', ['model' => $model]);
	}

	public function actionTenagaAhli($id)
	{
		$dompdf = new Dompdf();
		$model['paket'] = $this->findModel($id);
        $model['paket_form'] = $model['paket']->paketForm;
        foreach ($model['paket']->paketTahapJadwals as $key => $paketTahapJadwal)
            $model['paket_tahap_jadwal'][] = $paketTahapJadwal;

		Yii::$app->view->params['title'] = \yii\helpers\Inflector::titleize('tenaga_ahli', true) . ' ' . $model['paket']->nama_paket;
		$this->layout = 'download';
		$dompdf->loadHtml($this->render('/paket/one/prakualifikasi/_one-tenaga_ahli', ['model' => $model]));
        $dompdf->setPaper('A4', 'landscape');
        $dompdf->render();
        $dompdf->stream();

		// return $this->render('/paket/one/prakualifikasi/_one-tenaga_ahli', ['model' => $model]);
	}

	public function actionPeralatan($id)
	{
		$dompdf = new Dompdf();
		$model['paket'] = $this->findModel($id);
        $model['paket_form'] = $model['paket']->paketForm;
        foreach ($model['paket']->paketTahapJadwals as $key => $paketTahapJadwal)
            $model['paket_tahap_jadwal'][] = $paketTahapJadwal;

		Yii::$app->view->params['title'] = \yii\helpers\Inflector::titleize('peralatan', true) . ' ' . $model['paket']->nama_paket;
		$this->layout = 'download';
		$dompdf->loadHtml($this->render('/paket/one/prakualifikasi/_one-peralatan', ['model' => $model]));
        $dompdf->setPaper('A4', 'landscape');
        $dompdf->render();
        $dompdf->stream();

		// return $this->render('/paket/one/prakualifikasi/_one-peralatan', ['model' => $model]);
	}

	public function actionPengalaman10Tahun($id)
	{
		$dompdf = new Dompdf();
		$model['paket'] = $this->findModel($id);
        $model['paket_form'] = $model['paket']->paketForm;
        foreach ($model['paket']->paketTahapJadwals as $key => $paketTahapJadwal)
            $model['paket_tahap_jadwal'][] = $paketTahapJadwal;

		Yii::$app->view->params['title'] = \yii\helpers\Inflector::titleize('pengalaman_10_tahun', true) . ' ' . $model['paket']->nama_paket;
		$this->layout = 'download';
		$dompdf->loadHtml($this->render('/paket/one/prakualifikasi/_one-pengalaman_10_tahun', ['model' => $model]));
        $dompdf->setPaper('A4', 'landscape');
        $dompdf->render();
        $dompdf->stream();

		// return $this->render('/paket/one/prakualifikasi/_one-pengalaman_10_tahun', ['model' => $model]);
	}

	public function actionPengalamanSejenis($id)
	{
		$dompdf = new Dompdf();
		$model['paket'] = $this->findModel($id);
        $model['paket_form'] = $model['paket']->paketForm;
        foreach ($model['paket']->paketTahapJadwals as $key => $paketTahapJadwal)
            $model['paket_tahap_jadwal'][] = $paketTahapJadwal;

		Yii::$app->view->params['title'] = \yii\helpers\Inflector::titleize('pengalaman_sejenis', true) . ' ' . $model['paket']->nama_paket;
		$this->layout = 'download';
		$dompdf->loadHtml($this->render('/paket/one/prakualifikasi/_one-pengalaman_sejenis', ['model' => $model]));
        $dompdf->setPaper('A4', 'landscape');
        $dompdf->render();
        $dompdf->stream();

		// return $this->render('/paket/one/prakualifikasi/_one-pengalaman_sejenis', ['model' => $model]);
	}

	public function actionPengalaman4Tahun($id)
	{
		$dompdf = new Dompdf();
		$model['paket'] = $this->findModel($id);
        $model['paket_form'] = $model['paket']->paketForm;
        foreach ($model['paket']->paketTahapJadwals as $key => $paketTahapJadwal)
            $model['paket_tahap_jadwal'][] = $paketTahapJadwal;

		Yii::$app->view->params['title'] = \yii\helpers\Inflector::titleize('pengalaman_4_tahun', true) . ' ' . $model['paket']->nama_paket;
		$this->layout = 'download';
		$dompdf->loadHtml($this->render('/paket/one/prakualifikasi/_one-pengalaman_4_tahun', ['model' => $model]));
        $dompdf->setPaper('A4', 'landscape');
        $dompdf->render();
        $dompdf->stream();

		// return $this->render('/paket/one/prakualifikasi/_one-pengalaman_4_tahun', ['model' => $model]);
	}

	public function actionSuratKuasa($id)
	{
		$dompdf = new Dompdf();
		$model['paket'] = $this->findModel($id);
        $model['paket_form'] = $model['paket']->paketForm;
        foreach ($model['paket']->paketTahapJadwals as $key => $paketTahapJadwal)
            $model['paket_tahap_jadwal'][] = $paketTahapJadwal;

		Yii::$app->view->params['title'] = \yii\helpers\Inflector::titleize('surat_kuasa', true) . ' ' . $model['paket']->nama_paket;
		$this->layout = 'download';
		$dompdf->loadHtml($this->render('/paket/one/proposal/_one-surat_kuasa', ['model' => $model]));
        $dompdf->setPaper('A4', 'landscape');
        $dompdf->render();
        $dompdf->stream();

		// return $this->render('/paket/one/proposal/_one-surat_kuasa', ['model' => $model]);
	}

	public function actionPenawaranAdministrasiDanTeknisKerjaan($id)
	{
		$dompdf = new Dompdf();
		$model['paket'] = $this->findModel($id);
        $model['paket_form'] = $model['paket']->paketForm;
        foreach ($model['paket']->paketTahapJadwals as $key => $paketTahapJadwal)
            $model['paket_tahap_jadwal'][] = $paketTahapJadwal;

		Yii::$app->view->params['title'] = \yii\helpers\Inflector::titleize('penawaran_administrasi_dan_teknis_kerjaan', true) . ' ' . $model['paket']->nama_paket;
		$this->layout = 'download';
		$dompdf->loadHtml($this->render('/paket/one/proposal/_one-penawaran_administrasi_dan_teknis_kerjaan', ['model' => $model]));
        $dompdf->setPaper('A4', 'landscape');
        $dompdf->render();
        $dompdf->stream();

		// return $this->render('/paket/one/proposal/_one-penawaran_administrasi_dan_teknis_kerjaan', ['model' => $model]);
	}

	public function actionRab($id)
	{
		$dompdf = new Dompdf();
		$model['paket'] = $this->findModel($id);
        $model['paket_form'] = $model['paket']->paketForm;
        foreach ($model['paket']->paketTahapJadwals as $key => $paketTahapJadwal)
            $model['paket_tahap_jadwal'][] = $paketTahapJadwal;

		Yii::$app->view->params['title'] = \yii\helpers\Inflector::titleize('rab', true) . ' ' . $model['paket']->nama_paket;
		$this->layout = 'download';
		$dompdf->loadHtml($this->render('/paket/one/proposal/_one-rab', ['model' => $model]));
        $dompdf->setPaper('A4', 'landscape');
        $dompdf->render();
        $dompdf->stream();

		// return $this->render('/paket/one/proposal/_one-penawaran_biaya_paket_pekerjaan', ['model' => $model]);
	}

	public function actionPenawaranBiayaPaketPekerjaan($id)
	{
		$dompdf = new Dompdf();
		$model['paket'] = $this->findModel($id);
        $model['paket_form'] = $model['paket']->paketForm;
        foreach ($model['paket']->paketTahapJadwals as $key => $paketTahapJadwal)
            $model['paket_tahap_jadwal'][] = $paketTahapJadwal;

		Yii::$app->view->params['title'] = \yii\helpers\Inflector::titleize('penawaran_biaya_paket_pekerjaan', true) . ' ' . $model['paket']->nama_paket;
		$this->layout = 'download';
		$dompdf->loadHtml($this->render('/paket/one/proposal/_one-penawaran_biaya_paket_pekerjaan', ['model' => $model]));
        $dompdf->setPaper('A4', 'landscape');
        $dompdf->render();
        $dompdf->stream();

		// return $this->render('/paket/one/proposal/_one-penawaran_biaya_paket_pekerjaan', ['model' => $model]);
	}

	public function actionPds($id)
	{
		$dompdf = new Dompdf();
		$model['paket'] = $this->findModel($id);
        $model['paket_form'] = $model['paket']->paketForm;
        foreach ($model['paket']->paketTahapJadwals as $key => $paketTahapJadwal)
            $model['paket_tahap_jadwal'][] = $paketTahapJadwal;

		Yii::$app->view->params['title'] = \yii\helpers\Inflector::titleize('pds', true) . ' ' . $model['paket']->nama_paket;
		$this->layout = 'download';
		$dompdf->loadHtml($this->render('/paket/one/proposal/_one-pds', ['model' => $model]));
        $dompdf->setPaper('A4', 'landscape');
        $dompdf->render();
        $dompdf->stream();

		// return $this->render('/paket/one/proposal/_one-pds', ['model' => $model]);
	}

	public function actionCvTenagaAhli($id)
	{
		$dompdf = new Dompdf();
		$model['paket'] = $this->findModel($id);
        $model['paket_form'] = $model['paket']->paketForm;
        foreach ($model['paket']->paketTahapJadwals as $key => $paketTahapJadwal)
            $model['paket_tahap_jadwal'][] = $paketTahapJadwal;

		Yii::$app->view->params['title'] = \yii\helpers\Inflector::titleize('cv_tenaga_ahli', true) . ' ' . $model['paket']->nama_paket;
		$this->layout = 'download';
		$dompdf->loadHtml($this->render('/paket/one/proposal/_one-cv_tenaga_ahli', ['model' => $model]));
        $dompdf->setPaper('A4', 'landscape');
        $dompdf->render();
        $dompdf->stream();

		// return $this->render('/paket/one/proposal/_one-cv_tenaga_ahli', ['model' => $model]);
	}

	public function actionDataKontrak($id)
	{
		$dompdf = new Dompdf();
		$model['paket'] = $this->findModel($id);
        $model['paket_form'] = $model['paket']->paketForm;
        foreach ($model['paket']->paketTahapJadwals as $key => $paketTahapJadwal)
            $model['paket_tahap_jadwal'][] = $paketTahapJadwal;

		Yii::$app->view->params['title'] = \yii\helpers\Inflector::titleize('data_kontrak', true) . ' ' . $model['paket']->nama_paket;
		$this->layout = 'download';
		$dompdf->loadHtml($this->render('/paket/one/kontrak/_one-data_kontrak', ['model' => $model]));
        $dompdf->setPaper('A4', 'landscape');
        $dompdf->render();
        $dompdf->stream();

		// return $this->render('/paket/one/kontrak/_one-data_kontrak', ['model' => $model]);
	}

}