<?php
namespace app_virama_karya\controllers;

use Yii;
use app_virama_karya\models\Paket;
use app_virama_karya\models\PaketMitra;
use app_virama_karya\models\PaketTahapJadwal;
use app_virama_karya\models\Tahap;
use app_virama_karya\models\PaketForm;
use app_virama_karya\models\PaketFormKomisaris;
use app_virama_karya\models\PaketFormDireksi;
use app_virama_karya\models\PaketFormSbuk;
use app_virama_karya\models\PaketFormSbunk;
use app_virama_karya\models\PaketFormIjinLainnya;
use app_virama_karya\models\PaketFormPemilikSaham;
use app_virama_karya\models\PaketFormPajak;
use app_virama_karya\models\PaketFormBuktiPajak;
use app_virama_karya\models\PaketTenagaAhli;
use app_virama_karya\models\PaketTenagaAhliPendidikan;
use app_virama_karya\models\PaketTenagaAhliKeahlian;
use app_virama_karya\models\Form;
use app_virama_karya\models\FormKomisaris;
use app_virama_karya\models\FormDireksi;
use app_virama_karya\models\FormSbuk;
use app_virama_karya\models\FormSbunk;
use app_virama_karya\models\FormIjinLainnya;
use app_virama_karya\models\FormPemilikSaham;
use app_virama_karya\models\FormPajak;
use app_virama_karya\models\FormBuktiPajak;
use app_virama_karya\models\TenagaAhli;
use technosmart\yii\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * PaketController implements highly advanced CRUD actions for Paket model.
 */
class PaketController extends Controller
{
    /*public static $permissions = [
        ['view', 'View Paket'], ['create', 'Create Paket'], ['update', 'Update Paket'], ['delete', 'Delete Paket'],
    ];

    public function behaviors()
    {
        return [
            'access' => $this->access([
                [['index'], 'view'],
                [['index', 'create'], 'create'],
                [['index', 'update'], 'update'],
                [['index', 'delete'], 'delete', null, ['POST']],
            ]),
        ];
    }*/

    public function beforeAction($action)
    {
        updateStatus();
        return parent::beforeAction($action);
    }

    protected function findModel($id)
    {
        if (($model = Paket::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    protected function findModelPaketMitra($id)
    {
        if (($model = PaketMitra::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    protected function findModelPaketTahapJadwal($id)
    {
        if (($model = PaketTahapJadwal::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    protected function findModelTahap()
    {
        if (($model = Tahap::find()->all()) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('Master tahap tidak boleh kosong. Hanya developer yang dapat melakukan input master tahap.');
        }
    }

    //

    protected function findModelPaketForm($id)
    {
        if (($model = PaketForm::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    protected function findModelPaketFormKomisaris($id)
    {
        if (($model = PaketFormKomisaris::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    protected function findModelPaketFormDireksi($id)
    {
        if (($model = PaketFormDireksi::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    protected function findModelPaketFormSbuk($id)
    {
        if (($model = PaketFormSbuk::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    protected function findModelPaketFormSbunk($id)
    {
        if (($model = PaketFormSbunk::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    protected function findModelPaketFormIjinLainnya($id)
    {
        if (($model = PaketFormIjinLainnya::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    protected function findModelPaketFormPemilikSaham($id)
    {
        if (($model = PaketFormPemilikSaham::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    protected function findModelPaketFormPajak($id)
    {
        if (($model = PaketFormPajak::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    protected function findModelPaketFormBuktiPajak($id)
    {
        if (($model = PaketFormBuktiPajak::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    protected function findModelPaketTenagaAhli($id)
    {
        if (($model = PaketTenagaAhli::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    protected function findModelPaketTenagaAhliPendidikan($id)
    {
        if (($model = PaketTenagaAhliPendidikan::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    protected function findModelPaketTenagaAhliKeahlian($id)
    {
        if (($model = PaketTenagaAhliKeahlian::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    //

    protected function findModelForm()
    {
        if (($model = Form::findOne(1)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    protected function findModelFormKomisaris()
    {
        if (($model = FormKomisaris::find(['id_form' => 1])->all()) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    protected function findModelFormDireksi()
    {
        if (($model = FormDireksi::find(['id_form' => 1])->all()) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    protected function findModelFormSbuk()
    {
        if (($model = FormSbuk::find(['id_form' => 1])->all()) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    protected function findModelFormSbunk()
    {
        if (($model = FormSbunk::find(['id_form' => 1])->all()) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    protected function findModelFormIjinLainnya()
    {
        if (($model = FormIjinLainnya::find(['id_form' => 1])->all()) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    protected function findModelFormPemilikSaham()
    {
        if (($model = FormPemilikSaham::find(['id_form' => 1])->all()) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    protected function findModelFormPajak()
    {
        if (($model = FormPajak::find(['id_form' => 1])->all()) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    protected function findModelFormBuktiPajak()
    {
        if (($model = FormBuktiPajak::find(['id_form' => 1])->all()) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    protected function findModelTenagaAhli($id)
    {
        if (($model = TenagaAhli::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    //

    public function actionDatatables()
    {
        $query = new \yii\db\Query();
        $query
            ->select([
                'p.id',
                'uk.nama_unit_kerja AS nama_unit_kerja',
                'nama_paket',
                'i.nama_instansi AS nama_instansi',
                'p.hps',
                'GROUP_CONCAT(t.nama_tahap) as `tahap`',
            ])
            ->from('paket p')
            ->join('LEFT JOIN', 'unit_kerja uk', 'p.id_unit_kerja = uk.id')
            ->join('LEFT JOIN', 'instansi i', 'p.id_instansi = i.id')
            ->join('LEFT JOIN', 'paket_tahap_jadwal ptj', 'ptj.id_paket = p.id and now() between ptj.tanggal_mulai and ptj.tanggal_selesai')
            ->join('LEFT JOIN', 'tahap t', 'ptj.id_tahap = t.id')
            ->groupBy(['p.id'])
        ;

        // ddx($query->createCommand()->getRawSql());
        
        return $this->datatables($query, Yii::$app->request->post(), Paket::getDb());
    }

    public function actionCreate()
    {
        $render = false;

        $model['paket'] = new Paket();

        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();

            $model['paket']->load($post);
            if (isset($post['PaketMitra'])) {
                foreach ($post['PaketMitra'] as $key => $value) {
                    if ($value['id'] > 0) {
                        $paketMitra = $this->findModelPaketMitra($value['id']);
                        $paketMitra->setAttributes($value);
                    } else if($value['id'] < 0) {
                        $paketMitra = $this->findModelPaketMitra(($value['id']*-1));
                        $paketMitra->isDeleted = true;
                    } else {
                        $paketMitra = new PaketMitra();
                        $paketMitra->setAttributes($value);
                    }
                    $model['paket_mitra'][] = $paketMitra;
                }
            }

            $transaction['paket'] = Paket::getDb()->beginTransaction();

            try {
                if ($model['paket']->isNewRecord) {
                    $model['paket']->status = ['Info Paket'];
                    $model['paket']->auto_forward = 1;
                    $model['paket']->peluang = '25';
                }
                if (!$model['paket']->save()) {
                    throw new \yii\base\UserException('Data cannot be saved. Please try again.');
                }

                $error = false;
                $model['tahap'] = $this->findModelTahap();
                foreach ($model['tahap'] as $key => $tahap) {
                    $paketTahapJadwal = new PaketTahapJadwal();
                    $paketTahapJadwal->id_paket = $model['paket']->id;
                    $paketTahapJadwal->id_tahap = $tahap->id;
                    if (!$paketTahapJadwal->save()) {
                        $error = true;
                    }
                }

                $model['form'] = $this->findModelForm();
                $paketForm = new PaketForm();
                $paketForm->setAttributes($model['form']->getAttributes());
                $paketForm->id_paket = $model['paket']->id;
                if (!$paketForm->save()) {
                    $error = true;
                }

                $model['form_komisaris'] = $this->findModelFormKomisaris();
                foreach ($model['form_komisaris'] as $key => $formKomisaris) {
                    $paketFormKomisaris = new PaketFormKomisaris();
                    $paketFormKomisaris->setAttributes($formKomisaris->getAttributes());
                    $paketFormKomisaris->id_paket_form = $paketForm->id_paket;
                    if (!$paketFormKomisaris->save()) {
                        $error = true;
                    }
                }
                $model['form_direksi'] = $this->findModelFormDireksi();
                foreach ($model['form_direksi'] as $key => $formDireksi) {
                    $paketFormDireksi = new PaketFormDireksi();
                    $paketFormDireksi->setAttributes($formDireksi->getAttributes());
                    $paketFormDireksi->id_paket_form = $paketForm->id_paket;
                    if (!$paketFormDireksi->save()) {
                        $error = true;
                    }
                }
                $model['form_sbuk'] = $this->findModelFormSbuk();
                foreach ($model['form_sbuk'] as $key => $formSbuk) {
                    $paketFormSbuk = new PaketFormSbuk();
                    $paketFormSbuk->setAttributes($formSbuk->getAttributes());
                    $paketFormSbuk->id_paket_form = $paketForm->id_paket;
                    if (!$paketFormSbuk->save()) {
                        $error = true;
                    }
                }
                $model['form_sbunk'] = $this->findModelFormSbunk();
                foreach ($model['form_sbunk'] as $key => $formSbunk) {
                    $paketFormSbunk = new PaketFormSbunk();
                    $paketFormSbunk->setAttributes($formSbunk->getAttributes());
                    $paketFormSbunk->id_paket_form = $paketForm->id_paket;
                    if (!$paketFormSbunk->save()) {
                        $error = true;
                    }
                }
                $model['form_ijin_lainnya'] = $this->findModelFormIjinLainnya();
                foreach ($model['form_ijin_lainnya'] as $key => $formIjinLainnya) {
                    $paketFormIjinLainnya = new PaketFormIjinLainnya();
                    $paketFormIjinLainnya->setAttributes($formIjinLainnya->getAttributes());
                    $paketFormIjinLainnya->id_paket_form = $paketForm->id_paket;
                    if (!$paketFormIjinLainnya->save()) {
                        $error = true;
                    }
                }
                $model['form_pemilik_saham'] = $this->findModelFormPemilikSaham();
                foreach ($model['form_pemilik_saham'] as $key => $formPemilikSaham) {
                    $paketFormPemilikSaham = new PaketFormPemilikSaham();
                    $paketFormPemilikSaham->setAttributes($formPemilikSaham->getAttributes());
                    $paketFormPemilikSaham->id_paket_form = $paketForm->id_paket;
                    if (!$paketFormPemilikSaham->save()) {
                        $error = true;
                    }
                }
                $model['form_pajak'] = $this->findModelFormPajak();
                foreach ($model['form_pajak'] as $key => $formPajak) {
                    $paketFormPajak = new PaketFormPajak();
                    $paketFormPajak->setAttributes($formPajak->getAttributes());
                    $paketFormPajak->id_paket_form = $paketForm->id_paket;
                    if (!$paketFormPajak->save()) {
                        $error = true;
                    }
                }
                $model['form_bukti_pajak'] = $this->findModelFormBuktiPajak();
                foreach ($model['form_bukti_pajak'] as $key => $formBuktiPajak) {
                    $paketFormBuktiPajak = new PaketFormBuktiPajak();
                    $paketFormBuktiPajak->setAttributes($formBuktiPajak->getAttributes());
                    $paketFormBuktiPajak->id_paket_form = $paketForm->id_paket;
                    if (!$paketFormBuktiPajak->save()) {
                        $error = true;
                    }
                }

                if (isset($model['paket_mitra']) and is_array($model['paket_mitra'])) {
                    foreach ($model['paket_mitra'] as $key => $paketMitra) {
                        if ($paketMitra->isDeleted) {
                            if (!$paketMitra->delete()) {
                                $error = true;
                            }
                        } else {
                            $paketMitra->id_paket = $model['paket']->id;
                            if (!$paketMitra->save()) {
                                $error = true;
                            }
                        }
                    }
                }

                if ($error) {
                    throw new \yii\base\UserException('Data cannot be saved. Please try again.');
                }
                
                $transaction['paket']->commit();
                Yii::$app->session->setFlash('success', 'Data has been saved.');
            } catch (\Exception $e) {
                $render = true;
                $transaction['paket']->rollBack();
            } catch (\Throwable $e) {
                $render = true;
                $transaction['paket']->rollBack();
            }
        } else {
            foreach ($model['paket']->paketMitras as $key => $paketMitra)
                $model['paket_mitra'][] = $paketMitra;

            if ($model['paket']->isNewRecord) {
                $model['paket']->tahun_anggaran = date('Y');
                $model['paket']->is_kso = 'Tidak';
                $model['paket']->peluang = 25;
            }
            $render = true;
        }

        if ($render)
            return $this->render('form-create', [
                'model' => $model,
                'title' => 'Tambah Paket',
            ]);
        else
            return $this->redirect(['site/index']);
    }

    public function actionCreatePengalamanPerusahaan()
    {
        $render = false;

        $model['paket'] = new Paket();

        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();

            $model['paket']->load($post);
            if (isset($post['PaketMitra'])) {
                foreach ($post['PaketMitra'] as $key => $value) {
                    if ($value['id'] > 0) {
                        $paketMitra = $this->findModelPaketMitra($value['id']);
                        $paketMitra->setAttributes($value);
                    } else if($value['id'] < 0) {
                        $paketMitra = $this->findModelPaketMitra(($value['id']*-1));
                        $paketMitra->isDeleted = true;
                    } else {
                        $paketMitra = new PaketMitra();
                        $paketMitra->setAttributes($value);
                    }
                    $model['paket_mitra'][] = $paketMitra;
                }
            }

            $transaction['paket'] = Paket::getDb()->beginTransaction();

            try {
                if ($model['paket']->isNewRecord) {
                    $model['paket']->status = ['Kontrak'];
                    $model['paket']->auto_forward = 0;
                    $model['paket']->peluang = '95';
                }
                if (!$model['paket']->save()) {
                    throw new \yii\base\UserException('Data cannot be saved. Please try again.');
                }

                $error = false;
                $model['tahap'] = $this->findModelTahap();
                foreach ($model['tahap'] as $key => $tahap) {
                    $paketTahapJadwal = new PaketTahapJadwal();
                    $paketTahapJadwal->id_paket = $model['paket']->id;
                    $paketTahapJadwal->id_tahap = $tahap->id;
                    if (!$paketTahapJadwal->save()) {
                        $error = true;
                    }
                }

                $model['form'] = $this->findModelForm();
                $paketForm = new PaketForm();
                $paketForm->setAttributes($model['form']->getAttributes());
                $paketForm->id_paket = $model['paket']->id;
                if (!$paketForm->save()) {
                    $error = true;
                }

                if (isset($model['paket_mitra']) and is_array($model['paket_mitra'])) {
                    foreach ($model['paket_mitra'] as $key => $paketMitra) {
                        if ($paketMitra->isDeleted) {
                            if (!$paketMitra->delete()) {
                                $error = true;
                            }
                        } else {
                            $paketMitra->id_paket = $model['paket']->id;
                            if (!$paketMitra->save()) {
                                $error = true;
                            }
                        }
                    }
                }

                if ($error) {
                    throw new \yii\base\UserException('Data cannot be saved. Please try again.');
                }
                
                $transaction['paket']->commit();
                Yii::$app->session->setFlash('success', 'Data has been saved.');
            } catch (\Exception $e) {
                $render = true;
                $transaction['paket']->rollBack();
            } catch (\Throwable $e) {
                $render = true;
                $transaction['paket']->rollBack();
            }
        } else {
            foreach ($model['paket']->paketMitras as $key => $paketMitra)
                $model['paket_mitra'][] = $paketMitra;

            if ($model['paket']->isNewRecord) {
                $model['paket']->tahun_anggaran = date('Y');
                $model['paket']->is_kso = 'Tidak';
            }
            $render = true;
        }

        if ($render)
            return $this->render('form-create-pengalaman-perusahaan', [
                'model' => $model,
                'title' => 'Tambah Pengalaman Perusahaan',
            ]);
        else
            return $this->redirect(['site/index']);
    }

    public function actionIndex($id)
    {
        $model['paket'] = $this->findModel($id);
        $model['paket_form'] = $model['paket']->paketForm;
        foreach ($model['paket']->paketTahapJadwals as $key => $paketTahapJadwal)
            $model['paket_tahap_jadwal'][] = $paketTahapJadwal;
        
        return $this->render('one', [
            'model' => $model,
            'title' => 'Detail Paket',
        ]);
    }

    // Info Paket
    public function actionUpdateInfoPaketDetail($id)
    {
        $render = false;

        $model['paket'] = $this->findModel($id);

        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();

            $model['paket']->load($post);
            if (isset($post['PaketMitra'])) {
                foreach ($post['PaketMitra'] as $key => $value) {
                    if ($value['id'] > 0) {
                        $paketMitra = $this->findModelPaketMitra($value['id']);
                        $paketMitra->setAttributes($value);
                    } else if($value['id'] < 0) {
                        $paketMitra = $this->findModelPaketMitra(($value['id']*-1));
                        $paketMitra->isDeleted = true;
                    } else {
                        $paketMitra = new PaketMitra();
                        $paketMitra->setAttributes($value);
                    }
                    $model['paket_mitra'][] = $paketMitra;
                }
            }

            $transaction['paket'] = Paket::getDb()->beginTransaction();

            try {
                if (!$model['paket']->save()) {
                    throw new \yii\base\UserException('Data cannot be saved. Please try again.');
                }

                $error = false;
                if (isset($model['paket_mitra']) and is_array($model['paket_mitra'])) {
                    foreach ($model['paket_mitra'] as $key => $paketMitra) {
                        if ($paketMitra->isDeleted) {
                            if (!$paketMitra->delete()) {
                                $error = true;
                            }
                        } else {
                            $paketMitra->id_paket = $model['paket']->id;
                            if (!$paketMitra->save()) {
                                $error = true;
                            }
                        }
                    }
                }

                if ($error) {
                    throw new \yii\base\UserException('Data cannot be saved. Please try again.');
                }
                
                $transaction['paket']->commit();
                Yii::$app->session->setFlash('success', 'Data has been saved.');
            } catch (\Exception $e) {
                $render = true;
                $transaction['paket']->rollBack();
            } catch (\Throwable $e) {
                $render = true;
                $transaction['paket']->rollBack();
            }
        } else {
            foreach ($model['paket']->paketMitras as $key => $paketMitra)
                $model['paket_mitra'][] = $paketMitra;
            
            $render = true;
        }

        if ($render)
            return $this->render('form-update-info_paket_detail', [
                'model' => $model,
                'title' => 'Update Paket ' . $model['paket']->nama_paket,
            ]);
        else
            return $this->redirect(['paket/index', 'id' => $model['paket']->id]);
    }

    public function actionUpdateTahapLelang($id)
    {
        $render = false;

        $model['paket'] = $this->findModel($id);

        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();

            $model['paket']->load($post);
            if (isset($post['PaketTahapJadwal'])) {
                foreach ($post['PaketTahapJadwal'] as $key => $value) {
                    $paketTahapJadwal = $this->findModelPaketTahapJadwal($value['id']);
                    $paketTahapJadwal->setAttributes($value);
                    $model['paket_tahap_jadwal'][] = $paketTahapJadwal;
                }
            }

            $transaction['paket'] = Paket::getDb()->beginTransaction();

            try {
                if (!$model['paket']->save()) {
                    throw new \yii\base\UserException('Data cannot be saved. Please try again.');
                }

                $error = false;
                if (isset($model['paket_tahap_jadwal']) and is_array($model['paket_tahap_jadwal'])) {
                    foreach ($model['paket_tahap_jadwal'] as $key => $paketTahapJadwal) {
                        if (!$paketTahapJadwal->save()) {
                            $error = true;
                        }
                    }
                }

                if ($error) {
                    throw new \yii\base\UserException('Data cannot be saved. Please try again.');
                }
                
                $transaction['paket']->commit();
                Yii::$app->session->setFlash('success', 'Data has been saved.');
            } catch (\Exception $e) {
                $render = true;
                $transaction['paket']->rollBack();
            } catch (\Throwable $e) {
                $render = true;
                $transaction['paket']->rollBack();
            }
        } else {
            foreach ($model['paket']->paketTahapJadwals as $key => $paketTahapJadwal)
                $model['paket_tahap_jadwal'][] = $paketTahapJadwal;

            $render = true;
        }

        if ($render)
            return $this->render('form-update-tahap_lelang', [
                'model' => $model,
                'title' => 'Update Paket ' . $model['paket']->nama_paket,
            ]);
        else
            return $this->redirect(['paket/index', 'id' => $model['paket']->id]);
    }

    //Prakualifikasi
    public function actionUpdateFormIsianAg($id)
    {
        $render = false;

        $model['paket'] = $this->findModel($id);
        $model['paket_form'] = $this->findModelPaketForm($id);

        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();

            $model['paket_form']->load($post);
            if (isset($post['PaketFormKomisaris'])) {
                foreach ($post['PaketFormKomisaris'] as $key => $value) {
                    if ($value['id'] > 0) {
                        $paketFormKomisaris = $this->findModelPaketFormKomisaris($value['id']);
                        $paketFormKomisaris->setAttributes($value);
                    } else if($value['id'] < 0) {
                        $paketFormKomisaris = $this->findModelPaketFormKomisaris(($value['id']*-1));
                        $paketFormKomisaris->isDeleted = true;
                    } else {
                        $paketFormKomisaris = new PaketFormKomisaris();
                        $paketFormKomisaris->setAttributes($value);
                    }
                    $model['paket_form_komisaris'][] = $paketFormKomisaris;
                }
            }
            if (isset($post['PaketFormDireksi'])) {
                foreach ($post['PaketFormDireksi'] as $key => $value) {
                    if ($value['id'] > 0) {
                        $paketFormDireksi = $this->findModelPaketFormDireksi($value['id']);
                        $paketFormDireksi->setAttributes($value);
                    } else if($value['id'] < 0) {
                        $paketFormDireksi = $this->findModelPaketFormDireksi(($value['id']*-1));
                        $paketFormDireksi->isDeleted = true;
                    } else {
                        $paketFormDireksi = new PaketFormDireksi();
                        $paketFormDireksi->setAttributes($value);
                    }
                    $model['paket_form_direksi'][] = $paketFormDireksi;
                }
            }
            if (isset($post['PaketFormSbuk'])) {
                foreach ($post['PaketFormSbuk'] as $key => $value) {
                    if ($value['id'] > 0) {
                        $paketFormSbuk = $this->findModelPaketFormSbuk($value['id']);
                        $paketFormSbuk->setAttributes($value);
                    } else if($value['id'] < 0) {
                        $paketFormSbuk = $this->findModelPaketFormSbuk(($value['id']*-1));
                        $paketFormSbuk->isDeleted = true;
                    } else {
                        $paketFormSbuk = new PaketFormSbuk();
                        $paketFormSbuk->setAttributes($value);
                    }
                    $model['paket_form_sbuk'][] = $paketFormSbuk;
                }
            }
            if (isset($post['PaketFormSbunk'])) {
                foreach ($post['PaketFormSbunk'] as $key => $value) {
                    if ($value['id'] > 0) {
                        $paketFormSbunk = $this->findModelPaketFormSbunk($value['id']);
                        $paketFormSbunk->setAttributes($value);
                    } else if($value['id'] < 0) {
                        $paketFormSbunk = $this->findModelPaketFormSbunk(($value['id']*-1));
                        $paketFormSbunk->isDeleted = true;
                    } else {
                        $paketFormSbunk = new PaketFormSbunk();
                        $paketFormSbunk->setAttributes($value);
                    }
                    $model['paket_form_sbunk'][] = $paketFormSbunk;
                }
            }
            if (isset($post['PaketFormIjinLainnya'])) {
                foreach ($post['PaketFormIjinLainnya'] as $key => $value) {
                    if ($value['id'] > 0) {
                        $paketFormIjinLainnya = $this->findModelPaketFormIjinLainnya($value['id']);
                        $paketFormIjinLainnya->setAttributes($value);
                    } else if($value['id'] < 0) {
                        $paketFormIjinLainnya = $this->findModelPaketFormIjinLainnya(($value['id']*-1));
                        $paketFormIjinLainnya->isDeleted = true;
                    } else {
                        $paketFormIjinLainnya = new PaketFormIjinLainnya();
                        $paketFormIjinLainnya->setAttributes($value);
                    }
                    $model['paket_form_ijin_lainnya'][] = $paketFormIjinLainnya;
                }
            }
            if (isset($post['PaketFormPemilikSaham'])) {
                foreach ($post['PaketFormPemilikSaham'] as $key => $value) {
                    if ($value['id'] > 0) {
                        $paketFormPemilikSaham = $this->findModelPaketFormPemilikSaham($value['id']);
                        $paketFormPemilikSaham->setAttributes($value);
                    } else if($value['id'] < 0) {
                        $paketFormPemilikSaham = $this->findModelPaketFormPemilikSaham(($value['id']*-1));
                        $paketFormPemilikSaham->isDeleted = true;
                    } else {
                        $paketFormPemilikSaham = new PaketFormPemilikSaham();
                        $paketFormPemilikSaham->setAttributes($value);
                    }
                    $model['paket_form_pemilik_saham'][] = $paketFormPemilikSaham;
                }
            }
            if (isset($post['PaketFormPajak'])) {
                foreach ($post['PaketFormPajak'] as $key => $value) {
                    if ($value['id'] > 0) {
                        $paketFormPajak = $this->findModelPaketFormPajak($value['id']);
                        $paketFormPajak->setAttributes($value);
                    } else if($value['id'] < 0) {
                        $paketFormPajak = $this->findModelPaketFormPajak(($value['id']*-1));
                        $paketFormPajak->isDeleted = true;
                    } else {
                        $paketFormPajak = new PaketFormPajak();
                        $paketFormPajak->setAttributes($value);
                    }
                    $model['paket_form_pajak'][] = $paketFormPajak;
                }
            }
            if (isset($post['PaketFormBuktiPajak'])) {
                foreach ($post['PaketFormBuktiPajak'] as $key => $value) {
                    if ($value['id'] > 0) {
                        $paketFormBuktiPajak = $this->findModelPaketFormBuktiPajak($value['id']);
                        $paketFormBuktiPajak->setAttributes($value);
                    } else if($value['id'] < 0) {
                        $paketFormBuktiPajak = $this->findModelPaketFormBuktiPajak(($value['id']*-1));
                        $paketFormBuktiPajak->isDeleted = true;
                    } else {
                        $paketFormBuktiPajak = new PaketFormBuktiPajak();
                        $paketFormBuktiPajak->setAttributes($value);
                    }
                    $model['paket_form_bukti_pajak'][] = $paketFormBuktiPajak;
                }
            }

            $transaction['paket_form'] = PaketForm::getDb()->beginTransaction();

            try {
                if (!$model['paket_form']->save()) {
                    throw new \yii\base\UserException('Data cannot be saved. Please try again.');
                }

                $error = false;
                if (isset($model['paket_form_komisaris']) and is_array($model['paket_form_komisaris'])) {
                    foreach ($model['paket_form_komisaris'] as $key => $paketFormKomisaris) {
                        if ($paketFormKomisaris->isDeleted) {
                            if (!$paketFormKomisaris->delete()) {
                                $error = true;
                            }
                        } else {
                            $paketFormKomisaris->id_paket_form = $model['paket_form']->id_paket;
                            if (!$paketFormKomisaris->save()) {
                                $error = true;
                            }
                        }
                    }
                }
                if (isset($model['paket_form_direksi']) and is_array($model['paket_form_direksi'])) {
                    foreach ($model['paket_form_direksi'] as $key => $paketFormDireksi) {
                        if ($paketFormDireksi->isDeleted) {
                            if (!$paketFormDireksi->delete()) {
                                $error = true;
                            }
                        } else {
                            $paketFormDireksi->id_paket_form = $model['paket_form']->id_paket;
                            if (!$paketFormDireksi->save()) {
                                $error = true;
                            }
                        }
                    }
                }
                if (isset($model['paket_form_sbuk']) and is_array($model['paket_form_sbuk'])) {
                    foreach ($model['paket_form_sbuk'] as $key => $paketFormSbuk) {
                        if ($paketFormSbuk->isDeleted) {
                            if (!$paketFormSbuk->delete()) {
                                $error = true;
                            }
                        } else {
                            $paketFormSbuk->id_paket_form = $model['paket_form']->id_paket;
                            if (!$paketFormSbuk->save()) {
                                $error = true;
                            }
                        }
                    }
                }
                if (isset($model['paket_form_sbunk']) and is_array($model['paket_form_sbunk'])) {
                    foreach ($model['paket_form_sbunk'] as $key => $paketFormSbunk) {
                        if ($paketFormSbunk->isDeleted) {
                            if (!$paketFormSbunk->delete()) {
                                $error = true;
                            }
                        } else {
                            $paketFormSbunk->id_paket_form = $model['paket_form']->id_paket;
                            if (!$paketFormSbunk->save()) {
                                $error = true;
                            }
                        }
                    }
                }
                if (isset($model['paket_form_ijin_lainnya']) and is_array($model['paket_form_ijin_lainnya'])) {
                    foreach ($model['paket_form_ijin_lainnya'] as $key => $paketFormIjinLainnya) {
                        if ($paketFormIjinLainnya->isDeleted) {
                            if (!$paketFormIjinLainnya->delete()) {
                                $error = true;
                            }
                        } else {
                            $paketFormIjinLainnya->id_paket_form = $model['paket_form']->id_paket;
                            if (!$paketFormIjinLainnya->save()) {
                                $error = true;
                            }
                        }
                    }
                }
                if (isset($model['paket_form_pemilik_saham']) and is_array($model['paket_form_pemilik_saham'])) {
                    foreach ($model['paket_form_pemilik_saham'] as $key => $paketFormPemilikSaham) {
                        if ($paketFormPemilikSaham->isDeleted) {
                            if (!$paketFormPemilikSaham->delete()) {
                                $error = true;
                            }
                        } else {
                            $paketFormPemilikSaham->id_paket_form = $model['paket_form']->id_paket;
                            if (!$paketFormPemilikSaham->save()) {
                                $error = true;
                            }
                        }
                    }
                }
                if (isset($model['paket_form_pajak']) and is_array($model['paket_form_pajak'])) {
                    foreach ($model['paket_form_pajak'] as $key => $paketFormPajak) {
                        if ($paketFormPajak->isDeleted) {
                            if (!$paketFormPajak->delete()) {
                                $error = true;
                            }
                        } else {
                            $paketFormPajak->id_paket_form = $model['paket_form']->id_paket;
                            if (!$paketFormPajak->save()) {
                                $error = true;
                            }
                        }
                    }
                }
                if (isset($model['paket_form_bukti_pajak']) and is_array($model['paket_form_bukti_pajak'])) {
                    foreach ($model['paket_form_bukti_pajak'] as $key => $paketFormBuktiPajak) {
                        if ($paketFormBuktiPajak->isDeleted) {
                            if (!$paketFormBuktiPajak->delete()) {
                                $error = true;
                            }
                        } else {
                            $paketFormBuktiPajak->id_paket_form = $model['paket_form']->id_paket;
                            if (!$paketFormBuktiPajak->save()) {
                                $error = true;
                            }
                        }
                    }
                }

                if ($error) {
                    throw new \yii\base\UserException('Data cannot be saved. Please try again.');
                }
                
                $transaction['paket_form']->commit();
                Yii::$app->session->setFlash('success', 'Data has been saved.');
            } catch (\Exception $e) {
                $render = true;
                $transaction['paket_form']->rollBack();
            } catch (\Throwable $e) {
                $render = true;
                $transaction['paket_form']->rollBack();
            }
        } else {
            foreach ($model['paket_form']->paketFormKomisarises as $key => $paketFormKomisaris)
                $model['paket_form_komisaris'][] = $paketFormKomisaris;
            foreach ($model['paket_form']->paketFormDireksis as $key => $paketFormDireksi)
                $model['paket_form_direksi'][] = $paketFormDireksi;
            foreach ($model['paket_form']->paketFormSbuks as $key => $paketFormSbuk)
                $model['paket_form_sbuk'][] = $paketFormSbuk;
            foreach ($model['paket_form']->paketFormSbunks as $key => $paketFormSbunk)
                $model['paket_form_sbunk'][] = $paketFormSbunk;
            foreach ($model['paket_form']->paketFormIjinLainnyas as $key => $paketFormIjinLainnya)
                $model['paket_form_ijin_lainnya'][] = $paketFormIjinLainnya;
            foreach ($model['paket_form']->paketFormPemilikSahams as $key => $paketFormPemilikSaham)
                $model['paket_form_pemilik_saham'][] = $paketFormPemilikSaham;
            foreach ($model['paket_form']->paketFormPajaks as $key => $paketFormPajak)
                $model['paket_form_pajak'][] = $paketFormPajak;
            foreach ($model['paket_form']->paketFormBuktiPajaks as $key => $paketFormBuktiPajak)
                $model['paket_form_bukti_pajak'][] = $paketFormBuktiPajak;

            $render = true;
        }

        if ($render)
            return $this->render('form-update-form-isian-ag', [
                'model' => $model,
                'title' => 'Update Paket ' . $model['paket']->nama_paket,
            ]);
        else
            return $this->redirect(['paket/index', 'id' => $model['paket']->id]);
    }

    public function actionUpdateTenagaAhli($id)
    {
        $render = false;

        $model['paket'] = $this->findModel($id);

        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();
            if (isset($post['PaketTenagaAhli'])) {
                foreach ($post['PaketTenagaAhli'] as $key => $value) {
                    if ($value['id'] > 0) {
                        $paketTenagaAhli = $this->findModelPaketTenagaAhli($value['id']);
                        $paketTenagaAhli->setAttributes($value);
                    } else if($value['id'] < 0) {
                        $paketTenagaAhli = $this->findModelPaketTenagaAhli(($value['id']*-1));
                        $paketTenagaAhli->isDeleted = true;
                    } else {
                        $paketTenagaAhli = new PaketTenagaAhli();
                        $paketTenagaAhli->setAttributes($value);
                        // ddx($paketTenagaAhli->id_tenaga_ahli);
                        $tenagaAhli = $this->findModelTenagaAhli($paketTenagaAhli->id_tenaga_ahli);
                        foreach ($tenagaAhli->tenagaAhliPendidikans as $key => $tenagaAhliPendidikan) {
                            $temp = new PaketTenagaAhliPendidikan();
                            $temp->setAttributes($tenagaAhliPendidikan->getAttributes());
                            $model['paket_tenaga_ahli_pendidikan'][] = $temp;
                        }
                        foreach ($tenagaAhli->tenagaAhliKeahlians as $key => $tenagaAhliKeahlian) {
                            $temp = new PaketTenagaAhliKeahlian();
                            $temp->setAttributes($tenagaAhliKeahlian->getAttributes());
                            $model['paket_tenaga_ahli_keahlian'][] = $temp;
                        }
                        // ddx($tenagaAhli);
                        $paketTenagaAhli->setAttributes($tenagaAhli->getAttributes());
                        // ddx($paketTenagaAhli->validate(), $paketTenagaAhli->errors);
                    }
                    $model['paket_tenaga_ahli'][] = $paketTenagaAhli;
                }
            }

            $model['paket']->load($post);

            $transaction['paket'] = Paket::getDb()->beginTransaction();

            try {
                if (!$model['paket']->save()) {
                    throw new \yii\base\UserException('Data cannot be saved. Please try again.');
                }

                $error = false;
                if (isset($model['paket_tenaga_ahli']) and is_array($model['paket_tenaga_ahli'])) {
                    foreach ($model['paket_tenaga_ahli'] as $key => $paketTenagaAhli) {
                        if ($paketTenagaAhli->isDeleted) {
                            if (!$paketTenagaAhli->delete()) {
                                $error = true;
                            }
                        } else {
                            $paketTenagaAhli->id_paket = $model['paket']->id;
                            // $tenagaAhli = $this->findModelTenagaAhli($paketTenagaAhli->id_tenaga_ahli);
                            // ddx($paketTenagaAhli->validate(), $paketTenagaAhli->errors);
                            if (!$paketTenagaAhli->save()) {
                                // ddx($paketTenagaAhli->errors, $post);
                                $error = true;
                            }
                            foreach ($model['paket_tenaga_ahli_pendidikan'] as $key => $paketTenagaAhliPendidikan) {
                                $paketTenagaAhliPendidikan->id_paket_tenaga_ahli = $paketTenagaAhli->id;
                                if (!$paketTenagaAhliPendidikan->save()) {
                                    ddx($paketTenagaAhliPendidikan->errors, $post);
                                    $error = true;
                                }
                            }
                            foreach ($model['paket_tenaga_ahli_keahlian'] as $key => $paketTenagaAhliKeahlian) {
                                $paketTenagaAhliKeahlian->id_paket_tenaga_ahli = $paketTenagaAhli->id;
                                if (!$paketTenagaAhliKeahlian->save()) {
                                    ddx($paketTenagaAhliKeahlian->errors, $post);
                                    $error = true;
                                }
                            }
                        }
                    }
                }

                if ($error) {
                    throw new \yii\base\UserException('Data cannot be saved. Please try again.');
                }
                
                $transaction['paket']->commit();
                Yii::$app->session->setFlash('success', 'Data has been saved.');
            } catch (\Exception $e) {
                $render = true;
                $transaction['paket']->rollBack();
            } catch (\Throwable $e) {
                $render = true;
                $transaction['paket']->rollBack();
            }
        } else {
            foreach ($model['paket']->paketTenagaAhlis as $key => $paketTenagaAhli)
                $model['paket_tenaga_ahli'][] = $paketTenagaAhli;

            $render = true;
        }

        if ($render)
            return $this->render('form-update-tenaga_ahli', [
                'model' => $model,
                'title' => 'Update Paket ' . $model['paket']->nama_paket,
            ]);
        else
            return $this->redirect(['paket/index', 'id' => $model['paket']->id]);
    }

    //proposal
    public function actionUpdateJadwalPelaksanaanKegiatan($id)
    {
        if ($render)
            return $this->render('form-update-jadwal_pelaksanaan_pekerjaan', [
                'model' => $model,
                'title' => 'Update Paket ' . $model['paket']->nama_paket,
            ]);
        else
            return $this->redirect(['paket/index', 'id' => $model['paket']->id]);
    }


    //

    /*public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }*/
}
