<?php
namespace app_virama_karya\controllers;

use Yii;
use technosmart\controllers\SiteController as SiteControl;
use app_virama_karya\models\User;
use app_virama_karya\models\UserExtend;
use app_virama_karya\models\Login;

class SiteController extends SiteControl
{
    public function actionIndex()
    {
    	if (\Yii::$app->user->isGuest) {
            return $this->redirect(['login']);
        }
        return $this->render('/paket/list', [
            'title' => 'Dashboard',
        ]);
    }
}