<?php
namespace app_virama_karya\controllers;

use Yii;
use app_virama_karya\models\Bidang;
use technosmart\yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\widgets\ActiveForm;

class DaftarMenuController extends Controller
{
    public function actionIndex()
    {
        return $this->render('index', [
            'title' => 'Daftar Menu',
        ]);
    }
}
