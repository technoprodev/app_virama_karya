<?php
namespace app_virama_karya\controllers;

use Yii;
use app_virama_karya\models\Satker;
use technosmart\yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\widgets\ActiveForm;

/**
 * SatkerController implements highly advanced CRUD actions for Satker model.
 */
class SatkerController extends Controller
{
    /*public static $permissions = [
        ['view', 'View Satker'], ['create', 'Create Satker'], ['update', 'Update Satker'], ['delete', 'Delete Satker'],
    ];

    public function behaviors()
    {
        return [
            'access' => $this->access([
                [['index'], 'view'],
                [['index', 'create'], 'create'],
                [['index', 'update'], 'update'],
                [['index', 'delete'], 'delete', null, ['POST']],
            ]),
        ];
    }*/

    /**
     * Finds the Satker model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Satker the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Satker::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionDatatables()
    {
        $db = Satker::getDb();
        $post = Yii::$app->request->post();

        // serve data for datatables
        if (isset($post['draw'])) {
            $query = new \yii\db\Query();
            $query
                ->select('count(*)')
                ->from('satker');
            $countWhere = count($query->where);

            $total = $query->scalar($db);
            $return['recordsTotal'] = $total;
            $return['recordsFiltered'] = $total;

            $allWhere = ['or'];
            $allSearch = $post['search']['value'];
            foreach ($post['columns'] as $key => $value) {
                if ($value['searchable'] == 'true') {
                    $column = $value['data'];
                    if (is_array($column)) {
                        if ( isset($column['filter']) )
                            $column = $column['filter'];
                        else
                            $column = $column['_'];
                    }

                    if ($value['search']['regex'] == 'false') {
                        $query->andFilterWhere(['like', $column, $value['search']['value']]);
                    } else if ($value['search']['regex'] == 'true') {
                        $query->andFilterWhere(['regexp', $column, $value['search']['value']]);
                    }

                    if ($allSearch) {
                        if ($post['search']['regex'] == 'false') {
                            $allWhere[] = ['like', $column, $allSearch];
                        } else if ($post['search']['regex'] == 'true') {
                            $allWhere[] = ['regexp', $column, $allSearch];
                        }
                    }
                }
            }
            if (count($allWhere) > 1)
                $query->andFilterWhere($allWhere);
            if (count($query->where) > $countWhere)
                $return['recordsFiltered'] = $query->scalar($db);

            $query->select([
                'id',
                'nama_satker',
                'id_satminkal',
            ]);

            $order = [];
            if (isset($post['order'])) {
                foreach ($post['order'] as $key => $value) {
                    $column = $post['columns'][$value['column']]['data'];
                    if ($post['columns'][$value['column']]['orderable'] == 'false') {
                        continue;
                    }
                    if (is_array($column)) {
                        if ( isset($column['sort']) )
                            $column = $column['sort'];
                        else
                            $column = $column['_'];
                    }

                    if ($value['dir'] == 'asc')
                        $order[$column] = SORT_ASC;
                    else if ($value['dir'] == 'desc')
                        $order[$column] = SORT_DESC;
                }
            }
            count($order) ? $query->orderBy($order) : 0;

            if (isset($post['length']))
                $query->limit(intval($post['length']));

            if (isset($post['start']))
                $query->offset(intval($post['start']));

            $return['draw'] = intval($post['draw']);
            $return['data'] = $query->all($db);
            return $this->json($return);
        }
    }

    /**
     * If param(s) is null, display all datas from models.
     * If all param(s) is not null, display a data from model.
     * @param integer $id
     * @return mixed
     */
    public function actionIndex($id = null)
    {
        // view all data
        if (!$id) {
            return $this->render('list', [
                'title' => 'List of Satkers',
            ]);
        }
        
        // view single data
        $model['satker'] = $this->findModel($id);
        return $this->render('one', [
            'model' => $model,
            'title' => 'Detail of Satker ' . $model['satker']->id,
        ]);
    }

    /**
     * Creates new data(s) from model(s).
     * If submission is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionCreate()
    {
        $render = false;

        $model['satker'] = isset($id) ? $this->findModel($id) : new Satker();

        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();

            $model['satker']->load($post);

            $transaction['satker'] = Satker::getDb()->beginTransaction();

            try {
                if (!$model['satker']->save()) {
                    throw new \yii\base\UserException('Data cannot be saved. Please try again.');
                }
                
                $transaction['satker']->commit();
                Yii::$app->session->setFlash('success', 'Data has been saved.');
            } catch (\Exception $e) {
                $render = true;
                $transaction['satker']->rollBack();
            } catch (\Throwable $e) {
                $render = true;
                $transaction['satker']->rollBack();
            }
        } else {
            $render = true;
        }

        if ($render)
            return $this->render('form', [
                'model' => $model,
                'title' => 'Add New Satker',
            ]);
        else
            return $this->redirect(['index', 'id' => $model['satker']->id]);
    }

    /**
     * Updates existing data(s) from model(s).
     * If submission is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id = null)
    {
        $render = false;

        $model['satker'] = isset($id) ? $this->findModel($id) : new Satker();

        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();

            $model['satker']->load($post);

            $transaction['satker'] = Satker::getDb()->beginTransaction();

            try {
                if (!$model['satker']->save()) {
                    throw new \yii\base\UserException('Data cannot be saved. Please try again.');
                }
                
                $transaction['satker']->commit();
                Yii::$app->session->setFlash('success', 'Data has been saved.');
            } catch (\Exception $e) {
                $render = true;
                $transaction['satker']->rollBack();
            } catch (\Throwable $e) {
                $render = true;
                $transaction['satker']->rollBack();
            }
        } else {
            $render = true;
        }

        if ($render)
            return $this->render('form', [
                'model' => $model,
                'title' => 'Update Satker ' . $model['satker']->id,
            ]);
        else
            return $this->redirect(['index', 'id' => $model['satker']->id]);
    }

    /**
     * Deletes an existing Satker model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }
}
