<?php

namespace app_virama_karya\controllers;

use Yii;
use app_virama_karya\models\Timeline;
use app_virama_karya\models\TimelineDetail;
use technosmart\yii\web\Controller;
use yii\db\Expression;
use yii\db\Query;
use yii\helpers\ArrayHelper;

class TimelineController extends Controller 
{
    public function actionIndex($id)
    {
    	$request = Yii::$app->request;

    	if ($request->isAjax) {
            $query = (new Query())->from("timeline t")
                ->where(["t.paket_id" => $id])
                ->orderBy(["t.index" => SORT_ASC]);

            $data = [];

            foreach ($query->each() as $item) {
                $data[] = [
                    "task" => $item["task"],
                    "index" => $item["index"],
                    "parent_index" => $item["parent_index"],
                    "month_start" => $item["month_start"],
                    "month_end" => $item["month_end"],
                ];
            }

            return $this->json($data);
        }
    	else if ($request->isPost) {
            $post = $request->post();
            $data = [];

            foreach ($post["timeline"] as $item) {
                $data[] = [
                    "task" => $item["task"],
                    "month_start" => $item["month_start"],
                    "month_end" => $item["month_end"],
                    "parent_index" => $item["parent_index"]=="" ? null : $item["parent_index"],
                    "index" => $item["index"],
                    "paket_id" => $id,
                ];
            }

            $db = Yii::$app->db;
            $tr = $db->beginTransaction();
            $cmd = $db->createCommand();

            try {
                $cmd->delete('timeline', ["paket_id" => $id])->execute();
                $cmd->batchInsert("timeline", ["task","month_start","month_end","parent_index","index", "paket_id"], $data)->execute();

                $tr->commit();
            }
            catch (\Exception $e) {
                $tr->rollback();
                throw $e;
            }

            return $this->redirect(['paket/index', "id" => $id]);
        }

        return $this->render("timeline", [
            "paket_id" => $id, 
            "period_length" => 24,
        ]);
    }

    private function buildTimelineTree($list, $parent_id = null) 
    {
        $data = [];

        foreach ($list as $id => $item) {
            if ($item["parent_id"] == $parent_id) {
                $data[] = [
                    "id" => $id,
                    "timeline_item" => $item["timeline_item_id"],
                    "value" => 0,
                    "items" => $this->buildTimelineTree($list, $id),
                ];
            }
        }

        return $data;
    }
}