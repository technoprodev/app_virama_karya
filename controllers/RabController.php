<?php

namespace app_virama_karya\controllers;

use Yii;
use app_virama_karya\models\Rab;
use app_virama_karya\models\RabData;
use app_virama_karya\models\RabDetail;
use app_virama_karya\models\Paket;
use technosmart\yii\web\Controller;
use yii\db\Expression;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;
use Dompdf\Dompdf;

class RabController extends Controller 
{
    public function behaviors()
    {
        return [
            'access' => $this->access([
                [['delete-category', 'delete-item'], true, ['*'], ['POST']],
            ]),
        ];
    }

    protected function findModel($id)
    {
        if (($model = Paket::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    protected function findModelRabData($id)
    {
        if (($model = RabData::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionIndex($id)
    {
    	$request = Yii::$app->request;

    	if ($request->isAjax) {
            // Data
    		$query = (new Query())
                ->from("rab r")
                ->leftJoin("rab_data rd", "r.id=rd.rab_id")
                ->leftJoin("rab_detail rdt", "rd.id=rdt.rab_data_id")
                ->leftJoin("rab_item ri", "ri.id=rd.rab_item_id")
                ->select(["rd.*", "r.name", "r.id as rab_id"])
                ->addSelect(new Expression(
                    "coalesce(sum(json_extract(
                        rdt.value, 
                        concat('$.', json_extract(ri.detail_structure, '$._config.total'))
                    )), 0) as value"
                ))
                ->where(["paket_id" => $id])
                ->groupBy(["r.id", "rd.id", "rd.rab_item_id"])
	    		->orderBy(["r.id" => SORT_ASC]);

            $data = [];

            foreach ($query->each() as $item) {
                // category
                if (!isset($data[$item["rab_id"]])) {
                    $data[$item["rab_id"]] = [
                        "id" => $item["rab_id"],
                        "name" => $item["name"],
                        "items" => [],
                        "value" => 0,
                    ];
                }

                // items
                if (isset($item["id"])) {
                    $data[$item["rab_id"]]["items"][] = [
                        "id" => $item["id"],
                        "rab_item" => $item["rab_item_id"],
                        "value" => $item["value"],
                    ];

                    $data[$item["rab_id"]]["value"] += $item["value"];
                }
            }

            // Input Options
            $options = (new Query())->from("rab_item")->all();

    		return $this->json([
    			"options" => ArrayHelper::map($options, "id", "name"),
                "data" => array_values($data),
	    	]);
    	}
    	else if ($request->isPost) {
    		$post = $request->post();

            $db = Yii::$app->db;
            $tr = $db->beginTransaction();
            $cmd = $db->createCommand();

            try {
                $data = [];

        		foreach ($post["category"] as $i => $category) {
                    if (!empty($post["category_ids"][$i])) {
                        $model = Rab::findOne($post["category_ids"][$i]);
                    }
                    else {
                        $model = new Rab;
                        $model->paket_id = $id;
                    }

    				$model->name = $category;
                    $model->save();

                    if (isset($post["rab_item"][$i])) {
                        foreach ($post["rab_item"][$i] as $j => $rab_item) {
                            $data[] = [
                                "rab_id" => $model->id,
                                "rab_item_id" => $rab_item,
                            ];
                        }
                    }
        		}

        		$cmd->batchInsert("rab_data", ["rab_id", "rab_item_id"], $data)->execute();
                $tr->commit();
            }
            catch (\Exception $e) {
                $tr->rollback();
                throw $e;
            }

	        return $this->redirect(['paket/index', 'id' => $id]);
    	}

        $model['paket'] = $this->findModel($id);

        return $this->render("rab", [
            'model' => $model,
            "paket_id" => $id,
            // 'title' => 'Detail Paket',
        ]);
    }

    public function actionDetail($id)
    {
        $request = Yii::$app->request;

        // validation
        $rab = (new Query())
            ->from("rab_data rd")
            ->innerJoin("rab_item ri", "rd.rab_item_id = ri.id")
            ->where(["rd.id" => $id])
            ->select(["ri.name", "ri.detail_structure as struct_def"])
            ->one();

        if ($request->isAjax) {
            // table structure
            $rab["struct_def"] = json_decode($rab["struct_def"]);

            // default value item
            $default = [];

            foreach ($rab["struct_def"] as $key => $col) {
                if (!isset($col->type)) {
                    continue;
                }

                switch ($col->type) {
                    case "pattern":
                    case "numeric":
                        $default[$key] = 0;
                        break;

                    case "string":
                        $default[$key] = "";
                        break;
                }
            }

            // data
            $data = [];
            $query = (new Query())->from("rab_detail rd")->where(["rab_data_id" => $id]);

            foreach ($query->each() as $item) {
                $value = array_merge($default, (array) json_decode($item["value"]));
                $value["id"] = $item["id"];

                $data[] = $value;
            }

            return $this->json([
                "rab" => $rab,
                "default" => $default,
                "data" => $data,
            ]);
        }
        else if ($request->isPost) {
            $post = $request->post();
            $data = [];

            foreach ($post["data"] as $i => $item) {
                $data[] = [
                    "rab_data_id" => $id,
                    "value" => json_encode($item),
                ];
            }

            $db = Yii::$app->db;
            $tr = $db->beginTransaction();
            $cmd = $db->createCommand();

            try {
                $cmd->delete('rab_detail', ["rab_data_id" => $id])->execute();
                $cmd->batchInsert("rab_detail", ["rab_data_id", "value"], $data)->execute();

                $tr->commit();
            }
            catch (\Exception $e) {
                $tr->rollback();
                throw $e;
            }

            return $this->redirect(["rab/detail?id=$id"]);
        }

        $paket = (new Query())
            ->from("rab_data rd")
            ->innerJoin("rab r", "rd.rab_id = r.id")
            ->innerJoin("paket p", "p.id = r.paket_id")
            ->select("p.*")
            ->where(["rd.id" => $id])
            ->one();

        return $this->render("rab_detail", [
            "title" => 'Update Paket ' . $paket['nama_paket'],
        	"subtitle" => $rab["name"],
            "readonly" => false,
        ]);
    }

    public function actionDeleteRab($id) 
    {
        $db = Yii::$app->db;
        $tr = $db->beginTransaction();
        $cmd = $db->createCommand();

        try {
            $db->createCommand(
                "DELETE rdt from rab_detail as rdt
                inner join rab_data rd on rd.id=rdt.rab_data_id
                where rd.rab_id=$id"
            )->execute();

            $cmd->delete("rab_data", ["rab_id" => $id])->execute();
            $cmd->delete("rab", ["id" => $id])->execute();

            $tr->commit();
        }
        catch (\Exception $e) {
            $tr->rollback();
            throw $e;
        }

        if (Yii::$app->request->isAjax) {
            return;
        }

        return $this->redirect(["index"]);
    }

    public function actionDeleteRabData($id) 
    {
        $db = Yii::$app->db;
        $tr = $db->beginTransaction();
        $cmd = $db->createCommand();

        try {
            $cmd->delete("rab_data", ["id" => $id])->execute();
            $cmd->delete("rab_detail", ["rab_data_id" => $id])->execute();
            $tr->commit();
        }
        catch (\Exception $e) {
            $tr->rollback();
            throw $e;
        }

        if (Yii::$app->request->isAjax) {
            return $id;
        }

        return $this->redirect(["index"]);
    }

    public function actionPrintRab($id) 
    {
        $query = (new Query())
            ->from("rab r")
            ->leftJoin("rab_data rd", "r.id=rd.rab_id")
            ->leftJoin("rab_detail rdt", "rd.id=rdt.rab_data_id")
            ->leftJoin("rab_item ri", "ri.id=rd.rab_item_id")
            ->select(["rd.*", "r.name", "r.id as rab_id", "ri.name as item_name"])
            ->addSelect(new Expression(
                "coalesce(sum(json_extract(
                    rdt.value, 
                    concat('$.', json_extract(ri.detail_structure, '$._config.total'))
                )), 0) as value"
            ))
            ->where(["paket_id" => $id])
            ->groupBy(["r.id", "rd.id", "rd.rab_item_id"])
            ->orderBy(["r.id" => SORT_ASC]);

        $data = [];

        foreach ($query->each() as $item) {
            // category
            if (!isset($data[$item["rab_id"]])) {
                $data[$item["rab_id"]] = [
                    "id" => $item["rab_id"],
                    "name" => $item["name"],
                    "items" => [],
                    "value" => 0,
                ];
            }

            // items
            if (isset($item["id"])) {
                $data[$item["rab_id"]]["items"][] = [
                    "id" => $item["id"],
                    "name" => $item["item_name"],
                    "value" => $item["value"],
                ];

                $data[$item["rab_id"]]["value"] += $item["value"];
            }
        }

        $dompdf = new Dompdf();
        $model['paket'] = $this->findModel($id);

        Yii::$app->view->params['title'] = \yii\helpers\Inflector::titleize('rab', true) . ' ' . $model['paket']->nama_paket;
        $this->layout = 'download';
        $dompdf->loadHtml($this->render("print-rab", ["data" => $data]));
        $dompdf->setPaper('A4', 'landscape');
        $dompdf->render();
        $dompdf->stream();
    }

    public function actionPrintRabDetailAll($id) 
    {
        $query = (new Query())
            ->from("rab r")
            ->leftJoin("rab_data rd", "r.id=rd.rab_id")
            ->leftJoin("rab_detail rdt", "rd.id=rdt.rab_data_id")
            ->leftJoin("rab_item ri", "ri.id=rd.rab_item_id")
            ->select(["rd.*", "r.name", "r.id as rab_id", "ri.name as item_name"])
            ->where(["paket_id" => $id])
            ->groupBy(["r.id", "rd.id", "rd.rab_item_id"])
            ->orderBy(["r.id" => SORT_ASC]);

        $html = "";

        foreach ($query->each() as $item) {
            $html .= $this->actionPrintRabDetail($item["id"]);
        }

        return $this->render("print_rab_detail_all", ["content" => $html]);
    }

    public function actionPrintRabDetail($id) 
    {
        $rab = (new Query())
            ->from("rab_data rd")
            ->innerJoin("rab_item ri", "rd.rab_item_id = ri.id")
            ->where(["rd.id" => $id])
            ->select(["ri.name", "ri.detail_structure as struct_def"])
            ->one();

        // table structure
        $rab["struct_def"] = json_decode($rab["struct_def"]);

        if ($rab["struct_def"] == null) {
            return;
        }

        // default value item
        $default = [];
        $column = [];

        foreach ($rab["struct_def"] as $key => $col) {
            if (!isset($col->type)) {
                continue;
            }

            if (isset($col->order)) {
                $col->id = $key;
                $column[$col->order] = $col;
            }

            switch ($col->type) {
                case "pattern":
                case "numeric":
                    $default[$key] = 0;
                    break;

                case "string":
                    $default[$key] = "";
                    break;
            }
        }

        // data
        $data = [];
        $query = (new Query())->from("rab_detail rd")->where(["rab_data_id" => $id]);

        foreach ($query->each() as $item) {
            $value = array_merge($default, (array) json_decode($item["value"]));
            $value["id"] = $item["id"];

            $data[] = $value;
        }

        ksort($column);

        return $this->renderPartial("print_rab_detail", [
            "rab" => $rab,
            // "title" => $rab["name"],
            "column" => $column,
            // "column" => $column,
            "data" => $data,
        ]);
    }
}